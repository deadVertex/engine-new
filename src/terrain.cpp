#define STB_PERLIN_IMPLEMENTATION
#include <stb_perlin.h>

internal void GenerateTerrainHeightMap(HeightMap *heightMap)
{
    Assert(heightMap->values != NULL);

    u32 width = heightMap->width;
    u32 height = heightMap->height;

    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            float fx = (float)x / (float)width;
            float fy = (float)y / (float)height;
            // float a = stb_perlin_ridge_noise3(
            // fx, fy, 0.0f, 2.0f, 0.5f, 1.0f, 6, 0, 0, 0);
            float t =
                stb_perlin_fbm_noise3(fx, fy, 0.0f, 2.0f, 0.5f, 6, 0, 0, 0);
            // float b = stb_perlin_turbulence_noise3(
            // fx, fy, 0.0f, 2.0f, 0.5f, 6, 0, 0, 0);
            // float t = stb_perlin_noise3(fx, fy, 0.0f, 0, 0, 0);
            // float t = a;
            t = t * 0.5f + 0.5f;
            t = Clamp(Pow(t, 2.0f), 0.0f, 1.0f);
            // t = t > 0.2f ? 1.0f : 0.0f;
            heightMap->values[x + y * width] = t;
        }
    }
}

internal void CreateHeightMapTexture(HeightMap heightMap,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    u32 bufferSize = heightMap.width * heightMap.height;
    float *pixels = AllocateArray(tempArena, float, bufferSize);
    memcpy(pixels, heightMap.values, bufferSize * sizeof(float));

    RenderCommandAllocateTexture *allocateTexture =
        RenderCommandAllocate(renderCommands, RenderCommandAllocateTexture, 0);
    allocateTexture->pixels = pixels;
    allocateTexture->width = heightMap.width;
    allocateTexture->height = heightMap.height;
    allocateTexture->bytesPerPixel = 1;
    allocateTexture->index = VULKAN_TEXTURE_PERLIN;
    allocateTexture->materialIndex = VULKAN_MATERIAL_PERLIN;
    allocateTexture->useSRGB = false;
    allocateTexture->repeatMapping = false;
    allocateTexture->bytesPerChannel = 2;
}

internal void CreateNormalMapTexture(HeightMap heightMap,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    i32 width = (i32)heightMap.width;
    i32 height = (i32)heightMap.height;

    // TODO: Fix bug with wrapping, issue is caused by having the normal
    // map resolution match the heightmap resolution. Heightmap should be a
    // power of 2 + 1 and normal map should just be the power 2.
    u32 normalMapBytesPerPixel = 4;
    u32 *normalMapPixels = AllocateArray(tempArena, u32, width * height);
    for (i32 y = 0; y < height; ++y)
    {
        for (i32 x = 0; x < width; ++x)
        {
            i32 x0 = Clamp(x - 1, 0, width);
            i32 x1 = Clamp(x + 1, 0, width);

            float t0 = SampleHeightMap(heightMap, x0, y);
            float t1 = SampleHeightMap(heightMap, x1, y);

            i32 y0 = Clamp(y - 1, 0, height);
            i32 y1 = Clamp(y + 1, 0, height);

            float t2 = SampleHeightMap(heightMap, x, y0);
            float t3 = SampleHeightMap(heightMap, x, y1);

            float hx = (t1 - t0) * 0.5f;
            float hy = (t3 - t2) * 0.5f;

            hx *= 64.0f;
            hy *= 64.0f;

            u8 r = (u8)(hx * 127.0f) + 128;
            u8 g = (u8)(hy * 127.0f) + 128;
            u8 b = 255;
            u8 a = 255;

            u32 pixel = (a << 24) | (b << 16) | (g << 8) | r;

            normalMapPixels[y * width + x] = pixel;
        }
    }

    RenderCommandAllocateTexture *allocateNormalMap =
        RenderCommandAllocate(renderCommands, RenderCommandAllocateTexture, 0);
    allocateNormalMap->pixels = normalMapPixels;
    allocateNormalMap->width = (u32)width;
    allocateNormalMap->height = (u32)height;
    allocateNormalMap->bytesPerPixel = 4;
    allocateNormalMap->index = VULKAN_TEXTURE_PERLIN_NORMAL;
    allocateNormalMap->materialIndex = VULKAN_MATERIAL_PERLIN_NORMAL;
    allocateNormalMap->useSRGB = false;
    allocateNormalMap->repeatMapping = true;
    allocateNormalMap->bytesPerChannel = 1;
}

internal void DrawTerrain(RenderCommandQueue *renderCommands, mat4 view,
    mat4 projection, vec3 translation, float scaleXZ, float scaleY)
{
    // TODO: Make entity system
    RenderCommandDrawMeshUniforms *drawTerrain =
        RenderCommandAllocate(renderCommands, RenderCommandDrawMeshUniforms, 0);
    drawTerrain->model = Translate(translation) * Scale(scaleXZ);
    drawTerrain->view = view;
    drawTerrain->projection = projection;
    drawTerrain->mesh = VULKAN_MESH_TERRAIN_PATCH;
    drawTerrain->shader = VULKAN_SHADER_TERRAIN;
    PushUniformTexture(drawTerrain, UNIFORM_KEY_TERRAIN_NORMAL_MAP,
        VULKAN_MATERIAL_PERLIN_NORMAL);
    PushUniformTexture(
        drawTerrain, UNIFORM_KEY_TEX_SAMPLER, VULKAN_MATERIAL_DEV_GRID512_DARK);
    PushUniformTexture(
        drawTerrain, UNIFORM_KEY_TERRAIN_HEIGHT_MAP, VULKAN_MATERIAL_PERLIN);
    PushUniformSpecialType(
        drawTerrain, UNIFORM_KEY_SHADOW_MAP, UNIFORM_TYPE_SHADOW_MAP);

    PushUniformV2(drawTerrain, UNIFORM_KEY_TERRAIN_TEX_COORD_OFFSET, Vec2(0.0));
    PushUniformV2(drawTerrain, UNIFORM_KEY_TERRAIN_TEX_COORD_SCALE, Vec2(1.0));
    PushUniformV2(drawTerrain, UNIFORM_KEY_TEX_COORD_OFFSET, Vec2(0.0));
    PushUniformV2(
        drawTerrain, UNIFORM_KEY_TEX_COORD_SCALE, Vec2(scaleXZ * 0.25f));
}
