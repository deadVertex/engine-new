#pragma once

#include "key_codes.h"

struct InputSystem
{
  u8 currentKeyStates[MAX_KEYS];
  u8 prevKeyStates[MAX_KEYS];
  u8 keyRepeatStates[MAX_KEYS];
  u32 keyBindings[MAX_KEYS];
  u32 currentKeyInput;
  u32 prevKeyInput;
  double prevMouseX;
  double prevMouseY;
  double mouseX;
  double mouseY;
};

inline bool IsKeyDown( InputSystem *inputSystem, u8 key )
{
  return ( inputSystem->currentKeyStates[key] != 0 );
}

inline bool WasKeyPressed( InputSystem *inputSystem, u8 key )
{
  return ( ( inputSystem->currentKeyStates[key] != 0 ) &&
           ( inputSystem->prevKeyStates[key] == 0 ) );
}

inline bool WasKeyReleased( InputSystem *inputSystem, u8 key )
{
  return ( ( inputSystem->currentKeyStates[key] == 0 ) &&
           ( inputSystem->prevKeyStates[key] == 1 ) );
}

inline bool WasKeyRepeated(InputSystem *inputSystem, u8 key)
{
    return (inputSystem->keyRepeatStates[key] != 0);
}

inline bool WasKeyPressedOrRepeated(InputSystem *inputSystem, u8 key)
{
    return (
        WasKeyPressed(inputSystem, key) || WasKeyRepeated(inputSystem, key));
}

inline bool IsInputActive( InputSystem *inputSystem, u32 input )
{
  return ( ( inputSystem->currentKeyInput & input ) != 0 );
}

inline bool WasInputActivated( InputSystem *inputSystem, u32 input )
{
  return ( ( ( inputSystem->currentKeyInput & input ) != 0 ) &&
           ( ( inputSystem->prevKeyInput & input ) == 0 ) );
}

inline void input_OnBeginFrame(InputSystem *inputSystem)
{
    for (u32 i = 0; i < MAX_KEYS; ++i)
    {
        inputSystem->prevKeyStates[i] = inputSystem->currentKeyStates[i];
        inputSystem->keyRepeatStates[i] = 0;
    }
    inputSystem->prevKeyInput = inputSystem->currentKeyInput;
    inputSystem->prevMouseX = inputSystem->mouseX;
    inputSystem->prevMouseY = inputSystem->mouseY;
}
