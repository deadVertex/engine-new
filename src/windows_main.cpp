/* TODO
 * - Platform event recording
 * - Mesh loading
 * - Lighting
 * - Level editor
 */

//#define USE_VULKAN // Deprecating Vulkan renderer
#define USE_OPENGL

#ifdef USE_VULKAN
#undef USE_OPENGL
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <TimeAPI.h>

#ifdef USE_VULKAN
#define GLFW_EXPOSE_NATIVE_WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#else
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#endif

#include "utils.h"
#include "key_codes.h"
#include "platform_events.h"
#include "logging.h"
#include "math_lib.h"

#include "game.h"
#include "render_commands.h"

#define GAME_LIB "game_lib.dll"
#define TEMP_GAME_LIB "game_lib_temp.dll"
#define GAME_CODE_LOCK "lock.tmp"

internal DEBUG_READ_ENTIRE_FILE(DebugReadEntireFile);
internal DEBUG_FREE_FILE(DebugFreeFile);

internal void Win32_FreeMemory(void *p);
internal void *Win32_AllocateMemory(size_t numBytes, size_t baseAddress = 0);

#ifdef USE_VULKAN
#include "vulkan_renderer.cpp"
#endif

#ifdef USE_OPENGL
#include "opengl_renderer.cpp"
#endif

u32 log_activeChannels = U32_MAX;

GameMemory *gDebugGameMemory;

global bool g_IsRunning = true;
internal void WindowCloseCallback(GLFWwindow *window) { g_IsRunning = false; }

internal void MouseCursorPositionCallback(GLFWwindow *window, double x,
                                          double y)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event;
    event.type = PlatformEvent_MouseMotion;
    event.mouseMotion.x = x;
    event.mouseMotion.y = y;

    PushPlatformEvent(eventQueue, event);
}

internal u8 ConvertMouseButton(int button)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        return K_MOUSE_BUTTON_LEFT;
    case GLFW_MOUSE_BUTTON_MIDDLE:
        return K_MOUSE_BUTTON_MIDDLE;
    case GLFW_MOUSE_BUTTON_RIGHT:
        return K_MOUSE_BUTTON_RIGHT;
    }
    return K_UNKNOWN;
}

internal void MouseButtonCallback(GLFWwindow *window, int button, int action,
                                  int mods)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    // Hopefully GLFW_REPEAT is not valid for mouse buttons
    event.type = (action == GLFW_PRESS) ? (u8)PlatformEvent_KeyPress
                                        : (u8)PlatformEvent_KeyRelease;
    event.key = ConvertMouseButton(button);

    PushPlatformEvent(eventQueue, event);
}

void MouseScrollCallback(GLFWwindow *window, double xoffset, double yoffset)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    event.type = PlatformEvent_MouseScroll;
    event.mouseScroll = (float)yoffset; // Win32: Observed to be +/- 1.0

    PushPlatformEvent(eventQueue, event);
}

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return K_##NAME;
internal u8 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return (u8)key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return K_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return K_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return K_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return K_NUM0;
    case GLFW_KEY_KP_1:
        return K_NUM1;
    case GLFW_KEY_KP_2:
        return K_NUM2;
    case GLFW_KEY_KP_3:
        return K_NUM3;
    case GLFW_KEY_KP_4:
        return K_NUM4;
    case GLFW_KEY_KP_5:
        return K_NUM5;
    case GLFW_KEY_KP_6:
        return K_NUM6;
    case GLFW_KEY_KP_7:
        return K_NUM7;
    case GLFW_KEY_KP_8:
        return K_NUM8;
    case GLFW_KEY_KP_9:
        return K_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return K_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return K_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return K_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return K_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return K_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return K_NUM_ENTER;
    }
    return K_UNKNOWN;
}

internal void KeyCallback(GLFWwindow *window, int glfwKey, int scancode,
                          int action, int mods)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    u8 key = ConvertKey(glfwKey);
    if (key != K_UNKNOWN)
    {
        PlatformEvent event = {};
        event.key = key;

        if (action == GLFW_RELEASE)
        {
            event.type = PlatformEvent_KeyRelease;
        }
        else if (action == GLFW_PRESS)
        {
            event.type = PlatformEvent_KeyPress;
        }
        else
        {
            event.type = PlatformEvent_KeyRepeat;
            // Ignore key repeats as we use character events for text input
            //return;
        }

        PushPlatformEvent(eventQueue, event);
    }
}

internal void CharacterCallback(GLFWwindow *window, u32 codepoint)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    event.type = PlatformEvent_CharacterPressed;
    event.character = codepoint;

    PushPlatformEvent(eventQueue, event);
}

internal void WindowFocusCallback(GLFWwindow *window, int focused)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    event.type = PlatformEvent_WindowFocusChanged;
    event.windowHasFocus = (focused == GLFW_TRUE);

    PushPlatformEvent(eventQueue, event);
}

internal void ErrorCallback(int error, const char *description)
{
    LOG_ERROR("GLFW ERROR: %s", description);
}

// This creates a single frame buffer resize event when the user resizes the
// window. When the window is being resized the application is paused until the
// user stops resizing. This creates a storm of resize events which invokes the
// however we are only interested in the last one.
global u32 g_FrameBufferNewWidth;
global u32 g_FrameBufferNewHeight;
global b32 g_FrameBufferWasResized = false;
internal void FrameBufferResizeCallback(GLFWwindow *window, int width,
                                        int height)
{
    g_FrameBufferNewWidth = width;
    g_FrameBufferNewHeight = height;
    g_FrameBufferWasResized = true;
}

internal void *Win32_AllocateMemory(size_t numBytes, size_t baseAddress)
{
    void *result = VirtualAlloc((void *)baseAddress, numBytes,
                                MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

    Assert(result);
    return result;
}

internal void Win32_FreeMemory(void *p)
{ 
    VirtualFree(p, 0, MEM_RELEASE);
}

internal DEBUG_FREE_FILE(DebugFreeFile)
{
    Win32_FreeMemory(fileResult.memory);
}

internal DEBUG_READ_ENTIRE_FILE(DebugReadEntireFile)
{
    ReadFileResult result = {};
    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, 0,
                              OPEN_EXISTING, 0, 0);
    if (file != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER tempSize;
        if (GetFileSizeEx(file, &tempSize))
        {
            result.size = SafeTruncateU64ToU32(tempSize.QuadPart);
            result.memory = Win32_AllocateMemory(result.size);
            if (result.memory)
            {
                DWORD bytesRead;
                if (!ReadFile(file, result.memory, result.size, &bytesRead,
                              0) ||
                    (result.size != bytesRead))
                {
                    LOG_ERROR("Failed to read file %s", path);
                    Win32_FreeMemory(result.memory);
                    result.memory = nullptr;
                    result.size = 0;
                }
            }
            else
            {
                LOG_ERROR("Failed to allocate %d bytes for file %s",
                          result.size, path);
            }
        }
        else
        {
            LOG_ERROR("Failed to read file size for file %s", path);
        }
        CloseHandle(file);
    }
    else
    {
        LOG_ERROR("Failed to open file %s", path);
    }
    return result;
}

internal DEBUG_WRITE_FILE(Win32_WriteFile)
{
    bool result = false;
    HANDLE file =
        CreateFileA(path, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, 0);
    if (file != INVALID_HANDLE_VALUE)
    {
        DWORD bytesWritten;
        if (WriteFile(file, data, length, &bytesWritten, 0) ||
            (bytesWritten != length))
        {
            result = true;
        }
        else
        {
            LOG_ERROR("Failed to write file %s", path);
        }
        CloseHandle(file);
    }
    return result;
}

struct GameCode
{
    HMODULE handle;
    GameUpdateFunction *update;

    bool isValid;
    FILETIME lastWriteTime;
};

internal FILETIME GetFileLastWriteTime(const char *path)
{
    FILETIME lastWriteTime = {};
    WIN32_FILE_ATTRIBUTE_DATA data;
    if (GetFileAttributesEx(path, GetFileExInfoStandard, &data))
    {
        lastWriteTime = data.ftLastWriteTime;
    }
    return lastWriteTime;
}

internal bool IsLockFileActive(const char *lockFileName)
{
    WIN32_FILE_ATTRIBUTE_DATA ignored;
    if (!GetFileAttributesEx(lockFileName, GetFileExInfoStandard, &ignored))
        return false;

    return true;
}

internal GameCode LoadGameCode(
    const char *libraryName, const char *tempLibraryName)
{
    GameCode result = {};
    CopyFile(libraryName, tempLibraryName, FALSE);
    result.lastWriteTime = GetFileLastWriteTime(libraryName);

    result.handle = LoadLibraryA(tempLibraryName);

    if (result.handle)
    {
        result.update =
            (GameUpdateFunction *)GetProcAddress(result.handle, "GameUpdate");
        result.isValid = result.update;
    }
    else
    {
        DWORD error = GetLastError();
        LPVOID msgBuffer;
        FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                          FORMAT_MESSAGE_FROM_SYSTEM |
                          FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR)&msgBuffer, 0, NULL);

        LOG_ERROR("Failed to load game code from %s, %s\n", libraryName,
            (char *)msgBuffer);
        LocalFree(msgBuffer);
    }

    return result;
}

internal void UnloadGameCode(GameCode *gameCode)
{
    if (gameCode->handle)
    {
        FreeLibrary(gameCode->handle);
        gameCode->handle = 0;
    }
    ZeroPointerToStruct(gameCode);
}

// TODO: Probably want to remove this
global GLFWwindow *g_Window;
DEBUG_SHOW_CURSOR(DebugShowCursor)
{
    if (showCursor)
    {
        glfwSetInputMode(g_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    else
    {
        glfwSetInputMode(g_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
}

#ifdef ENGINE_INTERNAL
internal void Debug_DumpCounters(GameMemory *memory)
{
    for (u32 id = 0; id < ArrayCount(memory->debugCounters); ++id)
    {
        DebugCounter *counter = &memory->debugCounters[id];

        if (counter->hitCount > 0)
        {
            char buffer[256];
            snprintf(buffer, sizeof(buffer),
                "%38s: %16llu cycles %8u hits %16llu cycles/hit\n",
                gDebugCounterNames[id], counter->elapsed, counter->hitCount,
                counter->elapsed / counter->hitCount);
            OutputDebugString(buffer);
            counter->elapsed = 0;
            counter->hitCount = 0;
        }
    }
}
#endif

int WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR cmdLine,
            int cmdShow)
{
    u32 targetSchedulerMilliseconds = 1;
    bool isSleepGranular = (timeBeginPeriod(targetSchedulerMilliseconds) ==
            TIMERR_NOERROR);

    GameCode gameCode = LoadGameCode( GAME_LIB, TEMP_GAME_LIB);
    if (!gameCode.isValid)
    {
        LOG_ERROR("Failed to load game code.");
        return -1;
    }


    if (glfwInit() != GLFW_TRUE)
    {
        return -1;
    }
    glfwSetErrorCallback(ErrorCallback);

    glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
#ifdef USE_VULKAN
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
#endif

#ifdef USE_OPENGL
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif

    int windowWidth = 1920;
    int windowHeight = 980; //1080;
    GLFWwindow *window = glfwCreateWindow(windowWidth, windowHeight,
                                          "Vulkan Engine", NULL, NULL);
    g_Window = window;

    if (window == NULL)
    {
        glfwTerminate();
        return -1;
    }
    glfwSetWindowCloseCallback(window, WindowCloseCallback);
    glfwSetCursorPosCallback(window, MouseCursorPositionCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetScrollCallback(window, MouseScrollCallback);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetCharCallback(window, CharacterCallback);
    glfwSetWindowFocusCallback(window, WindowFocusCallback);
    glfwSetFramebufferSizeCallback(window, FrameBufferResizeCallback);

    u32 frameBufferWidth = windowWidth;
    u32 frameBufferHeight = windowHeight;
#ifdef USE_VULKAN
    VulkanInit(window, frameBufferWidth, frameBufferHeight);
#endif

#ifdef USE_OPENGL
    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    OpenGL_Init(window, frameBufferWidth, frameBufferHeight);
#endif

    GameMemory gameMemory = {};
    gameMemory.persistentStorageSize = MEGABYTES(64);
    gameMemory.persistentStorageBase =
            Win32_AllocateMemory(gameMemory.persistentStorageSize, 0);
    gameMemory.transientStorageSize = MEGABYTES(100);
    gameMemory.transientStorageBase =
            Win32_AllocateMemory(gameMemory.transientStorageSize, 0);
    gameMemory.readEntireFile = &DebugReadEntireFile;
    gameMemory.writeFile = &Win32_WriteFile;
    gameMemory.freeFile = &DebugFreeFile;
    gameMemory.showCursor = &DebugShowCursor;
#ifdef USE_VULKAN
    gameMemory.usingVulkan = true;
#endif

#ifdef ENGINE_INTERNAL
    gDebugGameMemory = &gameMemory;
#endif




    PlatformEventQueue eventQueue = {};

    glfwSetWindowUserPointer(window, &eventQueue);

    i32 frameRateCap = 145;
    double minFrameTime = 1.0 / (double)frameRateCap;
    double maxFrameTime = 0.25;
    double currentTime = glfwGetTime();
    double sleepMaxError = 0.0;
    double accumulator = 0.0;
    while (g_IsRunning)
    {
        double loopFrameTime = glfwGetTime() - currentTime;

        double newTime = glfwGetTime();
        double dt = newTime - currentTime;
        if (dt > maxFrameTime)
            dt = maxFrameTime;

        currentTime = newTime;

        u32 fps = (u32)(1.0 / dt);
        //LOG_DEBUG("FPS: %u (%g ms)", fps, dt * 1000.0);

        FILETIME newWriteTime = GetFileLastWriteTime(GAME_LIB);
        if (CompareFileTime(&newWriteTime, &gameCode.lastWriteTime) != 0)
        {
            if (!IsLockFileActive(GAME_CODE_LOCK))
            {
                UnloadGameCode(&gameCode);
                auto newGameCode = LoadGameCode(GAME_LIB, TEMP_GAME_LIB);
                if (newGameCode.isValid)
                {
                    gameCode = newGameCode;
                    PlatformEvent event;
                    event.type = PlatformEvent_GameCodeReload;

                    PushPlatformEvent(&eventQueue, event);
                    LOG_DEBUG("Game code reloaded!");
                }
            }
        }

        glfwPollEvents();

        if (g_FrameBufferWasResized)
        {
#ifdef USE_VULKAN
            g_VkIsSwapChainValid = false;
#endif
            frameBufferWidth = g_FrameBufferNewWidth;
            frameBufferHeight = g_FrameBufferNewHeight;


            PlatformEvent event = {};
            event.type = PlatformEvent_FrameBufferResized;
            event.frameBuffer.width = g_FrameBufferNewWidth;
            event.frameBuffer.height = g_FrameBufferNewHeight;

            PushPlatformEvent(&eventQueue, event);
            g_FrameBufferWasResized = false;
        }

        u8 renderCommandsBuffer[KILOBYTES(512)] = {};
        RenderCommandQueue renderCommands = {};
        renderCommands.start = renderCommandsBuffer;
        renderCommands.capacity = sizeof(renderCommandsBuffer);

        gameCode.update(&gameMemory, (float)dt, eventQueue.events,
            eventQueue.count, &renderCommands, frameBufferWidth, frameBufferHeight);
        eventQueue.count = 0;

#ifdef USE_VULKAN
        if (!g_VkIsSwapChainValid)
        {
            if (frameBufferWidth > 0 && frameBufferHeight > 0)
            {
                VulkanRecreateSwapChain(frameBufferWidth, frameBufferHeight);
            }
        }

        if (g_VkIsSwapChainValid)
        {
            // TODO: Pass framebuffer dimensions
            VulkanDrawFrame(1.0f / 60.0f, &renderCommands);
            //glfwSwapBuffers(window);
        }
#endif

#ifdef USE_OPENGL
        Debug_BeginTimedBlock(ProcessRenderCommands);
        OpenGL_DrawFrame(&renderCommands, frameBufferWidth, frameBufferHeight);
        Debug_EndTimedBlock(ProcessRenderCommands);

        Debug_BeginTimedBlock(SwapBuffers);
        glfwSwapBuffers(window);
        Debug_EndTimedBlock(SwapBuffers);
#endif

#ifdef ENGINE_INTERNAL
        Debug_DumpCounters(&gameMemory);
#endif



        double totalFrameTime = glfwGetTime() - currentTime;
        if (totalFrameTime < minFrameTime)
        {
            double remainder = minFrameTime - totalFrameTime;
            if (isSleepGranular)
            {
                u32 millisecondsRemaining = (u32)(remainder * 1000.0);
                if (millisecondsRemaining > 2)
                {
                    millisecondsRemaining -= 1;
                    double sleepStart = glfwGetTime();
                    Sleep(millisecondsRemaining);
                    double sleepTime = glfwGetTime() - sleepStart;
                    double sleepError = sleepTime - ((double)millisecondsRemaining / (1000.0));
                    if (sleepError > sleepMaxError)
                        sleepMaxError = sleepError;
                }
            }

            totalFrameTime = glfwGetTime() - currentTime;
            remainder = minFrameTime - totalFrameTime;
            if (remainder > 0.0)
            {
                double targetTime = glfwGetTime() + remainder;
                while (glfwGetTime() <= targetTime);
            }
        }
    }
#ifdef USE_VULKAN
    VulkanCleanUp();
#endif

#ifdef USE_OPENGL
    OpenGL_CleanUp();
#endif

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
