/*
 * TODO:
 * Specify a default value for a component when registering it
 */
#include "ecs.h"

internal void ecs_Initialize(ecs_EntitySystem *entitySystem, u32 entityCapacity,
                             u32 componentCapacity, MemoryArena *arena)
{
    entitySystem->entityCapacity = entityCapacity;
    entitySystem->entityCount = 0;
    entitySystem->entities = AllocateArray(arena, EntityId, entityCapacity);
    entitySystem->nextEntityId = 1;

    entitySystem->componentCapacity = componentCapacity;
    entitySystem->componentCount = 0;
    entitySystem->componentKeys = AllocateArray(arena, u32, componentCapacity);
    entitySystem->componentValues = AllocateArray(arena, ecs_ComponentTable, componentCapacity);
}

// TODO: Free list and garbage queue

internal void ecs_RegisterComponent(ecs_EntitySystem *entitySystem,
                                    MemoryArena *arena, u32 componentTypeId,
                                    u32 objectSize, u32 capacity, void *defaultValue)
{
    Assert(entitySystem->componentCount < entitySystem->componentCapacity);
    u32 index = entitySystem->componentCount++;

    ecs_ComponentTable table = {};
    table.capacity = capacity;
    table.objectSize = objectSize;
    table.length = 0;
    table.keys = AllocateArray(arena, EntityId, capacity);

    if (objectSize != 0)
    {
        table.values = MemoryArenaAllocate(arena, objectSize * capacity);

        if (defaultValue != NULL)
        {
            table.defaultValue = MemoryArenaAllocate(arena, objectSize);
            memcpy(table.defaultValue, defaultValue, objectSize);
        }
    }

    entitySystem->componentValues[index] = table;
    entitySystem->componentKeys[index] = componentTypeId;
}

internal ecs_ComponentTable *
ecs_InternalGetComponentTable(ecs_EntitySystem *entitySystem,
                              u32 componentTypeId)
{
    for (u32 i = 0; i < entitySystem->componentCount; ++i)
    {
        if (entitySystem->componentKeys[i] == componentTypeId)
        {
            return entitySystem->componentValues + i;
        }
    }

    return NULL;
}

internal void ecs_ComponentTableAddEntity(ecs_ComponentTable *table, EntityId entity)
{
    Assert(table->length < table->capacity);
    u32 index = table->length++;
    table->keys[index] = entity;
}

inline void* ecs_ComponentTableGet(ecs_ComponentTable *table, EntityId entity)
{
    for (u32 i = 0; i < table->length; ++i)
    {
        if (table->keys[i] == entity)
        {
            u8 *value = (u8*)table->values + table->objectSize * i;
            return value;
        }
    }
    return NULL;
}

inline bool ecs_ComponentTableHas(ecs_ComponentTable *table, EntityId entity)
{
    for (u32 i = 0; i < table->length; ++i)
    {
        if (table->keys[i] == entity)
        {
            return true;
        }
    }
    return false;
}

internal void ecs_ComponentTableSet(ecs_ComponentTable *table, EntityId entity,
                                    void *value, u32 objectSize)
{
    Assert(objectSize == table->objectSize);
    void *component = ecs_ComponentTableGet(table, entity);
    Assert(component);
    memcpy(component, value, objectSize);
}

internal void ecs_CreateEntities(ecs_EntitySystem *entitySystem, EntityId *entities, u32 count)
{
    Assert(entitySystem->entityCount + count <= entitySystem->entityCapacity);
    // FIXME: Handle next entityId wrapping, need test for this!
    Assert(entitySystem->nextEntityId != NULL_ENTITY); // Useless assertion!

    for (u32 i = 0; i < count; ++i)
    {
        // Allocate entity ids
        entities[i] = entitySystem->nextEntityId++;
    }

    u32 start = entitySystem->entityCount;
    memcpy(entitySystem->entities + start, entities, count * sizeof(EntityId));
    entitySystem->entityCount += count;
}

// FIXME: This is going to be slow as the number of entities increases, need to
// choose a different data structure
inline bool ecs_InternalRemoveEntity(ecs_EntitySystem *entitySystem, EntityId entity)
{
    Assert(entity != NULL_ENTITY);
    bool found = false;
    for (u32 i = 0; i < entitySystem->entityCount; ++i)
    {
        if (entitySystem->entities[i] == entity)
        {
            entitySystem->entities[i] =
                    entitySystem->entities[entitySystem->entityCount - 1];
            entitySystem->entityCount--;
            found = true;
            break;
        }
    }
    return found;
}

// FIXME: Modify to work for hash tables as well as linear arrays
internal void ecs_InternalComponentTableAdd(ecs_ComponentTable *table,
                                            EntityId *entities, u32 count,
                                            void *values, u32 objectSize)
{
    Assert(table->length + count <= table->capacity);
    u32 start = table->length;
    memcpy(table->keys + start, entities, count * sizeof(EntityId));
    table->length += count;

    if (values != NULL)
    {
        Assert(objectSize == table->objectSize);
        memcpy((u8 *)table->values + start * objectSize, values,
               count * objectSize);
    }
    else
    {
        if (table->defaultValue != NULL)
        {
            for (u32 i = 0; i < count; ++i)
            {
                memcpy((u8 *)table->values + (start + i) * table->objectSize,
                    table->defaultValue, table->objectSize);
            }
        }
        else
        {
            ClearToZero((u8 *)table->values + start * table->objectSize,
                count * table->objectSize);
        }
    }
}

inline bool
ecs_InternalComponentTableLinearArrayRemove(ecs_ComponentTable *table,
                                            EntityId entity)
{
    Assert(entity != NULL_ENTITY);
    bool found = false;
    for (u32 i = 0; i < table->length; ++i)
    {
        if (table->keys[i] == entity)
        {
            table->keys[i] = table->keys[table->length - 1];
            memcpy((u8*)table->values + table->objectSize * i,
                    (u8*)table->values + table->objectSize * (table->length - 1),
                    table->objectSize);
            table->length--;
            found = true;
            break;
        }
    }

    return found;
}

// TODO: Support hash tables as well as linear arrays
internal void ecs_InternalComponentTableRemove(ecs_ComponentTable *table,
                                               EntityId *entities, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        ecs_InternalComponentTableLinearArrayRemove(table, entities[i]);
    }
}

internal void ecs_InternalComponentTableSet(ecs_ComponentTable *table,
                                            EntityId *entities, u32 count,
                                            void *values, u32 objectSize)
{
    Assert(objectSize == table->objectSize);
    for (u32 i = 0; i < count; ++i)
    {
        void *component = ecs_ComponentTableGet(table, entities[i]);
        Assert(component);
        memcpy(component, (u8*)values + objectSize * i, objectSize);
    }
}

internal void ecs_InternalComponentTableGet(ecs_ComponentTable *table,
                                            EntityId *entities, u32 count,
                                            void *output, u32 objectSize)
{
    Assert(objectSize == table->objectSize);
    for (u32 i = 0; i < count; ++i)
    {
        void *component = ecs_ComponentTableGet(table, entities[i]);
        Assert(component);
        memcpy((u8*)output + objectSize * i, component, objectSize);
    }
}

internal void ecs_InternalComponentTableHas(ecs_ComponentTable *table,
                                            EntityId *entities, u32 count,
                                            bool *output)
{
    for (u32 i = 0; i < count; ++i)
    {
        output[i] = ecs_ComponentTableHas(table, entities[i]);
    }
}

internal u32 ecs_InternalComponentTableFindEntities(ecs_ComponentTable *table,
        EntityId *entitiesToFind, u32 count, EntityId *entitiesFound)
{
    u32 foundCount = 0;
    for (u32 i = 0; i < count; ++i)
    {
        EntityId entityToFind = entitiesToFind[i];
        for (u32 j = 0; j < table->length; ++j)
        {
            if (table->keys[j] == entityToFind)
            {
                entitiesFound[foundCount++] = entityToFind;
            }
        }
    }

    return foundCount;
}

internal void ecs_InternalComponentTableIsEqual(ecs_ComponentTable *table,
                                            EntityId *entities, u32 count,
                                            bool *output, void *value, u32 objectSize)
{
    Assert(objectSize == table->objectSize);
    for (u32 i = 0; i < count; ++i)
    {
        void *component = ecs_ComponentTableGet(table, entities[i]);
        Assert(component);
        output[i] = Memcmp(component, value, objectSize);
    }
}

internal void ecs_DeleteEntities(ecs_EntitySystem *entitySystem, EntityId *entities, u32 count)
{
    Assert(entitySystem->entityCount >= count);

    for (u32 i = 0; i < count; ++i)
    {
        bool success = ecs_InternalRemoveEntity(entitySystem, entities[i]);
        Assert(success);
    }

    for (u32 i = 0; i < entitySystem->componentCount; ++i)
    {
        ecs_ComponentTable *table = entitySystem->componentValues + i;
        ecs_InternalComponentTableRemove(table, entities, count);
    }
}

internal void ecs_DeleteAllEntities(ecs_EntitySystem *entitySystem)
{
    entitySystem->entityCount = 0;
    entitySystem->nextEntityId = 1;

    for (u32 i = 0; i < entitySystem->componentCount; ++i)
    {
        ecs_ComponentTable *table = entitySystem->componentValues + i;
        table->length = 0;
    }
}

internal void ecs_AddComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, void *values, u32 objectSize)
{
    for (u32 i = 0; i < count; ++i)
    {
        Assert(entities[i] != NULL_ENTITY);
    }

    if (values != NULL)
    {
        Assert(objectSize != 0);
    }

    ecs_ComponentTable *table =
            ecs_InternalGetComponentTable(entitySystem, componentTypeId);
    Assert(table);

    ecs_InternalComponentTableAdd(table, entities, count, values, objectSize);
}

internal void ecs_RemoveComponent(ecs_EntitySystem *entitySystem,
                                  u32 componentTypeId, EntityId *entities,
                                  u32 count)
{
    ecs_ComponentTable *table =
            ecs_InternalGetComponentTable(entitySystem, componentTypeId);
    Assert(table);

    ecs_InternalComponentTableRemove(table, entities, count);
}

internal void ecs_SetComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, void *values, u32 objectSize)
{
    ecs_ComponentTable *table =
            ecs_InternalGetComponentTable(entitySystem, componentTypeId);
    Assert(table);

    ecs_InternalComponentTableSet(table, entities, count, values, objectSize);
}

internal void ecs_GetComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, void *output, u32 objectSize)
{
    ecs_ComponentTable *table =
            ecs_InternalGetComponentTable(entitySystem, componentTypeId);
    Assert(table);

    ecs_InternalComponentTableGet(table, entities, count, output, objectSize);
}

internal void ecs_HasComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, bool *output)
{
    ecs_ComponentTable *table =
            ecs_InternalGetComponentTable(entitySystem, componentTypeId);
    Assert(table);

    ecs_InternalComponentTableHas(table, entities, count, output);
}

// TODO: Major refactor of this to make it easier to implement different types
// of queries
internal ecs_QueryResult ecs_ExecuteQueryObject(ecs_EntitySystem *entitySystem,
        MemoryArena *arena, ecs_Query *query)
{
    ecs_QueryResult result = {};

    u32 minTableLength = 0;
    u32 startingComponentTypeId = 0;
    ecs_ComponentTable *startingTable = NULL;
    ecs_ComponentTable *tables[0xFF] = {NULL};

    Assert(query->count < ArrayCount(tables));

    // Find component table in query with the least number of entities
    for (u32 i = 0; i < query->count; ++i)
    {
        u32 componentTypeId = query->parameters[i].componentTypeId;
        ecs_ComponentTable *table =
            ecs_InternalGetComponentTable(entitySystem, componentTypeId);
        Assert(table);
        tables[i] = table;

        if (table->length == 0)
        {
            minTableLength = 0;
            break;
        }

        if (query->parameters[i].queryType != ecs_QueryParameterType_IsEqual)
        {
            if (startingTable == NULL || table->length < minTableLength)
            {
                minTableLength = table->length;
                startingTable = table;
                startingComponentTypeId = componentTypeId;
            }
        }
    }

    if (minTableLength == 0)
    {
        return result;
    }

    if (startingTable != NULL)
    {
        // TODO: Use different memory arena so that we can free this entity set
        // before releasing all memory allocated for a query.
        EntityId *entitySet = AllocateArray(arena, EntityId, minTableLength);
        u32 entitySetLength = minTableLength;
        memcpy(entitySet, startingTable->keys, entitySetLength * sizeof(EntityId));

        for (u32 i = 0; i < query->count; ++i)
        {
            if (entitySetLength == 0)
                break;

            ecs_QueryParameter *parameter = query->parameters + i;
            u32 componentTypeId = parameter->componentTypeId;
            if (componentTypeId != startingComponentTypeId)
            {
                ecs_ComponentTable *table = tables[i];

                // FIXME: AllocateArray zeroes memory, should move this out of the loop
                // however it is good when debugging
                EntityId *foundEntities = AllocateArray(arena, EntityId, entitySetLength);
                u32 count = ecs_InternalComponentTableFindEntities(table,
                        entitySet, entitySetLength, foundEntities);

                Assert(count <= entitySetLength);
                if (parameter->queryType == ecs_QueryParameterType_IsEqual)
                {
                    bool *isEqual = AllocateArray(arena, bool, count);
                    ecs_InternalComponentTableIsEqual(
                            table, foundEntities, count, isEqual,
                            parameter->value, parameter->objectSize);

                    u32 isEqualCount = 0;
                    for (u32 j = 0; j < count; ++j)
                    {
                        if (isEqual[j])
                        {
                            entitySet[isEqualCount++] = foundEntities[j];
                        }
                    }
                    entitySetLength = isEqualCount;
                    MemoryArenaFree(arena, isEqual);
                }
                else
                {
                    memcpy(entitySet, foundEntities, count * sizeof(EntityId));
                    entitySetLength = count;
                }
                MemoryArenaFree(arena, foundEntities);
            }
        }

        if (entitySetLength > 0)
        {
            for (u32 i = 0; i < query->count; ++i)
            {
                ecs_QueryParameter *parameter = query->parameters + i;
                if (parameter->queryType == ecs_QueryParameterType_GetComponent)
                {
                    ecs_ComponentTable *table = tables[i];

                    void *output = MemoryArenaAllocate(
                            arena, parameter->objectSize * entitySetLength);

                    ecs_InternalComponentTableGet(table, entitySet,
                                                  entitySetLength, output,
                                                  parameter->objectSize);
                    *parameter->output = output;
                }
            }
        }

        result.length = entitySetLength;
        result.arena = arena;
        result.entities = entitySet;
    }
    result.isValid = true;
    return result;
}

internal void ecs_ReleaseQueryResult(ecs_QueryResult result)
{
    if (result.isValid)
    {
        Assert(result.arena);
        Assert(result.entities);

        MemoryArenaFree(result.arena, result.entities);
    }
}
