#include "vulkan_renderer.h"

/* TODO
 * - Pass generic uniform values through render commands
 *
 */

struct OpenGL_VertexBuffer
{
    u32 vao;
    u32 vbo;
    u32 capacity;
};

global OpenGL_VertexBuffer g_LineVertexBuffer;
global OpenGL_VertexBuffer g_TextVertexBuffer;

struct OpenGL_UniformBuffer
{
    u32 ubo;
    u32 capacity;
};

global u32 g_DynamicUboMaxEnties = 256;
global u32 g_UboOffsetAlignment;
global OpenGL_UniformBuffer g_DynamicUbo;
global u32 g_DynamicUboAlignment;

struct UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

struct DirectionalLight
{
    vec4 direction;
    vec4 color;
};

struct PointLight
{
    vec4 position;
    vec4 color;
    vec4 attenuation;
};

// FIXME: Must match constants defined in standard_shader.uber
#define MAX_DIRECTIONAL_LIGHTS 4
#define MAX_POINT_LIGHTS 32

struct LightUbo
{
    DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
    PointLight pointLights[MAX_POINT_LIGHTS];
    vec4 ambientLight;
    u32 directionalLightCount;
    u32 pointLightCount;
};

struct ShadowBuffer
{
    u32 size;
    u32 fbo;
    u32 depthTexture;
};

global OpenGL_UniformBuffer g_LightUbo;

global ShadowBuffer g_ShadowBuffer;

struct ShaderUniform
{
    u32 key;
    i32 location;
    u32 type;
};

enum
{
    FACE_CULLING_DISABLED,
    FACE_CULLING_FRONT_FACE,
    FACE_CULLING_BACK_FACE,
};

enum
{
    DEPTH_FUNCTION_LESS,
    DEPTH_FUNCTION_ALWAYS,
};

// TODO: Rework this
struct ShaderPipelineState
{
    b32 depthTestEnabled; // Default is true
    b32 depthMaskEnabled; // Default is true
    b32 useLinePolygonMode; // Default is false
    u32 faceCulling; // Default is 0=disabled
    u32 depthFunction; // Default is 0=less
};

struct GenericShader
{
    u32 program;
    ShaderPipelineState pipelineState;

    ShaderUniform uniforms[10];
    u32 uniformCount;

    b32 useUbo;
    b32 useLightUbo;

    i32 ubo;
    i32 lightUbo;
};

struct UniformDefinition
{
    const char *name;
    u32 key;
    u32 type;
};

global GenericShader g_Shaders[VULKAN_MAX_SHADERS];

internal OpenGL_UniformBuffer OpenGL_CreateUniformBuffer(u32 capacity)
{
    OpenGL_UniformBuffer result = {};
    glGenBuffers(1, &result.ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, result.ubo);
    glBufferData(GL_UNIFORM_BUFFER, capacity, NULL, GL_DYNAMIC_DRAW); // Not static!
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    return result;
}

internal OpenGL_VertexBuffer OpenGL_CreateVertexBuffer(u32 capacity, bool useTextVertexLayout)
{
    OpenGL_VertexBuffer result = {};
    result.capacity = capacity;

    glGenVertexArrays(1, &result.vao);
    glBindVertexArray(result.vao);

    glGenBuffers(1, &result.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, result.vbo);
    glBufferData(GL_ARRAY_BUFFER, capacity, NULL, GL_DYNAMIC_DRAW);

    if (useTextVertexLayout)
    {
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                sizeof(TextVertex), (void*)offsetof(TextVertex, position));
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                sizeof(TextVertex), (void*)offsetof(TextVertex, texCoord));
    }
    else
    {
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                sizeof(VertexPC), (void*)offsetof(VertexPC, position));
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE,
                sizeof(VertexPC), (void*)offsetof(VertexPC, color));
    }

    glBindVertexArray(0);

    return result;
}

internal void OpenGL_UpdateVertexBuffer(
    OpenGL_VertexBuffer vertexBuffer, void *vertices, u32 size)
{
    glBindVertexArray(vertexBuffer.vao);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer.vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, size, vertices);
    //glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

internal void OpenGL_DestroyVertexBuffer(OpenGL_VertexBuffer vertexBuffer)
{
    glBindVertexArray(0);
    //glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteVertexArrays(1, &vertexBuffer.vao);
    glDeleteBuffers(1, &vertexBuffer.vbo);
}

internal u32 OpenGL_CreateTexture(void *pixels, u32 width, u32 height,
    u32 bytesPerPixel, bool useSRGB = true, bool repeatMapping = false,
    u32 bytesPerChannel = 1, bool generateMipMaps = false)
{
    u32 result;
    glGenTextures(1, &result);
    glBindTexture(GL_TEXTURE_2D, result);

    i32 wrapping = repeatMapping ? GL_REPEAT : GL_CLAMP_TO_EDGE;
    i32 filter = generateMipMaps ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapping);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapping);

    // TODO: Support HDR textures for color textures
    u32 format = GL_RGBA;
    u32 internalFormat = useSRGB ? GL_SRGB8_ALPHA8 : GL_RGBA8;
    u32 dataType = GL_UNSIGNED_BYTE;
    if (bytesPerPixel == 1)
    {
        if (bytesPerChannel == 1)
        {
            internalFormat = GL_R8;
        }
        else if (bytesPerChannel == 2)
        {
            // FIXME: Support integer 16-bit textures
            internalFormat = GL_R16F;
            dataType = GL_FLOAT;
        }
        else
        {
            InvalidCodePath();
        }

        format = GL_RED;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format,
        dataType, pixels);

    if (generateMipMaps)
    {
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);

    glBindTexture(GL_TEXTURE_2D, 0);

    return result;
}

global u32 g_Textures[VULKAN_MAX_TEXTURES];

internal void OpenGL_CreateCubeMap(RenderCommandAllocateCubeMap *cmd)
{
    u32 texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    Assert(cmd->bytesPerPixel == 4);

    u32 internalFormat = GL_SRGB8_ALPHA8;
    u32 format = GL_RGBA;

    // +Z
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, internalFormat, cmd->width,
        cmd->width, 0, format, GL_UNSIGNED_BYTE,
        cmd->pixels[CUBE_MAP_FACE_Z_POS]);

    // -Z
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, internalFormat, cmd->width,
        cmd->width, 0, format, GL_UNSIGNED_BYTE,
        cmd->pixels[CUBE_MAP_FACE_Z_NEG]);

    // +Y
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, internalFormat, cmd->width,
        cmd->width, 0, format, GL_UNSIGNED_BYTE,
        cmd->pixels[CUBE_MAP_FACE_Y_POS]);

    // -Y
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, internalFormat, cmd->width,
        cmd->width, 0, format, GL_UNSIGNED_BYTE,
        cmd->pixels[CUBE_MAP_FACE_Y_NEG]);

    // +X
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, internalFormat, cmd->width,
        cmd->width, 0, format, GL_UNSIGNED_BYTE,
        cmd->pixels[CUBE_MAP_FACE_X_POS]);

    // -X
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, internalFormat, cmd->width,
        cmd->width, 0, format, GL_UNSIGNED_BYTE,
        cmd->pixels[CUBE_MAP_FACE_X_NEG]);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    Assert(cmd->textureIndex < VULKAN_MAX_TEXTURES);
    g_Textures[cmd->textureIndex] = texture;
}

struct OpenGL_Mesh
{
    u32 vao;
    u32 vbo;
    u32 ibo;
    u32 indexCount;
};

internal OpenGL_Mesh OpenGL_CreateMesh(
    Vertex *vertices, u32 vertexCount, u32 *indices, u32 indexCount)
{
    OpenGL_Mesh result = {};
    result.indexCount = indexCount;

    glGenVertexArrays(1, &result.vao);
    glBindVertexArray(result.vao);

    glGenBuffers(1, &result.vbo);
    glGenBuffers(1, &result.ibo);

    glBindBuffer(GL_ARRAY_BUFFER, result.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertexCount, vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, result.ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(u32) * indexCount, indices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
            sizeof(Vertex), (void*)offsetof(Vertex, pos));
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
            sizeof(Vertex), (void*)offsetof(Vertex, color));
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
            sizeof(Vertex), (void*)offsetof(Vertex, texCoord));

    glBindVertexArray(0);

    return result;
}

internal void OpenGL_DeleteMesh(OpenGL_Mesh mesh)
{
    if (mesh.vao != 0)
    {
        glBindVertexArray(0);
        glDeleteVertexArrays(1, &mesh.vao);
        glDeleteBuffers(1, &mesh.vbo);
        glDeleteBuffers(1, &mesh.ibo);
    }
}

global OpenGL_Mesh g_Meshes[VULKAN_MAX_MESHES];

bool OpenGL_CompileShader(
    GLuint shader, const char *src, bool isVertex, const char *defines = "")
{
    const char *version = "#version 330\n";
    const char *typeDefine = isVertex ? "#define VERTEX_SHADER\n" : "#define FRAGMENT_SHADER\n";
    const char *line = "#line 1\n";
    const char *sources[] = { version, typeDefine, defines, line, src };
    glShaderSource(shader, ArrayCount(sources), sources, NULL);
    glCompileShader(shader);

    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) // Check if the shader was compiled successfully.
    {
        int logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);

        char log[4096];
        Assert((u32)logLength < ArrayCount(log));

        i32 len = 0;
        glGetShaderInfoLog(shader, logLength, &len, log);

        if (isVertex)
        {
            LOG_ERROR("[VERTEX]\n%s", log);
        }
        else
        {
            LOG_ERROR("[FRAGMENT]\n%s", log);
        }
        return false;
    }
    return true;
}

bool OpenGL_LinkShader(GLuint vertex, GLuint fragment, GLuint program)
{
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);

    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) // Check if the shader was linked successfully.
    {
        int logLength;
        glGetShaderiv(program, GL_INFO_LOG_LENGTH, &logLength);

        char log[4096];
        int len = 0;
        glGetProgramInfoLog(program, logLength, &len, log);
        LOG_ERROR("[LINKER]\n%s", log);
        return false;
    }
    return true;
}

void OpenGL_DeleteShader(GLuint program)
{
    if (program != 0)
    {
        glUseProgram(0);
        GLsizei count = 0;
        GLuint shaders[2];
        // Need to retrieve the vertex and fragment shaders and delete them
        // explicitly, deleting the shader program only detaches the two
        // shaders, it does not free the resources for them.
        glGetAttachedShaders(program, 2, &count, shaders);
        glDeleteProgram(program);
        glDeleteShader(shaders[0]);
        glDeleteShader(shaders[1]);
    }
}

GLuint OpenGL_CreateShader(const char *shaderSource, const char *defines = "")
{
    GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
    GLuint program = glCreateProgram();

    bool vertexSuccess = OpenGL_CompileShader(vertex, shaderSource, true, defines);
    bool fragmentSuccess =
        OpenGL_CompileShader(fragment, shaderSource, false, defines);

    if (vertexSuccess && fragmentSuccess)
    {
        if (OpenGL_LinkShader(vertex, fragment, program))
        {
            return program;
        }
    }
    OpenGL_DeleteShader(program);
    return 0;
}

const char *vertexColorShader = R"FOO(
#ifdef VERTEX_SHADER
layout (location=0) in vec3 vertexPosition;
layout (location=3) in vec4 vertexColor;

layout (std140) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

out vec4 color;
void main()
{
  color = vertexColor;
  gl_Position = proj * view * model * vec4(vertexPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
out vec4 outputColor;
in vec4 color;
void main()
{
  outputColor = color;
}
#endif
)FOO";

const char *textShader = R"FOO(
#ifdef VERTEX_SHADER
layout(location=0) in vec2 vertexPosition;
layout(location=1) in vec2 vertexTexCoord;

layout (std140) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

out vec2 fragTexCoord;
void main()
{
    fragTexCoord = vertexTexCoord;
    gl_Position = proj * view * model * vec4(vertexPosition, 0.0, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
uniform sampler2D texSampler;
uniform vec4 color;
in vec2 fragTexCoord;
out vec4 outColor;

void main()
{
    float a = texture(texSampler, fragTexCoord).r;
    outColor = vec4(color.rgb, color.a * a);
}
#endif
)FOO";

const char *cubeMapShader = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

layout (std140) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
};
out vec3 textureCoordinates;
void main()
{
  textureCoordinates = inPosition;
  gl_Position = proj * view * model * vec4(inPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
in vec3 textureCoordinates;
out vec4 outputColor;
uniform samplerCube texSampler;
void main()
{
   outputColor = texture(texSampler, textureCoordinates);
   float gamma = 2.2;
   outputColor.rgb = pow(outputColor.rgb, vec3(1.0 / gamma));
}
#endif
)FOO";

const char *colorOnlyShader = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

layout (std140) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

void main()
{
  gl_Position = proj * view * model * vec4(inPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
uniform vec4 color;
out vec4 outColor;
void main()
{
    outColor = color;
}
#endif
)FOO";

const char *textureOnlyShader = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

// if we support OpenGL 4.2 we can specify the binding index in the layout
// specifier like in Vulkan
layout (std140) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

out vec2 textureCoordinates;
void main()
{
  textureCoordinates = inTexCoord;
  gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
in vec2 textureCoordinates;
out vec4 outputColor;
uniform sampler2D texSampler;
void main()
{
   outputColor = texture(texSampler, textureCoordinates);
   float gamma = 2.2;
   outputColor.rgb = pow(outputColor.rgb, vec3(1.0 / gamma));
}
#endif
)FOO";

const char *depthTextureShader = R"FOO(
#ifdef VERTEX_SHADER
layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

// if we support OpenGL 4.2 we can specify the binding index in the layout
// specifier like in Vulkan
layout (std140) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

out vec2 textureCoordinates;
void main()
{
  textureCoordinates = inTexCoord;
  gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0);
}
#endif

#ifdef FRAGMENT_SHADER
in vec2 textureCoordinates;
out vec4 outputColor;
uniform sampler2D texSampler;
void main()
{
   float depth = texture(texSampler, textureCoordinates).r;
   outputColor = vec4(depth, depth, depth, 1.0);
   float gamma = 2.2;
   outputColor.rgb = pow(outputColor.rgb, vec3(1.0 / gamma));
}
#endif
)FOO";

inline void DumpUniformValue(ShaderUniformValue *value)
{
    Assert(value->key < ArrayCount(g_UniformKeyNames));
    Assert(value->type < ArrayCount(g_UniformTypeNames));

    char buffer[256];
    switch (value->type)
    {
    case UNIFORM_TYPE_2F:
        snprintf(buffer, sizeof(buffer), "(%g, %g)", value->v2.x, value->v2.y);
        break;
    case UNIFORM_TYPE_3F:
        snprintf(buffer, sizeof(buffer), "(%g, %g, %g)", value->v3.x,
            value->v3.y, value->v3.z);
        break;
    case UNIFORM_TYPE_4F:
        snprintf(buffer, sizeof(buffer), "(%g, %g, %g, %g)", value->v4.x,
            value->v4.y, value->v4.z, value->v4.w);
        break;
    case UNIFORM_TYPE_MAT4:
        break;
    case UNIFORM_TYPE_TEXTURE2D:
        Assert(value->texture < ArrayCount(g_TextureNames));
        strncpy(buffer, g_TextureNames[value->texture], sizeof(buffer));
        break;
    case UNIFORM_TYPE_TEXTURE_CUBE_MAP:
        Assert(value->texture < ArrayCount(g_TextureNames));
        strncpy(buffer, g_TextureNames[value->texture], sizeof(buffer));
        break;
    case UNIFORM_TYPE_SHADOW_MAP:
        strncpy(buffer, "[SHADOW_MAP]", sizeof(buffer));
        break;
    default:
        InvalidCodePath();
        break;
    }

    LOG_DEBUG("\tUniform key: %s type: %s value: %s",
        g_UniformKeyNames[value->key], g_UniformTypeNames[value->type],
        buffer);
}

// WARNING: This needs to be updated if the render command layout ever changes
// or any new commands are added. In short it probably will need to be updated
// every time a change to OpenGL_DrawFrame is made.
internal void DumpRenderCommand(RenderCommandHeader *header, void *data)
{
    Assert(header->type < ArrayCount(g_RenderCommandTypeNames));
    LOG_DEBUG("Dump of render command %s:", g_RenderCommandTypeNames[header->type]);

    switch (header->type)
    {
    case RenderCommandDrawMeshUniformsTypeId:
    {
        RenderCommandDrawMeshUniforms *cmd =
            (RenderCommandDrawMeshUniforms *)data;
        Assert(cmd->shader < ArrayCount(g_ShaderNames));
        Assert(cmd->mesh < ArrayCount(g_MeshNames));
        LOG_DEBUG("\tShader: %s, Mesh: %s", g_ShaderNames[cmd->shader],
            g_MeshNames[cmd->mesh]);

        for (u32 i = 0; i < cmd->count; ++i)
        {
            ShaderUniformValue *value = cmd->values + i;
            DumpUniformValue(value);
        }
    }
    break;
    case RenderCommandAllocateTextureTypeId:
    {
        RenderCommandAllocateTexture *cmd =
            (RenderCommandAllocateTexture *)data;
    }
    break;
    case RenderCommandAllocateCubeMapTypeId:
    {
        RenderCommandAllocateCubeMap *cmd =
            (RenderCommandAllocateCubeMap *)data;
    }
    break;
    case RenderCommandUpdateVertexBufferTypeId:
    {
        RenderCommandUpdateVertexBuffer *cmd =
            (RenderCommandUpdateVertexBuffer *)data;
    }
    break;
    case RenderCommandDrawVertexBufferTypeId:
    {
        RenderCommandDrawVertexBuffer *cmd =
            (RenderCommandDrawVertexBuffer *)data;
    }
    break;
    case RenderCommandAllocateMeshTypeId:
    {
        RenderCommandAllocateMesh *cmd = (RenderCommandAllocateMesh *)data;
    }
    break;
    case RenderCommandDrawLightTypeId:
    {
        RenderCommandDrawLight *cmd = (RenderCommandDrawLight *)data;
    }
    break;
    case RenderCommandDrawShadowTypeId:
    {
        RenderCommandDrawShadow *cmd = (RenderCommandDrawShadow *)data;
    }
    break;
    default:
        InvalidCodePath();
        break;
    }
}

global RenderCommandHeader *g_CurrentRenderCommand = NULL;

void OpenGLReportErrorMessage(GLenum source, GLenum type, GLuint id,
                              GLenum severity, GLsizei length,
                              const GLchar *message, const void *userParam)
{
    const char *typeStr = nullptr;
    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:
        typeStr = "ERROR";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        typeStr = "DEPRECATED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        typeStr = "UNDEFINED_BEHAVIOR";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        typeStr = "PORTABILITY";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        typeStr = "PERFORMANCE";
        return; // FIXME: Skip perf warnings for now
        break;
    case GL_DEBUG_TYPE_OTHER:
        typeStr = "OTHER";
        break;
    default:
        typeStr = "";
        break;
    }

    const char *severityStr = nullptr;
    switch (severity)
    {
    case GL_DEBUG_SEVERITY_LOW:
        severityStr = "LOW";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        severityStr = "MEDIUM";
        break;
    case GL_DEBUG_SEVERITY_HIGH:
        severityStr = "HIGH";
        break;
    default:
        severityStr = "";
        break;
    }

    LOG_ERROR("OPENGL|%s:%s:%s", typeStr, severityStr, message);

    if (g_CurrentRenderCommand != NULL)
    {
        DumpRenderCommand(
            g_CurrentRenderCommand, (void *)(g_CurrentRenderCommand + 1));
    }
}

internal void OpenGL_DestroyShadowBuffer(ShadowBuffer buffer)
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glDeleteFramebuffers(1, &buffer.fbo);
    glDeleteTextures(1, &buffer.depthTexture);
}

internal ShadowBuffer OpenGL_CreateShadowBuffer(u32 size)
{
    ShadowBuffer buffer = {};
    buffer.size = size;
    glGenFramebuffers(1, &buffer.fbo);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, buffer.fbo);

    glGenTextures(1, &buffer.depthTexture);
    glBindTexture(GL_TEXTURE_2D, buffer.depthTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, buffer.size,
        buffer.size, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor); 
    glTexParameteri(
        GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
        GL_TEXTURE_2D, buffer.depthTexture, 0);

    glDrawBuffer(GL_NONE);

    GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE)
    {
        LOG_ERROR("Framebuffer error, status: 0x%x", status);
        OpenGL_DestroyShadowBuffer(buffer);
        ZeroStruct(buffer);
        return buffer;
    }
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    return buffer;
}

internal bool CreateGenericShader(GenericShader *shader, const char *shaderCode,
    const char *defines, UniformDefinition *uniformDefs, u32 count,
    b32 useUbo, b32 useLightUbo, ShaderPipelineState pipelineState)
{
    shader->program = OpenGL_CreateShader(shaderCode, defines);
    if (!shader->program)
        return false;

    shader->useUbo = useUbo;
    shader->useLightUbo = useLightUbo;
    shader->pipelineState = pipelineState;

    if (useUbo)
    {
        shader->ubo = 0;
        i32 uboIdx = glGetUniformBlockIndex(shader->program, "UniformBufferObject");
        if (uboIdx < 0)
        {
            LOG_WARNING("Shader does not have a UniformBufferObject.");
        }
        glUniformBlockBinding(shader->program, uboIdx, shader->ubo);
    }

    if (useLightUbo)
    {
        shader->lightUbo = 1;
        i32 uboIdx = glGetUniformBlockIndex(shader->program, "LightUbo");
        if (uboIdx < 0)
        {
            LOG_WARNING("Shader does not have a LightUbo.");
        }
        glUniformBlockBinding(shader->program, uboIdx, shader->lightUbo);
    }

    Assert(count < ArrayCount(shader->uniforms));
    for (u32 i = 0; i < count; ++i)
    {
        i32 location = glGetUniformLocation(shader->program, uniformDefs[i].name);
        if (location < 0)
        {
            LOG_WARNING("Failed to find location for uniform \"%s\" in shader.",
                uniformDefs[i].name);
        }

        shader->uniforms[i].location = location;
        shader->uniforms[i].key = uniformDefs[i].key;
        shader->uniforms[i].type = uniformDefs[i].type;
    }
    shader->uniformCount = count;

    return true;
}

inline ShaderUniformValue *FindValue(
    ShaderUniform uniform, ShaderUniformValue *values, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        if (values[i].key == uniform.key)
            return values + i;
    }

    return NULL;
}

internal void BindGenericShader(
    GenericShader *shader, ShaderUniformValue *values, u32 count)
{
    glUseProgram(shader->program);

    ShaderPipelineState *pipelineState = &shader->pipelineState;

    if (pipelineState->depthTestEnabled)
    {
        glEnable(GL_DEPTH_TEST);
        if (pipelineState->depthFunction == DEPTH_FUNCTION_ALWAYS)
            glDepthFunc(GL_ALWAYS);
        else if (pipelineState->depthFunction == DEPTH_FUNCTION_LESS)
            glDepthFunc(GL_LESS);
        else
            InvalidCodePath();
    }
    else
        glDisable(GL_DEPTH_TEST);

    if (pipelineState->depthMaskEnabled)
        glDepthMask(GL_TRUE);
    else
        glDepthMask(GL_FALSE);

    if (pipelineState->useLinePolygonMode)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (pipelineState->faceCulling != FACE_CULLING_DISABLED)
    {
        glEnable(GL_CULL_FACE);
        if (pipelineState->faceCulling == FACE_CULLING_FRONT_FACE)
            glCullFace(GL_FRONT);
        else
            glCullFace(GL_BACK);
    }
    else
        glDisable(GL_CULL_FACE);

    u32 activeTexture = 0;
    // Fill uniform data
    for (u32 i = 0; i < shader->uniformCount; ++i)
    {
        ShaderUniform uniform = shader->uniforms[i];

        ShaderUniformValue *value = FindValue(uniform, values, count);
        if (value)
        {
            Assert(value->type == uniform.type);

            // Find input
            switch (uniform.type)
            {
            case UNIFORM_TYPE_2F:
                glUniform2fv(uniform.location, 1, value->v2.data);
                break;
            case UNIFORM_TYPE_3F:
                glUniform3fv(uniform.location, 1, value->v3.data);
                break;
            case UNIFORM_TYPE_4F:
                glUniform4fv(uniform.location, 1, value->v4.data);
                break;
            case UNIFORM_TYPE_MAT4:
                glUniformMatrix4fv(
                    uniform.location, 1, GL_FALSE, value->m4.raw);
                break;
            case UNIFORM_TYPE_TEXTURE2D:
            {
                Assert(value->texture < VULKAN_MAX_TEXTURES);
                glActiveTexture(GL_TEXTURE0 + activeTexture);
                glBindTexture(GL_TEXTURE_2D, g_Textures[value->texture]);
                glUniform1i(uniform.location, activeTexture);
                activeTexture++;
            }
            break;
            case UNIFORM_TYPE_TEXTURE_CUBE_MAP:
            {
                Assert(value->texture < VULKAN_MAX_TEXTURES);
                glActiveTexture(GL_TEXTURE0 + activeTexture);
                glBindTexture(GL_TEXTURE_CUBE_MAP, g_Textures[value->texture]);
                glUniform1i(uniform.location, activeTexture);
                activeTexture++;
            }
            break;
            case UNIFORM_TYPE_SHADOW_MAP:
            {
                glActiveTexture(GL_TEXTURE0 + activeTexture);
                glBindTexture(GL_TEXTURE_2D, g_ShadowBuffer.depthTexture);
                glUniform1i(uniform.location, activeTexture);
                activeTexture++;
            }
            break;
            default:
                InvalidCodePath();
                break;
            }
        }
    }

    if (shader->useLightUbo)
    {
        glBindBufferBase(GL_UNIFORM_BUFFER, 1, g_LightUbo.ubo);
    }
}

internal void CreateVertexColorShader()
{
    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = false;
    pipelineState.depthMaskEnabled = false;
    pipelineState.useLinePolygonMode = true;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_VERTEX_COLOR], vertexColorShader,
            "", NULL, 0, true, false, pipelineState);
}

internal void CreateTextShader()
{
    UniformDefinition uniformDefs[2];
    uniformDefs[0].name = "texSampler";
    uniformDefs[0].key = UNIFORM_KEY_TEX_SAMPLER;
    uniformDefs[0].type = UNIFORM_TYPE_TEXTURE2D;
    uniformDefs[1].name = "color";
    uniformDefs[1].key = UNIFORM_KEY_COLOR;
    uniformDefs[1].type = UNIFORM_TYPE_4F;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = false;
    pipelineState.depthMaskEnabled = false;
    pipelineState.useLinePolygonMode = false;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_TEXT], textShader, "",
        uniformDefs, ArrayCount(uniformDefs), true, false, pipelineState);
}

internal void CreateCubeMapShader()
{
    UniformDefinition uniformDefs[1];
    uniformDefs[0].name = "texSampler";
    uniformDefs[0].key = UNIFORM_KEY_TEX_SAMPLER;
    uniformDefs[0].type = UNIFORM_TYPE_TEXTURE_CUBE_MAP;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = false;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_FRONT_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_ALWAYS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_CUBE_MAP], cubeMapShader,
            "", uniformDefs, ArrayCount(uniformDefs), true, false, pipelineState);
}

internal void CreateColorOnlyShader()
{
    UniformDefinition uniformDefs[1];
    uniformDefs[0].name = "color";
    uniformDefs[0].key = UNIFORM_KEY_COLOR;
    uniformDefs[0].type = UNIFORM_TYPE_4F;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_COLOR_ONLY], colorOnlyShader,
            "", uniformDefs, ArrayCount(uniformDefs), true, false, pipelineState);
}

internal void CreateShadowShader()
{
    UniformDefinition uniformDefs[1];
    uniformDefs[0].name = "color";
    uniformDefs[0].key = UNIFORM_KEY_COLOR;
    uniformDefs[0].type = UNIFORM_TYPE_4F;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_FRONT_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_SHADOW], colorOnlyShader,
            "", uniformDefs, ArrayCount(uniformDefs), true, false, pipelineState);
}

// TODO: Reuse colorOnly shader program rather than creating a new one for each
// variant
internal void CreateColorWireframeShader()
{
    UniformDefinition uniformDefs[1];
    uniformDefs[0].name = "color";
    uniformDefs[0].key = UNIFORM_KEY_COLOR;
    uniformDefs[0].type = UNIFORM_TYPE_4F;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = true;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_COLOR_WIREFRAME],
        colorOnlyShader, "", uniformDefs, ArrayCount(uniformDefs), true, false,
        pipelineState);
}

internal void CreateColorOnlyNoDepthShader()
{
    UniformDefinition uniformDefs[1];
    uniformDefs[0].name = "color";
    uniformDefs[0].key = UNIFORM_KEY_COLOR;
    uniformDefs[0].type = UNIFORM_TYPE_4F;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = false;
    pipelineState.depthMaskEnabled = false;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_ALWAYS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_COLOR_ONLY_NO_DEPTH],
        colorOnlyShader, "", uniformDefs, ArrayCount(uniformDefs), true, false,
        pipelineState);
}

internal void CreateTextureOnlyShader()
{
    UniformDefinition uniformDefs[1];
    uniformDefs[0].name = "texSampler";
    uniformDefs[0].key = UNIFORM_KEY_TEX_SAMPLER;
    uniformDefs[0].type = UNIFORM_TYPE_TEXTURE2D;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_TEXTURE_ONLY], textureOnlyShader,
            "", uniformDefs, ArrayCount(uniformDefs), true, false, pipelineState);
}

internal void CreateDepthTextureShader()
{
    UniformDefinition uniformDefs[1];
    uniformDefs[0].name = "texSampler";
    uniformDefs[0].key = UNIFORM_KEY_TEX_SAMPLER;
    uniformDefs[0].type = UNIFORM_TYPE_SHADOW_MAP;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_DEPTH_TEXTURE], depthTextureShader,
            "", uniformDefs, ArrayCount(uniformDefs), true, false, pipelineState);
}

internal void CreateDiffuseLightingShader()
{
    ReadFileResult shaderFile = DebugReadEntireFile("../shaders/gl/standard_shader.uber");
    Assert(shaderFile.memory);

    UniformDefinition uniformDefs[3];
    uniformDefs[0].name = "color";
    uniformDefs[0].key = UNIFORM_KEY_COLOR;
    uniformDefs[0].type = UNIFORM_TYPE_4F;
    uniformDefs[1].name = "lightViewProjection";
    uniformDefs[1].key = UNIFORM_KEY_LIGHT_VIEW_PROJECTION;
    uniformDefs[1].type = UNIFORM_TYPE_MAT4;
    uniformDefs[2].name = "shadowMap";
    uniformDefs[2].key = UNIFORM_KEY_SHADOW_MAP;
    uniformDefs[2].type = UNIFORM_TYPE_SHADOW_MAP;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_DIFFUSE_LIGHTING],
        (const char *)shaderFile.memory, "", uniformDefs,
        ArrayCount(uniformDefs), true, true, pipelineState);

    DebugFreeFile(shaderFile);
}

internal void CreateTextureDiffuseLightingShader()
{
    ReadFileResult shaderFile = DebugReadEntireFile("../shaders/gl/standard_shader.uber");
    Assert(shaderFile.memory);

    UniformDefinition uniformDefs[3];
    uniformDefs[0].name = "texSampler";
    uniformDefs[0].key = UNIFORM_KEY_TEX_SAMPLER;
    uniformDefs[0].type = UNIFORM_TYPE_TEXTURE2D;
    uniformDefs[1].name = "lightViewProjection";
    uniformDefs[1].key = UNIFORM_KEY_LIGHT_VIEW_PROJECTION;
    uniformDefs[1].type = UNIFORM_TYPE_MAT4;
    uniformDefs[2].name = "shadowMap";
    uniformDefs[2].key = UNIFORM_KEY_SHADOW_MAP;
    uniformDefs[2].type = UNIFORM_TYPE_SHADOW_MAP;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_TEXTURE_DIFFUSE_LIGHTING],
        (const char *)shaderFile.memory, "#define DIFFUSE_TEXTURE\n", uniformDefs,
        ArrayCount(uniformDefs), true, true, pipelineState);

    DebugFreeFile(shaderFile);
}

internal void CreateWorldTexCoordShader()
{
    ReadFileResult shaderFile = DebugReadEntireFile("../shaders/gl/standard_shader.uber");
    Assert(shaderFile.memory);

    UniformDefinition uniformDefs[3];
    uniformDefs[0].name = "texSampler";
    uniformDefs[0].key = UNIFORM_KEY_TEX_SAMPLER;
    uniformDefs[0].type = UNIFORM_TYPE_TEXTURE2D;
    uniformDefs[1].name = "lightViewProjection";
    uniformDefs[1].key = UNIFORM_KEY_LIGHT_VIEW_PROJECTION;
    uniformDefs[1].type = UNIFORM_TYPE_MAT4;
    uniformDefs[2].name = "shadowMap";
    uniformDefs[2].key = UNIFORM_KEY_SHADOW_MAP;
    uniformDefs[2].type = UNIFORM_TYPE_SHADOW_MAP;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_WORLD_TEX_COORD],
        (const char *)shaderFile.memory,
        "#define WORLD_TEXTURE_COORDINATES\n#define DIFFUSE_TEXTURE\n",
        uniformDefs, ArrayCount(uniformDefs), true, true, pipelineState);

    DebugFreeFile(shaderFile);
}

internal void CreateTerrainShader()
{
    ReadFileResult shaderFile = DebugReadEntireFile("../shaders/gl/standard_shader.uber");
    Assert(shaderFile.memory);

    UniformDefinition uniformDefs[9];
    uniformDefs[0].name = "texSampler";
    uniformDefs[0].key = UNIFORM_KEY_TEX_SAMPLER;
    uniformDefs[0].type = UNIFORM_TYPE_TEXTURE2D;
    uniformDefs[1].name = "lightViewProjection";
    uniformDefs[1].key = UNIFORM_KEY_LIGHT_VIEW_PROJECTION;
    uniformDefs[1].type = UNIFORM_TYPE_MAT4;
    uniformDefs[2].name = "shadowMap";
    uniformDefs[2].key = UNIFORM_KEY_SHADOW_MAP;
    uniformDefs[2].type = UNIFORM_TYPE_SHADOW_MAP;
    uniformDefs[3].name = "terrainHeightMap";
    uniformDefs[3].key = UNIFORM_KEY_TERRAIN_HEIGHT_MAP;
    uniformDefs[3].type = UNIFORM_TYPE_TEXTURE2D;
    uniformDefs[4].name = "terrainNormalMap";
    uniformDefs[4].key = UNIFORM_KEY_TERRAIN_NORMAL_MAP;
    uniformDefs[4].type = UNIFORM_TYPE_TEXTURE2D;
    uniformDefs[5].name = "terrainTexCoordOffset";
    uniformDefs[5].key = UNIFORM_KEY_TERRAIN_TEX_COORD_OFFSET;
    uniformDefs[5].type = UNIFORM_TYPE_2F;
    uniformDefs[6].name = "terrainTexCoordScale";
    uniformDefs[6].key = UNIFORM_KEY_TERRAIN_TEX_COORD_SCALE;
    uniformDefs[6].type = UNIFORM_TYPE_2F;
    uniformDefs[7].name = "texCoordScale";
    uniformDefs[7].key = UNIFORM_KEY_TEX_COORD_SCALE;
    uniformDefs[7].type = UNIFORM_TYPE_2F;
    uniformDefs[8].name = "texCoordOffset";
    uniformDefs[8].key = UNIFORM_KEY_TEX_COORD_OFFSET;
    uniformDefs[8].type = UNIFORM_TYPE_2F;

    ShaderPipelineState pipelineState = {};
    pipelineState.depthTestEnabled = true;
    pipelineState.depthMaskEnabled = true;
    pipelineState.useLinePolygonMode = false;
    pipelineState.faceCulling = FACE_CULLING_BACK_FACE;
    pipelineState.depthFunction = DEPTH_FUNCTION_LESS;

    CreateGenericShader(&g_Shaders[VULKAN_SHADER_TERRAIN],
        (const char *)shaderFile.memory,
        "#define DIFFUSE_TEXTURE\n#define TERRAIN\n", uniformDefs,
        ArrayCount(uniformDefs), true, true, pipelineState);

    DebugFreeFile(shaderFile);
}

internal void OpenGL_Init(GLFWwindow *window, u32 frameBufferWidth, u32 frameBufferHeight)
{
    GLenum result = glewInit();
    if (result != GLEW_OK)
    {
        LOG_ERROR("%s\n", glewGetErrorString(result));
    }

    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(OpenGLReportErrorMessage, nullptr);
    GLuint unusedIds = 0;
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                          &unusedIds, GL_TRUE);

    g_LineVertexBuffer =
        OpenGL_CreateVertexBuffer(VULKAN_MAX_LINES * 2 * sizeof(VertexPC), false);
    g_TextVertexBuffer = OpenGL_CreateVertexBuffer(
        VULKAN_MAX_TEXT_VERTICES * sizeof(TextVertex), true);

    i32 uboOffsetAlignment = 0;
    glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &uboOffsetAlignment);
    g_UboOffsetAlignment = (u32)uboOffsetAlignment;

    u32 alignment = sizeof(UniformBufferObject);
    if (g_UboOffsetAlignment > 0)
    {
        alignment =
            (alignment + g_UboOffsetAlignment - 1) & ~(g_UboOffsetAlignment - 1);
    }
    g_DynamicUboAlignment = alignment;

    g_DynamicUbo =
        OpenGL_CreateUniformBuffer(g_DynamicUboMaxEnties * alignment);

    g_LightUbo = OpenGL_CreateUniformBuffer(sizeof(LightUbo));

    g_ShadowBuffer = OpenGL_CreateShadowBuffer(1024);

    // Generic shaders
    CreateTextShader();
    CreateVertexColorShader();
    CreateCubeMapShader();
    CreateColorOnlyShader();
    CreateColorWireframeShader();
    CreateColorOnlyNoDepthShader();
    CreateTextureOnlyShader();
    CreateDiffuseLightingShader();
    CreateTextureDiffuseLightingShader();
    CreateWorldTexCoordShader();
    CreateTerrainShader();
    CreateDepthTextureShader();
    CreateShadowShader();
}

internal void ProcessDrawMeshUniforms(RenderCommandDrawMeshUniforms *cmd, u32 *entryCount)
{
    Assert(cmd->mesh < VULKAN_MAX_MESHES);
    OpenGL_Mesh mesh = g_Meshes[cmd->mesh];
    glBindVertexArray(mesh.vao);

    bool shaderBound = false;
    Assert(cmd->shader < VULKAN_MAX_SHADERS);
    GenericShader *shader = g_Shaders + cmd->shader;

    // Validation
    if (shader->uniformCount != cmd->count)
    {
        Assert(cmd->shader < ArrayCount(g_ShaderNames));
        const char *shaderName = g_ShaderNames[cmd->shader];
        LOG_WARNING("%u uniforms specified by shader %s but %u are provided "
                    "by the render command.",
            shader->uniformCount, shaderName, cmd->count);
    }

    for (u32 i = 0; i < shader->uniformCount; ++i)
    {
        ShaderUniformValue *value =
            FindValue(shader->uniforms[i], cmd->values, cmd->count);

        if (!value)
        {
            Assert(shader->uniforms[i].key < ArrayCount(g_UniformKeyNames));
            const char *keyName = g_UniformKeyNames[shader->uniforms[i].key];
            Assert(cmd->shader < ArrayCount(g_ShaderNames));
            const char *shaderName = g_ShaderNames[cmd->shader];

            LOG_WARNING("Uniform value for uniform key %s is not provided by "
                        "render command for shader %s",
                keyName, shaderName);
        }
    }

    BindGenericShader(shader, cmd->values, cmd->count);
    shaderBound = true;

    if (shaderBound)
    {
        // Bind and update UBO
        Assert(*entryCount < g_DynamicUboMaxEnties);
        u32 offset = (*entryCount)++ * g_DynamicUboAlignment;
        glBindBufferRange(GL_UNIFORM_BUFFER, 0, g_DynamicUbo.ubo, offset,
                sizeof(UniformBufferObject));

        UniformBufferObject ubo = {};
        ubo.model = cmd->model;
        ubo.view = cmd->view;
        ubo.proj = cmd->projection;
        glBindBuffer(GL_UNIFORM_BUFFER, g_DynamicUbo.ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(UniformBufferObject), &ubo);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);

        // Draw geometry and reset state machine
        glDrawElements(GL_TRIANGLES, mesh.indexCount, GL_UNSIGNED_INT, NULL);
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    }
}

internal void OpenGL_RenderShadowPass(
    RenderCommandQueue *renderCommands, ShadowBuffer buffer, u32 *entryCount)
{
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_FRONT);
    glBindFramebuffer(GL_FRAMEBUFFER, buffer.fbo);
    glViewport(0, 0, buffer.size, buffer.size);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

    // Process render commands
    u32 cursor = 0;
    while (cursor < renderCommands->length)
    {
        RenderCommandHeader *header =
            (RenderCommandHeader *)(renderCommands->start + cursor);
        void *data = (void *)(header + 1);
        Assert(cursor + header->size <= renderCommands->length);

        if (header->type == RenderCommandDrawShadowTypeId)
        {
            RenderCommandDrawShadow *cmd = (RenderCommandDrawShadow *)data;

            Assert(cmd->mesh < VULKAN_MAX_MESHES);
            OpenGL_Mesh mesh = g_Meshes[cmd->mesh];
            glBindVertexArray(mesh.vao);

            {
                ShaderUniformValue values[1];
                values[0].key = UNIFORM_KEY_COLOR;
                values[0].type = UNIFORM_TYPE_4F;
                values[0].v4 = Vec4(0, 0, 0, 1);
                BindGenericShader(&g_Shaders[VULKAN_SHADER_SHADOW], values,
                    ArrayCount(values));
            }

            // Bind and update UBO
            Assert(*entryCount < g_DynamicUboMaxEnties);
            u32 offset = (*entryCount)++ * g_DynamicUboAlignment;
            glBindBufferRange(GL_UNIFORM_BUFFER, 0, g_DynamicUbo.ubo, offset,
                    sizeof(UniformBufferObject));

            UniformBufferObject ubo = {};
            ubo.model = cmd->model;
            ubo.view = cmd->view;
            ubo.proj = cmd->projection;
            glBindBuffer(GL_UNIFORM_BUFFER, g_DynamicUbo.ubo);
            glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(UniformBufferObject), &ubo);
            glBindBuffer(GL_UNIFORM_BUFFER, 0);

            // Draw geometry and reset state machine
            glDrawElements(GL_TRIANGLES, mesh.indexCount, GL_UNSIGNED_INT, NULL);
            glBindVertexArray(0);
            glBindTexture(GL_TEXTURE_2D, 0);
            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }

        cursor += sizeof(RenderCommandHeader) + header->size;
    }
}

// WARNING: Anytime a change is made here it is likely that the same change
// will need to be made to OpenGL_DumpState.
internal void OpenGL_DrawFrame(RenderCommandQueue *renderCommands,
    u32 frameBufferWidth, u32 frameBufferHeight)
{
    // Process asset allocations and buffer updates
    {
        u32 cursor = 0;
        while (cursor < renderCommands->length)
        {
            RenderCommandHeader *header =
                (RenderCommandHeader *)(renderCommands->start + cursor);
            void *data = (void *)(header + 1);
            Assert(cursor + header->size <= renderCommands->length);

            g_CurrentRenderCommand = header;

            if (header->type == RenderCommandAllocateTextureTypeId)
            {
                RenderCommandAllocateTexture *cmd = (RenderCommandAllocateTexture *)data;
                // FIXME: Need a better system for assets indices/ids
                Assert(cmd->index < VULKAN_MAX_TEXTURES);
                Assert(cmd->materialIndex < VULKAN_MAX_MATERIALS);

                if (g_Textures[cmd->index] != 0)
                {
                    glDeleteTextures(1, &g_Textures[cmd->index]);
                }

                g_Textures[cmd->index] =
                    OpenGL_CreateTexture(cmd->pixels, cmd->width, cmd->height,
                        cmd->bytesPerPixel, cmd->useSRGB, cmd->repeatMapping,
                        cmd->bytesPerChannel, cmd->generateMipMaps);
            }
            else if (header->type == RenderCommandUpdateVertexBufferTypeId)
            {
                RenderCommandUpdateVertexBuffer *cmd =
                    (RenderCommandUpdateVertexBuffer *)data;

                if (cmd->lengthInBytes > 0)
                {
                    // TODO: Merge into same buffer with 2 partitions
                    OpenGL_VertexBuffer vbo = {};
                    if (cmd->bufferIndex == VULKAN_VERTEX_BUFFER_TEXT)
                    {
                        vbo = g_TextVertexBuffer;
                    }
                    else if (cmd->bufferIndex == VULKAN_VERTEX_BUFFER_LINES)
                    {
                        vbo = g_LineVertexBuffer;
                    }
                    else
                    {
                        InvalidCodePath();
                    }
                    OpenGL_UpdateVertexBuffer(vbo, cmd->vertices, cmd->lengthInBytes);
                }
            }
            else if (header->type == RenderCommandAllocateCubeMapTypeId)
            {
                RenderCommandAllocateCubeMap *cmd =
                    (RenderCommandAllocateCubeMap *)data;

                OpenGL_CreateCubeMap(cmd);
            }
            else if (header->type == RenderCommandAllocateMeshTypeId)
            {
                RenderCommandAllocateMesh *cmd = (RenderCommandAllocateMesh *)data;

                Assert(cmd->meshIndex < VULKAN_MAX_MESHES);
                g_Meshes[cmd->meshIndex] = OpenGL_CreateMesh(cmd->vertices,
                    cmd->vertexCount, cmd->indices, cmd->indexCount);
            }

            cursor += sizeof(RenderCommandHeader) + header->size;
        }
    }

    g_CurrentRenderCommand = NULL;

    u32 entryCount = 0;
    OpenGL_RenderShadowPass(renderCommands, g_ShadowBuffer, &entryCount);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, frameBufferWidth, frameBufferHeight);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    LightUbo lightUbo = {};

    // Process render commands
    u32 cursor = 0;
    while (cursor < renderCommands->length)
    {
        RenderCommandHeader *header =
            (RenderCommandHeader *)(renderCommands->start + cursor);
        void *data = (void *)(header + 1);
        Assert(cursor + header->size <= renderCommands->length);

        g_CurrentRenderCommand = header;

        // FIXME: Refactor this into a switch statement
        //Assert(entryCount < maxEntries);
        if (header->type == RenderCommandDrawVertexBufferTypeId)
        {
            RenderCommandDrawVertexBuffer *cmd =
                (RenderCommandDrawVertexBuffer *)data;

            if (cmd->shader == VULKAN_SHADER_VERTEX_COLOR)
            {
                BindGenericShader(
                    &g_Shaders[VULKAN_SHADER_VERTEX_COLOR], NULL, 0);
            }
            else if (cmd->shader == VULKAN_SHADER_TEXT)
            {
                Assert(cmd->material < VULKAN_MAX_MATERIALS);
                ShaderUniformValue values[2];
                values[0].key = UNIFORM_KEY_TEX_SAMPLER;
                values[0].type = UNIFORM_TYPE_TEXTURE2D;
                values[0].texture = cmd->material;
                values[1].key = UNIFORM_KEY_COLOR;
                values[1].type = UNIFORM_TYPE_4F;
                values[1].v4 = cmd->color;
                BindGenericShader(
                    &g_Shaders[VULKAN_SHADER_TEXT], values, ArrayCount(values));
            }

            // Bind and update UBO
            Assert(entryCount < g_DynamicUboMaxEnties);
            u32 offset = entryCount++ * g_DynamicUboAlignment;
            glBindBufferRange(GL_UNIFORM_BUFFER, 0, g_DynamicUbo.ubo, offset,
                    sizeof(UniformBufferObject));

            UniformBufferObject ubo = {};
            ubo.model = cmd->model;
            ubo.view = cmd->view;
            ubo.proj = cmd->projection;
            glBindBuffer(GL_UNIFORM_BUFFER, g_DynamicUbo.ubo);
            glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(UniformBufferObject), &ubo);
            glBindBuffer(GL_UNIFORM_BUFFER, 0);

            OpenGL_VertexBuffer vbo = {};
            if (cmd->vbo == VULKAN_VERTEX_BUFFER_TEXT)
            {
                vbo = g_TextVertexBuffer;
                glBindVertexArray(vbo.vao);
                glDrawArrays(GL_TRIANGLES, cmd->firstVertex, cmd->vertexCount);
                glBindVertexArray(0);
            }
            else if (cmd->vbo == VULKAN_VERTEX_BUFFER_LINES)
            {
                vbo = g_LineVertexBuffer;
                glBindVertexArray(vbo.vao);
                glDrawArrays(GL_LINES, cmd->firstVertex, cmd->vertexCount);
                glBindVertexArray(0);
            }

            glUseProgram(0);
            glBindTexture(GL_TEXTURE_2D, 0);

        }
        else if (header->type == RenderCommandDrawMeshUniformsTypeId)
        {
            RenderCommandDrawMeshUniforms *cmd = (RenderCommandDrawMeshUniforms *)data;
            ProcessDrawMeshUniforms(cmd, &entryCount);
        }
        else if (header->type == RenderCommandDrawLightTypeId)
        {
            RenderCommandDrawLight *cmd = (RenderCommandDrawLight *)data;
            switch (cmd->type)
            {
                case LightType_Ambient:
                    lightUbo.ambientLight = Vec4(cmd->color, 0);
                    break;
                case LightType_Directional:
                    {
                        Assert(lightUbo.directionalLightCount < MAX_DIRECTIONAL_LIGHTS);
                        DirectionalLight *directionalLight =
                            lightUbo.directionalLights +
                            lightUbo.directionalLightCount++;
                        directionalLight->direction = Vec4(cmd->direction, 0);
                        directionalLight->color = Vec4(cmd->color, 0);
                    }
                    break;
                case LightType_Point:
                    {
                        Assert(lightUbo.pointLightCount < MAX_POINT_LIGHTS);
                        PointLight *pointLight =
                            lightUbo.pointLights + lightUbo.pointLightCount++;
                        pointLight->position = Vec4(cmd->position, 0);
                        pointLight->color = Vec4(cmd->color, 0);
                        pointLight->attenuation = Vec4(cmd->attenuation, 0);
                    }
                    break;
                case LightType_Spot:
                    InvalidCodePath();
                    break;
                default:
                    InvalidCodePath();
                    break;
            }
        }
        else
        {
            // FIXME: Probably worth having a separate queue for asset commands
            // Skip asset render commands
        }


        cursor += sizeof(RenderCommandHeader) + header->size;
    }

    g_CurrentRenderCommand = NULL;

    glBindBuffer(GL_UNIFORM_BUFFER, g_LightUbo.ubo);
    glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(LightUbo), &lightUbo);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

internal void OpenGL_CleanUp()
{
    OpenGL_DestroyVertexBuffer(g_LineVertexBuffer);
    OpenGL_DestroyVertexBuffer(g_TextVertexBuffer);

    for (u32 i = 0; i < VULKAN_MAX_SHADERS; ++i)
    {
        OpenGL_DeleteShader(g_Shaders[i].program);
    }

    glDeleteTextures(VULKAN_MAX_TEXTURES, g_Textures);

    for (u32 i = 0; i < VULKAN_MAX_MESHES; ++i)
    {
        OpenGL_DeleteMesh(g_Meshes[i]);
    }
}
