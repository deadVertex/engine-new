#if !defined(STB_RECT_PACK_IMPLEMENTATION)
#define STB_RECT_PACK_IMPLEMENTATION
#include <stb_rect_pack.h>
#endif

#if !defined(STB_TRUETYPE_IMPLEMENTATION)
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>
#endif

#define GLYPH_COUNT 128
struct Font
{
    stbtt_packedchar charData[GLYPH_COUNT];
    u32 texture;
    u32 material;
    u32 textureWidth;
    u32 textureHeight;
    float spaceAdvance;
    float lineSpacing;
    float height;
};

bool CreateFont(Font *fontAsset, const void *assetData, u32 assetDataLength,
    MemoryArena *tempArena, u32 bitmapWidth, u32 bitmapHeight, float fontSize,
    RenderCommandQueue *renderCommands, u32 textureIndex, u32 materialIndex)
{
    stbtt_pack_context packingContext;

    u8 *bitmap =
        (u8 *)MemoryArenaAllocate(tempArena, bitmapWidth * bitmapHeight);

    // TODO: Manage memory allocation
    stbtt_PackBegin(&packingContext, bitmap, bitmapWidth, bitmapHeight, 0, 1, NULL);
    stbtt_PackSetOversampling(&packingContext, 2, 2);
    stbtt_PackFontRange(&packingContext, (const u8 *)assetData, 0, fontSize, 32,
        95, fontAsset->charData + 32);
    stbtt_PackEnd(&packingContext);

    RenderCommandAllocateTexture *allocateTexture =
        RenderCommandAllocate(renderCommands, RenderCommandAllocateTexture, 0);
    allocateTexture->pixels = bitmap;
    allocateTexture->width = bitmapWidth;
    allocateTexture->height = bitmapHeight;
    allocateTexture->bytesPerPixel = 1;
    allocateTexture->index = textureIndex;
    allocateTexture->materialIndex = materialIndex;
    allocateTexture->useSRGB = false;
    allocateTexture->repeatMapping = false;
    allocateTexture->bytesPerChannel = 1;


    stbtt_fontinfo fontInfo;
    if (!stbtt_InitFont(&fontInfo, (const u8 *)assetData, 0))
    {
        Assert(!"stbtt_InitFont failed");
    }
    i32 ascent, descent, lineGap;
    stbtt_GetFontVMetrics(&fontInfo, &ascent, &descent, &lineGap);

    i32 advance, leftSideBearing;
    stbtt_GetCodepointHMetrics(&fontInfo, ' ', &advance,
            &leftSideBearing);
    float scale = stbtt_ScaleForMappingEmToPixels(&fontInfo, fontSize);

    fontAsset->texture = textureIndex;
    fontAsset->material = materialIndex;
    fontAsset->textureWidth = bitmapWidth;
    fontAsset->textureHeight = bitmapHeight;
    fontAsset->spaceAdvance = advance * scale;
    fontAsset->lineSpacing = (ascent + descent + lineGap) * scale;
    fontAsset->height = (ascent + descent) * scale;

    // Memory is never freed because this should use the temp allocator
    // NOTE: This frees all of the other bitmaps as well.
    //MemoryArenaFree(arena, bitmap);

    return true;
}

internal float AddCharacterToBuffer(
    TextBuffer *buffer, Font *font, i32 c, float x, float y)
{
    stbtt_aligned_quad q;
    stbtt_GetPackedQuad(font->charData, font->textureWidth, font->textureHeight,
        c, &x, &y, &q, 0);

    TextVertex topLeft, bottomLeft, bottomRight, topRight;

    float minX = q.x0;
    float minY = -q.y1;
    float maxX = q.x1;
    float maxY = -q.y0;

    topLeft.position = Vec2(minX, maxY);
    topLeft.texCoord = Vec2(q.s0, q.t0);

    bottomLeft.position = Vec2(minX, minY);
    bottomLeft.texCoord = Vec2(q.s0, q.t1);

    bottomRight.position = Vec2(maxX, minY);
    bottomRight.texCoord = Vec2(q.s1, q.t1);

    topRight.position = Vec2(maxX, maxY);
    topRight.texCoord = Vec2(q.s1, q.t0);

    auto vertex = buffer->vertices + buffer->vertexCount;
    *vertex++ = topRight;
    *vertex++ = topLeft;
    *vertex++ = bottomLeft;

    *vertex++ = bottomLeft;
    *vertex++ = bottomRight;
    *vertex++ = topRight;
    buffer->vertexCount += 6;

    return x;
}

internal float CalculateTextLength(Font *font, const char *str, u32 length = 0)
{
    float result = 0.0f;
    float current = 0.0f;
    u32 count = 0;
    while (*str)
    {
        if (length > 0 && count >= length)
        {
            break;
        }
        count++;

        if (*str >= 32)
        {
            float x = 0.0f;
            float y = 0.0f;
            stbtt_aligned_quad q;
            stbtt_GetPackedQuad(font->charData, font->textureWidth, font->textureHeight,
                    (i32)(*str), &x, &y, &q, 0);

            current += x;
        }
        else if (*str == ' ')
        {
            current += font->spaceAdvance;
        }
        else if (*str == '\n')
        {
            if (current > result)
            {
                result = current;
            }
            current = 0.0f;
        }
        //else if (*str == '\t')
        //{
            //current += (i32)font->glyphs[32].advance * 2;
        //}
        str++;
    }
    if (current > result)
    {
        result = current;
    }
    return result;
}

struct AddTextToBufferResult
{
    u32 firstVertex;
    u32 vertexCount;
};

internal AddTextToBufferResult AddTextToBuffer(
    TextBuffer *textBuffer, const char *text, Font *font, u32 length = 0)
{
    AddTextToBufferResult result = {};
    result.firstVertex = textBuffer->vertexCount;

    if (length == 0)
    {
        length = (u32)strlen(text);
    }

    float xOffset = 0.0f;
    float yOffset = 0.0f;
    const char *cursor = text;
    for (u32 i = 0; i < length; ++i)
    {
        if (*cursor > 32)
        {
            xOffset = AddCharacterToBuffer(
                textBuffer, font, *cursor, xOffset, yOffset);
        }
        else if (*cursor == ' ')
        {
            xOffset += font->spaceAdvance;
        }
        else if (*cursor == '\n')
        {
            yOffset += font->lineSpacing;
            xOffset = 0.0f;
        }
        cursor++;
    }
    result.vertexCount = textBuffer->vertexCount - result.firstVertex;

    return result;
}
