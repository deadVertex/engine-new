// Designed for a unit sized quad (-0.5, 0.5)
inline mat4 CalculateModelMatrix(vec2 p, float w, float h,
                                 vec2 anchorPoint)
{
    mat4 result = Translate(Vec3(p.x, p.y, 0.0)) *
                  Scale(Vec3(w, h, 0.0f)) *
                  Translate(Vec3(anchorPoint.x, anchorPoint.y, 0) * 0.5f);
    return result;
}

enum HorizontalAlignment
{
    HorizontalAlign_Left,
    HorizontalAlign_Center,
    HorizontalAlign_Right,
};

enum VerticalAlignment
{
    VerticalAlign_Top,
    VerticalAlign_Center,
    VerticalAlign_Bottom,
};

inline vec2 CalculateAnchorPoint(HorizontalAlignment horizontalAlignment,
    VerticalAlignment verticalAlignment)
{
    vec2 anchorPoint = Vec2(1, -1); // Top left alignment
    if (horizontalAlignment == HorizontalAlign_Center)
    {
        anchorPoint.x = 0.0f;
    }
    else if (horizontalAlignment == HorizontalAlign_Right)
    {
        anchorPoint.x = -1.0f;
    }

    if (verticalAlignment == VerticalAlign_Center)
    {
        anchorPoint.y = 0.0f;
    }
    else if (verticalAlignment == VerticalAlign_Bottom)
    {
        anchorPoint.y = 1.0f;
    }

    return anchorPoint;
}

inline void ui_PushQuad(RenderCommandQueue *renderCommands,
    mat4 orthoProjection, vec2 p, float w, float h, vec4 color,
    HorizontalAlignment horizontalAlignment = HorizontalAlign_Left,
    VerticalAlignment verticalAlignment = VerticalAlign_Top)
{
    RenderCommandDrawMeshUniforms *pushQuad = RenderCommandAllocate(
            renderCommands, RenderCommandDrawMeshUniforms, 0);

    vec2 anchorPoint =
        CalculateAnchorPoint(horizontalAlignment, verticalAlignment);

    pushQuad->model = CalculateModelMatrix(p, w, h, anchorPoint);
    pushQuad->view = Identity();
    pushQuad->projection = orthoProjection;
    pushQuad->shader = VULKAN_SHADER_COLOR_ONLY_NO_DEPTH;
    pushQuad->mesh = VULKAN_MESH_QUAD;
    PushUniformV4(pushQuad, UNIFORM_KEY_COLOR, color);
}

inline void ui_PushQuad(RenderCommandQueue *renderCommands,
    mat4 orthoProjection, vec2 p, float w, float h, u32 texture,
    HorizontalAlignment horizontalAlignment = HorizontalAlign_Left,
    VerticalAlignment verticalAlignment = VerticalAlign_Top)
{
    RenderCommandDrawMeshUniforms *pushQuad = RenderCommandAllocate(
            renderCommands, RenderCommandDrawMeshUniforms, 0);

    vec2 anchorPoint =
        CalculateAnchorPoint(horizontalAlignment, verticalAlignment);

    pushQuad->model = CalculateModelMatrix(p, w, h, anchorPoint);
    pushQuad->view = Identity();
    pushQuad->projection = orthoProjection;
    pushQuad->shader = VULKAN_SHADER_TEXTURE_ONLY;
    pushQuad->mesh = VULKAN_MESH_QUAD;
    PushUniformTexture(pushQuad, UNIFORM_KEY_TEX_SAMPLER, texture);
}

struct ui_PushStringArguments
{
    const char *text;
    u32 length;
    vec2 position;
    vec4 color;
    Font *font;
    HorizontalAlignment horizontalAlignment;
    VerticalAlignment verticalAlignment;
    bool drawShadow;
    vec4 shadowColor;
};

inline void ui_PushStringFillArguments(ui_PushStringArguments *args,
    Font *font, const char *text, vec2 position)
{
    args->font = font;
    args->text = text;
    args->position = position;
    args->color = Vec4(1);
    args->horizontalAlignment = HorizontalAlign_Left;
    args->verticalAlignment = VerticalAlign_Bottom;
    args->drawShadow = false;
    args->shadowColor = Vec4(0, 0, 0, 1);
}

inline void ui_PushString(RenderCommandQueue *renderCommands,
    TextBuffer *textBuffer, mat4 orthoProjection, ui_PushStringArguments *args)
{
    Font *font = args->font;
    vec2 p = args->position;

    auto result = AddTextToBuffer(textBuffer, args->text, font, args->length);

    if (args->horizontalAlignment != HorizontalAlign_Left)
    {
        float length = CalculateTextLength(font, args->text, args->length);
        if (args->horizontalAlignment == HorizontalAlign_Center)
        {
            p.x -= length * 0.5f;
        }
        else if (args->horizontalAlignment == HorizontalAlign_Right)
        {
            p.x -= length;
        }
        else
        {
            InvalidCodePath();
        }
    }

    if (args->verticalAlignment == VerticalAlign_Top)
    { 
        p.y -= font->height;
    }
    else if (args->verticalAlignment == VerticalAlign_Center)
    {
        p.y -= font->height * 0.5f;
    }

    if (args->drawShadow)
    {
        RenderCommandDrawVertexBuffer *drawText = RenderCommandAllocate(
                renderCommands, RenderCommandDrawVertexBuffer, 0);
        drawText->model = Translate(Vec3(p.x + 1.0f, p.y - 1.0f, 0));
        drawText->view = Identity();
        drawText->projection = orthoProjection;
        drawText->color = args->shadowColor;
        drawText->vbo = VULKAN_VERTEX_BUFFER_TEXT;
        drawText->shader = VULKAN_SHADER_TEXT;
        drawText->material = font->material;
        drawText->vertexCount = result.vertexCount;
        drawText->firstVertex = result.firstVertex;
    }

    RenderCommandDrawVertexBuffer *drawText = RenderCommandAllocate(
            renderCommands, RenderCommandDrawVertexBuffer, 0);
    drawText->model = Translate(Vec3(p.x, p.y, 0));
    drawText->view = Identity();
    drawText->projection = orthoProjection;
    drawText->color = args->color;
    drawText->vbo = VULKAN_VERTEX_BUFFER_TEXT;
    drawText->shader = VULKAN_SHADER_TEXT;
    drawText->material = font->material;
    drawText->vertexCount = result.vertexCount;
    drawText->firstVertex = result.firstVertex;
}

struct ButtonStyle
{
    Font *font;
    vec4 color;
    vec4 hoverColor;
    vec4 clickColor;
    vec4 textColor;

};

struct UiBuilder
{
    RenderCommandQueue *renderCommands;
    mat4 orthoProjection;
    TextBuffer *textBuffer;
    ButtonStyle *buttonStyle;
    InputSystem *inputSystem;
    float windowHeight;
};

// TODO: Support alignment
inline bool ui_PushButton(
    UiBuilder *builder, vec2 p, const char *text, bool *isHover = NULL)
{
    InputSystem *inputSystem = builder->inputSystem;
    RenderCommandQueue *renderCommands = builder->renderCommands;
    ButtonStyle *style = builder->buttonStyle;
    Font *font = style->font;


    b32 result = false;
    float width = CalculateTextLength(font, text);
    float height = font->height;
    vec4 color = style->color;

    vec2 mousePosition = Vec2((float)inputSystem->mouseX,
            builder->windowHeight - (float)inputSystem->mouseY);

    float horizontalPadding = 25.0f;
    float verticalPadding = 5.0f;

    // Top left alignment
    rect2 rect = {};
    rect.min.x = p.x;
    rect.min.y = p.y - height - verticalPadding * 2.0f;
    rect.max.x = p.x + width + horizontalPadding * 2.0f;
    rect.max.y = p.y;
    if (ContainsPoint(rect, mousePosition))
    {
        if (IsKeyDown(inputSystem, K_MOUSE_BUTTON_LEFT))
        {
            // Click
            color = style->clickColor;
        }
        else
        {
            if (WasKeyReleased(inputSystem, K_MOUSE_BUTTON_LEFT))
            {
                // Process click
                result = true;
            }
            else
            {
                // Hover
                color = style->hoverColor;
                if (isHover)
                    *isHover = true;
            }
        }
    }
    // if ( isSelected )
    //{
    // colour = clickColour;
    //}
    ui_PushQuad(renderCommands, builder->orthoProjection, p,
        width + 2.0f * horizontalPadding, height + 2.0f * verticalPadding,
        color);

    ui_PushStringArguments args = {};
    ui_PushStringFillArguments(&args, font, text, p);
    args.position += Vec2(horizontalPadding, -verticalPadding);
    args.color = style->textColor;
    args.horizontalAlignment = HorizontalAlign_Left;
    args.verticalAlignment = VerticalAlign_Top;

    ui_PushString(
        renderCommands, builder->textBuffer, builder->orthoProjection, &args);

    return result;
}
