enum
{
    PlayerAction_None = 0,
    PlayerAction_PrimaryFire = Bit(0),
    PlayerAction_SecondaryFire = Bit(1),
    PlayerAction_Interact = Bit(2),
    PlayerAction_Jump = Bit(3),
    PlayerAction_Sprint = Bit(4),
};

struct PlayerCommand
{
    vec2 angles;
    float forwardMove;
    float rightMove;
    float upMove;
    u8 actions;
    u8 activeWeaponSlot;
};

struct WeaponController
{
    u8 activeWeaponSlot;
};

enum
{
    GameEvent_FireWeapon,
};

struct GameEvent
{
    u32 type;
    vec3 fireWeaponPosition;
};

struct GameEventQueue
{
    GameEvent events[64];
    u32 length;
};

enum
{
    EntityDesc_Player,
    EntityDesc_Enemy,
    EntityDesc_PlayerSpawner,
    EntityDesc_EnemySpawner,
    EntityDesc_SkyLight,
    EntityDesc_SunLight,
    EntityDesc_PointLight,
    EntityDesc_MuzzleFlashLight,
    EntityDesc_Block,
    MAX_ENTITY_DESC,
};

global EntityDescription g_EntityDescriptions[MAX_ENTITY_DESC];
global const char *g_EntityDescriptionNames[MAX_ENTITY_DESC];

internal void PopulateEntityDescriptionsTable()
{
    // Player
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_Player;

        EntityDescription_AddComponentDefault(desc, PositionComponentTypeId);
        EntityDescription_AddComponentDefault(desc, VelocityComponentTypeId);
        EntityDescription_AddComponentDefault(desc, ViewAnglesComponentTypeId);
        EntityDescription_AddComponentDefault(desc, LastShotTimeComponentTypeId);
        EntityDescription_AddComponentDefault(desc, CollisionMaskComponentTypeId);
        EntityDescription_AddComponentDefault(desc, PreviousCommandComponentTypeId);
        EntityDescription_AddComponentDefault(desc, IsJumpingComponentTypeId);
        EntityDescription_AddComponentDefault(desc, WeaponControllerComponentTypeId);
        EntityDescription_AddComponentVec3(
            desc, ScaleComponentTypeId, Vec3(1.0f, 1.8f, 1.0f));
        EntityDescription_AddComponentVec3(
            desc, BoxHalfExtentsComponentTypeId, Vec3(0.5f));
        EntityDescription_AddComponentU8(desc, TeamComponentTypeId, 1);

        g_EntityDescriptionNames[EntityDesc_Player] = "Player";
    }

    // Player Spawner
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_PlayerSpawner;

        EntityDescription_AddComponentVec3(
                desc, PositionComponentTypeId, Vec3(0, 0.9, 6));
        EntityDescription_AddComponentU32(desc, EntitySpawnerComponentTypeId,
                EntityDesc_Player);

        g_EntityDescriptionNames[EntityDesc_PlayerSpawner] = "Player Spawner";
    }

    // Enemy Spawner
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_EnemySpawner;

        EntityDescription_AddComponentVec3(
            desc, PositionComponentTypeId, Vec3(-10, 0.9, -30));
        EntityDescription_AddComponentU32(
            desc, EntitySpawnerComponentTypeId, EntityDesc_Enemy);

        g_EntityDescriptionNames[EntityDesc_EnemySpawner] = "Enemy Spawner";
    }

    // Enemy
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_Enemy;

        EntityDescription_AddComponentDefault(desc, PositionComponentTypeId);
        EntityDescription_AddComponentDefault(desc, VelocityComponentTypeId);
        EntityDescription_AddComponentDefault(desc, ViewAnglesComponentTypeId);
        EntityDescription_AddComponentDefault(desc, CollisionMaskComponentTypeId);
        EntityDescription_AddComponentF32(desc, HealthComponentTypeId, 100.0f);
        EntityDescription_AddComponentVec3(desc, ScaleComponentTypeId, Vec3(1, 1.8, 1));
        EntityDescription_AddComponentU32(desc, RenderedMeshComponentTypeId, VULKAN_MESH_CUBE);
        EntityDescription_AddComponentDefault(desc, TransformComponentTypeId);
        EntityDescription_AddComponentDefault(desc, IsJumpingComponentTypeId);
        EntityDescription_AddComponentU32(
                desc, ShaderComponentTypeId, VULKAN_SHADER_COLOR_WIREFRAME);
        EntityDescription_AddComponentVec3(
                desc, BoxHalfExtentsComponentTypeId, Vec3(0.5f));
        EntityDescription_AddComponentDefault(desc, MoveTowardsTargetComponentTypeId);
        EntityDescription_AddComponentDefault(desc, TeamComponentTypeId);

        g_EntityDescriptionNames[EntityDesc_Enemy] = "Enemy";
    }

    // Sky Light
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_SkyLight;
        EntityDescription_AddComponentU8(
                desc, LightTypeComponentTypeId, LightType_Ambient);
        EntityDescription_AddComponentVec3(
                desc, LightColorComponentTypeId, Vec3(0.559, 0.668, 1.0));
        EntityDescription_AddComponentDefault(desc, PositionComponentTypeId);

        g_EntityDescriptionNames[EntityDesc_SkyLight] = "Sky Light";
    }

    // Sun Light
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_SunLight;
        EntityDescription_AddComponentU8(
                desc, LightTypeComponentTypeId, LightType_Directional);
        EntityDescription_AddComponentQuat(
                desc, RotationComponentTypeId, Quat(UVec3(1, 0, 0.3), PI * -0.3f));
        EntityDescription_AddComponentVec3(
                desc, LightColorComponentTypeId, Vec3(0.911, 0.747, 0.563));
        EntityDescription_AddComponentDefault(desc, PositionComponentTypeId);

        g_EntityDescriptionNames[EntityDesc_SunLight] = "Sun Light";
    }

    // Point Light
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_PointLight;
        EntityDescription_AddComponentU8(
                desc, LightTypeComponentTypeId, LightType_Point);
        EntityDescription_AddComponentVec3(
                desc, LightColorComponentTypeId, Vec3(0.4, 1.0, 0.6));
        EntityDescription_AddComponentDefault(desc, PositionComponentTypeId);
        EntityDescription_AddComponentVec3(
            desc, LightAttenuationComponentTypeId, Vec3(2.0f, 0.01f, 15.0f));

        g_EntityDescriptionNames[EntityDesc_PointLight] = "Point Light";
    }

    // Muzzle Flash Light
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_MuzzleFlashLight;
        EntityDescription_AddComponentU8(
                desc, LightTypeComponentTypeId, LightType_Point);
        EntityDescription_AddComponentVec3(
                desc, LightColorComponentTypeId, Vec3(1.0f, 0.7f, 0.0f));
        EntityDescription_AddComponentDefault(desc, PositionComponentTypeId);
        EntityDescription_AddComponentVec3(
            desc, LightAttenuationComponentTypeId, Vec3(4.0f, 0.01f, 5.0f));
        EntityDescription_AddComponentF32(desc, LifeTimeComponentTypeId, 0.06f);

        g_EntityDescriptionNames[EntityDesc_MuzzleFlashLight] = "Muzzle Flash Light";
    }

    // Block
    {
        EntityDescription *desc = g_EntityDescriptions + EntityDesc_Block;
        EntityDescription_AddComponentDefault(desc, PositionComponentTypeId);
        EntityDescription_AddComponentDefault(desc, ScaleComponentTypeId);
        EntityDescription_AddComponentVec3(
            desc, BoxHalfExtentsComponentTypeId, Vec3(0.5f));
        EntityDescription_AddComponentDefault(
            desc, CollisionMaskComponentTypeId);
        EntityDescription_AddComponentU32(desc, RenderedMeshComponentTypeId,
                VULKAN_MESH_CUBE);
        EntityDescription_AddComponentDefault(desc, ShadowCastingMeshComponentTypeId);
        EntityDescription_AddComponentDefault(desc, TransformComponentTypeId);
        EntityDescription_AddComponentU32(desc, ShaderComponentTypeId,
                VULKAN_SHADER_WORLD_TEX_COORD);
        EntityDescription_AddComponentU32(desc, MaterialComponentTypeId,
                VULKAN_MATERIAL_DEV_GRID512);

        g_EntityDescriptionNames[EntityDesc_Block] = "Block";
    }
}

inline vec2 ClampViewAngles(vec2 viewAngles)
{
    float tolerance = 0.9999f;
    viewAngles.x =
        Clamp(viewAngles.x, -PI * 0.5f * tolerance, PI * 0.5f * tolerance);
    if (viewAngles.y > PI)
    {
        viewAngles.y -= 2.0f * PI;
    }
    else if (viewAngles.y < -PI)
    {
        viewAngles.y += 2.0f * PI;
    }
    return viewAngles;
}

inline vec3 GetViewDirection(vec2 viewAngles)
{
    vec3 result = Normalize(Vec3(-Sin(viewAngles.y), Tan(viewAngles.x),
                -Cos(viewAngles.y)));
    return result;
}

internal PlayerCommand CreatePlayerCommand(InputSystem *inputSystem,
    PlayerCommand prevCmd, float windowWidth, float windowHeight,
    bool updateMouse)
{
    PlayerCommand result = {};
    if (IsKeyDown(inputSystem, K_W))
        result.forwardMove -= 1.0f;
    if (IsKeyDown(inputSystem, K_S))
        result.forwardMove += 1.0f;
    if (IsKeyDown(inputSystem, K_A))
        result.rightMove -= 1.0f;
    if (IsKeyDown(inputSystem, K_D))
        result.rightMove += 1.0f;

    if (updateMouse)
    {
        if (IsKeyDown(inputSystem, K_MOUSE_BUTTON_LEFT))
            result.actions |= PlayerAction_PrimaryFire;
        if (IsKeyDown(inputSystem, K_MOUSE_BUTTON_RIGHT))
            result.actions |= PlayerAction_SecondaryFire;
        if (IsKeyDown(inputSystem, K_F))
            result.actions |= PlayerAction_Interact;
    }

    if (IsKeyDown(inputSystem, K_SPACE))
        result.actions |= PlayerAction_Jump;
    if (IsKeyDown(inputSystem, K_LEFT_SHIFT))
        result.actions |= PlayerAction_Sprint;

    result.activeWeaponSlot = prevCmd.activeWeaponSlot;
    if (IsKeyDown(inputSystem, K_1))
        result.activeWeaponSlot = 0;
    if (IsKeyDown(inputSystem, K_2))
        result.activeWeaponSlot = 1;
    if (IsKeyDown(inputSystem, K_3))
        result.activeWeaponSlot = 2;
    if (IsKeyDown(inputSystem, K_4))
        result.activeWeaponSlot = 3;
    if (IsKeyDown(inputSystem, K_5))
        result.activeWeaponSlot = 4;


    result.angles = prevCmd.angles;
    float sensitivity = 0.5f;
    float mouseX = 0.0f;
    float mouseY = 0.0f;
    if (updateMouse)
    {
        mouseX = (float)(inputSystem->mouseX - inputSystem->prevMouseX) /
                 windowWidth;
        mouseY = (float)(inputSystem->mouseY - inputSystem->prevMouseY) /
                 windowHeight;
    }
    vec2 delta = Vec2(mouseY * -2.0f * PI, -2.0f * PI * mouseX);
    delta *= sensitivity;
    result.angles = ClampViewAngles(result.angles + delta);

    return result;
}

inline vec3 CalculateBulletVelocity(vec3 acceleration, vec3 velocity,
                                    vec3 gravity, float dt)
{
    acceleration += gravity;
    float airFriction = 0.05f;
    acceleration += (velocity * airFriction);
    vec3 result = acceleration * dt + velocity;
    return result;
}

global vec3 g_Gravity = Vec3(0, -10, 0);

internal void CreateBulletEntity(ecs_EntitySystem *entitySystem, vec3 position,
    vec3 direction, float acceleration, float damage, EntityId owner, float dt)
{
    EntityId entity;
    ecs_CreateEntities(entitySystem, &entity, 1);

    ecs_AddComponent(entitySystem, BulletOwnerComponentTypeId, &entity, 1,
        &owner, sizeof(owner));
    ecs_AddComponent(entitySystem, PositionComponentTypeId, &entity, 1,
                     &position, sizeof(position));
    ecs_AddComponent(entitySystem, DamageComponentTypeId, &entity, 1, &damage,
                     sizeof(damage));

    vec3 velocity = CalculateBulletVelocity(direction * acceleration, Vec3(0),
                                            g_Gravity, dt);
    ecs_AddComponent(entitySystem, VelocityComponentTypeId, &entity, 1,
                     &velocity, sizeof(velocity));

    float lifeTime = 5.0f;
    ecs_AddComponent(entitySystem, LifeTimeComponentTypeId, &entity, 1,
        &lifeTime, sizeof(lifeTime));

    ecs_AddComponent(entitySystem, TracerRendererComponentTypeId, &entity, 1);
}

// TODO: Rather do filtering when viewing the data rather than creating
global EntityId g_PhysicsDebuggerFilterEntity = NULL_ENTITY;
global bool g_PhysicsDebuggerEnabled = true;
internal EntityId AabbSweepWorld(ecs_EntitySystem *entitySystem,
    MemoryArena *arena, vec3 aabbHalfExtents, vec3 start, vec3 end,
    EntityId ignoreEntity, RaycastResult *raycastResult)
{
    vec3 *halfExtents = NULL;
    vec3 *positions = NULL;
    vec3 *scales = NULL;
    u32 *collisionMasks = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 4,
        GetComponent(BoxHalfExtentsComponentTypeId, halfExtents),
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(ScaleComponentTypeId, scales),
        GetComponent(CollisionMaskComponentTypeId, collisionMasks));

    float tmin = 1.0f;
    EntityId hitEntity = NULL_ENTITY;
    for (u32 i = 0; i < result.length; ++i)
    {
        if (collisionMasks[i] == 0)
        {
            continue;
        }

        if (ignoreEntity == result.entities[i])
        {
            continue;
        }

        AabbShape box;
        box.min = positions[i] - halfExtents[i] * scales[i];
        box.max = positions[i] + halfExtents[i] * scales[i];

        //if (g_PhysicsDebuggerFilterEntity == result.entities[i])
        if (g_PhysicsDebuggerEnabled)
        {
            PhysicsFunctionCall call = {};
            call.type = PhysicsCall_AabbSweep;
            call.aabbSweep.halfExtents = aabbHalfExtents;
            call.aabbSweep.aabb = box;
            call.aabbSweep.start = start;
            call.aabbSweep.end = end;
            PhysicsDebugger_AddCall(g_PhysicsDebugger, call);
        }
        RaycastResult tempResult = {};
        if (AabbSweepTest(aabbHalfExtents, box, start, end, &tempResult))
        {
            if (tempResult.t < tmin)
            {
                *raycastResult = tempResult;
                hitEntity = result.entities[i];
                tmin = tempResult.t;
            }
        }
    }

    ecs_ReleaseQueryResult(result);

    return hitEntity;
}

internal TriangleMeshShape CreateTriangleCollisionMeshForWorld(
    ecs_EntitySystem *entitySystem, MemoryArena *tempArena,
    MemoryArena *storageArena)
{
    // For world geometry
    vec3 *halfExtents = NULL;
    vec3 *positions = NULL;
    vec3 *scales = NULL;

    auto queryResult = ecs_ExecuteQuery(entitySystem, tempArena, 3,
        GetComponent(BoxHalfExtentsComponentTypeId, halfExtents),
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(ScaleComponentTypeId, scales));

    u32 verticesPerAabb = 36;
    u32 trianglesPerAabb = verticesPerAabb / 3;

    u32 vertexCount = verticesPerAabb * queryResult.length;
    vec3 *vertices = AllocateArray(tempArena, vec3, vertexCount);

    u32 vertexIdx = 0;

    // clang-format off
    vec3 boxVertices[] = {
        // Top
        {-0.5f, 0.5f, -0.5f},
        {0.5f, 0.5f, -0.5f},
        {0.5f, 0.5f, 0.5f},
        {-0.5f, 0.5f, 0.5f},

        // Bottom
        {-0.5f, -0.5f, -0.5f},
        {0.5f, -0.5f, -0.5f},
        {0.5f, -0.5f, 0.5f},
        {-0.5f, -0.5f, 0.5f},

        // Back
        {-0.5f, 0.5f, -0.5f},
        {0.5f, 0.5f, -0.5f},
        {0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f, -0.5f},

        // Front
        {-0.5f, 0.5f, 0.5f},
        {0.5f, 0.5f, 0.5f},
        {0.5f, -0.5f, 0.5f},
        {-0.5f, -0.5f, 0.5f},

        // Left
        {-0.5f, 0.5f, -0.5f},
        {-0.5f, -0.5f, -0.5f},
        {-0.5f, -0.5f, 0.5f},
        {-0.5f, 0.5f, 0.5f},

        // Right
        {0.5f, 0.5f, -0.5f},
        {0.5f, -0.5f, -0.5f},
        {0.5f, -0.5f, 0.5f},
        {0.5f, 0.5f, 0.5f},
    };

    u32 boxIndices[] = {
        2, 1, 0,
        0, 3, 2,

        4, 5, 6,
        6, 7, 4,

        8, 9, 10,
        10, 11, 8,

        14, 13, 12,
        12, 15, 14,

        16, 17, 18,
        18, 19, 16,

        22, 21, 20,
        20, 23, 22
    };
    // clang-format on

    // Generate vertices for static geometry
    for (u32 i = 0; i < queryResult.length; ++i)
    {
        vec3 aabbMin = positions[i] - halfExtents[i] * scales[i];
        vec3 aabbMax = positions[i] + halfExtents[i] * scales[i];

        for (u32 triangleIdx = 0; triangleIdx < trianglesPerAabb; ++triangleIdx)
        {
            u32 idxA = boxIndices[triangleIdx*3];
            u32 idxB = boxIndices[triangleIdx*3 + 2];
            u32 idxC = boxIndices[triangleIdx*3 + 1];

            AabbShape aabb;
            aabb.min = positions[i] - halfExtents[i] * scales[i];
            aabb.max = positions[i] + halfExtents[i] * scales[i];

            vec3 center = (aabb.min + aabb.max) * 0.5f;
            vec3 halfDim = (aabb.max - aabb.min) * 0.5f;
            vec3 scale = halfDim * 2.0f;

            vec3 a = boxVertices[idxA] * scale + center;
            vec3 b = boxVertices[idxB] * scale + center;
            vec3 c = boxVertices[idxC] * scale + center;

            vertices[vertexIdx++] = a;
            vertices[vertexIdx++] = b;
            vertices[vertexIdx++] = c;
        }
    }

    TriangleMeshShape mesh =
        CreateTriangleMesh(vertices, vertexCount, storageArena);

    // NOTE: This also frees vertices and indices array
    ecs_ReleaseQueryResult(queryResult);

    return mesh;
}


global TriangleMeshShape g_WorldCollisionMesh;
global Bvh g_WorldCollisionMeshBvh;

internal bool AabbSweepWorldTriangleMesh(MemoryArena *arena, vec3 aabbHalfExtents,
    vec3 start, vec3 end, RaycastResult *raycastResult)
{
    Debug_BeginTimedBlock(AabbSweepWorldTriangleMesh);

    if (g_PhysicsDebuggerEnabled)
    {
        PhysicsFunctionCall call = {};
        call.type = PhysicsCall_AabbSweepTriangleMesh;
        call.aabbSweepTriangleMesh.halfExtents = aabbHalfExtents;
        call.aabbSweepTriangleMesh.start = start;
        call.aabbSweepTriangleMesh.end = end;
        call.aabbSweepTriangleMesh.triangleMesh = g_WorldCollisionMesh;
        call.aabbSweepTriangleMesh.bvh = g_WorldCollisionMeshBvh;
        PhysicsDebugger_AddCall(g_PhysicsDebugger, call);
    }
    if (AabbSweepTriangleMesh(g_WorldCollisionMesh, g_WorldCollisionMeshBvh,
            arena, aabbHalfExtents, start, end, raycastResult))
    {
    }

    Debug_EndTimedBlock(AabbSweepWorldTriangleMesh);

    return raycastResult->isValid;
}

// TODO: Parameter for collision masks
internal EntityId RaycastWorld(ecs_EntitySystem *entitySystem,
    MemoryArena *arena, vec3 start, vec3 end, EntityId ignoreEntity,
    RaycastResult *raycastResult)
{
    vec3 *halfExtents = NULL;
    vec3 *positions = NULL;
    vec3 *scales = NULL;
    u32 *collisionMasks = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 4,
        GetComponent(BoxHalfExtentsComponentTypeId, halfExtents),
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(ScaleComponentTypeId, scales),
        GetComponent(CollisionMaskComponentTypeId, collisionMasks));

    float tmin = 1.0f;
    EntityId hitEntity = NULL_ENTITY;
    for (u32 i = 0; i < result.length; ++i)
    {
        if (collisionMasks[i] == 0)
        {
            continue;
        }

        if (result.entities[i] == ignoreEntity)
        {
            continue;
        }

        AabbShape box;
        box.min = positions[i] - halfExtents[i] * scales[i];
        box.max = positions[i] + halfExtents[i] * scales[i];

        {
            PhysicsFunctionCall call = {};
            call.type = PhysicsCall_RayAabb;
            call.rayAabb.aabb = box;
            call.rayAabb.start = start;
            call.rayAabb.end = end;
            PhysicsDebugger_AddCall(g_PhysicsDebugger, call);
        }
        RaycastResult tempResult = {};
        if (RayAabbTest(box, start, end, &tempResult))
        {

            if (tempResult.t < tmin)
            {
                *raycastResult = tempResult;
                hitEntity = result.entities[i];
                tmin = tempResult.t;
            }
        }
    }

    ecs_ReleaseQueryResult(result);

    return hitEntity;
}

internal bool GroundTrace(
    CollisionWorld *collisionWorld, MemoryArena *arena, vec3 *currentPosition)
{
    float epsilon = 0.00001f;
    float radius = 0.5f;
    vec3 delta = Vec3(0, 0.9f - radius * 0.99f, 0);
    vec3 start = *currentPosition;
    vec3 end = start - delta;

    RaycastResult raycastResult =
        SphereSweepWorld(collisionWorld, arena, start, end, radius);
    if (raycastResult.isValid)
    {
        *currentPosition += (1.0f - raycastResult.t - epsilon) * delta;
        return true;
    }

    return false;
}

struct PushBackEntry
{
    float penetrationDepth;
    TriangleShape triangle;
};

// Want to sort elements in descending order
inline i32 ComparePushBackEntries(const void *p0, const void *p1)
{
    PushBackEntry *a = (PushBackEntry *)p0;
    PushBackEntry *b = (PushBackEntry *)p1;

    if (a->penetrationDepth > b->penetrationDepth)
        return -1;
    if (a->penetrationDepth == b->penetrationDepth)
        return 0;

    return 1;
}


internal vec3 PerformPushBack(TriangleMeshShape mesh, Bvh tree,
        MemoryArena *arena, vec3 center, float radius)
{
    Assert(tree.root);

    BvhNode *stack[0x1000];
    u32 stackSize = 1;
    stack[0] = tree.root;

    PushBackEntry entries[0x20];
    u32 entryCount = 0;

    while (stackSize > 0)
    {
        BvhNode *node = stack[--stackSize];

        if (SphereAabbIntersect(center, radius, node->aabb))
        {
            if (node->left == NULL && node->right == NULL)
            {
                //Debug_DrawAabb(node->aabb.min, node->aabb.max, Vec3(0.4));
                // leaf node
                for (u32 idx = 0; idx < node->triangleIndexCount; ++idx)
                {
                    u32 triangleIndex =
                        tree.triangleIndices[node->triangleIndexOffset + idx];
                    Assert(triangleIndex < mesh.triangleCount);
                    ConvexHullShape triangle = mesh.triangles[triangleIndex];

                    TriangleShape t = {};
                    t.a = triangle.vertices[0];
                    t.b = triangle.vertices[1];
                    t.c = triangle.vertices[2];
                    vec3 closestPoint = TriangleClosestPoint(t, center);
                    vec3 d = center - closestPoint;
                    float mag = Length(d);
                    if (mag - radius <= 0.0f)
                    {
                        vec3 normal = triangle.faces[0].normal;
                        vec3 direction = Normalize(d);

                        if (Dot(normal, direction) > 0.0f)
                        {
                            PushBackEntry entry = {};
                            entry.penetrationDepth = radius - mag;
                            entry.triangle = t;
                            Assert(entryCount < ArrayCount(entries));
                            entries[entryCount++] = entry;

                        }

                        DrawConvexHullShape(triangle, Vec3(0.2, 0.8, 0.4),
                                DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES);
                    }
                }
            }
            else
            {
                if (node->left != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->left;
                }
                if (node->right != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->right;
                }
            }
        }
    }

    qsort(entries, entryCount, sizeof(PushBackEntry), ComparePushBackEntries);

    for (u32 i = 0; i < entryCount; ++i)
    {
        vec3 closestPoint = TriangleClosestPoint(entries[i].triangle, center);
        vec3 d = closestPoint - center;
        float mag = Length(d);
        vec3 direction = Normalize(d);

        if (radius - mag > 0.0f)
        {
            // Calculate support for sphere in the direction of
            // penetration
            vec3 support = center + direction * radius;
            Debug_DrawPoint(support, Vec3(0, 1, 1));
            // Calculate minimum translation vector to resolve
            // the collision
            vec3 translation = closestPoint - support;
            center += translation;
        }
    }

    return center;
}

#if 1
internal vec3 PerformCapsulePushBack(TriangleMeshShape mesh, Bvh tree,
        MemoryArena *arena, vec3 center, float radius)
{
    Assert(tree.root);

    BvhNode *stack[0x1000];
    u32 stackSize = 1;
    stack[0] = tree.root;

    PushBackEntry entries[0x20];
    u32 entryCount = 0;

    // halfPlayerHeight - radius
    float centerOffset = 0.8f - radius;
    vec3 p = center - Vec3(0, centerOffset, 0);
    vec3 q = center + Vec3(0, centerOffset, 0);

    AabbShape a0 = {};
    a0.min = p - Vec3(radius);
    a0.max = p + Vec3(radius);

    AabbShape a1 = {};
    a1.min = q - Vec3(radius);
    a1.max = q + Vec3(radius);

    float radiusSq = radius * radius;

    AabbShape capsuleAabb = AabbMerge(a0, a1);
    //Debug_DrawAabb(capsuleAabb.min, capsuleAabb.max, Vec3(0.8f, 0.3f, 0.5f));

    //DrawCapsule(p, q, radius, Vec3(0.4f, 0.8f, 0.6f));

    while (stackSize > 0)
    {
        BvhNode *node = stack[--stackSize];

        if (AabbVsAabbIntersect(capsuleAabb, node->aabb))
        {
            if (node->left == NULL && node->right == NULL)
            {
                //Debug_DrawAabb(node->aabb.min, node->aabb.max, Vec3(0.4));
                // leaf node
                for (u32 idx = 0; idx < node->triangleIndexCount; ++idx)
                {
                    u32 triangleIndex =
                        tree.triangleIndices[node->triangleIndexOffset + idx];
                    Assert(triangleIndex < mesh.triangleCount);
                    ConvexHullShape triangle = mesh.triangles[triangleIndex];

                    TriangleShape t = {};
                    t.a = triangle.vertices[0];
                    t.b = triangle.vertices[1];
                    t.c = triangle.vertices[2];

                    // This wouldn't be necessary if we used bottom up tree
                    // construction and only allowed one triangle per BVH node

                    // Add safety margin to prevent issues when triangle is
                    // parallel to basis axis
                    AabbShape triangleAabb = {};
                    triangleAabb.min = Min(t.c, Min(t.b, t.a)) - Vec3(0.05f);
                    triangleAabb.max = Max(t.c, Max(t.b, t.a)) + Vec3(0.05f);

                    //DrawAabb(triangleAabb, Vec3(0, 0.8f, 0.8f));

                    if (AabbVsAabbIntersect(capsuleAabb, triangleAabb))
                    {

                        ClosestPointResult closestPointResult =
                            LineSegmentVsTriangleClosestPoint(
                                p, q, t.a, t.b, t.c);

                        if (closestPointResult.distanceSq - radiusSq <= 0.0f)
                        {
                            vec3 normal = triangle.faces[0].normal;
                            //vec3 c =
                                //(triangle.vertices[0] + triangle.vertices[1] +
                                    //triangle.vertices[2]) *
                                //(1.0f / 3.0f);
                            //Debug_DrawLine(c, c + normal * 0.8f, Vec3(1, 0, 0));
                            vec3 direction = Normalize(
                                closestPointResult.c0 - closestPointResult.c1);

                            if (Dot(normal, direction) > 0.0f)
                            {
                                float distance = SquareRoot(
                                    closestPointResult.distanceSq - radiusSq);

                                PushBackEntry entry = {};
                                entry.penetrationDepth = SquareRoot(
                                    radiusSq - closestPointResult.distanceSq);
                                entry.triangle = t;
                                Assert(entryCount < ArrayCount(entries));
                                entries[entryCount++] = entry;
                            }

                            DrawConvexHullShape(triangle, Vec3(0.2, 0.8, 0.4),
                                DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES);
                        }
                    }
                }
            }
            else
            {
                if (node->left != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->left;
                }
                if (node->right != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->right;
                }
            }
        }
    }

    qsort(entries, entryCount, sizeof(PushBackEntry), ComparePushBackEntries);

    for (u32 i = 0; i < entryCount; ++i)
    {
        TriangleShape t = entries[i].triangle;

        p = center - Vec3(0, centerOffset, 0);
        q = center + Vec3(0, centerOffset, 0);

        ClosestPointResult closestPointResult =
            LineSegmentVsTriangleClosestPoint(p, q, t.a, t.b, t.c);

        vec3 d = closestPointResult.c1 - closestPointResult.c0;
        float mag = Length(d);
        vec3 direction = Normalize(d);

        if (radius - mag > 0.0f)
        {
            //Debug_DrawPoint(closestPointResult.c0, Vec3(0, 0, 1));
            //Debug_DrawPoint(closestPointResult.c1, Vec3(1, 0, 0));


            // Calculate support for sphere in the direction of penetration
            vec3 support = closestPointResult.c0 + direction * radius;

            // Calculate minimum translation vector to resolve
            // the collision
            vec3 translation = closestPointResult.c1 - support;
            center += translation;
        }
    }

    return center;
}
#endif

internal vec3 PerformPushBack(ecs_EntitySystem *entitySystem,
    MemoryArena *arena, vec3 center, float radius, EntityId ignoreEntity)
{
    vec3 *halfExtents = NULL;
    vec3 *positions = NULL;
    vec3 *scales = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 3,
        GetComponent(BoxHalfExtentsComponentTypeId, halfExtents),
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(ScaleComponentTypeId, scales));

    float tmin = 1.0f;
    EntityId hitEntity = NULL_ENTITY;
    for (u32 i = 0; i < result.length; ++i)
    {
        if (result.entities[i] == ignoreEntity)
        {
            continue;
        }

        AabbShape box;
        box.min = positions[i] - halfExtents[i] * scales[i];
        box.max = positions[i] + halfExtents[i] * scales[i];

        //DrawSphere(center, radius, Vec3(1, 1, 0));

        {
            SphereShape sphere = {};
            sphere.center = center;
            sphere.radius = radius;

            PhysicsFunctionCall call = {};
            call.type = PhysicsCall_SphereAabbIntersect;
            call.sphereAabbIntersect.sphere = sphere;
            call.sphereAabbIntersect.aabb = box;
            PhysicsDebugger_AddCall(g_PhysicsDebugger, call);
        }

        if (SphereAabbIntersect(center, radius, box))
        {
            {
                PhysicsFunctionCall call = {};
                call.type = PhysicsCall_AabbClosestPoint;
                call.aabbClosestPoint.aabb = box;
                call.aabbClosestPoint.p = center;
                PhysicsDebugger_AddCall(g_PhysicsDebugger, call);
            }

            vec3 contactPoint = AabbClosestPoint(box, center);

            vec3 direction = Normalize(contactPoint - center);
            // Calculate support for sphere in the direction of penetration
            vec3 support = center + direction * radius;
            //Debug_DrawPoint(support, Vec3(0, 1, 1));
            // Calculate minimum translation vector to resolve the collision
            vec3 translation = contactPoint - support;
            center += translation;
        }
    }

    ecs_ReleaseQueryResult(result);

    return center;
}

internal void MovePlayer(ecs_EntitySystem *entitySystem,
    CollisionWorld *collisionWorld, MemoryArena *arena, EntityId entity,
    PlayerCommand cmd, PlayerCommand prevCmd, float dt)
{
    Debug_BeginTimedBlock(MovePlayer);
    float groundSpeed = 50.0f;
    float groundSprintSpeed = groundSpeed * 2.5f;
    float airSpeed = 3.0f;
    float groundFriction = 9.0f;
    float airFriction = 0.5f;

    vec3 currentPosition;
    ecs_GetComponent(entitySystem, PositionComponentTypeId, &entity, 1,
        &currentPosition, sizeof(currentPosition));
    vec3 currentVelocity;
    ecs_GetComponent(entitySystem, VelocityComponentTypeId, &entity, 1,
        &currentVelocity, sizeof(currentVelocity));
    vec3 halfExtents;
    ecs_GetComponent(entitySystem, BoxHalfExtentsComponentTypeId, &entity, 1,
        &halfExtents, sizeof(halfExtents));
    b32 isJumping;
    ecs_GetComponent(entitySystem, IsJumpingComponentTypeId, &entity, 1,
        &isJumping, sizeof(isJumping));

    bool onGround = GroundTrace(collisionWorld, arena, &currentPosition);

    Debug_Printf("OnGround: %s\n", onGround ? "true" : "false");

    vec3 playerGravity = g_Gravity * 4.0f;

    mat4 rotation = RotateY(cmd.angles.y);
    vec4 v = rotation * Vec4(0, 0, 1, 0);
    vec3 forward = Vec3(v.x, v.y, v.z);

    v = rotation * Vec4(1, 0, 0, 0);
    vec3 right = Vec3(v.x, v.y, v.z);

    vec3 targetDir = forward * cmd.forwardMove + right * cmd.rightMove;
    targetDir = Normalize(targetDir);

    float speed = airSpeed;
    if (onGround)
    {
        speed = (cmd.actions & PlayerAction_Sprint) ? groundSprintSpeed
                                                    : groundSpeed;
    }
    vec3 newVelocity = targetDir * speed * dt;
    newVelocity += playerGravity * dt * ( onGround ? 0.0f : 1.0f );

    // Clear out vertical velocity when on ground, otherwise jump height is inconsistent
    currentVelocity.y *= onGround ? 0.0f : 1.0f;

    float friction = onGround ? groundFriction : airFriction;
    vec3 targetVelocity = currentVelocity;
    targetVelocity -= currentVelocity * friction * dt;
    targetVelocity += newVelocity;

    if (onGround)
    {
        if ((cmd.actions & PlayerAction_Jump) && !isJumping)
        {
            targetVelocity.y += 12.0f;
            isJumping = true;
        }

    }
    if (!(cmd.actions & PlayerAction_Jump))
    {
        isJumping = false;
    }

    vec3 targetPosition = currentPosition;
    if (IsZero(targetVelocity))
    {
        targetVelocity = Vec3(0);
    }
    //else
    {
        vec3 displacement = targetVelocity * dt;
        targetPosition = currentPosition + displacement;

#if 0
        bool solutionFound = false;
        for (u32 i = 0; i < 4; ++i)
        {
            RaycastResult raycastResult = {};
            bool hasCollided = AabbSweepWorldTriangleMesh(arena, halfExtents,
                currentPosition, targetPosition, &raycastResult);
            if (hasCollided)
            {
                targetVelocity =
                    targetVelocity -
                    raycastResult.hitNormal *
                        Dot(raycastResult.hitNormal, targetVelocity);
                displacement = targetVelocity * dt;
                targetPosition = currentPosition + displacement;
            }
            else
            {
                solutionFound = true;
                break;
            }
        }
#endif

        //if (!solutionFound)
        //{
            //targetVelocity = Vec3(0);
            //targetPosition = currentPosition;
        //}
    }

    targetPosition = PerformCapsulePushBack(g_WorldCollisionMesh, g_WorldCollisionMeshBvh,
            arena, targetPosition, 0.5f);
    //targetPosition = PerformPushBack(g_WorldCollisionMesh, g_WorldCollisionMeshBvh,
            //arena, targetPosition, 0.5f);
    //targetPosition =
        //PerformPushBack(entitySystem, arena, targetPosition, 0.5f, entity);

    ecs_SetComponent(entitySystem, PositionComponentTypeId, &entity, 1,
        &targetPosition, sizeof(targetPosition));
    ecs_SetComponent(entitySystem, VelocityComponentTypeId, &entity, 1,
        &targetVelocity, sizeof(targetVelocity));
    ecs_SetComponent(entitySystem, ViewAnglesComponentTypeId, &entity, 1,
        &cmd.angles, sizeof(cmd.angles));
    ecs_SetComponent(entitySystem, IsJumpingComponentTypeId, &entity, 1,
        &isJumping, sizeof(isJumping));


    Debug_EndTimedBlock(MovePlayer);
}

global vec3 g_EyeOffset = Vec3(0, 0.85f, 0);

enum FireModeEnum
{
    FireMode_SingleFire,
    FireMode_Automatic,
};

enum FireActionEnum
{
    FireAction_SingleBullet,
    FireAction_MultiBullet,
};

struct WeaponProperties
{
    FireModeEnum fireMode;
    FireActionEnum fireAction;

    float minTimeBetweenShots;
    float verticalRecoilBase;
    float verticalRecoilMultiplier;
    float horizontalRecoilBase;
    float horizontalRecoilMultiplier;
    float bulletAcceleration;
    float damagePerBullet;
};

enum
{
    Weapon_Pistol,
    Weapon_Shotgun,
    Weapon_SMG,
    Weapon_AR,
    Weapon_Sniper,
    MAX_WEAPON_PROPERTIES,
};

WeaponProperties g_WeaponProperties[MAX_WEAPON_PROPERTIES];

// TODO:
// - Weapon spread
// - Bullet damage

internal void PopulateWeaponPropertiesTable()
{
    { // Pistol
        WeaponProperties props = {};
        props.fireMode = FireMode_SingleFire;
        props.fireAction = FireAction_SingleBullet;
        props.minTimeBetweenShots = 0.1f;
        props.verticalRecoilBase = 0.04f;
        props.verticalRecoilMultiplier = 0.04f;
        props.horizontalRecoilBase = 0.0f;
        props.horizontalRecoilMultiplier = 0.02f;
        props.bulletAcceleration = 60000.0f;
        props.damagePerBullet = 30.0f;
        g_WeaponProperties[Weapon_Pistol] = props;
    }

    { // Shotgun
        WeaponProperties props = {};
        props.fireMode = FireMode_SingleFire;
        props.fireAction = FireAction_MultiBullet;
        props.minTimeBetweenShots = 0.4f;
        props.verticalRecoilBase = 0.04f;
        props.verticalRecoilMultiplier = 0.04f;
        props.horizontalRecoilBase = 0.0f;
        props.horizontalRecoilMultiplier = 0.02f;
        props.bulletAcceleration = 40000.0f;
        props.damagePerBullet = 12.0f;
        g_WeaponProperties[Weapon_Shotgun] = props;
    }

    { // SMG
        WeaponProperties props = {};
        props.fireMode = FireMode_Automatic;
        props.fireAction = FireAction_SingleBullet;
        props.minTimeBetweenShots = 0.1f;
        props.verticalRecoilBase = 0.01f;
        props.verticalRecoilMultiplier = 0.03f;
        props.horizontalRecoilBase = 0.0f;
        props.horizontalRecoilMultiplier = 0.03f;
        props.bulletAcceleration = 60000.0f;
        props.damagePerBullet = 24.0f;
        g_WeaponProperties[Weapon_SMG] = props;
    }
}

internal void UpdatePlayer(ecs_EntitySystem *entitySystem,
    CollisionWorld *collisionWorld, MemoryArena *arena, EntityId entity,
    PlayerCommand cmd, float dt, float currentTime, RandomNumberGenerator *rng,
    GameEventQueue *eventQueue)
{
    Debug_BeginTimedBlock(UpdatePlayer);

    PlayerCommand prevCmd = {};
    ecs_GetComponent(entitySystem, PreviousCommandComponentTypeId, &entity, 1,
            &prevCmd, sizeof(prevCmd));

    MovePlayer(entitySystem, collisionWorld, arena, entity, cmd, prevCmd, dt);

    u8 weaponSlot = (u8)Min((u32)cmd.activeWeaponSlot, Weapon_SMG);
    WeaponProperties weapon = g_WeaponProperties[weaponSlot];

    // TODO: Support other fire modes
    if (cmd.actions & PlayerAction_PrimaryFire)
    {
        bool canFire = true;
        if (weapon.fireMode == FireMode_SingleFire)
        {
            canFire = !(prevCmd.actions & PlayerAction_PrimaryFire);
        }

        if (canFire)
        {
            float lastShotTime;
            ecs_GetComponent(entitySystem, LastShotTimeComponentTypeId, &entity,
                1, &lastShotTime, sizeof(lastShotTime));

            vec3 velocity;
            ecs_GetComponent(entitySystem, VelocityComponentTypeId,
                    &entity, 1, &velocity, sizeof(velocity));

            float maxVelocity = 10.0f; // FIXME: This should be a constant defined somewhere else!
            float t = Min(Length(velocity) / maxVelocity, 1.0f);
            // TODO: Weapon specific spread values
            float spreadMulAtMinVelocity = 0.01f;
            float spreadMulAtMaxVelocity = 0.4f;
            float spread = Lerp(spreadMulAtMinVelocity, spreadMulAtMaxVelocity, t);

            if (currentTime - lastShotTime > weapon.minTimeBetweenShots)
            {
                vec3 position;
                ecs_GetComponent(entitySystem, PositionComponentTypeId, &entity,
                    1, &position, sizeof(position));
                vec2 viewAngles = ClampViewAngles(cmd.angles);

                vec3 bulletPosition = position + g_EyeOffset;

                if (weapon.fireAction == FireAction_SingleBullet)
                {
                    vec2 offsetViewAngles = viewAngles;
                    offsetViewAngles.x += RandomFloat(rng) * spread;
                    offsetViewAngles.y += RandomFloat(rng) * spread;
                    vec3 bulletDirection = GetViewDirection(viewAngles);
                    CreateBulletEntity(entitySystem, bulletPosition,
                        bulletDirection, weapon.bulletAcceleration,
                        weapon.damagePerBullet, entity, dt);
                }
                else if (weapon.fireAction == FireAction_MultiBullet)
                {
                    u32 bulletCount = 8;

                    float pelletVerticalSpread = 0.04f;
                    float pelletHorizontalSpread = 0.03f;
                    for (u32 i = 0; i < bulletCount; ++i)
                    {
                        vec2 offsetViewAngles = viewAngles;
                        offsetViewAngles.x +=
                            RandomFloat(rng) * pelletVerticalSpread * (1.0f + spread);
                        offsetViewAngles.y += RandomFloat(rng) *
                                              pelletHorizontalSpread * (1.0f + spread);
                        vec3 bulletDirection =
                            GetViewDirection(offsetViewAngles);
                        CreateBulletEntity(entitySystem, bulletPosition,
                            bulletDirection, weapon.bulletAcceleration,
                            weapon.damagePerBullet, entity, dt);
                    }
                }
                else
                {
                    InvalidCodePath();
                }
                ecs_SetComponent(entitySystem, LastShotTimeComponentTypeId,
                    &entity, 1, &currentTime, sizeof(currentTime));

                // TODO: Better recoil model
                viewAngles.x +=
                    weapon.verticalRecoilBase +
                    RandomFloat(rng) * weapon.verticalRecoilMultiplier;
                viewAngles.y +=
                    weapon.horizontalRecoilBase +
                    RandomBinomial(rng) * weapon.horizontalRecoilMultiplier;
                ecs_SetComponent(entitySystem, ViewAnglesComponentTypeId,
                    &entity, 1, &viewAngles, sizeof(viewAngles));

                // Store updated view angles for recoil
                cmd.angles = viewAngles;

                vec3 forwardDirection = GetViewDirection(viewAngles);

                GameEvent event = {};
                event.type = GameEvent_FireWeapon;
                event.fireWeaponPosition = bulletPosition + forwardDirection * 1.0f;

                Assert(eventQueue->length < ArrayCount(eventQueue->events));
                eventQueue->events[eventQueue->length++] = event;
            }
        }
    }

    WeaponController weaponController = {};
    weaponController.activeWeaponSlot = weaponSlot;

    ecs_SetComponent(entitySystem, WeaponControllerComponentTypeId, &entity, 1,
        &weaponController, sizeof(weaponController));
    ecs_SetComponent(entitySystem, PreviousCommandComponentTypeId, &entity, 1,
        &cmd, sizeof(cmd));

    Debug_EndTimedBlock(UpdatePlayer);
}

global bool g_DebugDrawBulletPaths = false;

internal void UpdateBulletPositionSystem(ecs_EntitySystem *entitySystem, MemoryArena *arena,
        float dt)
{
    vec3 *velocities;
    vec3 *positions;
    EntityId *owners;
    float *damageValues;

    auto result =
            ecs_ExecuteQuery(entitySystem, arena, 4,
                             GetComponent(VelocityComponentTypeId, velocities),
                             GetComponent(PositionComponentTypeId, positions),
                             GetComponent(BulletOwnerComponentTypeId, owners),
                             GetComponent(DamageComponentTypeId, damageValues));

    if (result.length > 0)
    {
        EntityId *entitiesToRemove = AllocateArray(arena, EntityId, result.length);
        u32 entitiesToRemoveCount = 0;

        for (u32 i = 0; i < result.length; ++i)
        {
            vec3 currentPosition = positions[i];
            vec3 targetPosition = positions[i] + velocities[i] * dt;

            RaycastResult raycastResult = {};
            EntityId hitEntity = RaycastWorld(entitySystem, arena,
                currentPosition, targetPosition, owners[i], &raycastResult);

            if (hitEntity != NULL_ENTITY)
            {
                positions[i] = raycastResult.hitPoint;
                velocities[i] = Vec3(0);
                entitiesToRemove[entitiesToRemoveCount++] = result.entities[i];
                Debug_DrawPoint(raycastResult.hitPoint, Vec3(1, 0, 0), 0.2f, 5.0f);
                targetPosition = raycastResult.hitPoint;

                bool hasHealth;
                ecs_HasComponent(entitySystem, HealthComponentTypeId,
                    &hitEntity, 1, &hasHealth);
                if (hasHealth)
                {
                    float currentHealth;
                    ecs_GetComponent(entitySystem, HealthComponentTypeId,
                        &hitEntity, 1, &currentHealth, sizeof(currentHealth));
                    currentHealth -= damageValues[i];
                    ecs_SetComponent(entitySystem, HealthComponentTypeId,
                            &hitEntity, 1, &currentHealth, sizeof(currentHealth));
                }
            }
            else
            {
                positions[i] = targetPosition;
                velocities[i] = CalculateBulletVelocity(
                    Vec3(0), velocities[i], g_Gravity, dt);

            }

            if (g_DebugDrawBulletPaths)
            {
                Debug_DrawLine(
                    currentPosition, targetPosition, Vec3(0, 1, 1), 5.0f);
            }
        }

        // Update position and velocity even if bullet is going to be destroyed
        ecs_SetComponent(entitySystem, PositionComponentTypeId, result.entities,
                         result.length, positions, sizeof(positions[0]));
        ecs_SetComponent(entitySystem, VelocityComponentTypeId, result.entities,
                         result.length, velocities, sizeof(velocities[0]));

        ecs_DeleteEntities(entitySystem, entitiesToRemove, entitiesToRemoveCount);

        MemoryArenaFree(arena, entitiesToRemove);
    }

    ecs_ReleaseQueryResult(result);
}

// TODO: Don't remove entities here!
internal void UpdateLifeTimeSystem(
    ecs_EntitySystem *entitySystem, MemoryArena *arena, float dt)
{
    float *lifeTimes = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 1,
        GetComponent(LifeTimeComponentTypeId, lifeTimes));

    if (result.length > 0)
    {
        EntityId *entitiesToRemove = AllocateArray(arena, EntityId, result.length);
        u32 entitiesToRemoveCount = 0;

        for (u32 i = 0; i < result.length; ++i)
        {
            lifeTimes[i] -= dt;

            if (lifeTimes[i] < 0.0f)
            {
                entitiesToRemove[entitiesToRemoveCount++] = result.entities[i];
            }
        }

        ecs_SetComponent(entitySystem, LifeTimeComponentTypeId, result.entities,
                result.length, lifeTimes, sizeof(lifeTimes[0]));

        ecs_DeleteEntities(entitySystem, entitiesToRemove, entitiesToRemoveCount);

        MemoryArenaFree(arena, entitiesToRemove);
    }
}

internal void UpdateHealthComponentSystem(
    ecs_EntitySystem *entitySystem, MemoryArena *arena)
{
    float *health = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 1,
        GetComponent(HealthComponentTypeId, health));

    if (result.length > 0)
    {
        EntityId *entitiesToRemove = AllocateArray(arena, EntityId, result.length);
        u32 entitiesToRemoveCount = 0;

        for (u32 i = 0; i < result.length; ++i)
        {
            if (health[i] <= 0.0f)
            {
                entitiesToRemove[entitiesToRemoveCount++] = result.entities[i];
            }
        }

        ecs_DeleteEntities(entitySystem, entitiesToRemove, entitiesToRemoveCount);

        MemoryArenaFree(arena, entitiesToRemove);
    }

    ecs_ReleaseQueryResult(result);
}

internal EntityId AquireTarget(ecs_EntitySystem *entitySystem,
    MemoryArena *arena, vec3 currentPosition, EntityId entity, u8 enemyTeam)
{
    EntityId newTarget = NULL_ENTITY;

    vec3 *positions = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 2,
            IsEqual(TeamComponentTypeId, enemyTeam),
            GetComponent(PositionComponentTypeId, positions));

    for (u32 i = 0; i < result.length; ++i)
    {
        // TODO: Should have some system of prioritizing targets

        // Check for line of sight on potential target
        RaycastResult raycastResult = {};
        EntityId hitEntity = RaycastWorld(entitySystem, arena, currentPosition,
            positions[i], entity, &raycastResult);

        if (hitEntity == result.entities[i])
        {
            newTarget = hitEntity;
        }
    }

    ecs_ReleaseQueryResult(result);

    return newTarget;
}

internal void UpdateAquireTargetSystem(ecs_EntitySystem *entitySystem, MemoryArena *arena)
{
    u8 *teams = NULL;
    vec3 *positions = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 3,
            HasComponent(MoveTowardsTargetComponentTypeId),
            GetComponent(TeamComponentTypeId, teams),
            GetComponent(PositionComponentTypeId, positions));

    for (u32 i = 0; i < result.length; ++i)
    {
        u8 enemyTeam =
            (teams[i] == 0) ? 1 : 0; // TODO: Replace with an enum or something

        EntityId newTarget = AquireTarget(
            entitySystem, arena, positions[i], result.entities[i], enemyTeam);

        if (newTarget != NULL_ENTITY)
        {
            ecs_SetComponent(entitySystem, MoveTowardsTargetComponentTypeId,
                &result.entities[i], 1, &newTarget, sizeof(newTarget));
        }
    }

    ecs_ReleaseQueryResult(result);
}

internal void UpdateMoveTowardsTargetSystem(ecs_EntitySystem *entitySystem,
    CollisionWorld *collisionWorld, MemoryArena *arena, float dt)
{
    EntityId *targetEntities = NULL;
    vec3 *positions = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 2,
            GetComponent(MoveTowardsTargetComponentTypeId, targetEntities),
            GetComponent(PositionComponentTypeId, positions));

    PlayerCommand nullCmd = {};
    for (u32 i = 0; i < result.length; ++i)
    {
        if (targetEntities[i] != NULL_ENTITY)
        {
            vec3 targetPosition;
            ecs_GetComponent(entitySystem, PositionComponentTypeId,
                &targetEntities[i], 1, &targetPosition, sizeof(targetPosition));

            vec3 delta = targetPosition - positions[i];
            // TODO: Scale movement speed based on length of delta
            //delta.y = 0.0f; // Ignore height for now
            vec3 direction = Normalize(delta);
            vec3 forward = Vec3(0, 0, 1);
            float dot = Dot(direction, forward);
            float yRotation = Atan2(direction.x, direction.z);

            PlayerCommand cmd = {};
            cmd.forwardMove = 1.0f;
            cmd.angles.y = yRotation;
            //cmd.actions = PlayerAction_Jump;
            g_PhysicsDebuggerEnabled = true;
            MovePlayer(entitySystem, collisionWorld, arena, result.entities[i], cmd, nullCmd, dt);
            g_PhysicsDebuggerEnabled = false;
        }
    }

    ecs_ReleaseQueryResult(result);
}

internal EntityId SpawnPlayerSystem(ecs_EntitySystem *entitySystem, MemoryArena *arena)
{
    EntityId player = NULL_ENTITY;
    vec3 *positions = NULL;
    u32 entityToSpawn = EntityDesc_Player;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 2,
            IsEqual(EntitySpawnerComponentTypeId, entityToSpawn),
            GetComponent(PositionComponentTypeId, positions));

    if (result.length > 0)
    {
        player = CreateEntityFromDescription(
            entitySystem, g_EntityDescriptions + EntityDesc_Player);
        ecs_SetComponent(entitySystem, PositionComponentTypeId, &player, 1,
            &positions[0], sizeof(positions[0]));
    }

    ecs_ReleaseQueryResult(result);

    return player;
}

internal void SpawnEnemies(ecs_EntitySystem *entitySystem, MemoryArena *arena)
{
    vec3 *positions = NULL;
    u32 entityToSpawn = EntityDesc_Enemy;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 2,
            IsEqual(EntitySpawnerComponentTypeId, entityToSpawn),
            GetComponent(PositionComponentTypeId, positions));

    for (u32 i = 0; i < result.length; ++i)
    {
        EntityId entity = CreateEntityFromDescription(
            entitySystem, g_EntityDescriptions + EntityDesc_Enemy);
        ecs_SetComponent(entitySystem, PositionComponentTypeId, &entity, 1,
            &positions[i], sizeof(positions[0]));
    }

    ecs_ReleaseQueryResult(result);
}
