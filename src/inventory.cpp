struct Inventory
{
    u32 width;
    u32 height;
    u32 *slots;
};

inline void InitializeInventory(Inventory *inventory, MemoryArena *arena)
{
    inventory->width = 6;
    inventory->height = 4;

    u32 slotCount = inventory->width * inventory->height;

    inventory->slots = AllocateArray(arena, u32, slotCount);
}

