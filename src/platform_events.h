#pragma once

#include "utils.h"

enum
{
    PlatformEvent_KeyPress,
    PlatformEvent_KeyRelease,
    PlatformEvent_KeyRepeat,
    PlatformEvent_MouseMotion,
    PlatformEvent_MouseScroll,
    PlatformEvent_PacketReceived,
    PlatformEvent_CharacterPressed,
    PlatformEvent_GameCodeReload,
    PlatformEvent_WindowFocusChanged,
    PlatformEvent_FrameBufferResized,
};

struct PlatformEvent
{
    u8 type;
    union {
        u8 key;
        u32 character;
        float mouseScroll;
        b32 windowHasFocus;

        struct
        {
            u32 width;
            u32 height;
        } frameBuffer;

        struct
        {
            double x;
            double y;
        } mouseMotion;

        struct
        {
            u8 *packetData;
            u16 packetLength;
            u32 addressId;
        } packetReceived;
    };
};

#define MAX_PLATFORM_EVENTS 1024

struct PlatformEventQueue
{
    PlatformEvent events[MAX_PLATFORM_EVENTS];
    u32 count;
};

inline void PushPlatformEvent(PlatformEventQueue *queue, PlatformEvent event)
{
    Assert(queue->count < ArrayCount(queue->events));
    queue->events[queue->count++] = event;
}
