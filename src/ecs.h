#pragma once

typedef u32 EntityId;
#define NULL_ENTITY 0

// NOTE: Try keep this generic so that components could be stored in either a
// linear array (N<200), hash table or possibly even a radix tree.
struct ecs_ComponentTable
{
    u32 capacity;
    u32 length;
    u32 objectSize;
    EntityId *keys;
    void *values;
    void *defaultValue;
};

struct ecs_EntitySystem
{
    u32 entityCapacity;
    u32 entityCount;
    EntityId *entities;
    EntityId nextEntityId;

    u32 componentCapacity;
    u32 componentCount;
    u32 *componentKeys;
    ecs_ComponentTable *componentValues;
};

enum
{
    ecs_QueryParameterType_GetComponent,
    ecs_QueryParameterType_HasComponent,
    ecs_QueryParameterType_IsEqual,
};

struct ecs_QueryParameter
{
    u32 queryType;
    u32 componentTypeId;
    u32 objectSize;
    void **output;
    void *value;
};

struct ecs_Query
{
    u32 count;
    ecs_QueryParameter *parameters;
};

struct ecs_QueryResult
{
    bool isValid;
    MemoryArena *arena;
    EntityId *entities;
    u32 length;
};

internal void ecs_Initialize(ecs_EntitySystem *entitySystem, u32 entityCapacity,
                             u32 componentCapacity, MemoryArena *arena);

// TODO: Default values for a component
internal void ecs_RegisterComponent(ecs_EntitySystem *entitySystem,
    MemoryArena *arena, u32 componentTypeId, u32 objectSize, u32 capacity,
    void *defaultValue = NULL);

internal void ecs_CreateEntities(ecs_EntitySystem *entitySystem,
                                 EntityId *entities, u32 count);

internal void ecs_DeleteEntities(ecs_EntitySystem *entitySystem,
                                 EntityId *entities, u32 count);

internal void ecs_DeleteAllEntities(ecs_EntitySystem *entitySystem);

internal void ecs_AddComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, void *values = NULL,
                               u32 objectSize = 0);

internal void ecs_RemoveComponent(ecs_EntitySystem *entitySystem,
                                  u32 componentTypeId, EntityId *entities,
                                  u32 count);

internal void ecs_SetComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, void *values, u32 objectSize);

internal void ecs_GetComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, void *output, u32 objectSize);

internal void ecs_HasComponent(ecs_EntitySystem *entitySystem,
                               u32 componentTypeId, EntityId *entities,
                               u32 count, bool *output);

internal ecs_QueryResult ecs_ExecuteQueryObject(ecs_EntitySystem *entitySystem,
                                                MemoryArena *arena,
                                                ecs_Query *query);

internal void ecs_ReleaseQueryResult(ecs_QueryResult result);

inline ecs_QueryParameter ecs_CreateQueryParameter(u32 queryType,
                                                   u32 componentTypeId,
                                                   u32 objectSize,
                                                   void **output,
                                                   void *value = NULL)
{
    ecs_QueryParameter result = {};
    result.queryType = queryType;
    result.componentTypeId = componentTypeId;
    result.objectSize = objectSize;
    result.output = output;
    result.value = value;
    return result;
}

#include <cstdarg>

#define MakeTypeId(COMPONENT) \
    COMPONENT##TypeId

#define GetComponent(COMPONENT_TYPE_ID, OUTPUT)                                \
    ecs_CreateQueryParameter(ecs_QueryParameterType_GetComponent,              \
                             COMPONENT_TYPE_ID, sizeof(OUTPUT[0]),             \
                             (void **)&OUTPUT, NULL)

#define HasComponent(COMPONENT_TYPE_ID)                                        \
    ecs_CreateQueryParameter(ecs_QueryParameterType_HasComponent,              \
                             COMPONENT_TYPE_ID, 0, NULL, NULL)

#define IsEqual(COMPONENT_TYPE_ID, VALUE)                                      \
    ecs_CreateQueryParameter(ecs_QueryParameterType_IsEqual,                   \
                             COMPONENT_TYPE_ID, sizeof(VALUE), NULL, &VALUE)

#define ecs_ExecuteQuery(ENTITY_SYSTEM, ARENA, ARGC, ...) \
    ecs_ExecuteQuery_(ENTITY_SYSTEM, ARENA, ARGC, __VA_ARGS__)

inline ecs_QueryResult ecs_ExecuteQuery_(ecs_EntitySystem *entitySystem,
                                         MemoryArena *arena, u32 paramCount,
                                         ...)
{
    va_list args;
    va_start(args, paramCount);
    ecs_QueryParameter params[64];
    Assert(paramCount < ArrayCount(params));
    for (u32 i = 0; i < paramCount; ++i)
    {
        params[i] = va_arg(args, ecs_QueryParameter);
    }
    va_end(args);

    ecs_Query query = {};
    query.count = paramCount;
    query.parameters = params;

    return ecs_ExecuteQueryObject(entitySystem, arena, &query);
}
