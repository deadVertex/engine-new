/*
 * ===== TODO =====
 * EDITOR
 * - Save map on command
 * - Load map on command
 * - Allow switching from XZ to Y axis while translating
 *
 * GAME
 * - Player death and respawning
 * - Fix player controller triangle mesh collision
 * - Add padding to triangles in triangle mesh to prevent rays travelling
 *   through the boundaries between two triangles
 *
 * ENGINE
 * - Drop down console
 * - CVars
 * - Rename asset enums so they are no longer prefixed with VULKAN
 */
#include "game.h"

#include "logging.h"
#include "math_lib.h"

#include "render_commands.h"
#include "input.h"

// TODO: Replace with asset names
#include "vulkan_renderer.h"

#define USE_TERRAIN 1
#define TERRAIN_GRID_DIM 128

#include "string_utils.cpp"
#include "camera.cpp"
#include "font.cpp"
#include "ecs.cpp"
#include "debug.cpp"
#include "collision_detection.cpp"
#include "data_type.cpp"
#include "entity_description.cpp"
#include "mesh.cpp"
#include "heightmap.cpp"
#include "heightmap_collision_shape.cpp"
#include "collision_world.cpp"
#include "player.cpp"
#include "ui.cpp"
#include "asset.cpp"
#include "entity_systems.cpp"
#include "physics_engine.cpp"
#include "terrain.cpp"
#include "inventory.cpp"

u32 log_activeChannels = U32_MAX;

GameMemory *gDebugGameMemory;

enum
{
    GLOBAL_MODE_GAME,
    GLOBAL_MODE_EDITOR,
    GLOBAL_MODE_MENU,
    GLOBAL_MODE_PHYSICS_DEBUGGER,
    GLOBAL_MODE_PHYSICS_TEST_BENCH,
    GLOBAL_MODE_COUNT,
};

float g_Fov = 60.0f;
float g_NearClip = 0.01f;
float g_FarClip = 1000.0f;


enum
{
    EDITOR_MODE_SELECTING,
    EDITOR_MODE_TRANSLATING,
    EDITOR_MODE_SCALING,
    EDITOR_MODE_SPAWNING,
};

struct ClosestPointSceneShape
{
    u32 type;
    vec3 position;
    union
    {
        float radius;
        vec3 boxHalfExtents;
        struct
        {
            vec3 vertexA;
            vec3 vertexB;
            vec3 vertexC;
        };
        struct
        {
            vec3 offsetP2;
            float capsuleRadius;
        };
    };
};

struct ClosestPointSceneState
{
    u32 mode;
    ClosestPointSceneShape shapes[8];
    u32 shapeCount;

    i32 selectedShape;
    vec3 shapeBegin;
    vec3 translationBegin;
    vec3 translationEnd;
    b32 isVerticalTranslation;
    Plane translationPlane;
};

struct RaycastSceneState
{
    vec3 start; 
    vec3 end;
};

struct GameState
{
    b32 isInitialized;
    MemoryArena testArena;
    MemoryArena transArena;
    InputSystem inputSystem;
    Camera camera;

    u32 globalMode;
    bool isDebugCameraActive;
    Camera debugCamera;

    Font testFont;
    Font smallFont;
    Font debugFont;

    AssetCatalog assetCatalog;

    ecs_EntitySystem entitySystem;

    TextVertex textBufferVertices[VULKAN_MAX_TEXT_VERTICES];

    float currentTime;

    float dtSamples[60];
    u32 dtSamplesCount;
    u32 dtSamplesTail;

    EntityId playerEntity;
    PlayerCommand prevPlayerCommand;

    DebugSystem debugSystem;

    RandomNumberGenerator rng;

    PhysicsDebugger physicsDebugger;

    u32 selectedPhysicsCall;


    EntityId selectedEntity;
    u32 editorMode;

    bool isVerticalTranslation;
    Plane translationPlane;
    vec3 translationBegin;
    vec3 translationEnd;
    vec3 entityBegin;
    vec3 entityBeginScale;
    float scalingBegin;
    float scalingEnd;
    vec3 scaleMask;
    u32 entityDescToSpawn;

    vec2 prevViewAngles;
    vec2 viewModelRotations;
    float smoothedForwardMove;

    GameEventQueue eventQueue;

    float vmCurrentPoseZ;
    float vmCurrentPoseXRot;
    float vmStartT;

    bool showInventory;

    MemoryArena worldCollisionMeshArena;
    TriangleMeshShape worldCollisionMesh;
    Bvh worldBvh;

    u32 selectedSatIteration;

    u32 selectedPhysicsTestBenchScene;

    ClosestPointSceneState closestPointState;
    ClosestPointSceneState intersectionState;
    ClosestPointSceneState capsuleTriangleState;
    RaycastSceneState sphereTriangleSweepState;

    PhysicsEngine physicsEngine;
    u32 maxIterations;

    MemoryArena terrainArena;
    HeightMap terrainHeightMap;
    HeightMapCollisionMesh terrainCollisionMesh;
    vec3 terrainTranslation;
    float terrainScaleXZ;
    float terrainScaleY;

    CollisionWorld collisionWorld;

    Inventory playerInventory;
    bool isDragging;
    u32 slotBeingDragged;
};

#include "editor.cpp"
#include "physics_test_bench.cpp"

global u32 g_ItemIconTextures[3];

internal void PopulateItemIconTexturesTable()
{
    g_ItemIconTextures[0] = 0; // Ignored
    g_ItemIconTextures[1] = VULKAN_MATERIAL_ICON_WOOD;
    g_ItemIconTextures[2] = VULKAN_MATERIAL_ICON_STONE;
}

internal void PopulateComponentNamesTable()
{
    g_ComponentNames[PositionComponentTypeId] = "Position";
    g_ComponentNames[RotationComponentTypeId] = "Rotation";
    g_ComponentNames[ScaleComponentTypeId] = "Scale";
    g_ComponentNames[VelocityComponentTypeId] = "Velocity";
    g_ComponentNames[ViewAnglesComponentTypeId] = "ViewAngles";
    g_ComponentNames[RenderedMeshComponentTypeId] = "RenderedMesh";
    g_ComponentNames[TransformComponentTypeId] = "Transform";
    g_ComponentNames[ShaderComponentTypeId] = "Shader";
    g_ComponentNames[MaterialComponentTypeId] = "Material";
    g_ComponentNames[BulletOwnerComponentTypeId] = "BulletOwner";
    g_ComponentNames[LastShotTimeComponentTypeId] = "LastShotTime";
    g_ComponentNames[LifeTimeComponentTypeId] = "LifeTime";
    g_ComponentNames[HealthComponentTypeId] = "Health";
    g_ComponentNames[BoxHalfExtentsComponentTypeId] = "BoxHalfExtents";
    g_ComponentNames[CollisionMaskComponentTypeId] = "CollisionMask";
    g_ComponentNames[MoveTowardsTargetComponentTypeId] = "MoveTowardsTarget";
    g_ComponentNames[TeamComponentTypeId] = "Team";
    g_ComponentNames[EntitySpawnerComponentTypeId] = "EntitySpawner";
    g_ComponentNames[PreviousCommandComponentTypeId] = "PreviousPlayerCommand";
    g_ComponentNames[LightTypeComponentTypeId] = "LightType";
    g_ComponentNames[LightColorComponentTypeId] = "LightColor";
    g_ComponentNames[LightAttenuationComponentTypeId] = "LightAttenuation";
    g_ComponentNames[IsJumpingComponentTypeId] = "IsJumping";
    g_ComponentNames[DamageComponentTypeId] = "Damage";
    g_ComponentNames[WeaponControllerComponentTypeId] = "WeaponController";
    g_ComponentNames[TracerRendererComponentTypeId] = "TracerRenderer";
    g_ComponentNames[ShadowCastingMeshComponentTypeId] = "ShadowCastingMesh";
}

internal void PopulateComponentDataTypesTable()
{
    g_ComponentDataTypes[PositionComponentTypeId] = DataType_Vec3;
    g_ComponentDataTypes[RotationComponentTypeId] = DataType_Quat;
    g_ComponentDataTypes[ScaleComponentTypeId] = DataType_Vec3;
    g_ComponentDataTypes[VelocityComponentTypeId] = DataType_Vec3;
    g_ComponentDataTypes[ViewAnglesComponentTypeId] = DataType_Vec2;
    g_ComponentDataTypes[RenderedMeshComponentTypeId] = DataType_U32;
    g_ComponentDataTypes[TransformComponentTypeId] = DataType_None; // TODO:
    g_ComponentDataTypes[ShaderComponentTypeId] = DataType_U32;
    g_ComponentDataTypes[MaterialComponentTypeId] = DataType_U32;
    g_ComponentDataTypes[BulletOwnerComponentTypeId] = DataType_U32;
    g_ComponentDataTypes[LastShotTimeComponentTypeId] = DataType_F32;
    g_ComponentDataTypes[LifeTimeComponentTypeId] = DataType_F32;
    g_ComponentDataTypes[HealthComponentTypeId] = DataType_F32;
    g_ComponentDataTypes[BoxHalfExtentsComponentTypeId] = DataType_Vec3;
    g_ComponentDataTypes[CollisionMaskComponentTypeId] = DataType_U32;
    g_ComponentDataTypes[MoveTowardsTargetComponentTypeId] = DataType_U32;
    g_ComponentDataTypes[TeamComponentTypeId] = DataType_U8;
    g_ComponentDataTypes[EntitySpawnerComponentTypeId] = DataType_U32;
    g_ComponentDataTypes[PreviousCommandComponentTypeId] = DataType_None;
    g_ComponentDataTypes[LightTypeComponentTypeId] = DataType_U8;
    g_ComponentDataTypes[LightColorComponentTypeId] = DataType_Vec3;
    g_ComponentDataTypes[LightAttenuationComponentTypeId] = DataType_Vec3;
    g_ComponentDataTypes[IsJumpingComponentTypeId] = DataType_None;
    g_ComponentDataTypes[DamageComponentTypeId] = DataType_F32;
    g_ComponentDataTypes[WeaponControllerComponentTypeId] = DataType_None;
    g_ComponentDataTypes[TracerRendererComponentTypeId] = DataType_None;
    g_ComponentDataTypes[ShadowCastingMeshComponentTypeId] = DataType_None;
}

internal void GenerateAssets(GameState *gameState, RenderCommandQueue *renderCommands)
{
    // Generate terrain
    gameState->terrainArena.used = 0; // Clear terrain memory arena

    gameState->terrainHeightMap = AllocateHeightMap(&gameState->terrainArena, 1024, 1024);

    gameState->terrainTranslation = Vec3(-50, -10, 50) * 10.0f;
    gameState->terrainScaleXZ = 100.0f * 10.0f;
    gameState->terrainScaleY = 0.4f;

    GenerateTerrainHeightMap(&gameState->terrainHeightMap);
    CreateHeightMapTexture(
        gameState->terrainHeightMap, &gameState->transArena, renderCommands);
    CreateNormalMapTexture(
        gameState->terrainHeightMap, &gameState->transArena, renderCommands);

    gameState->terrainCollisionMesh =
        BuildHeightMapCollisionMesh(gameState->terrainHeightMap,
            &gameState->terrainArena, gameState->terrainTranslation,
            gameState->terrainScaleXZ, gameState->terrainScaleY);
}

internal void LoadAssets(
    GameState *gameState, GameMemory *memory, RenderCommandQueue *renderCommands)
{

    CreateHardcodedMeshes(renderCommands, &gameState->transArena);

    {
        ReadFileResult file = memory->readEntireFile("../content/Inconsolata-Regular.ttf");
        Assert(file.memory);
        if (!CreateFont(&gameState->testFont, file.memory, file.size,
                &gameState->transArena, 512, 512, 48, renderCommands,
                VULKAN_TEXTURE_TEMP_GLYPH_SHEET,
                VULKAN_MATERIAL_TEMP_GLYPH_SHEET))
        {
            InvalidCodePath();
        }

        if (!CreateFont(&gameState->smallFont, file.memory, file.size,
                &gameState->transArena, 256, 256, 24, renderCommands,
                VULKAN_TEXTURE_TEMP_GLYPH_SHEET2,
                VULKAN_MATERIAL_TEMP_GLYPH_SHEET2))
        {
            InvalidCodePath();
        }

        if (!CreateFont(&gameState->debugFont, file.memory, file.size,
                &gameState->transArena, 256, 256, 18, renderCommands,
                VULKAN_TEXTURE_TEMP_GLYPH_SHEET3,
                VULKAN_MATERIAL_TEMP_GLYPH_SHEET3))
        {
            InvalidCodePath();
        }
        memory->freeFile(file);
    }

    CreateAssetCatalog(&gameState->assetCatalog);

    LoadAssetsFromCatalog(&gameState->assetCatalog, memory,
        &gameState->transArena, renderCommands);

    GenerateAssets(gameState, renderCommands);
}

internal void CreateWallEntity(ecs_EntitySystem *entitySystem, vec3 position,
    vec3 dimensions)
{
    EntityId entity;
    ecs_CreateEntities(entitySystem, &entity, 1);

    ecs_AddComponent(entitySystem, PositionComponentTypeId, &entity, 1,
        &position, sizeof(position));

    ecs_AddComponent(entitySystem, ScaleComponentTypeId, &entity, 1,
        &dimensions, sizeof(dimensions));

    vec3 halfExtents = Vec3(0.5f);
    ecs_AddComponent(entitySystem, BoxHalfExtentsComponentTypeId, &entity,
        1, &halfExtents, sizeof(halfExtents));

    ecs_AddComponent(entitySystem, CollisionMaskComponentTypeId, &entity, 1);

    u32 mesh = VULKAN_MESH_CUBE;
    ecs_AddComponent(entitySystem, RenderedMeshComponentTypeId, &entity, 1,
        &mesh, sizeof(mesh));

    ecs_AddComponent(entitySystem, TransformComponentTypeId, &entity, 1);

    u32 shader = VULKAN_SHADER_WORLD_TEX_COORD;
    ecs_AddComponent(entitySystem, ShaderComponentTypeId, &entity, 1, &shader,
        sizeof(shader));

    u32 material = VULKAN_MATERIAL_DEV_GRID512;
    ecs_AddComponent(entitySystem, MaterialComponentTypeId, &entity, 1, &material,
        sizeof(material));
}

internal void CreateEntities(ecs_EntitySystem *entitySystem)
{
    {
        EntityId entity;
        ecs_CreateEntities(entitySystem, &entity, 1);

        vec3 position = Vec3(4, 1, 2);
        ecs_AddComponent(entitySystem, PositionComponentTypeId, &entity, 1,
            &position, sizeof(position));

        ecs_AddComponent(entitySystem, ScaleComponentTypeId, &entity, 1);

        u32 mesh = VULKAN_MESH_CUBE;
        ecs_AddComponent(entitySystem, RenderedMeshComponentTypeId, &entity, 1,
            &mesh, sizeof(mesh));
        ecs_AddComponent(entitySystem, ShadowCastingMeshComponentTypeId, &entity, 1);

        ecs_AddComponent(entitySystem, TransformComponentTypeId, &entity, 1);

        u32 shader = VULKAN_SHADER_TEXTURE_DIFFUSE_LIGHTING;
        ecs_AddComponent(entitySystem, ShaderComponentTypeId, &entity, 1,
            &shader, sizeof(shader));

        u32 material = VULKAN_MATERIAL_DEV_GRID512;
        ecs_AddComponent(entitySystem, MaterialComponentTypeId, &entity, 1, &material,
                sizeof(material));

        vec3 halfExtents = Vec3(0.5f);
        ecs_AddComponent(entitySystem, BoxHalfExtentsComponentTypeId, &entity, 1,
                &halfExtents, sizeof(halfExtents));
        ecs_AddComponent(entitySystem, CollisionMaskComponentTypeId, &entity, 1);
    }

    for (u32 i = 0; i < 3; ++i)
    {
        EntityId entity;
        ecs_CreateEntities(entitySystem, &entity, 1);

        vec3 position = Vec3(2.0f * i, 1, 6);
        ecs_AddComponent(entitySystem, PositionComponentTypeId, &entity, 1,
            &position, sizeof(position));

        ecs_AddComponent(entitySystem, ScaleComponentTypeId, &entity, 1);

        u32 mesh = VULKAN_MESH_ICOSAHEDRON + i;
        ecs_AddComponent(entitySystem, RenderedMeshComponentTypeId, &entity, 1,
            &mesh, sizeof(mesh));
        ecs_AddComponent(entitySystem, ShadowCastingMeshComponentTypeId, &entity, 1);

        ecs_AddComponent(entitySystem, TransformComponentTypeId, &entity, 1);

        u32 shader = VULKAN_SHADER_DIFFUSE_LIGHTING;
        ecs_AddComponent(entitySystem, ShaderComponentTypeId, &entity, 1,
            &shader, sizeof(shader));
    }

    {
        vec3 d = Vec3(80, 3, 80); // dimensions
        vec3 hd = d * 0.5f;
        float t = 1.0f; // thickness
        float ht = t * 0.5f;

        EntityId entities[5];
        u32 count = ArrayCount(entities);
        ecs_CreateEntities(entitySystem, entities, count);

        vec3 positions[5];
        positions[0] = Vec3(0, -ht, 0);
        positions[1] = Vec3(0, hd.y, -hd.z - ht);
        positions[2] = Vec3(0, hd.y, hd.z + ht);
        positions[3] = Vec3(-hd.x - ht, hd.y, 0);
        positions[4] = Vec3(hd.x + ht, hd.y, 0);

        ecs_AddComponent(entitySystem, PositionComponentTypeId, entities, count,
            positions, sizeof(positions[0]));

        vec3 scales[5];
        scales[0] = Vec3(d.x, t, d.z);
        scales[1] = Vec3(d.x, d.y, t);
        scales[2] = Vec3(d.x, d.y, t);
        scales[3] = Vec3(t, d.y, d.z);
        scales[4] = Vec3(t, d.y, d.z);

        ecs_AddComponent(entitySystem, ScaleComponentTypeId, entities, count,
            scales, sizeof(scales[0]));

        vec3 halfExtents[5];
        FillArray(halfExtents, count, Vec3(0.5f));
        ecs_AddComponent(entitySystem, BoxHalfExtentsComponentTypeId, entities, count,
                halfExtents, sizeof(halfExtents[0]));

        ecs_AddComponent(entitySystem, CollisionMaskComponentTypeId, entities, count);

        u32 meshes[5];
        FillArray(meshes, count, VULKAN_MESH_CUBE);
        ecs_AddComponent(entitySystem, RenderedMeshComponentTypeId, entities,
            count, meshes, sizeof(meshes[0]));
        ecs_AddComponent(entitySystem, ShadowCastingMeshComponentTypeId, entities, count);

        mat4 transforms[5];
        FillArray(transforms, count, Identity());
        ecs_AddComponent(entitySystem, TransformComponentTypeId, entities,
            count, transforms, sizeof(transforms[0]));

        u32 shaders[5];
        FillArray(shaders, count, VULKAN_SHADER_WORLD_TEX_COORD);
        ecs_AddComponent(entitySystem, ShaderComponentTypeId, entities, count,
            shaders, sizeof(shaders[0]));

        u32 materials[5];
        FillArray(materials, count, VULKAN_MATERIAL_DEV_GRID512);
        ecs_AddComponent(entitySystem, MaterialComponentTypeId, entities, count, materials,
                sizeof(materials[0]));
    }

#if 0
    CreateWallEntity(
        entitySystem, Vec3(5.0f, 1.5f, -15.0f), Vec3(70.0f, 3.0f, 1.0f));

    CreateWallEntity(
        entitySystem, Vec3(-25.0f, 1.5f, 15.0f), Vec3(1.0f, 3.0f, 40.0f));
#endif

    EntityId spawner = CreateEntityFromDescription(
        entitySystem, g_EntityDescriptions + EntityDesc_PlayerSpawner);
#if USE_TERRAIN
    vec3 spawnerPosition = Vec3(1, 10, 1);
#else
    vec3 spawnerPosition = Vec3(0, 5, 0);
#endif
    ecs_SetComponent(entitySystem, PositionComponentTypeId,
            &spawner, 1, &spawnerPosition, sizeof(spawnerPosition));

    //CreateEntityFromDescription(
        //entitySystem, g_EntityDescriptions + EntityDesc_EnemySpawner);

    CreateEntityFromDescription(
        entitySystem, g_EntityDescriptions + EntityDesc_SkyLight);

    CreateEntityFromDescription(
        entitySystem, g_EntityDescriptions + EntityDesc_SunLight);
}

internal void ConfigureEntitySystem(ecs_EntitySystem *entitySystem, MemoryArena *arena)
{
    u32 maxEntities = 64;

    vec3 defaultScale = Vec3(1);
    quat defaultRotation = Quat();
    mat4 defaultTransform = Identity();
    u32 defaultCollisionMask = U32_MAX;

    ecs_Initialize(entitySystem, maxEntities, MAX_COMPONENT_TYPES, arena);
    ecs_RegisterComponent(entitySystem, arena, PositionComponentTypeId,
        sizeof(vec3), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, VelocityComponentTypeId,
        sizeof(vec3), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, ViewAnglesComponentTypeId,
        sizeof(vec2), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, RenderedMeshComponentTypeId,
        sizeof(u32), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, ScaleComponentTypeId,
        sizeof(vec3), maxEntities, &defaultScale);
    ecs_RegisterComponent(entitySystem, arena, TransformComponentTypeId,
        sizeof(mat4), maxEntities, &defaultTransform);
    ecs_RegisterComponent(entitySystem, arena, ShaderComponentTypeId,
        sizeof(u32), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, MaterialComponentTypeId,
        sizeof(u32), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, BulletOwnerComponentTypeId,
        sizeof(EntityId), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, LastShotTimeComponentTypeId,
        sizeof(float), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, LifeTimeComponentTypeId,
        sizeof(float), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, HealthComponentTypeId,
        sizeof(float), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, BoxHalfExtentsComponentTypeId,
        sizeof(vec3), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, CollisionMaskComponentTypeId,
        sizeof(u32), maxEntities, &defaultCollisionMask);
    ecs_RegisterComponent(entitySystem, arena, CollisionMaskComponentTypeId,
        sizeof(u32), maxEntities, &defaultCollisionMask);
    ecs_RegisterComponent(entitySystem, arena, MoveTowardsTargetComponentTypeId,
        sizeof(EntityId), maxEntities);
    ecs_RegisterComponent(
        entitySystem, arena, TeamComponentTypeId, sizeof(u8), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, EntitySpawnerComponentTypeId,
        sizeof(u32), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, PreviousCommandComponentTypeId,
        sizeof(PlayerCommand), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, RotationComponentTypeId,
        sizeof(quat), maxEntities, &defaultRotation);
    ecs_RegisterComponent(
        entitySystem, arena, LightTypeComponentTypeId, sizeof(u8), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, LightColorComponentTypeId,
        sizeof(vec3), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, LightAttenuationComponentTypeId,
        sizeof(vec3), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, IsJumpingComponentTypeId,
        sizeof(b32), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, DamageComponentTypeId,
        sizeof(f32), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, WeaponControllerComponentTypeId,
        sizeof(WeaponController), maxEntities);
    ecs_RegisterComponent(entitySystem, arena, TracerRendererComponentTypeId,
        0, maxEntities);
    ecs_RegisterComponent(
        entitySystem, arena, ShadowCastingMeshComponentTypeId, 0, maxEntities);
}

internal void OnStartEditor(GameState *gameState, GameMemory *memory)
{
    // Clear editor state
    gameState->selectedEntity = NULL_ENTITY;
    gameState->editorMode = EDITOR_MODE_SELECTING;
    gameState->isVerticalTranslation = false;

    ecs_DeleteAllEntities(&gameState->entitySystem);

    ReadFileResult mapFile = memory->readEntireFile("test.map");
    Assert(mapFile.memory);
    String mapData =
        CreateString((const char *)mapFile.memory, mapFile.size);
    ReadEntityDescriptions(&gameState->entitySystem, mapData);
    memory->freeFile(mapFile);
}

internal void OnStopEditor(GameState *gameState, GameMemory *memory)
{
    char stringBuffer[0x1000];
    StringBuilder builder = CreateStringBuilder(stringBuffer, sizeof(stringBuffer));
    DumpEntitySystem(&builder, &gameState->entitySystem);
    if (builder.length > 0)
    {
        memory->writeFile("test.map", builder.buffer, builder.length - 1); // Remote NUL terminator
    }
}

internal void LoadMap(GameState *gameState, GameMemory *memory)
{
    ecs_DeleteAllEntities(&gameState->entitySystem);

    ReadFileResult mapFile = memory->readEntireFile("test.map");
    Assert(mapFile.memory);
    String mapData =
        CreateString((const char *)mapFile.memory, mapFile.size);
    ReadEntityDescriptions(&gameState->entitySystem, mapData);
    memory->freeFile(mapFile);

    gameState->worldCollisionMeshArena.used = 0;
    gameState->worldCollisionMesh =
        CreateTriangleCollisionMeshForWorld(&gameState->entitySystem,
            &gameState->transArena, &gameState->worldCollisionMeshArena);
    //BottomUpBuildBvh(&gameState->worldBvh, &gameState->worldCollisionMesh,
            //&gameState->transArena, &gameState->worldCollisionMeshArena);
    TopDownBuildBvh(&gameState->worldBvh, &gameState->worldCollisionMesh,
            &gameState->transArena, &gameState->worldCollisionMeshArena);


    g_WorldCollisionMesh = gameState->worldCollisionMesh;
    g_WorldCollisionMeshBvh = gameState->worldBvh;

    gameState->playerEntity =
        SpawnPlayerSystem(&gameState->entitySystem, &gameState->transArena);

    SpawnEnemies(&gameState->entitySystem, &gameState->transArena);

    ClearCollisionWorld(&gameState->collisionWorld);

    AddTriangleMeshToCollisionWorld(&gameState->collisionWorld,
        gameState->worldCollisionMesh, gameState->worldBvh);
    AddHeightMapMeshToCollisionWorld(
        &gameState->collisionWorld, gameState->terrainCollisionMesh);

    LOG_DEBUG("Map loaded!");
}

internal void OnStartGame(GameState *gameState, GameMemory *memory)
{
    ecs_DeleteAllEntities(&gameState->entitySystem);


#if 0
    CreateEntities(&gameState->entitySystem);
#else
    ReadFileResult mapFile = memory->readEntireFile("test.map");
    Assert(mapFile.memory);
    String mapData =
        CreateString((const char *)mapFile.memory, mapFile.size);
    ReadEntityDescriptions(&gameState->entitySystem, mapData);
    memory->freeFile(mapFile);
#endif

    gameState->worldCollisionMeshArena.used = 0;
    gameState->worldCollisionMesh =
        CreateTriangleCollisionMeshForWorld(&gameState->entitySystem,
            &gameState->transArena, &gameState->worldCollisionMeshArena);
    //BottomUpBuildBvh(&gameState->worldBvh, &gameState->worldCollisionMesh,
            //&gameState->transArena, &gameState->worldCollisionMeshArena);
    TopDownBuildBvh(&gameState->worldBvh, &gameState->worldCollisionMesh,
            &gameState->transArena, &gameState->worldCollisionMeshArena);


    g_WorldCollisionMesh = gameState->worldCollisionMesh;
    g_WorldCollisionMeshBvh = gameState->worldBvh;

    gameState->playerEntity =
        SpawnPlayerSystem(&gameState->entitySystem, &gameState->transArena);

    SpawnEnemies(&gameState->entitySystem, &gameState->transArena);

    ClearCollisionWorld(&gameState->collisionWorld);

    AddTriangleMeshToCollisionWorld(&gameState->collisionWorld,
        gameState->worldCollisionMesh, gameState->worldBvh);
    AddHeightMapMeshToCollisionWorld(
        &gameState->collisionWorld, gameState->terrainCollisionMesh);
}

internal void PopulateTables()
{
    PopulateDataTypeLengthsTable();
    PopulateComponentDataTypesTable();
    PopulateComponentNamesTable();
    PopulateEntityDescriptionsTable();
    PopulateWeaponPropertiesTable();
    PopulateItemIconTexturesTable();
}

internal void InitializeGame(GameState *gameState, GameMemory *memory)
{
    size_t remaining = memory->persistentStorageSize - sizeof(GameState);
    MemoryArenaInitialize(&gameState->testArena, remaining,
                          (u8 *)memory->persistentStorageBase +
                                  sizeof(GameState));

    Debug_Init(&gameState->debugSystem, 0x2000, 0x1000, &gameState->testArena);

    PopulateTables();

    ConfigureEntitySystem(&gameState->entitySystem, &gameState->testArena);

    gameState->camera.position = Vec3(0, 1, 2);

    gameState->rng = CreateRandomNumberGenerator(0xFF123, 0x66234234);

    PhysicsDebugger_Init(
        &gameState->physicsDebugger, &gameState->testArena, 0x800);

    InitializePhysicsEngine(
        &gameState->physicsEngine, &gameState->testArena, 32, 32);

    AllocateAndInitialzeArena(&gameState->worldCollisionMeshArena,
        &gameState->testArena, KILOBYTES(100));

    AllocateAndInitialzeArena(
        &gameState->terrainArena, &gameState->testArena, MEGABYTES(10));

    InitializeInventory(&gameState->playerInventory, &gameState->testArena);

    gameState->playerInventory.slots[0] = 1;
    gameState->playerInventory.slots[1] = 2;

    //gameState->globalMode = GLOBAL_MODE_EDITOR;
}

internal void ProcessGlobalModeInput(GameState *gameState, GameMemory *memory)
{
    InputSystem *inputSystem = &gameState->inputSystem;

    if (WasKeyPressed(inputSystem, K_F1))
    {
        if (gameState->globalMode == GLOBAL_MODE_EDITOR)
        {
            gameState->globalMode = GLOBAL_MODE_GAME;
            //OnStopEditor(gameState, memory);
            OnStartGame(gameState, memory);
        }
        else if (gameState->globalMode == GLOBAL_MODE_GAME)
        {
            gameState->globalMode = GLOBAL_MODE_EDITOR;
            OnStartEditor(gameState, memory);
        }
    }

    if (WasKeyPressed(inputSystem, K_F2))
    {
        if (gameState->globalMode == GLOBAL_MODE_PHYSICS_DEBUGGER)
        {
            gameState->globalMode = GLOBAL_MODE_GAME;
        }
        else if (gameState->globalMode == GLOBAL_MODE_GAME)
        {
            gameState->globalMode = GLOBAL_MODE_PHYSICS_DEBUGGER;
        }
    }

    if (WasKeyPressed(inputSystem, K_F3))
    {
        if (gameState->globalMode == GLOBAL_MODE_PHYSICS_TEST_BENCH)
        {
            gameState->globalMode = GLOBAL_MODE_GAME;
        }
        else if (gameState->globalMode == GLOBAL_MODE_GAME)
        {
            gameState->globalMode = GLOBAL_MODE_PHYSICS_TEST_BENCH;
        }
    }
}

internal void UpdateMenu(GameState *gameState, GameMemory *memory)
{
}

internal void UpdateGame(GameState *gameState, GameMemory *memory,
    float frameBufferWidth, float frameBufferHeight, float dt)
{
    if (WasKeyPressed(&gameState->inputSystem, K_F10))
    {
        LoadMap(gameState, memory);
    }

    if (WasKeyPressed(&gameState->inputSystem, K_F5))
    {
        gameState->isDebugCameraActive = !gameState->isDebugCameraActive;
    }

    PlayerCommand prevCmd = {};
    if (gameState->playerEntity != NULL_ENTITY)
    {
        ecs_GetComponent(&gameState->entitySystem,
            PreviousCommandComponentTypeId, &gameState->playerEntity, 1,
            &prevCmd, sizeof(prevCmd));
    }

    PlayerCommand cmd = prevCmd;

    if (gameState->isDebugCameraActive)
    {
        memory->showCursor(true);
        UpdateCamera(&gameState->debugCamera, &gameState->inputSystem, dt,
            frameBufferWidth, frameBufferHeight);
    }
    else
    {
        if (WasKeyPressed(&gameState->inputSystem, K_TAB))
        {
            gameState->showInventory = !gameState->showInventory;
        }
        if (gameState->showInventory)
        {
            memory->showCursor(true);
        }
        else
        {
            memory->showCursor(false);
        }
        cmd = CreatePlayerCommand(&gameState->inputSystem, prevCmd,
            frameBufferWidth, frameBufferHeight, !gameState->showInventory);
    }

    if (gameState->playerEntity != NULL_ENTITY)
    {
        UpdatePlayer(&gameState->entitySystem, &gameState->collisionWorld,
            &gameState->transArena, gameState->playerEntity, cmd, dt,
            gameState->currentTime, &gameState->rng, &gameState->eventQueue);
    }
    gameState->prevPlayerCommand = cmd;

    UpdateLifeTimeSystem(&gameState->entitySystem, &gameState->transArena, dt);
    UpdateHealthComponentSystem(
        &gameState->entitySystem, &gameState->transArena);
    UpdateBulletPositionSystem(
        &gameState->entitySystem, &gameState->transArena, dt);
    UpdateAquireTargetSystem(&gameState->entitySystem, &gameState->transArena);
    UpdateMoveTowardsTargetSystem(&gameState->entitySystem,
        &gameState->collisionWorld, &gameState->transArena, dt);

    for (u32 eventIdx = 0; eventIdx < gameState->eventQueue.length; ++eventIdx)
    {
        GameEvent *event = gameState->eventQueue.events + eventIdx;
        switch (event->type)
        {
        case GameEvent_FireWeapon:
        {
            gameState->vmStartT = gameState->currentTime;
            EntityId light =
                CreateEntityFromDescription(&gameState->entitySystem,
                    g_EntityDescriptions + EntityDesc_MuzzleFlashLight);
            ecs_SetComponent(&gameState->entitySystem, PositionComponentTypeId,
                &light, 1, &event->fireWeaponPosition,
                sizeof(event->fireWeaponPosition));
        }
        break;
        default:
            break;
        };
    }

    gameState->eventQueue.length = 0;

    // Update animations
    {

        float animationLengthInSeconds = 0.1f;
        float t = gameState->currentTime - gameState->vmStartT;
        if (t <= animationLengthInSeconds)
        {
            t = EaseOutQuad(t / animationLengthInSeconds);
            gameState->vmCurrentPoseZ = Lerp(0.04f, 0.0f, t);
            gameState->vmCurrentPoseXRot = Lerp(-0.03f, 0.0f, t);
        }
        else
        {
            gameState->vmCurrentPoseZ = 0.0f;
            gameState->vmCurrentPoseXRot = 0.0f;
        }
    }



    //DrawPositionSystem(&gameState->entitySystem, &gameState->transArena);
    //DrawBoxHalfExtentsSystem(&gameState->entitySystem, &gameState->transArena);
    UpdateTransformSystem(&gameState->entitySystem, &gameState->transArena);
}

internal void UpdateEditor(GameState *gameState, GameMemory *memory,
    float frameBufferWidth, float frameBufferHeight, float dt)
{
    UpdateCamera(&gameState->camera, &gameState->inputSystem, dt,
        frameBufferWidth, frameBufferHeight);
    if (gameState->camera.isRotating)
    {
        memory->showCursor(false);
    }
    else
    {
        memory->showCursor(true);
    }

    DoEditorStuff(gameState, memory, frameBufferWidth, frameBufferHeight);

    DrawPositionSystem(&gameState->entitySystem, &gameState->transArena);
    DrawBoxHalfExtentsSystem(&gameState->entitySystem, &gameState->transArena);
    UpdateTransformSystem(&gameState->entitySystem, &gameState->transArena);
}

internal void UpdatePhysicsDebugger(GameState *gameState, GameMemory *memory,
    float frameBufferWidth, float frameBufferHeight, float dt)
{
    memory->showCursor(true);
    UpdateCamera(&gameState->camera, &gameState->inputSystem, dt,
        frameBufferWidth, frameBufferHeight);

    InputSystem *inputSystem = &gameState->inputSystem;

    i32 increment = 0;
    if (WasKeyPressed(inputSystem, K_LEFT_BRACKET) ||
        WasKeyRepeated(inputSystem, K_LEFT_BRACKET))
    {

        increment = -1;
    }
    if (WasKeyPressed(inputSystem, K_RIGHT_BRACKET) ||
        WasKeyRepeated(inputSystem, K_RIGHT_BRACKET))
    {
        increment = 1;
    }

    if (IsKeyDown(inputSystem, K_LEFT_SHIFT))
    {
        increment *= 10;
    }

    i32 cursor = gameState->selectedPhysicsCall + increment;
    if (cursor < 0)
    {
        cursor = gameState->physicsDebugger.length + cursor;
    }
    else
    {
        cursor = cursor % gameState->physicsDebugger.length;
    }
    Assert(cursor >= 0);
    Assert(cursor < (i32)gameState->physicsDebugger.length);
    gameState->selectedPhysicsCall = cursor;

    PhysicsDebugger_SelectCall(&gameState->physicsDebugger,
        gameState->selectedPhysicsCall, &gameState->transArena);

    if (WasKeyPressed(inputSystem, K_0) || WasKeyRepeated(inputSystem, K_0))
    {
        if (IsKeyDown(inputSystem, K_LEFT_SHIFT))
        {
            gameState->selectedSatIteration++;
        }
    }

    if (WasKeyPressed(inputSystem, K_9) || WasKeyRepeated(inputSystem, K_9))
    {
        if (IsKeyDown(inputSystem, K_LEFT_SHIFT))
        {
            gameState->selectedSatIteration =
                Max(0, (i32)gameState->selectedSatIteration - 1);
        }
    }

    // DrawAllTrianglesInBvhNode(&g_WorldCollisionMeshBvh,
    // &g_WorldCollisionMesh, 1);

    g_SelectedSatIteration = gameState->selectedSatIteration;
}

enum
{
    UiEventType_DragStart,
    UiEventType_DragEnd,
};

struct UiEvent
{
    u32 type;
    u32 slotIdx;
};

struct UiEventQueue
{
    u32 length;
    UiEvent events[16];
};

inline void DrawInventorySlot(RenderCommandQueue *renderCommands,
    mat4 orthoProjection, InputSystem *inputSystem, float windowHeight, vec2 p,
    float size, vec4 color, u32 icon, u32 slotIdx, UiEventQueue *eventQueue,
    bool enableHover)
{
    // center center alignment
    rect2 rect = {};
    rect.min.x = p.x - size * 0.5f;
    rect.min.y = p.y - size * 0.5f;
    rect.max.x = p.x + size * 0.5f;
    rect.max.y = p.y + size * 0.5f;

    vec2 mousePosition = Vec2(
        (float)inputSystem->mouseX, windowHeight - (float)inputSystem->mouseY);

    if (ContainsPoint(rect, mousePosition))
    {
        if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
        {
            UiEvent event = {};
            event.type = UiEventType_DragStart;
            event.slotIdx = slotIdx;
            Assert(eventQueue->length < ArrayCount(eventQueue->events));
            eventQueue->events[eventQueue->length++] = event;

            color = Vec4(0.6, 0.6, 0.6, 1.0);
        }

        if (WasKeyReleased(inputSystem, K_MOUSE_BUTTON_LEFT))
        {
            UiEvent event = {};
            event.type = UiEventType_DragEnd;
            event.slotIdx = slotIdx;
            Assert(eventQueue->length < ArrayCount(eventQueue->events));
            eventQueue->events[eventQueue->length++] = event;
        }

        if (enableHover)
        {
            color = Vec4(0.3, 0.3, 0.3, 1.0);
            size += 2.0f;
        }
    }


    ui_PushQuad(renderCommands, orthoProjection, p, size, size, color,
        HorizontalAlign_Center, VerticalAlign_Center);

    Assert(icon < ArrayCount(g_ItemIconTextures));
    u32 texture = g_ItemIconTextures[icon];

    if (icon > 0)
    {
        ui_PushQuad(renderCommands, orthoProjection, p, size - 5.0f,
            size - 5.0f, texture, HorizontalAlign_Center,
            VerticalAlign_Center);
    }
}

internal void DrawInventory(RenderCommandQueue *renderCommands,
    mat4 orthoProjection, InputSystem *inputSystem, float windowWidth,
    float windowHeight, bool showInventory, Inventory inventory,
    GameState *gameState)
{
    float xCenter = windowWidth * 0.5f;
    float size = 96.0f;
    vec4 color = Vec4(0.1, 0.1, 0.1, 1.0);
    float xInc = size + 8.0f;

    UiEventQueue eventQueue = {};

    if (showInventory)
    {
        ui_PushQuad(renderCommands, orthoProjection, Vec2(0, 0), windowWidth,
            windowHeight, Vec4(0.6, 0.6, 0.6, 0.6), HorizontalAlign_Left,
            VerticalAlign_Bottom);

        {
            float yStart = windowHeight - 200.0f;
            float xStart = xCenter - xInc * ((inventory.width - 1) * 0.5f);

            for (u32 y = 0; y < inventory.height; ++y)
            {
                for (u32 x = 0; x < inventory.width; ++x)
                {
                    u32 idx = y * inventory.width + x;
                    u32 slot = inventory.slots[idx];
                    if (gameState->isDragging && gameState->slotBeingDragged == idx)
                    {
                        slot = 0; // Don't draw icon when we are dragging contents of this slot
                    }

                    vec2 p = Vec2(xStart + x * xInc, yStart - y * xInc);
                    // Last row is the action bar slot
                    if (y == inventory.height - 1)
                    {
                        p.y = 100.0f;
                    }

                    DrawInventorySlot(renderCommands, orthoProjection,
                        inputSystem, windowHeight, p, size, color, slot, idx,
                        &eventQueue, true);
                }

            }
        }

        Assert(eventQueue.length <= 1);
        if (eventQueue.length > 0)
        {
            UiEvent event = eventQueue.events[0];
            if (event.type == UiEventType_DragStart)
            {
                gameState->isDragging = true;
                gameState->slotBeingDragged = event.slotIdx;
            }
            else if (event.type == UiEventType_DragEnd)
            {
                gameState->isDragging = false;

                // Perform transaction
                Assert(event.slotIdx < inventory.width * inventory.height);
                u32 oldVal = inventory.slots[event.slotIdx];
                u32 newVal = inventory.slots[gameState->slotBeingDragged];

                if (newVal > 0)
                {
                    inventory.slots[event.slotIdx] = newVal;
                    inventory.slots[gameState->slotBeingDragged] = oldVal;
                }
            }
        }

        if (gameState->isDragging)
        {
            vec2 mousePosition = Vec2(
                    (float)inputSystem->mouseX, windowHeight - (float)inputSystem->mouseY);

            Assert(gameState->slotBeingDragged < inventory.width * inventory.height);
            u32 slot = inventory.slots[gameState->slotBeingDragged];

            Assert(slot < ArrayCount(g_ItemIconTextures));
            u32 texture = g_ItemIconTextures[slot];

            if (slot > 0)
            {
                ui_PushQuad(renderCommands, orthoProjection, mousePosition,
                    size - 5.0f, size - 5.0f, texture, HorizontalAlign_Center,
                    VerticalAlign_Center);
            }
        }
    }
    else
    {
        // Draw action bar without any input handling
        float yStart = 100.0f;
        float xStart = xCenter - xInc * ((inventory.width - 1) * 0.5f);

        for (u32 x = 0; x < inventory.width; ++x)
        {
            u32 idx = (inventory.height - 1) * inventory.width + x;
            u32 slot = inventory.slots[idx];

            vec2 p = Vec2(xStart + x * xInc, yStart);
            DrawInventorySlot(renderCommands, orthoProjection, inputSystem,
                windowHeight, p, size, color, slot, idx, &eventQueue, false);
        }
    }
}

extern "C" GAME_UPDATE(GameUpdate)
{
#ifdef ENGINE_INTERNAL
    gDebugGameMemory = memory;
#endif

    Debug_BeginTimedBlock(GameUpdate);

    Assert(sizeof(GameState) < memory->persistentStorageSize);
    Assert(((size_t)memory->persistentStorageBase & 0x3) == 0); // Check 4byte aligned
    Assert(((size_t)memory->transientStorageBase & 0x3) == 0);

    GameState *gameState = (GameState *)memory->persistentStorageBase;
    g_DebugSystem = &gameState->debugSystem;

    MemoryArenaInitialize(&gameState->transArena, memory->transientStorageSize,
                          (u8*)memory->transientStorageBase);

    if (!gameState->isInitialized)
    {
        InitializeGame(gameState, memory);
        gameState->isInitialized = true;

        LoadAssets(gameState, memory, renderCommands);
        if (gameState->globalMode == GLOBAL_MODE_GAME)
        {
            OnStartGame(gameState, memory);
        }
    }

    g_PhysicsDebugger = &gameState->physicsDebugger;
    g_WorldCollisionMesh = gameState->worldCollisionMesh;
    g_WorldCollisionMeshBvh = gameState->worldBvh;

    TextBuffer textBuffer = {};
    textBuffer.maxVertices = ArrayCount(gameState->textBufferVertices);
    textBuffer.vertices = gameState->textBufferVertices;
    Debug_Cleanup(&gameState->debugSystem, dt);

    InputSystem *inputSystem = &gameState->inputSystem;
    input_OnBeginFrame(inputSystem);

    for (u32 i = 0; i < eventCount; ++i)
    {
        PlatformEvent event = events[i];
        switch (event.type)
        {
        case PlatformEvent_KeyPress:
        {
            inputSystem->currentKeyStates[events[i].key] = 1;
            u32 input = inputSystem->keyBindings[events[i].key];
            inputSystem->currentKeyInput |= input;
            break;
        }
        case PlatformEvent_KeyRelease:
        {
            inputSystem->currentKeyStates[events[i].key] = 0;
            u32 input = inputSystem->keyBindings[events[i].key];
            inputSystem->currentKeyInput &= ~input;
            break;
        }
        case PlatformEvent_KeyRepeat:
            inputSystem->keyRepeatStates[events[i].key] = 1;
            break;
        case PlatformEvent_MouseMotion:
            inputSystem->mouseX = events[i].mouseMotion.x;
            inputSystem->mouseY = events[i].mouseMotion.y;
            break;
        case PlatformEvent_MouseScroll:
            break;
        case PlatformEvent_CharacterPressed:
            break;
        case PlatformEvent_WindowFocusChanged:
            LOG_DEBUG("Window focus changed");
            break;
        case PlatformEvent_FrameBufferResized:
            LOG_DEBUG("Frame Buffer resized");
            break;
        case PlatformEvent_GameCodeReload:
            PopulateTables();
            GenerateAssets(gameState, renderCommands);
            break;
        default:
            break;
        }
    }

    gameState->currentTime += dt;

    // FPS counter
    {
        u32 tail =
            (gameState->dtSamplesTail + 1) % ArrayCount(gameState->dtSamples);
        gameState->dtSamples[tail] = dt;
        gameState->dtSamplesTail = tail;
        gameState->dtSamplesCount = MinU(
            gameState->dtSamplesCount + 1, ArrayCount(gameState->dtSamples));
    }

    ProcessGlobalModeInput(gameState, memory);

    switch (gameState->globalMode)
    {
        case GLOBAL_MODE_MENU:
            UpdateMenu(gameState, memory);
            break;
        case GLOBAL_MODE_GAME:
            UpdateGame(gameState, memory, (float)frameBufferWidth,
                (float)frameBufferHeight, dt);
            break;
        case GLOBAL_MODE_EDITOR:
            UpdateEditor(gameState, memory, (float)frameBufferWidth,
                (float)frameBufferHeight, dt);
            break;
        case GLOBAL_MODE_PHYSICS_DEBUGGER:
            UpdatePhysicsDebugger(gameState, memory, (float)frameBufferWidth,
                (float)frameBufferHeight, dt);
            break;
        case GLOBAL_MODE_PHYSICS_TEST_BENCH:
            UpdatePhysicsTestBench(gameState, memory, (float)frameBufferWidth,
                (float)frameBufferHeight, dt);
            break;
        default:
            InvalidCodePath();
            break;
    }

    // TODO: Extract this into a function for retrieving the camera data
    vec3 cameraPosition = {};
    vec3 cameraRotation = {};
    vec3 cameraVelocity = {};
    vec2 viewAngles = {};
    mat4 view = Identity();
    mat4 projection = Perspective(Radians(g_Fov),
        (float)frameBufferWidth / (float)frameBufferHeight, g_NearClip, g_FarClip);

    if (gameState->globalMode == GLOBAL_MODE_EDITOR ||
        gameState->globalMode == GLOBAL_MODE_PHYSICS_DEBUGGER ||
        gameState->globalMode == GLOBAL_MODE_PHYSICS_TEST_BENCH)
    {
        view = CreateCameraViewMatrix(&gameState->camera);
        cameraRotation = gameState->camera.rotation;
        cameraPosition = gameState->camera.position;
        cameraVelocity = gameState->camera.velocity;
    }
    else if (gameState->globalMode == GLOBAL_MODE_GAME)
    {
        if (gameState->isDebugCameraActive)
        {
            view = CreateCameraViewMatrix(&gameState->debugCamera);
            cameraRotation = gameState->debugCamera.rotation;
            cameraPosition = gameState->debugCamera.position;
            cameraVelocity = gameState->debugCamera.velocity;
        }
        else
        {
            vec3 playerPosition = {};
            if (gameState->playerEntity != NULL_ENTITY)
            {
                ecs_GetComponent(&gameState->entitySystem,
                    PositionComponentTypeId, &gameState->playerEntity, 1,
                    &playerPosition, sizeof(playerPosition));
                ecs_GetComponent(&gameState->entitySystem,
                    ViewAnglesComponentTypeId, &gameState->playerEntity, 1,
                    &viewAngles, sizeof(viewAngles));

                cameraPosition = playerPosition + Vec3(0, 0.85f, 0);
                view = RotateX(-viewAngles.x) * RotateY(-viewAngles.y) *
                       Translate(-cameraPosition);
                cameraRotation = Vec3(viewAngles.x, viewAngles.y, 0.0f);

                ecs_GetComponent(&gameState->entitySystem,
                    VelocityComponentTypeId, &gameState->playerEntity, 1,
                    &cameraVelocity, sizeof(cameraVelocity));
            }
        }
    }

    Debug_Printf("Camera Position: %g, %g, %g\n", cameraPosition.x,
        cameraPosition.y, cameraPosition.z);
    Debug_Printf("Camera Rotation: %g %g %g\n", cameraRotation.x,
        cameraRotation.y, cameraRotation.z);
    Debug_Printf("Camera Velocity: %g %g %g\n", cameraVelocity.x,
        cameraVelocity.y, cameraVelocity.z);

    mat4 orthoProjection = Orthographic(0.0f, (float)frameBufferWidth,
            (float)frameBufferHeight, 0.0f);

    // Physics debugger
    {
        Debug_Printf("Physics Debugger Length: %u\n",
                gameState->physicsDebugger.length);

        if (gameState->globalMode == GLOBAL_MODE_PHYSICS_DEBUGGER)
        {
            Debug_Printf("Selected Physics Call: %u / %u\n",
                gameState->selectedPhysicsCall,
                gameState->physicsDebugger.length);

            Assert(gameState->selectedPhysicsCall <
                   gameState->physicsDebugger.length);
            PhysicsFunctionCall *call = gameState->physicsDebugger.callBuffer +
                                        gameState->selectedPhysicsCall;
            Assert(call->type < ArrayCount(g_PhysicsCallDescriptions));
            const char *description = g_PhysicsCallDescriptions[call->type];

            ui_PushStringArguments args = {};
            ui_PushStringFillArguments(&args, &gameState->smallFont, description,
                    Vec2(frameBufferWidth * 0.5f, 30.0f));
            args.horizontalAlignment = HorizontalAlign_Center;

            ui_PushString(renderCommands, &textBuffer, orthoProjection, &args);
        }
    }

    if (gameState->globalMode == GLOBAL_MODE_EDITOR)
    {
        if (gameState->selectedEntity != NULL_ENTITY)
        {
            EntityDescription desc = {};
            CreateDescriptionOfEntity(
                &gameState->entitySystem, gameState->selectedEntity, &desc);

            for (u32 i = 0; i < desc.count; ++i)
            {
                EntityComponentDescription *component = desc.components + i;

                const char *type = GetComponentName(component->typeId);
                u32 dataType = GetComponentDataType(component->typeId);

                char buffer[64];
                PrintDataTypeToBuffer(
                    dataType, component->data, buffer, ArrayCount(buffer));

                Debug_Printf("%s: %s\n", type, buffer);
            }
        }
    }



    // Draw scene
    if (gameState->globalMode == GLOBAL_MODE_GAME ||
        gameState->globalMode == GLOBAL_MODE_EDITOR)
    {
        {
            RenderCommandDrawMeshUniforms *drawSkybox = RenderCommandAllocate(
                    renderCommands, RenderCommandDrawMeshUniforms, 0);
            drawSkybox->model = Identity();
            drawSkybox->view = RotateX(-cameraRotation.x) * RotateY(-cameraRotation.y);
            drawSkybox->projection = projection;
            drawSkybox->shader = VULKAN_SHADER_CUBE_MAP;
            drawSkybox->mesh = VULKAN_MESH_CUBE;
            drawSkybox->values[0].key = UNIFORM_KEY_TEX_SAMPLER;
            drawSkybox->values[0].type = UNIFORM_TYPE_TEXTURE_CUBE_MAP;
            drawSkybox->values[0].texture = VULKAN_MATERIAL_CUBE_MAP;
            drawSkybox->count = 1;
        }

        DrawTerrain(renderCommands, view, projection,
            gameState->terrainTranslation, gameState->terrainScaleXZ,
            gameState->terrainScaleY);

        //DrawCollisionWorld(&gameState->collisionWorld);
        //DrawHeightMapCollisionMesh(gameState->terrainCollisionMesh);
        //DrawHeightMapCollisionMeshTraversal(gameState->terrainCollisionMesh,
                //Vec3( -14, 4.7, 20));

        DrawRenderedMeshSystem(&gameState->entitySystem, &gameState->transArena,
                renderCommands, view, projection, cameraPosition);
        DrawShadowsSystem(&gameState->entitySystem, &gameState->transArena,
                renderCommands, cameraPosition);

        if (gameState->globalMode == GLOBAL_MODE_EDITOR)
        {
            DrawEntitySpawnerSystem(&gameState->entitySystem,
                    &gameState->transArena, renderCommands, cameraPosition, view,
                    projection);

            DrawLightVisualisationSystem(&gameState->entitySystem,
                &gameState->transArena, renderCommands, cameraPosition, view,
                projection);
        }

        DrawAmbientLightSystem(
            &gameState->entitySystem, &gameState->transArena, renderCommands);
        DrawDirectionalLightsSystem(
            &gameState->entitySystem, &gameState->transArena, renderCommands);
        DrawPointLightsSystem(
            &gameState->entitySystem, &gameState->transArena, renderCommands);

        DrawTracersSystem(&gameState->entitySystem, &gameState->transArena,
            renderCommands, cameraPosition, view, projection);

        if (gameState->globalMode == GLOBAL_MODE_GAME)
        {
            //DrawBvh(&gameState->worldBvh, &gameState->worldCollisionMesh);
            //DrawAllTrianglesInBvhNode(
                //&gameState->worldBvh, &gameState->worldCollisionMesh, 3);
        }

        if (gameState->globalMode == GLOBAL_MODE_GAME && !gameState->isDebugCameraActive)
        {
            // Draw view model
            if (gameState->playerEntity != NULL_ENTITY)
            {
                float rate = 9.0f;
                PlayerCommand cmd;
                ecs_GetComponent(&gameState->entitySystem,
                    PreviousCommandComponentTypeId, &gameState->playerEntity, 1,
                    &cmd, sizeof(cmd));

                gameState->smoothedForwardMove =
                    Lerp(gameState->smoothedForwardMove,
                        Min(Abs(cmd.forwardMove) + Abs(cmd.rightMove), 1.0f),
                        dt * 8.0f);
                float zRot = gameState->smoothedForwardMove * 0.035f *
                             (Sin(0.5f * rate * gameState->currentTime) + 0.5f);
                float xOff = gameState->smoothedForwardMove * 0.0045f *
                             (Sin(0.5f * rate * gameState->currentTime) + 0.5f);
                float yOff = gameState->smoothedForwardMove * 0.0025f *
                             (Sin(rate * gameState->currentTime) - 1.0f);

                mat4 translation = Translate(Vec3(xOff, yOff, gameState->vmCurrentPoseZ));

                float amount = 2.0f;
                float maxAmount = 0.5f;
                float smooth = 3.0f;
                vec2 viewAngleDelta = viewAngles - gameState->prevViewAngles;
                float factorX = -viewAngleDelta.x * amount;
                float factorY = viewAngleDelta.y * amount;

                factorX = Clamp(factorX, -maxAmount, maxAmount);
                factorY = Clamp(factorY, -maxAmount, maxAmount);

                gameState->viewModelRotations =
                    Lerp(gameState->viewModelRotations, Vec2(factorX, factorY),
                        dt * smooth);

                mat4 rotation = RotateZ(gameState->viewModelRotations.x +
                                        gameState->vmCurrentPoseXRot) *
                                RotateY(gameState->viewModelRotations.y) *
                                RotateX(zRot);

                mat4 viewModelProjection = Perspective(Radians(60.0f),
                    (float)frameBufferWidth / (float)frameBufferHeight,
                    g_NearClip, g_FarClip);

                mat4 baseTransform = Translate(cameraPosition) *
                                     RotateY(cameraRotation.y) *
                                     RotateX(cameraRotation.x);
                mat4 transform = baseTransform *
                                 Translate(Vec3(0.03, -0.05, -0.11)) *
                                 translation * RotateY(-PI * 0.5f) * rotation *
                                 Scale(Vec3(0.02f));
                RenderCommandDrawMeshUniforms *drawViewModel =
                    RenderCommandAllocate(
                        renderCommands, RenderCommandDrawMeshUniforms, 0);
                drawViewModel->model = transform;
                drawViewModel->view = view;
                drawViewModel->projection = viewModelProjection;
                drawViewModel->mesh = VULKAN_MESH_BADGER;
                drawViewModel->shader = VULKAN_SHADER_DIFFUSE_LIGHTING;
                PushUniformV4(
                    drawViewModel, UNIFORM_KEY_COLOR, Vec4(0.3, 0.3, 0.3, 1.0));
                PushUniformSpecialType(drawViewModel, UNIFORM_KEY_SHADOW_MAP,
                    UNIFORM_TYPE_SHADOW_MAP);

                gameState->prevViewAngles = viewAngles;
            }
        }
    }

    // Draw HUD
    if (gameState->globalMode == GLOBAL_MODE_GAME && !gameState->isDebugCameraActive)
    {
        vec3 velocity = {};
        if (gameState->playerEntity != NULL_ENTITY)
        {
            ecs_GetComponent(&gameState->entitySystem, VelocityComponentTypeId,
                &gameState->playerEntity, 1, &velocity, sizeof(velocity));
        }

        float maxVelocity = 10.0f; // FIXME: This should be a constant defined somewhere else!
        float t = Min(Length(velocity) / maxVelocity, 1.0f);
        float minDistanceFromCenter = 5.0f;
        float maxDistanceFromCenter = 50.0f;

        // Draw cross-hair
        vec2 center =
            Vec2((float)frameBufferWidth, (float)frameBufferHeight) * 0.5f;
        float width = 2.0f;
        float length = 10.0f;
        float distanceFromCenter = Lerp(minDistanceFromCenter, maxDistanceFromCenter, t);
        vec4 color = Vec4(1, 0, 1, 1);

        {
            RenderCommandDrawMeshUniforms *pushQuad = RenderCommandAllocate(
                renderCommands, RenderCommandDrawMeshUniforms, 0);
            pushQuad->model = Translate(Vec3(center.x, center.y, 0.0f)) *
                              Scale(Vec3(width, width, 1.0f));
            pushQuad->view = Identity();
            pushQuad->projection = orthoProjection;
            pushQuad->shader = VULKAN_SHADER_COLOR_ONLY_NO_DEPTH;
            pushQuad->mesh = VULKAN_MESH_QUAD;
            PushUniformV4(pushQuad, UNIFORM_KEY_COLOR, color);
        }

        for (u32 i = 0; i < 4; ++i)
        {
            RenderCommandDrawMeshUniforms *pushShadow =
                RenderCommandAllocate(renderCommands, RenderCommandDrawMeshUniforms, 0);
            pushShadow->model =
                Translate(Vec3(center.x + 1.0f, center.y - 1.0f, 0.0f)) *
                RotateZ(PI * 0.5f * i) *
                Translate(Vec3(0, distanceFromCenter + length * 0.5f, 0)) *
                Scale(Vec3(width, length, 1.0f));
            pushShadow->view = Identity();
            pushShadow->projection = orthoProjection;
            pushShadow->shader = VULKAN_SHADER_COLOR_ONLY_NO_DEPTH;
            pushShadow->mesh = VULKAN_MESH_QUAD;
            PushUniformV4(pushShadow, UNIFORM_KEY_COLOR, Vec4(0, 0, 0, 1));

            RenderCommandDrawMeshUniforms *pushQuad =
                RenderCommandAllocate(renderCommands, RenderCommandDrawMeshUniforms, 0);
            pushQuad->model =
                Translate(Vec3(center.x, center.y, 0.0f)) *
                RotateZ(PI * 0.5f * i) *
                Translate(Vec3(0, distanceFromCenter + length * 0.5f, 0)) *
                Scale(Vec3(width, length, 1.0f));
            pushQuad->view = Identity();
            pushQuad->projection = orthoProjection;
            pushQuad->shader = VULKAN_SHADER_COLOR_ONLY_NO_DEPTH;
            pushQuad->mesh = VULKAN_MESH_QUAD;
            PushUniformV4(pushQuad, UNIFORM_KEY_COLOR, color);
        }

        if (gameState->playerEntity != NULL_ENTITY)
        {
            WeaponController weaponController = {};
            ecs_GetComponent(&gameState->entitySystem,
                    WeaponControllerComponentTypeId, &gameState->playerEntity, 1,
                    &weaponController, sizeof(weaponController));

            const char *weaponText = "Unknown";
            if (weaponController.activeWeaponSlot == Weapon_Pistol)
            {
                weaponText = "Pistol";
            }
            else if (weaponController.activeWeaponSlot == Weapon_Shotgun)
            {
                weaponText = "Shotgun";
            }
            else if (weaponController.activeWeaponSlot == Weapon_SMG)
            {
                weaponText = "SMG";
            }

            ui_PushStringArguments args = {};
            ui_PushStringFillArguments(&args, &gameState->smallFont, weaponText,
                    Vec2(frameBufferWidth - 250.0f, 30.0f));

            ui_PushString(renderCommands, &textBuffer, orthoProjection, &args);
        }

        DrawInventory(renderCommands, orthoProjection, &gameState->inputSystem,
            (float)frameBufferWidth, (float)frameBufferHeight,
            gameState->showInventory, gameState->playerInventory, gameState);
    }
    else if (gameState->globalMode == GLOBAL_MODE_EDITOR)
    {
        // Draw entities panel
        {
            ui_PushQuad(renderCommands, orthoProjection,
                    Vec2((float)frameBufferWidth, (float)frameBufferHeight), 400.0f,
                    (float)frameBufferHeight, Vec4(0.08, 0.08, 0.08, 1),
                    HorizontalAlign_Right);

            ButtonStyle buttonStyle = {};
            buttonStyle.font = &gameState->debugFont;
            buttonStyle.color = Vec4(0.1, 0.1, 0.1, 1);
            buttonStyle.hoverColor = Vec4(0.6, 0.6, 0.6, 1);
            buttonStyle.clickColor = Vec4(0.8, 0.8, 0.8, 1);
            buttonStyle.textColor = Vec4(1);

            UiBuilder uiBuilder = {};
            uiBuilder.renderCommands = renderCommands;
            uiBuilder.orthoProjection = orthoProjection;
            uiBuilder.textBuffer = &textBuffer;
            uiBuilder.buttonStyle = &buttonStyle;
            uiBuilder.inputSystem = inputSystem;
            uiBuilder.windowHeight = (float)frameBufferHeight;

            float panelStartY = frameBufferHeight - 200.0f;

            for (u32 i = EntityDesc_PlayerSpawner; i < MAX_ENTITY_DESC; ++i)
            {
                if (ui_PushButton(&uiBuilder,
                            Vec2(frameBufferWidth - 200.0f, panelStartY - i * 30.0f),
                            g_EntityDescriptionNames[i]))
                {
                    if (gameState->editorMode == EDITOR_MODE_SELECTING)
                    {
                        gameState->entityDescToSpawn = i;
                        gameState->editorMode = EDITOR_MODE_SPAWNING;
                    }

                }
            }
        }

        // Draw menu bar
        {
            ui_PushQuad(renderCommands, orthoProjection,
                    Vec2(0.0f, (float)frameBufferHeight), (float)frameBufferWidth,
                    40.0f, Vec4(0.08, 0.08, 0.08, 1));

            ButtonStyle buttonStyle = {};
            buttonStyle.font = &gameState->debugFont;
            buttonStyle.color = Vec4(0.1, 0.1, 0.1, 1);
            buttonStyle.hoverColor = Vec4(0.6, 0.6, 0.6, 1);
            buttonStyle.clickColor = Vec4(0.8, 0.8, 0.8, 1);
            buttonStyle.textColor = Vec4(1);

            UiBuilder uiBuilder = {};
            uiBuilder.renderCommands = renderCommands;
            uiBuilder.orthoProjection = orthoProjection;
            uiBuilder.textBuffer = &textBuffer;
            uiBuilder.buttonStyle = &buttonStyle;
            uiBuilder.inputSystem = inputSystem;
            uiBuilder.windowHeight = (float)frameBufferHeight;

            if (ui_PushButton(&uiBuilder,
                        Vec2(100.0f, (float)frameBufferHeight - 10.0f),
                        "New Map"))
            {
                Editor_NewMap(gameState);
            }
            if (ui_PushButton(&uiBuilder,
                        Vec2(220.0f, (float)frameBufferHeight - 10.0f),
                        "Save Map"))
            {
                Editor_SaveMap(gameState, memory);
            }
            if (ui_PushButton(&uiBuilder,
                        Vec2(340.0f, (float)frameBufferHeight - 10.0f),
                        "Load Map"))
            {
                LoadMap(gameState, memory);
            }
        }

    }
    else if (gameState->globalMode == GLOBAL_MODE_PHYSICS_TEST_BENCH)
    {
        ButtonStyle buttonStyle = {};
        buttonStyle.font = &gameState->debugFont;
        buttonStyle.color = Vec4(0.1, 0.1, 0.1, 1);
        buttonStyle.hoverColor = Vec4(0.6, 0.6, 0.6, 1);
        buttonStyle.clickColor = Vec4(0.8, 0.8, 0.8, 1);
        buttonStyle.textColor = Vec4(1);

        UiBuilder uiBuilder = {};
        uiBuilder.renderCommands = renderCommands;
        uiBuilder.orthoProjection = orthoProjection;
        uiBuilder.textBuffer = &textBuffer;
        uiBuilder.buttonStyle = &buttonStyle;
        uiBuilder.inputSystem = inputSystem;
        uiBuilder.windowHeight = (float)frameBufferHeight;

        float startX = 50.0f;
        float panelStartY = frameBufferHeight - 300.0f;

        for (u32 i = PhysicsTestBenchScene_None + 1;
             i < MAX_PHYSICS_TEST_BENCH_SCENES; ++i)
        {
            if (ui_PushButton(&uiBuilder, Vec2(startX, panelStartY - i * 25.0f),
                    g_PhysicsTestBenchSceneDescriptions[i]))
            {
                PhysicsTestBench_LoadScene(gameState, i);
            }
        }
    }

#if 0 // This only works if texture comparisions are disabled in OpenGL for the shadow map texture
    // Shadow debugging
    {
        vec2 p = Vec2(10, 600);
        float w = 580;
        float h = 580;
        RenderCommandDrawMeshUniforms *pushQuad = RenderCommandAllocate(
                renderCommands, RenderCommandDrawMeshUniforms, 0);

        vec2 anchorPoint =
            CalculateAnchorPoint(HorizontalAlign_Left, VerticalAlign_Top);

        pushQuad->model = CalculateModelMatrix(p, w, h, anchorPoint);
        pushQuad->view = Identity();
        pushQuad->projection = orthoProjection;
        pushQuad->shader = VULKAN_SHADER_DEPTH_TEXTURE;
        pushQuad->mesh = VULKAN_MESH_QUAD;
        PushUniformSpecialType(
            pushQuad, UNIFORM_KEY_TEX_SAMPLER, UNIFORM_TYPE_SHADOW_MAP);
    };
#endif

    { // Draw FPS display
        float avgDt = 0.0f;
        for (u32 i = 0; i < gameState->dtSamplesCount; ++i)
        {
            avgDt += gameState->dtSamples[i];
        }
        avgDt /= (float)gameState->dtSamplesCount;

        float fps = 1.0f / avgDt;
        float frameMs = avgDt * 1000.0f;

        const char *rendererName = memory->usingVulkan ? "Vulkan" : "OpenGL";

        char buffer[80];
        snprintf(buffer, sizeof(buffer), "FPS: %d (%g ms)\nRENDERER: %s",
            (i32)Round(fps), frameMs, rendererName);

        ui_PushStringArguments args = {};
        ui_PushStringFillArguments(&args, &gameState->debugFont, buffer,
            Vec2(frameBufferWidth - 250.0f, frameBufferHeight - 30.0f));
        args.color = Vec4(0.7f, 1.0f, 0.5f, 1.0f);
        args.drawShadow = true;

        ui_PushString(renderCommands, &textBuffer, orthoProjection, &args);
    }

    ui_PushQuad(renderCommands, orthoProjection, Vec2(0, (float)frameBufferHeight),
            100.0f, 30.0f, Vec4(0, 0, 0, 0.8));

    {
        const char *modeNames[GLOBAL_MODE_COUNT] = {
            "GAME", "EDITOR", "MENU", "PHYSICS_DEBUGGER", "PHYSICS_TEST_BENCH"};

        ui_PushStringArguments args = {};
        ui_PushStringFillArguments(&args, &gameState->smallFont,
            modeNames[gameState->globalMode],
            Vec2(10, frameBufferHeight - 22.0f));
        ui_PushString(renderCommands, &textBuffer, orthoProjection, &args);
    }

    // Draw debug line buffer
    {
        VertexPC *vertices =
            AllocateArray(&gameState->transArena, VertexPC, VULKAN_MAX_LINES * 2);
        u32 vertexCount = Debug_GetData(
            &gameState->debugSystem, vertices, VULKAN_MAX_LINES * 2);

        RenderCommandUpdateVertexBuffer *updateVertexBuffer =
            RenderCommandAllocate(
                renderCommands, RenderCommandUpdateVertexBuffer, 0);
        updateVertexBuffer->vertices = vertices;
        updateVertexBuffer->lengthInBytes = vertexCount * sizeof(VertexPC);
        updateVertexBuffer->bufferIndex = VULKAN_VERTEX_BUFFER_LINES;

        RenderCommandDrawVertexBuffer *drawLineBuffer = RenderCommandAllocate(
            renderCommands, RenderCommandDrawVertexBuffer, 0);
        drawLineBuffer->model = Identity();
        drawLineBuffer->view = view;
        drawLineBuffer->projection = projection;
        drawLineBuffer->vbo = VULKAN_VERTEX_BUFFER_LINES;
        drawLineBuffer->shader = VULKAN_SHADER_VERTEX_COLOR;
        drawLineBuffer->vertexCount = vertexCount;
    }

    // In world debug text
    DebugSystem *debugSystem = &gameState->debugSystem;
    for (u32 i = 0; i < debugSystem->count; ++i)
    {
        DebugElement *element = debugSystem->elements + i;

        if (element->type == DebugElement_Text)
        {
            // Inlining of CalculateScreenPosition
            vec4 v = projection * view * Translate(element->text.position) *
                     Vec4(0, 0, 0, 1.0);
            v = v * (1.0f / v.w);

            if (AabbContainsPoint(v.xyz, Vec3(-1.0f), Vec3(1.0f)))
            {
                vec2 p = Vec2(v.x, v.y);
                p.x = (p.x + 1.0f) * frameBufferWidth * 0.5f;
                p.y = (p.y + 1.0f) * frameBufferHeight * 0.5f;

                ui_PushStringArguments args = {};
                args.font = &gameState->debugFont;
                args.text = element->text.text;
                args.length = element->text.length;
                args.position = p;
                args.color = Vec4(1);
                args.horizontalAlignment = HorizontalAlign_Center;
                args.verticalAlignment = VerticalAlign_Center;
                args.drawShadow = true;
                args.shadowColor = Vec4(0, 0, 0, 1);
                ui_PushString(
                    renderCommands, &textBuffer, orthoProjection, &args);
            }
        }
    }

    // Debug text
    {
        const char *text = gameState->debugSystem.textBuffer;
        u32 length = gameState->debugSystem.textBufferLength;

        if (length > 0)
        {
            ui_PushStringArguments args = {};
            ui_PushStringFillArguments(&args, &gameState->debugFont,
                    text, Vec2(100.0f, frameBufferHeight - 100.0f));
            args.drawShadow = true;

            ui_PushString(
                renderCommands, &textBuffer, orthoProjection, &args);
        }
    }

    // Update Text vertex buffer
    {
        RenderCommandUpdateVertexBuffer *updateVertexBuffer =
            RenderCommandAllocate(
                renderCommands, RenderCommandUpdateVertexBuffer, 0);
        updateVertexBuffer->vertices = textBuffer.vertices;
        updateVertexBuffer->lengthInBytes =
            textBuffer.vertexCount * sizeof(TextVertex);
        updateVertexBuffer->bufferIndex = VULKAN_VERTEX_BUFFER_TEXT;
    }

    Debug_EndTimedBlock(GameUpdate);
}
