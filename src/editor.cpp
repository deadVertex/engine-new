struct LineSegment
{
    vec3 start;
    vec3 end;
};

internal LineSegment GenerateLineSegment(float x, float y,
    float frameBufferWidth, float frameBufferHeight, float fov, float nearClip,
    float farClip, Camera camera)
{
  y = frameBufferHeight - y;

  float aspect = frameBufferWidth / frameBufferHeight;
  auto rad = Radians(fov);
  auto vLen = Tan( rad / 2.0f ) * nearClip;
  auto hLen = vLen * aspect;

  mat4 rotation = RotateX(-camera.rotation.x) * RotateY(-camera.rotation.y);
  rotation = Transpose(rotation);

  mat4 transform = Translate(camera.position) * rotation;
  Debug_DrawAxis(transform, 20.0f);

  auto right = Normalize(rotation.col[0].xyz);
  auto up = Normalize(rotation.col[1].xyz);
  auto forward = -Normalize(rotation.col[2].xyz);

  auto v = up * vLen;
  auto h = right * hLen;

  x -= frameBufferWidth * 0.5f;
  y -= frameBufferHeight * 0.5f;

  x /= frameBufferWidth * 0.5f;
  y /= frameBufferHeight * 0.5f;

  auto end = camera.position + forward * nearClip + h * x + v * y;

  auto dir = Normalize(end - camera.position);

  LineSegment result = {};
  result.start = camera.position;
  result.end = result.start + dir * farClip;

  return result;
}

internal void EntitySelection(
    GameState *gameState, float frameBufferWidth, float frameBufferHeight)
{
    float x = (float)gameState->inputSystem.mouseX;
    float y = (float)gameState->inputSystem.mouseY;

    LineSegment lineSegment = GenerateLineSegment(x, y,
            frameBufferWidth, frameBufferHeight, g_Fov,
            g_NearClip, g_FarClip, gameState->camera);

    Debug_DrawLine(lineSegment.start, lineSegment.end, Vec3(1, 0, 0),
            20.0f);

    RaycastResult ignored;
    EntityId entity =
        RaycastWorld(&gameState->entitySystem, &gameState->testArena,
            lineSegment.start, lineSegment.end, NULL_ENTITY, &ignored);

    gameState->selectedEntity = entity;
}


inline float SnapToIncrement(float x, float increment)
{
    float scaledX = x * (1.0f / increment);
    float roundedX = Round(scaledX);
    float rescaledX = roundedX * increment;
    return rescaledX;
}

internal void Editor_SaveMap(GameState *gameState, GameMemory *memory)
{
    char stringBuffer[0x1000];
    StringBuilder builder =
        CreateStringBuilder(stringBuffer, sizeof(stringBuffer));
    DumpEntitySystem(&builder, &gameState->entitySystem);
    if (builder.length > 0)
    {
        memory->writeFile("test.map", builder.buffer,
                builder.length - 1); // Remote NUL terminator
    }
    LOG_DEBUG("Map saved!");
}

internal void Editor_NewMap(GameState *gameState)
{
    ecs_DeleteAllEntities(&gameState->entitySystem);
}

internal void DoEditorStuff(GameState *gameState, GameMemory *memory,
    float frameBufferWidth, float frameBufferHeight)
{
    InputSystem *inputSystem = &gameState->inputSystem;
    float x = (float)gameState->inputSystem.mouseX;
    float y = (float)gameState->inputSystem.mouseY;

    switch (gameState->editorMode)
    {
        case EDITOR_MODE_SELECTING:
        {
            if (WasKeyPressed(inputSystem, K_F9))
            {
                Editor_SaveMap(gameState, memory);
            }
            if (WasKeyPressed(inputSystem, K_F11))
            {
                Editor_NewMap(gameState);
            }

            if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
            {
                EntitySelection(gameState, frameBufferWidth, frameBufferHeight);
            }

            bool beginTranslation = false;
            if (WasKeyPressed(inputSystem, K_C))
            {
                if (gameState->selectedEntity != NULL_ENTITY)
                {
                    // Clone selected entity
                    EntityDescription desc = {};
                    CreateDescriptionOfEntity(&gameState->entitySystem,
                        gameState->selectedEntity, &desc);
                    EntityId entity = CreateEntityFromDescription(
                        &gameState->entitySystem, &desc);
                    gameState->selectedEntity = entity;
                    gameState->editorMode = EDITOR_MODE_SELECTING;
                    beginTranslation = true;
                }
            }

            if (gameState->selectedEntity != NULL_ENTITY)
            {
                vec3 position;
                ecs_GetComponent(&gameState->entitySystem,
                        PositionComponentTypeId, &gameState->selectedEntity, 1,
                        &position, sizeof(position));

                if (WasKeyPressed(inputSystem, K_BACKSPACE))
                {
                    ecs_DeleteEntities(&gameState->entitySystem,
                        &gameState->selectedEntity, 1);
                    gameState->selectedEntity = NULL_ENTITY;
                }

                if (WasKeyPressed(inputSystem, K_T) || beginTranslation)
                {
                    gameState->editorMode = EDITOR_MODE_TRANSLATING;
                    gameState->entityBegin = position;

                    if (IsKeyDown(inputSystem, K_LEFT_SHIFT))
                    {
                        gameState->isVerticalTranslation = true;
                        vec3 d = gameState->camera.position - position;
                        d.y = 0.0f;
                        gameState->translationPlane =
                            CreatePlane(Normalize(d), position);
                    }
                    else
                    {
                        gameState->isVerticalTranslation = false;
                        gameState->translationPlane =
                            CreatePlane(Vec3(0, 1, 0), position);
                    }

                    LineSegment lineSegment = GenerateLineSegment(x, y,
                        frameBufferWidth, frameBufferHeight, g_Fov, g_NearClip,
                        g_FarClip, gameState->camera);

                    vec3 intersectionPoint;
                    if (RayPlaneTest(gameState->translationPlane,
                            lineSegment.start, lineSegment.end,
                            &intersectionPoint))
                    {
                        gameState->translationBegin = intersectionPoint;
                    }
                }

                if (WasKeyPressed(inputSystem, K_U))
                {
                    bool hasScaleComponent = false;
                    ecs_HasComponent(&gameState->entitySystem,
                        ScaleComponentTypeId, &gameState->selectedEntity, 1,
                        &hasScaleComponent);
                    if (hasScaleComponent)
                    {
                        vec3 scale;
                        ecs_GetComponent(&gameState->entitySystem,
                            ScaleComponentTypeId, &gameState->selectedEntity, 1,
                            &scale, sizeof(scale));

                        gameState->editorMode = EDITOR_MODE_SCALING;
                        gameState->entityBeginScale = scale;
                        gameState->scaleMask = Vec3(1.0f);
                        vec3 d = gameState->camera.position - position;
                        gameState->translationPlane = // TODO: Scaling plane
                            CreatePlane(Normalize(d), position);

                        LineSegment lineSegment = GenerateLineSegment(x, y,
                            frameBufferWidth, frameBufferHeight, g_Fov,
                            g_NearClip, g_FarClip, gameState->camera);

                        vec3 intersectionPoint;
                        if (RayPlaneTest(gameState->translationPlane,
                                lineSegment.start, lineSegment.end,
                                &intersectionPoint))
                        {
                            vec3 delta = intersectionPoint - position;
                            gameState->scalingBegin = Length(delta);
                        }
                    }
                }
            }
        }
        break;
        case EDITOR_MODE_SPAWNING:
            if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
            {
                LineSegment lineSegment = GenerateLineSegment(x, y,
                    frameBufferWidth, frameBufferHeight, g_Fov, g_NearClip,
                    g_FarClip, gameState->camera);

                vec3 spawnPosition = {};

                RaycastResult result;
                if (RaycastWorld(&gameState->entitySystem,
                        &gameState->testArena, lineSegment.start,
                        lineSegment.end, NULL_ENTITY, &result) != NULL_ENTITY)
                {
                    spawnPosition = result.hitPoint;
                }
                else
                {
                    vec3 v = Normalize(lineSegment.end - lineSegment.start);
                    spawnPosition = lineSegment.start + v * 10.0f;
                }

                EntityId entity =
                    CreateEntityFromDescription(&gameState->entitySystem,
                        g_EntityDescriptions + gameState->entityDescToSpawn);

                ecs_SetComponent(&gameState->entitySystem,
                    PositionComponentTypeId, &entity, 1, &spawnPosition,
                    sizeof(spawnPosition));

                gameState->selectedEntity = entity;
                gameState->editorMode = EDITOR_MODE_SELECTING;
            }
            break;
        case EDITOR_MODE_TRANSLATING:
        {
            Assert(gameState->selectedEntity != NULL_ENTITY);
            LineSegment lineSegment =
                GenerateLineSegment(x, y, frameBufferWidth, frameBufferHeight,
                    g_Fov, g_NearClip, g_FarClip, gameState->camera);

            vec3 intersectionPoint;
            if (RayPlaneTest(gameState->translationPlane, lineSegment.start,
                    lineSegment.end, &intersectionPoint))
            {
                gameState->translationEnd = intersectionPoint;
            }

            vec3 delta =
                gameState->translationEnd - gameState->translationBegin;
            if (gameState->isVerticalTranslation)
            {
                delta.x = 0.0f;
                delta.z = 0.0f;
            }
            vec3 newPosition = gameState->entityBegin + delta;

            if (!IsKeyDown(inputSystem, K_LEFT_ALT))
            {
                float snapDistance = 0.5f;

                if (gameState->isVerticalTranslation)
                {
                    newPosition.y =
                        SnapToIncrement(newPosition.y, snapDistance);
                }
                else
                {
                    newPosition.x =
                        SnapToIncrement(newPosition.x, snapDistance);
                    newPosition.z =
                        SnapToIncrement(newPosition.z, snapDistance);
                }
            }

            ecs_SetComponent(&gameState->entitySystem, PositionComponentTypeId,
                &gameState->selectedEntity, 1, &newPosition,
                sizeof(newPosition));
            if (WasKeyPressed(inputSystem, K_ESCAPE))
            {
                gameState->editorMode = EDITOR_MODE_SELECTING;
                ecs_SetComponent(&gameState->entitySystem,
                    PositionComponentTypeId, &gameState->selectedEntity, 1,
                    &gameState->entityBegin, sizeof(gameState->entityBegin));
            }
            if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
            {
                gameState->editorMode = EDITOR_MODE_SELECTING;
            }
        }
            break;
        case EDITOR_MODE_SCALING:
        {
            Assert(gameState->selectedEntity != NULL_ENTITY);

            vec3 mask = gameState->scaleMask;
            if (WasKeyPressed(inputSystem, K_X))
            {
                mask = IsKeyDown(inputSystem, K_LEFT_SHIFT)
                           ? Vec3(0.0f, 1.0f, 1.0f)
                           : Vec3(1.0f, 0.0f, 0.0f);
            }
            if (WasKeyPressed(inputSystem, K_Y))
            {
                mask = IsKeyDown(inputSystem, K_LEFT_SHIFT)
                           ? Vec3(1.0f, 0.0f, 1.0f)
                           : Vec3(0.0f, 1.0f, 0.0f);
            }
            if (WasKeyPressed(inputSystem, K_Z))
            {
                mask = IsKeyDown(inputSystem, K_LEFT_SHIFT)
                           ? Vec3(1.0f, 1.0f, 0.0f)
                           : Vec3(0.0f, 0.0f, 1.0f);
            }
            gameState->scaleMask = mask;

            vec3 position;
            ecs_GetComponent(&gameState->entitySystem,
                    PositionComponentTypeId, &gameState->selectedEntity, 1,
                    &position, sizeof(position));

            LineSegment lineSegment =
                GenerateLineSegment(x, y, frameBufferWidth, frameBufferHeight,
                    g_Fov, g_NearClip, g_FarClip, gameState->camera);

            vec3 intersectionPoint;
            if (RayPlaneTest(gameState->translationPlane, lineSegment.start,
                    lineSegment.end, &intersectionPoint))
            {
                vec3 delta = intersectionPoint - position;
                gameState->scalingEnd = Length(delta);
            }

            float delta = Max(
                gameState->scalingEnd - gameState->scalingBegin + 1.0f, 0.0f);

            vec3 newScale = gameState->entityBeginScale * delta;
            vec3 maskedScale = Lerp(gameState->entityBeginScale, newScale, mask);
            ecs_SetComponent(&gameState->entitySystem,
                    ScaleComponentTypeId, &gameState->selectedEntity, 1,
                    &maskedScale, sizeof(maskedScale));

            if (WasKeyPressed(inputSystem, K_ESCAPE))
            {
                gameState->editorMode = EDITOR_MODE_SELECTING;
                ecs_SetComponent(&gameState->entitySystem, ScaleComponentTypeId,
                    &gameState->selectedEntity, 1, &gameState->entityBeginScale,
                    sizeof(gameState->entityBeginScale));
            }
            if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
            {
                gameState->editorMode = EDITOR_MODE_SELECTING;
            }

        }
            break;
        default:
            InvalidCodePath();
            break;
    }

    if (gameState->selectedEntity != NULL_ENTITY)
    {
        vec3 position;
        ecs_GetComponent(&gameState->entitySystem, PositionComponentTypeId,
            &gameState->selectedEntity, 1, &position, sizeof(position));

        bool hasScaleComponent = false;
        ecs_HasComponent(&gameState->entitySystem, ScaleComponentTypeId,
                &gameState->selectedEntity, 1, &hasScaleComponent);

        bool hasBoxHalfExtents = false;
        ecs_HasComponent(&gameState->entitySystem, BoxHalfExtentsComponentTypeId,
                &gameState->selectedEntity, 1, &hasBoxHalfExtents);

        if (hasScaleComponent && hasBoxHalfExtents)
        {
            vec3 scale;
            ecs_GetComponent(&gameState->entitySystem, ScaleComponentTypeId,
                &gameState->selectedEntity, 1, &scale, sizeof(scale));

            vec3 boxHalfExtents;
            ecs_GetComponent(&gameState->entitySystem,
                BoxHalfExtentsComponentTypeId, &gameState->selectedEntity, 1,
                &boxHalfExtents, sizeof(boxHalfExtents));

            AabbShape aabb;
            aabb.min = position - boxHalfExtents * scale;
            aabb.max = position + boxHalfExtents * scale;
            Debug_DrawAabb(aabb.min, aabb.max, Vec3(0.2, 0.6, 1));
        }
        else
        {
            mat4 transform = Translate(position);
            Debug_DrawAxis(transform);
        }
    }
}
