global u32 g_DataTypeLengths[MAX_DATA_TYPES];

inline u32 GetDataTypeLength(u32 dataType)
{
    Assert(dataType < ArrayCount(g_DataTypeLengths));
    return g_DataTypeLengths[dataType];
}

internal void PopulateDataTypeLengthsTable()
{
    g_DataTypeLengths[DataType_None] = 0;
    g_DataTypeLengths[DataType_Vec3] = sizeof(vec3);
    g_DataTypeLengths[DataType_Vec2] = sizeof(vec2);
    g_DataTypeLengths[DataType_U32] = sizeof(u32);
    g_DataTypeLengths[DataType_F32] = sizeof(float);
    g_DataTypeLengths[DataType_U8] = sizeof(u8);
    g_DataTypeLengths[DataType_Quat] = sizeof(quat);
}

internal void PrintDataTypeToBuffer(u32 dataType, void *value, char *buffer, u32 length)
{
    switch (dataType)
    {
    case DataType_None:
        strcpy(buffer, "{}");
        break;
    case DataType_Vec3:
    {
        vec3 *v = (vec3 *)value;
        snprintf(buffer, length, "[%g, %g, %g]", v->x, v->y, v->z);
    }
    break;
    case DataType_Vec2:
    {
        vec2 *v = (vec2 *)value;
        snprintf(buffer, length, "[%g, %g]", v->x, v->y);
    }
    break;
    case DataType_U32:
    {
        u32 *u = (u32 *)value;
        snprintf(buffer, length, "%u", *u);
    }
    break;
    case DataType_F32:
    {
        f32 *f = (f32 *)value;
        snprintf(buffer, length, "%g", *f);
    }
    break;
    case DataType_U8:
    {
        u8 *u = (u8 *)value;
        snprintf(buffer, length, "%u", *u);
    }
    break;
    case DataType_Quat:
    {
        quat *q = (quat *)value;
        snprintf(buffer, length, "[%g, %g, %g, %g]", q->x, q->y, q->z, q->w);
    }
    break;
    default:
        strcpy(buffer, "UNKNOWN");
        break;
    }
}

inline vec2 ReadVec2(String str)
{
    vec2 result = {};
    str = ConsumeNonNumericCharacters(str);
    char *y = NULL;
    result.x = strtof(str.data, &y);
    str = CreateString(y, SafeTruncateU64ToU32(str.length - (y - str.data)));
    str = ConsumeNonNumericCharacters(str);
    result.y = strtof(str.data, NULL);

    return result;
}

inline vec3 ReadVec3(String str)
{
    vec3 result = {};
    str = ConsumeNonNumericCharacters(str);
    char *y = NULL;
    char *z = NULL;
    result.x = strtof(str.data, &y);
    str = CreateString(y, SafeTruncateU64ToU32(str.length - (y - str.data)));
    str = ConsumeNonNumericCharacters(str);
    result.y = strtof(str.data, &z);
    str = CreateString(z, SafeTruncateU64ToU32(str.length - (z - str.data)));
    str = ConsumeNonNumericCharacters(str);
    result.z = strtof(str.data, NULL);

    return result;
}

inline vec4 ReadVec4(String str)
{
    vec4 result = {};
    str = ConsumeNonNumericCharacters(str);
    char *y = NULL;
    char *z = NULL;
    char *w = NULL;
    result.x = strtof(str.data, &y);
    str = CreateString(y, SafeTruncateU64ToU32(str.length - (y - str.data)));
    str = ConsumeNonNumericCharacters(str);
    result.y = strtof(str.data, &z);
    str = CreateString(z, SafeTruncateU64ToU32(str.length - (z - str.data)));
    str = ConsumeNonNumericCharacters(str);
    result.z = strtof(str.data, &w);
    str = CreateString(w, SafeTruncateU64ToU32(str.length - (w - str.data)));
    str = ConsumeNonNumericCharacters(str);
    result.w = strtof(str.data, NULL);

    return result;
}

internal u32 ReadDataType(String str, u32 dataType, u8 *data, u32 capacity)
{
    u32 length = GetDataTypeLength(dataType);

    switch (dataType)
    {
    case DataType_Vec3:
    {
        vec3 v = ReadVec3(str);
        memcpy(data, &v, sizeof(v));
    }
    break;
    case DataType_Vec2:
    {
        vec2 v = ReadVec2(str);
        memcpy(data, &v, sizeof(v));
    }
    break;
    case DataType_None:
        break;
    case DataType_U32:
    {
        Assert(length == sizeof(u32));
        u32 u = StringToU32(str);
        memcpy(data, &u, sizeof(u32));
    }
    break;
    case DataType_F32:
    {
        float f = strtof(str.data, NULL);
        memcpy(data, &f, sizeof(f));
    }
    break;
    case DataType_U8:
    {
        Assert(length == sizeof(u8));
        u8 u = SafeTruncateU32ToU8(StringToU32(str));
        memcpy(data, &u, sizeof(u8));
    }
    break;
    case DataType_Quat:
    {
        Assert(length == sizeof(quat));
        vec4 v = ReadVec4(str);
        quat q = Quat(v.x, v.y, v.z, v.w);
        q = Normalize(q);
        memcpy(data, &q, sizeof(quat));
    }
    break;
    default:
        InvalidCodePath();
        break;
    }

    return length;
}
