#if 0
inline vec3 CalculateLocalVelocity(
    ContactResolutionData *data, RigidBody *body, u32 bodyIdx, float dt)
{
    vec3 velocity =
        Cross(body->rotation, data->relativeContactPositions[bodyIdx]);
    velocity += body->velocity;

    mat3 worldToContact = Transpose(data->contactToWorld);
    vec3 contactVelocity = worldToContact * velocity;

    return contactVelocity;
}

inline float CalculateDesiredDeltaVelocity(
    ContactResolutionData *data, float dt)
{
    float velocityLimit = 0.25f;
    float restitution = data->restitution;

    if (Abs(data->contactVelocity.x) < velocityLimit)
    {
        restitution = 0.0f;
    }

    float desiredDeltaVelocity =
        -data->contactVelocity.x - restitution * data->contactVelocity.x;

    return desiredDeltaVelocity;
}

internal void PrepareContacts(ContactResolutionData *contactData,
    RigidBodyContact *contacts, u32 contactCount, RigidBody *rigidBodies,
    u32 rigidBodyCount, float dt)
{
    for (u32 i = 0; i < contactCount; ++i)
    {
        RigidBodyContact *contact = contacts + i;
        ContactResolutionData *data = contactData + i;

        data->point = contact->point;
        data->normal = contact->normal;
        data->penetration = contact->penetration;
        data->restitution = contact->restitution;

        Assert(contact->indices[0] >= 0);
        data->indices[0] = contact->indices[0];
        data->indices[1] = contact->indices[1];

        // Calculate contactToWorld matrix
        {
            vec3 x = contact->normal;
            vec3 z = Cross(x, Vec3(0, 1, 0));
            if (LengthSq(z) < 0.00001f)
            {
                z = Cross(x, Vec3(1, 0, 0));
                Assert(LengthSq(z) > 0.00001f);
            }
            z = Normalize(z);
            vec3 y = Normalize(Cross(z, x));

            data->contactToWorld.col[0] = x;
            data->contactToWorld.col[1] = y;
            data->contactToWorld.col[2] = z;
        }

        RigidBody *body0 = rigidBodies + data->indices[0];
        RigidBody *body1 = NULL;
        if (data->indices[1] >= 0)
        {
            body1 = rigidBodies + data->indices[1];
        }

        data->relativeContactPositions[0] = data->point - body0->position;
        if (body1 != NULL)
        {
            data->relativeContactPositions[1] =
                data->point - body1->position;
        }

        // Calculates the closing velocity
        data->contactVelocity = CalculateLocalVelocity(data, body0, 0, dt);
        if (body1 != NULL)
        {
            data->contactVelocity -= CalculateLocalVelocity(data, body1, 1, dt);
        }

        data->desiredDeltaVelocity = CalculateDesiredDeltaVelocity(data, dt);
    }
}

inline void LimitAngularMovement(
    vec3 relativeContactPosition, float *linearMovement, float *angularMovement)
{
    float angularLimitConstant = 0.2f;
    float scaledAngularLimit =
        angularLimitConstant * Length(relativeContactPosition);

    if (Abs(*angularMovement) > scaledAngularLimit)
    {
        float totalMovement = *linearMovement + *angularMovement;

        if (*angularMovement >= 0)
        {
            *angularMovement = scaledAngularLimit;
        }
        else
        {
            *angularMovement = -scaledAngularLimit;
        }

        *linearMovement = totalMovement - *angularMovement;
    }
}

inline vec3 ApplyAngularMovement(vec3 relativeContactPosition,
    vec3 contactNormal, RigidBody *body, float angularInertia,
    float angularMovement)
{
    vec3 impulsiveTorque =
        Cross(relativeContactPosition, contactNormal);
    vec3 impulsePerMove = body->inverseInertiaTensor * impulsiveTorque;

    vec3 rotationPerMove = impulsePerMove * (1.0f / angularInertia);
    vec3 rotation = rotationPerMove * angularMovement;

    vec3 axis = Normalize(rotation);
    float angle = Length(rotation);
    quat q = Quat(axis, angle);
    body->orientation *= q;

    return rotation;
}

internal void AdjustPositions(ContactResolutionData *contactData,
    u32 contactCount, RigidBody *rigidBodies, u32 rigidBodyCount, float dt)
{
    vec3 linearChange[2] = {};
    vec3 angularChange[2] = {};

    u32 maxIterations = 32;
    for (u32 iterationCount = 0; iterationCount < maxIterations; ++iterationCount)
    {
        ContactResolutionData *maxContact = NULL;
        float maxPenetration = 0.0f;
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            ContactResolutionData *contact = contactData + contactIdx;
            if (contact->penetration > maxPenetration)
            {
                maxContact = contact;
                maxPenetration = contact->penetration;
            }
        }

        if (maxContact == NULL)
            break;

        ContactResolutionData *contact = maxContact;

        // Calculate linear and angular inertia
        float angularInertia[2] = {};
        float linearInertia[2] = {};
        float totalInertia = 0.0f;

        RigidBody *bodies[2];
        bodies[0] = rigidBodies + contact->indices[0];
        bodies[1] =
            contact->indices[1] >= 0 ? rigidBodies + contact->indices[1] : NULL;

        for (u32 idx = 0; idx < 2; ++idx)
        {
            if (bodies[idx] != NULL)
            {
                vec3 angularInertiaLocal = Cross(
                    contact->relativeContactPositions[idx], contact->normal);

                vec3 angularInertiaWorld =
                    bodies[idx]->inverseInertiaTensorWorld *
                    angularInertiaLocal;

                angularInertiaWorld = Cross(angularInertiaWorld,
                    contact->relativeContactPositions[idx]);

                angularInertia[idx] = Dot(angularInertiaWorld, contact->normal);

                linearInertia[idx] = bodies[idx]->inverseMass;

                totalInertia += linearInertia[idx] + angularInertia[idx];
            }
        }

        float inverseInertia = 1.0f / totalInertia;
        float linearMovement[2] = {};
        float angularMovement[2] = {};

        linearMovement[0] =
            contact->penetration * linearInertia[0] * inverseInertia;
        linearMovement[1] =
            -contact->penetration * linearInertia[1] * inverseInertia;
        angularMovement[0] =
            contact->penetration * angularInertia[0] * inverseInertia;
        angularMovement[1] =
            -contact->penetration * angularInertia[1] * inverseInertia;

        // Prevent over-rotation
        LimitAngularMovement(contact->relativeContactPositions[0],
            &linearMovement[0], &angularMovement[0]);
        LimitAngularMovement(contact->relativeContactPositions[1],
            &linearMovement[1], &angularMovement[1]);

        // Apply linear movement and angular movement
        linearChange[0] = contact->normal * linearMovement[0];
        bodies[0]->position += linearChange[0];

        angularChange[0] = ApplyAngularMovement(
            contact->relativeContactPositions[0], contact->normal, bodies[0],
            angularInertia[0], angularMovement[0]);

        if (bodies[1] != NULL)
        {
            linearChange[1] = contact->normal * linearMovement[1];
            bodies[1]->position += linearChange[1];

            angularChange[1] = ApplyAngularMovement(
                contact->relativeContactPositions[1], contact->normal,
                bodies[1], angularInertia[1], angularMovement[1]);
        }

        // Update penetration values
        ContactResolutionData *resolvedContact = maxContact;
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            ContactResolutionData *contactToUpdate = &contactData[contactIdx];

            if (contactToUpdate->indices[0] == resolvedContact->indices[0])
            {
                vec3 deltaPosition =
                    linearChange[0] +
                    Cross(angularChange[0],
                        contactToUpdate->relativeContactPositions[0]);
                contactToUpdate->penetration -=
                    Dot(deltaPosition, contactToUpdate->normal);
            }

            if (contactToUpdate->indices[0] == resolvedContact->indices[1])
            {
                vec3 deltaPosition =
                    linearChange[1] +
                    Cross(angularChange[1],
                        contactToUpdate->relativeContactPositions[0]);
                contactToUpdate->penetration +=
                    Dot(deltaPosition, contactToUpdate->normal);
            }

            if (contactToUpdate->indices[1] >= 0)
            {
                if (contactToUpdate->indices[1] == resolvedContact->indices[0])
                {
                    vec3 deltaPosition =
                        linearChange[0] +
                        Cross(angularChange[0],
                                contactToUpdate->relativeContactPositions[1]);
                    contactToUpdate->penetration -=
                        Dot(deltaPosition, contactToUpdate->normal);
                }

                if (contactToUpdate->indices[1] == resolvedContact->indices[1])
                {
                    vec3 deltaPosition =
                        linearChange[1] +
                        Cross(angularChange[1],
                            contactToUpdate->relativeContactPositions[1]);
                    contactToUpdate->penetration +=
                        Dot(deltaPosition, contactToUpdate->normal);
                }
            }
        }
    }
}

inline vec3 CalculateFrictionlessImpulse(
    ContactResolutionData *contact, RigidBody *body0, RigidBody *body1)
{
    // Calculate linear and angular component for body0
    vec3 torquePerUnitImpulse =
        Cross(contact->relativeContactPositions[0], contact->normal);
    vec3 rotationPerUnitImpulse =
        body0->inverseInertiaTensorWorld * torquePerUnitImpulse;
    vec3 velocityPerUnitImpulse =
        Cross(rotationPerUnitImpulse, contact->relativeContactPositions[0]);

    //vec3 velocityPerUnitImpulseContact =
        //Transpose(contact->contactToWorld) * velocityPerUnitImpulse;
    //float angularComponent = velocityPerUnitImpulseContact.x;
    float angularComponent = Dot(velocityPerUnitImpulse, contact->normal);
    float linearComponent = body0->inverseMass;

    float totalDeltaV = angularComponent + linearComponent;

    if (body1 != NULL)
    {
        // Calculate linear and angular component for body1
        vec3 torquePerUnitImpulse =
            Cross(contact->relativeContactPositions[1], contact->normal);
        vec3 rotationPerUnitImpulse =
            body1->inverseInertiaTensorWorld * torquePerUnitImpulse;
        vec3 velocityPerUnitImpulse =
            Cross(rotationPerUnitImpulse, contact->relativeContactPositions[1]);

        float angularComponent = Dot(velocityPerUnitImpulse, contact->normal);
        float linearComponent = body1->inverseMass;

        totalDeltaV += angularComponent + linearComponent;
    }

    vec3 impulseContact = Vec3(0, 0, 0);
    impulseContact.x = contact->desiredDeltaVelocity / totalDeltaV;

    LOG_DEBUG("desiredDeltaVelocity: %g totalDeltaV: %g",
            contact->desiredDeltaVelocity, totalDeltaV);
    LOG_DEBUG("impulseContact.x = %g", impulseContact.x);

    return impulseContact;
}

struct VelocityAndRotationChange
{
    vec3 rotationChange[2];
    vec3 velocityChange[2];
};

internal VelocityAndRotationChange ApplyVelocityChange(
    ContactResolutionData *contact, RigidBody *rigidBodies, u32 rigidBodyCount)
{
    vec3 rotationChange[2] = {};
    vec3 velocityChange[2] = {};

    RigidBody *body0 = rigidBodies + contact->indices[0];
    RigidBody *body1 =
        (contact->indices[1] >= 0) ? rigidBodies + contact->indices[1] : NULL;

    // calculate frictionless impulse
    vec3 impulseContact = CalculateFrictionlessImpulse(contact, body0, body1);

    vec3 impulse = contact->contactToWorld * impulseContact;
    velocityChange[0] = impulse * body0->inverseMass;

    vec3 impulsiveTorque = Cross(impulse, contact->relativeContactPositions[0]);
    rotationChange[0] = body0->inverseInertiaTensorWorld * impulsiveTorque;

    LOG_DEBUG("rotationChange: %g %g %g", rotationChange[0].x,
        rotationChange[0].y, rotationChange[0].z);

    //rotationChange[0] = Vec3(0.0f); //body0->inverseInertiaTensorWorld * impulsiveTorque;

    LOG_DEBUG("Velocity Change: %g %g %g", velocityChange[0].x,
        velocityChange[0].y, velocityChange[0].z);
    body0->velocity += velocityChange[0];
    body0->rotation += rotationChange[0];
    //LOG_DEBUG("New velocity: %g %g %g",
            //body0->velocity.x,
            //body0->velocity.y,
            //body0->velocity.z);

    if (body1 != NULL)
    {
        vec3 impulsiveTorque =
            Cross(-impulse, contact->relativeContactPositions[1]);
        rotationChange[1] = body1->inverseInertiaTensorWorld * impulsiveTorque;
        velocityChange[1] = impulse * body1->inverseMass;

        body1->velocity += velocityChange[1];
        body1->rotation += rotationChange[1];
    }

    VelocityAndRotationChange result = {};
    result.velocityChange[0] = velocityChange[0];
    result.velocityChange[1] = velocityChange[1];
    result.rotationChange[0] = rotationChange[0];
    result.rotationChange[1] = rotationChange[1];

    return result;
}

internal void AdjustVelocities(ContactResolutionData *contactData,
    u32 contactCount, RigidBody *rigidBodies, u32 rigidBodyCount, float dt)
{
    vec3 velocityChange[2] = {};
    vec3 rotationChange[2] = {};

    u32 maxIterations = 1;
    for (u32 iterationCount = 0; iterationCount < maxIterations; ++iterationCount)
    {
        ContactResolutionData *maxContact = NULL;
        float maxClosingVelocity = 0.05f;
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            ContactResolutionData *contact = contactData + contactIdx;
            if (contact->desiredDeltaVelocity > maxClosingVelocity)
            {
                maxContact = contact;
                maxClosingVelocity = contact->desiredDeltaVelocity;
            }
        }

        if (maxContact == NULL)
            break;

        // Apply velocity change
        VelocityAndRotationChange velocityAndRotationChange =
            ApplyVelocityChange(maxContact, rigidBodies, rigidBodyCount);

        vec3 *velocityChange = &velocityAndRotationChange.velocityChange[0];
        vec3 *rotationChange = &velocityAndRotationChange.rotationChange[0];

        LOG_DEBUG("[%u] - %g %g %g", iterationCount, velocityChange[0].x,
                velocityChange[0].y, velocityChange[0].z);

        // Recompute relative closing velocities
        ContactResolutionData *resolvedContact = maxContact;
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            ContactResolutionData *contactToUpdate = &contactData[contactIdx];

            if (contactToUpdate->indices[0] == resolvedContact->indices[0])
            {
                vec3 deltaVelocity =
                    velocityChange[0] +
                    Cross(rotationChange[0],
                        contactToUpdate->relativeContactPositions[0]);
                LOG_DEBUG("DeltaVelocity: %g %g %g",
                        deltaVelocity.x, deltaVelocity.y, deltaVelocity.z);
                LOG_DEBUG("contactVelocity before: %g %g %g",
                        contactToUpdate->contactVelocity.x,
                        contactToUpdate->contactVelocity.y,
                        contactToUpdate->contactVelocity.z);
                contactToUpdate->contactVelocity +=
                    Transpose(contactToUpdate->contactToWorld) * deltaVelocity;
                LOG_DEBUG("contactVelocity after: %g %g %g",
                        contactToUpdate->contactVelocity.x,
                        contactToUpdate->contactVelocity.y,
                        contactToUpdate->contactVelocity.z);
            }

            if (contactToUpdate->indices[0] == resolvedContact->indices[1])
            {
                vec3 deltaVelocity =
                    velocityChange[1] +
                    Cross(rotationChange[1],
                        contactToUpdate->relativeContactPositions[0]);
                contactToUpdate->contactVelocity +=
                    Transpose(contactToUpdate->contactToWorld) * deltaVelocity;
            }

            if (contactToUpdate->indices[1] >= 0)
            {
                if (contactToUpdate->indices[1] == resolvedContact->indices[0])
                {
                    vec3 deltaVelocity =
                        velocityChange[0] +
                        Cross(rotationChange[0],
                            contactToUpdate->relativeContactPositions[1]);
                    contactToUpdate->contactVelocity +=
                        Transpose(contactToUpdate->contactToWorld) *
                        deltaVelocity;
                }

                if (contactToUpdate->indices[1] == resolvedContact->indices[1])
                {
                    vec3 deltaVelocity =
                        velocityChange[1] +
                        Cross(rotationChange[1],
                            contactToUpdate->relativeContactPositions[1]);
                    contactToUpdate->contactVelocity +=
                        Transpose(contactToUpdate->contactToWorld) *
                        deltaVelocity;
                }
            }

            contactToUpdate->desiredDeltaVelocity =
                CalculateDesiredDeltaVelocity(contactToUpdate, dt);
        }
    }
}
#endif
