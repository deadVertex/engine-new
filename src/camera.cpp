struct Camera
{
    vec3 position;
    vec3 velocity;
    vec3 rotation;
    b32 isRotating;
    b32 toggleRotation;
};

internal void UpdateCamera(Camera *camera, InputSystem *inputSystem, float dt,
                           float windowWidth, float windowHeight)
{
    if (WasKeyPressed(inputSystem, K_C))
    {
        camera->isRotating = true;
        camera->toggleRotation = true;
    }
    if (WasKeyPressed(inputSystem, K_ESCAPE))
    {
        camera->isRotating = false;
        camera->toggleRotation = false;
    }

    if (IsKeyDown(inputSystem, K_MOUSE_BUTTON_RIGHT))
    {
        camera->isRotating = true;
        camera->toggleRotation = false;
    }
    else
    {
        if (!camera->toggleRotation)
        {
            camera->isRotating = false;
        }
    }

    if (camera->isRotating)
    {
        float sensitivity = 0.5f;
        float mouseX = (float)(inputSystem->mouseX - inputSystem->prevMouseX) /
                       windowWidth;
        float mouseY = (float)(inputSystem->mouseY - inputSystem->prevMouseY) /
                       windowHeight;
        vec3 cameraRotation = Vec3(mouseY * -2.0f * PI, -2.0f * PI * mouseX, 0);
        cameraRotation *= sensitivity;
        camera->rotation += cameraRotation;
        camera->rotation.x = Clamp(camera->rotation.x, -PI * 0.5f, PI * 0.5f);
        if (camera->rotation.y > PI)
        {
            camera->rotation.y -= 2.0f * PI;
        }
        else if (camera->rotation.y < -PI)
        {
            camera->rotation.y += 2.0f * PI;
        }
    }

    vec3 acceleration = {};
    float speed = 80.0f;
    if (IsKeyDown(inputSystem, K_W))
        acceleration.z -= 1.0f;
    if (IsKeyDown(inputSystem, K_S))
        acceleration.z += 1.0f;
    if (IsKeyDown(inputSystem, K_A))
        acceleration.x -= 1.0f;
    if (IsKeyDown(inputSystem, K_D))
        acceleration.x += 1.0f;

    if (IsKeyDown(inputSystem, K_LEFT_SHIFT))
        speed = 900.0f;

    if (Normalize(&acceleration))
    {
        acceleration *= speed;
        camera->velocity += acceleration * dt;
    }
    camera->velocity -= camera->velocity * 7.0f * dt;
    if (IsZero(camera->velocity))
    {
        camera->velocity = Vec3(0);
    }
    vec4 v = Vec4(camera->velocity, 0.0f);
    v = RotateY(camera->rotation.y) * RotateX(camera->rotation.x) * v;
    vec3 velocity = Vec3(v.x, v.y, v.z);
    camera->position += velocity * dt;
}

inline mat4 CreateCameraViewMatrix(Camera *camera)
{
    mat4 result = RotateX(-camera->rotation.x) * RotateY(-camera->rotation.y) *
                  Translate(-camera->position);

    return result;
}
