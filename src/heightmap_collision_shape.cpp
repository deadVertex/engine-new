struct QuadTreeNode
{
    i32 children[4];
    AabbShape aabb;
    b32 isLeaf;
};

struct HeightMapCollisionMesh
{
    HeightMap heightMap;
    QuadTreeNode *root;
    QuadTreeNode *nodes;
    u32 nodeCount;
    u32 gridDim;
    vec3 translation;
    float scaleXZ;
    float scaleY;
};

internal u32 CreateQuadTreeParentNodes(QuadTreeNode *nodes, u32 maxNodes, u32 width,
    u32 childOffset, u32 parentOffset)
{
    u32 nodeCount = 0;
    for (u32 y = 0; y < width; y+=2)
    {
        for (u32 x = 0; x < width; x+=2)
        {
            i32 children[4];
            children[0] = (i32)(y * width + x);
            children[1] = (i32)(y * width + (x + 1));
            children[2] = (i32)((y + 1) * width + (x + 1));
            children[3] = (i32)((y + 1) * width + x);

            for (u32 i = 0; i < 4; ++i)
            {
                children[i] += childOffset;
            }

            AabbShape aabbs[4];
            for (u32 i = 0; i < 4; ++i)
            {
                aabbs[i] = nodes[children[i]].aabb;
            }

            AabbShape combinedAabb = AabbMerge(
                aabbs[3], AabbMerge(aabbs[2], AabbMerge(aabbs[1], aabbs[0])));

            QuadTreeNode node = {};
            node.aabb = combinedAabb;
            node.isLeaf = false;

            for (u32 i = 0; i < 4; ++i)
            {
                node.children[i] = children[i];
            }

            u32 idx = parentOffset + nodeCount;
            Assert(idx < maxNodes);
            nodes[idx] = node;
            nodeCount++;
        }
    }

    return nodeCount;
}

internal HeightMapCollisionMesh BuildHeightMapCollisionMesh(
    HeightMap heightMap, MemoryArena *arena, vec3 translation, float scaleXZ, float scaleY)
{
    u32 gridDim = TERRAIN_GRID_DIM; // TODO: Assert must be a power of 2

    // For terrain
    u32 baseNodeCount = gridDim * gridDim;

    // FIXME: Probably an easy to calculate this correctly
    u32 maxNodes = baseNodeCount * 2;

    QuadTreeNode *nodes = AllocateArray(arena, QuadTreeNode, maxNodes);

    // Generate vertices for terrain heightmap
    for (u32 y = 0; y < gridDim; y++)
    {
        for (u32 x = 0; x < gridDim; x++)
        {
            float fx[2];
            float fy[2];
            fx[0] = x / (float)gridDim;
            fy[0] = y / (float)gridDim;
            fx[1] = (x + 1) / (float)gridDim;
            fy[1] = (y + 1) / (float)gridDim;

            float t[4];
            t[0] = HeightMapSampleBilinear(heightMap, fx[0], fy[0]);
            t[1] = HeightMapSampleBilinear(heightMap, fx[1], fy[0]);
            t[2] = HeightMapSampleBilinear(heightMap, fx[1], fy[1]);
            t[3] = HeightMapSampleBilinear(heightMap, fx[0], fy[1]);

            vec3 p[4];
            p[0] = Vec3(fx[0], t[0] * scaleY, -fy[0]);
            p[1] = Vec3(fx[1], t[1] * scaleY, -fy[0]);
            p[2] = Vec3(fx[1], t[2] * scaleY, -fy[1]);
            p[3] = Vec3(fx[0], t[3] * scaleY, -fy[1]);

            for (u32 i = 0; i < 4; ++i)
            {
                p[i] = p[i] * scaleXZ + translation;
            }

            AabbShape aabb;
            aabb.min = Min(p[3], Min(p[2], Min(p[1], p[0])));
            aabb.max = Max(p[3], Max(p[2], Max(p[1], p[0])));

            u32 idx = y * gridDim + x;
            nodes[idx].aabb = aabb;
            nodes[idx].isLeaf = true;
        }
    }

    u32 nodeCount = baseNodeCount;
    u32 width = gridDim;
    u32 childCount = baseNodeCount;
    u32 childOffset = 0;
    while (childCount >= 4)
    {
        u32 parentCount = CreateQuadTreeParentNodes(
            nodes, maxNodes, width, childOffset, childOffset + childCount);
        nodeCount += parentCount;

        childOffset += childCount;
        childCount = parentCount;
        width = width >> 1;
    }

    HeightMapCollisionMesh result = {};
    result.nodes = nodes;
    result.root = nodes + nodeCount - 1;
    result.nodeCount = nodeCount;
    result.heightMap = heightMap;
    result.gridDim = gridDim;
    result.translation = translation;
    result.scaleXZ = scaleXZ;
    result.scaleY = scaleY;

    return result;
}

void DrawHeightMapCollisionMesh(HeightMapCollisionMesh mesh)
{
    for (u32 i = 0; i < mesh.nodeCount; ++i)
    {
        AabbShape aabb = mesh.nodes[i].aabb;
        if (mesh.nodes[i].isLeaf)
        {
            vec3 color = mesh.nodes[i].isLeaf ? Vec3(0.9, 0.5, 0.1)
                                              : Vec3(0.1, 0.5, 0.9);
            DrawAabb(aabb, color);
        }
    }
}

void DrawHeightMapCollisionMeshTraversal(HeightMapCollisionMesh mesh, vec3 p)
{
    QuadTreeNode *node = mesh.root;
    Debug_DrawPoint(p, Vec3(1, 0, 0));

    if (!AabbContainsPoint(node->aabb, p))
        return;

    u32 iteration = 0;
    while (node != NULL)
    {
        vec3 color = Vec3(0.1, 0.5, (iteration % 10) * 0.1f);
        DrawAabb(node->aabb, color);

        if (node->isLeaf)
        {
            break;
        }
        else
        {
            bool found = false;
            for (u32 i = 0; i < 4; ++i)
            {
                QuadTreeNode *child = mesh.nodes + node->children[i];
                if (AabbContainsPoint(child->aabb, p))
                {
                    node = child;
                    found = true;
                    break;
                }
            }

            if (!found)
                break;
        }

        iteration++;
    }
}

// TODO: Pass mesh via pointer as it is starting to get reasonably large
internal bool RayIntersectHeightMapMesh(HeightMapCollisionMesh mesh, vec3 start,
    vec3 end, RaycastResult *raycastResult)
{
    QuadTreeNode *node = mesh.root;

    RaycastResult ignored = {};
    if (!RayAabbTest(node->aabb, start, end, &ignored))
        return false;

    bool result = false;

    float tmin = 1.0f;
    vec3 intersectionNormal = {};
    u32 iteration = 0;
    while (node != NULL)
    {
        vec3 color = Vec3(0.1, 0.5, (iteration % 10) * 0.1f);
        //DrawAabb(node->aabb, color);

        if (node->isLeaf)
        {
            // FIXME: Should probably use size_t
            u32 idx = (u32)(node - mesh.nodes);
            u32 y = idx / mesh.gridDim;
            u32 x = idx % mesh.gridDim;

            float fx[2];
            float fy[2];
            fx[0] = x / (float)mesh.gridDim;
            fy[0] = y / (float)mesh.gridDim;
            fx[1] = (x + 1) / (float)mesh.gridDim;
            fy[1] = (y + 1) / (float)mesh.gridDim;

            float t[4];
            t[0] = HeightMapSampleBilinear(mesh.heightMap, fx[0], fy[0]);
            t[1] = HeightMapSampleBilinear(mesh.heightMap, fx[1], fy[0]);
            t[2] = HeightMapSampleBilinear(mesh.heightMap, fx[1], fy[1]);
            t[3] = HeightMapSampleBilinear(mesh.heightMap, fx[0], fy[1]);

            vec3 p[4];
            p[0] = Vec3(fx[0], t[0] * mesh.scaleY, -fy[0]);
            p[1] = Vec3(fx[1], t[1] * mesh.scaleY, -fy[0]);
            p[2] = Vec3(fx[1], t[2] * mesh.scaleY, -fy[1]);
            p[3] = Vec3(fx[0], t[3] * mesh.scaleY, -fy[1]);

            for (u32 i = 0; i < 4; ++i)
            {
                p[i] = p[i] * mesh.scaleXZ + mesh.translation;
            }
            
            // Generate triangles to perform the ray intersection test
            TriangleShape triangles[2];
            triangles[0].a = p[0];
            triangles[0].b = p[1];
            triangles[0].c = p[3];

            triangles[1].a = p[1];
            triangles[1].b = p[2];
            triangles[1].c = p[3];

            for (u32 i = 0; i < 2; ++i)
            {
                //DrawTriangle(triangles[i], Vec3(0.9, 0.6, 0.1), Vec3(1, 0, 1));

                float tFirst;
                vec3 potentialPoint = {};
                vec3 potentialNormal = {};
                if (RayTriangleTest(triangles[i], start, end, &potentialPoint,
                        &potentialNormal, &tFirst))
                {
                    if (tFirst < tmin)
                    {
                        tmin = tFirst;
                        intersectionNormal = potentialNormal;
                        result = true;
                    }
                }
            }

            break;
        }
        else
        {
            bool found = false;
            for (u32 i = 0; i < 4; ++i)
            {
                // FIXME: This returns the first node hit which may not be the
                // first that the ray would intersect!
                QuadTreeNode *child = mesh.nodes + node->children[i];
                if (RayAabbTest(child->aabb, start, end, &ignored))
                {
                    node = child;
                    found = true;
                    break;
                }
            }

            if (!found)
                break;
        }

        iteration++;
    }

    if (result)
    {
        raycastResult->isValid = result;
        raycastResult->t = tmin;
        raycastResult->hitNormal = intersectionNormal;
        raycastResult->hitPoint = start + tmin * (end - start);

        //Debug_DrawPoint(raycastResult->hitPoint, Vec3(1, 0, 0));
    }

    return result;
}
