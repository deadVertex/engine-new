#pragma once

#include <cstdarg>
#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif

extern uint32_t log_activeChannels;

enum
{
    LOG_CHANNEL_DEBUG = Bit(0),
    LOG_CHANNEL_WARNING = Bit(1),
    LOG_CHANNEL_ERROR = Bit(2),
    LOG_CHANNEL_INFO = Bit(3),
};

inline void log_Printf(uint32_t channel, const char *fmt, const char *file,
                       uint32_t lineNumber, const char *function, ...)
{
    char metaDataBuffer[128];
    char buffer[256];
    va_list args;
    if (log_activeChannels & channel)
    {
        va_start(args, function);
        auto n = vsnprintf(buffer, sizeof(buffer), fmt, args);
        Assert(n >= 0);
        const char *channelName;
        switch (channel)
        {
        case LOG_CHANNEL_DEBUG:
            channelName = "DEBUG";
            break;
        case LOG_CHANNEL_WARNING:
            channelName = "WARNING";
            break;
        case LOG_CHANNEL_ERROR:
            channelName = "ERROR";
            break;
        case LOG_CHANNEL_INFO:
            channelName = "INFO";
            break;
        default:
            channelName = "UNKNOWN";
            break;
        }

        n = snprintf(metaDataBuffer, sizeof(metaDataBuffer), "[%s %s:%d %s]",
                      channelName, file, lineNumber, function);
        Assert(n >= 0);

        char outputBuffer[400];
        snprintf(outputBuffer, sizeof(outputBuffer), "%s\t%s\n", metaDataBuffer,
                  buffer);
#ifdef _MSC_VER
        OutputDebugString(outputBuffer);
#else
        printf("%s %s\n", channelName, buffer);
#endif
    }
    va_end(args);
}

#ifdef _MSC_VER
#define LOG_MSG(CHANNEL, FMT, ...)                                             \
    log_Printf(CHANNEL, FMT, __FILE__, __LINE__, __FUNCTION__,  \
               __VA_ARGS__)
#define LOG_DEBUG(FMT, ...) LOG_MSG(LOG_CHANNEL_DEBUG, FMT, __VA_ARGS__)
#define LOG_WARNING(FMT, ...) LOG_MSG(LOG_CHANNEL_WARNING, FMT, __VA_ARGS__)
#define LOG_ERROR(FMT, ...) LOG_MSG(LOG_CHANNEL_ERROR, FMT, __VA_ARGS__)
#define LOG_INFO(FMT, ...) LOG_MSG(LOG_CHANNEL_INFO, FMT, __VA_ARGS__)
#else
#define LOG_MSG(CHANNEL, FMT, ...)                                         \
    log_Printf(CHANNEL, FMT, __FILE__, __LINE__, __FUNCTION__,  \
                ##__VA_ARGS__)

#define LOG_DEBUG(FMT, ...) LOG_MSG(LOG_CHANNEL_DEBUG, FMT, ##__VA_ARGS__)
#define LOG_WARNING(FMT, ...) LOG_MSG(LOG_CHANNEL_WARNING, FMT, ##__VA_ARGS__)
#define LOG_ERROR(FMT, ...) LOG_MSG(LOG_CHANNEL_ERROR, FMT, ##__VA_ARGS__)
#define LOG_INFO(FMT, ...) LOG_MSG(LOG_CHANNEL_INFO, FMT, ##__VA_ARGS__)
#endif
