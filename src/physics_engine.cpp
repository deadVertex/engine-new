struct Contact
{
    vec3 particleMovement[2];
    vec3 normal;
    i32 indices[2];
    float penetration;
    float restitution;
};

struct RigidBody
{
    vec3 position;
    vec3 velocity;
    vec3 acceleration;

    quat orientation;
    vec3 rotation;

    float inverseMass;
    float linearDamping;
    float angularDamping;

    vec3 forceAccum;
    vec3 torqueAccum;

    mat3 inverseInertiaTensor;
    mat3 inverseInertiaTensorWorld;

    mat4 transformMatrix;
};

// Particle physics engine
struct PhysicsEngine
{
    vec3 *positions;
    vec3 *velocities;
    vec3 *accelerations;
    vec3 *forces;
    vec3 *gravity;
    float *invMasses;
    float *dampening;
    u32 maxParticles;
    u32 particleCount;

    RigidBody *rigidBodies;
    u32 rigidBodyCount;
    u32 maxRigidBodies;
};

internal void InitializePhysicsEngine(PhysicsEngine *physicsEngine,
    MemoryArena *arena, u32 maxParticles, u32 maxRigidBodies)
{
    physicsEngine->positions = AllocateArray(arena, vec3, maxParticles);
    physicsEngine->velocities = AllocateArray(arena, vec3, maxParticles);
    physicsEngine->accelerations = AllocateArray(arena, vec3, maxParticles);
    physicsEngine->forces = AllocateArray(arena, vec3, maxParticles);
    physicsEngine->gravity = AllocateArray(arena, vec3, maxParticles);
    physicsEngine->invMasses = AllocateArray(arena, float, maxParticles);
    physicsEngine->dampening = AllocateArray(arena, float, maxParticles);
    physicsEngine->maxParticles = maxParticles;
    physicsEngine->particleCount = 0;

    physicsEngine->rigidBodies =
        AllocateArray(arena, RigidBody, maxRigidBodies);
    physicsEngine->maxRigidBodies = maxRigidBodies;
    physicsEngine->rigidBodyCount = 0;
}

internal void Integrate(PhysicsEngine *physicsEngine, float dt)
{
    vec3 *positions = physicsEngine->positions;
    vec3 *velocities = physicsEngine->velocities;
    vec3 *accelerations = physicsEngine->accelerations;
    vec3 *forces = physicsEngine->forces;
    vec3 *gravity = physicsEngine->gravity;
    float *invMasses = physicsEngine->invMasses;
    float *dampening = physicsEngine->dampening;
    u32 particleCount = physicsEngine->particleCount;

    for (u32 i = 0; i < particleCount; ++i)
    {
        if (invMasses[i] <= 0.0f)
        {
            continue;
        }

        vec3 initialVelocity = velocities[i];
        vec3 initialPosition = positions[i];
        positions[i] += velocities[i] * dt;

        accelerations[i] = gravity[i];
        vec3 totalAcceleration = forces[i] * invMasses[i] + accelerations[i];

        velocities[i] += totalAcceleration * dt;

        velocities[i] *= dampening[i];

        forces[i] = Vec3(0, 0, 0);
    }
}

internal void ResolveContacts(
    PhysicsEngine *physicsEngine, Contact *contacts, u32 contactCount, float dt)
{
    u32 maxIterations = contactCount * 2;
    u32 iterationCount = 0;

    for (iterationCount = 0; iterationCount < maxIterations; ++iterationCount)
    {
        float max = FLT_MAX;
        u32 maxIndex = contactCount;
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            Contact *contact = contacts + contactIdx;
            i32 idx0 = contact->indices[0];
            i32 idx1 = contact->indices[1];
            Assert(idx0 >= 0);

            vec3 velocities[2] = {};
            velocities[0] = physicsEngine->velocities[idx0];
            velocities[1] =
                idx1 >= 0 ? -physicsEngine->velocities[idx1] : Vec3(0, 0, 0);

            vec3 relativeVelocity = velocities[0] - velocities[1];
            float separatingVelocity = Dot(relativeVelocity, contact->normal);

            if (separatingVelocity < max &&
                (separatingVelocity < 0.0f || contact->penetration > 0.0f))
            {
                max = separatingVelocity;
                maxIndex = contactIdx;
            }
        }

        if (maxIndex == contactCount)
            break;

        Contact *contact = contacts + maxIndex;

        // Resolve most severe contact
        {
            i32 idx0 = contact->indices[0];
            i32 idx1 = contact->indices[1];
            Assert(idx0 >= 0);

            vec3 velocities[2] = {};
            velocities[0] = physicsEngine->velocities[idx0];
            velocities[1] =
                idx1 >= 0 ? -physicsEngine->velocities[idx1] : Vec3(0, 0, 0);

            vec3 relativeVelocity = velocities[0] - velocities[1];
            float separatingVelocity = Dot(relativeVelocity, contact->normal);

            if (separatingVelocity <= 0.0f)
            {
                float newSeparatingVelocity =
                    -separatingVelocity * contact->restitution;

                vec3 accCausedVelocity = physicsEngine->accelerations[idx0];
                accCausedVelocity -=
                    idx1 >= 0 ? physicsEngine->accelerations[idx1] : Vec3(0);
                float accCausedSepVelocity =
                    Dot(accCausedVelocity, contact->normal) * dt * 2.0f;

                if (accCausedSepVelocity < 0.0f)
                {
                    newSeparatingVelocity +=
                        contact->restitution * accCausedSepVelocity;
                    newSeparatingVelocity = Max(newSeparatingVelocity, 0.0f);
                }

                float deltaVelocity =
                    newSeparatingVelocity - separatingVelocity;

                float invMasses[2] = {};
                invMasses[0] = physicsEngine->invMasses[idx0];
                invMasses[1] =
                    idx1 >= 0 ? physicsEngine->invMasses[idx1] : 0.0f;

                float totalInvMass = invMasses[0] + invMasses[1];

                if (totalInvMass > 0.0f)
                {
                    float impulseMag = deltaVelocity / totalInvMass;
                    vec3 impulse = contact->normal * impulseMag;

                    physicsEngine->velocities[idx0] += impulse * invMasses[0];
                    if (idx1 >= 0)
                    {
                        physicsEngine->velocities[idx1] +=
                            impulse * -invMasses[0];
                    }
                }
            }
        }


        // Resolve penetration
        if (contact->penetration > 0)
        {
            i32 idx0 = contact->indices[0];
            i32 idx1 = contact->indices[1];
            Assert(idx0 >= 0);

            float invMasses[2] = {};
            invMasses[0] = physicsEngine->invMasses[idx0];
            invMasses[1] =
                idx1 >= 0 ? physicsEngine->invMasses[idx1] : 0.0f;

            float totalInvMass = invMasses[0] + invMasses[1];

            if (totalInvMass > 0.0f)
            {
                vec3 movementPerInvMass =
                    contact->normal * (contact->penetration / totalInvMass);

                contact->particleMovement[0] =
                    movementPerInvMass * invMasses[0];
                contact->particleMovement[1] =
                    movementPerInvMass * -invMasses[1];

                physicsEngine->positions[idx0] += contact->particleMovement[0];
                if (idx1 >= 0)
                {
                    physicsEngine->positions[idx1] +=
                        contact->particleMovement[1];
                }
            }
        }

        // Update interpenetrations for all contacts
        vec3 *movement = contact->particleMovement;
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            Contact *otherContact = contacts + contactIdx;
            if (otherContact->indices[0] == contact->indices[0])
            {
                otherContact->penetration -=
                    Dot(movement[0], otherContact->normal);
            }
            else if (otherContact->indices[0] == contact->indices[1])
            {
                otherContact->penetration -=
                    Dot(movement[1], otherContact->normal);
            }

            if (otherContact->indices[1] >= 0)
            {
                if (otherContact->indices[1] == contact->indices[0])
                {
                    otherContact->penetration -=
                        Dot(movement[0], otherContact->normal);
                }
                else if (otherContact->indices[1] == contact->indices[1])
                {
                    otherContact->penetration -=
                        Dot(movement[1], otherContact->normal);
                }
            }
        }
    }
}

internal void CalculateTransformMatrix(RigidBody *rigidBodies, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        rigidBodies[i].transformMatrix = Translate(rigidBodies[i].position) *
                                         Rotate(rigidBodies[i].orientation);
    }
}

internal void CalculateTransformedInertiaTensor(
    RigidBody *rigidBodies, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
#if 0
        mat3 rotationMatrix = Rotate3(rigidBodies[i].orientation);
        mat4 transform = rigidBodies[i].transformMatrix;
        mat3 iit = rigidBodies[i].inverseInertiaTensor;
        mat4 inverseInertiaTensor;
        inverseInertiaTensor.col[0] = Vec4(iit.col[0], 0);
        inverseInertiaTensor.col[1] = Vec4(iit.col[1], 0);
        inverseInertiaTensor.col[2] = Vec4(iit.col[2], 0);
        inverseInertiaTensor.col[3] = Vec4(0, 0, 0, 1);

        mat4 iitw = rigidBodies[i].transformMatrix * inverseInertiaTensor;
        //rigidBodies[i].inverseInertiaTensorWorld =
            //rotationMatrix * rigidBodies[i].inverseInertiaTensor;
        rigidBodies[i].inverseInertiaTensorWorld.col[0] = iitw.col[0].xyz;
        rigidBodies[i].inverseInertiaTensorWorld.col[1] = iitw.col[1].xyz;
        rigidBodies[i].inverseInertiaTensorWorld.col[2] = iitw.col[2].xyz;
#else
        rigidBodies[i].inverseInertiaTensorWorld =
            rigidBodies[i].inverseInertiaTensor;
#endif
    }
}

internal void UpdateDerivedData(RigidBody *rigidBodies, u32 count)
{
    for (u32 i = 0; i < count; ++i)
    {
        rigidBodies[i].orientation = Normalize(rigidBodies[i].orientation);
    }

    CalculateTransformMatrix(rigidBodies, count);

    CalculateTransformedInertiaTensor(rigidBodies, count);
}

inline mat3 BoxInertiaTensor(vec3 d, float m)
{
    mat3 result = Identity3();

    vec3 d2 = Vec3(d.x * d.x, d.y * d.y, d.z * d.z);

    result.raw[0] = (d2.y + d2.z) * (m / 12.0f);
    result.raw[4] = (d2.x + d2.z) * (m / 12.0f);
    result.raw[8] = (d2.x + d2.y) * (m / 12.0f);

    return result;
}

// NOTE: Force direction is in world space
inline void AddForceAtWorldPoint(
    RigidBody *rigidBodies, u32 count, vec3 force, vec3 worldPoint)
{
    for (u32 i = 0; i < count; ++i)
    {
        vec3 p = worldPoint - rigidBodies[i].position;
        rigidBodies[i].forceAccum += force;
        rigidBodies[i].torqueAccum += Cross(p, force);
    }
}

// NOTE: Force direction is in world space
inline void AddForceAtLocalPoint(
    RigidBody *rigidBodies, u32 count, vec3 force, vec3 localPoint)
{
    for (u32 i = 0; i < count; ++i)
    {
        vec4 v = rigidBodies[i].transformMatrix * Vec4(localPoint, 1.0f);
        vec3 p = v.xyz;
        AddForceAtWorldPoint(rigidBodies + i, 1, force, p);
    }
}

internal void Integrate(RigidBody *rigidBodies, u32 count, float dt)
{
    for (u32 i = 0; i < count; ++i)
    {
        RigidBody *rigidBody = rigidBodies + i;
        vec3 acceleration = rigidBody->acceleration;
        acceleration += rigidBody->forceAccum * rigidBody->inverseMass;

        vec3 angularAcceleration =
            rigidBody->inverseInertiaTensorWorld * rigidBody->torqueAccum;

        rigidBody->velocity += acceleration * dt;

        rigidBody->rotation += angularAcceleration * dt;

        rigidBody->velocity *= Pow(rigidBody->linearDamping, dt);
        rigidBody->rotation *= Pow(rigidBody->angularDamping, dt);

        rigidBody->position += rigidBody->velocity * dt;
        // orientation.addScaledVector
        vec3 rotation = rigidBody->rotation * dt;
        vec3 axis = Normalize(rotation);
        float angle = Length(rotation);
        quat q = Quat(axis, angle);
        rigidBody->orientation *= q;

        //quat q = Quat(rotation.x * dt, rotation.y * dt, rotation.z * dt, 0.0f);
        //rigidBody->orientation *= q;
        //rigidBody->orientation.w += q.w * 0.5f;
        //rigidBody->orientation.x += q.x * 0.5f;
        //rigidBody->orientation.y += q.y * 0.5f;
        //rigidBody->orientation.z += q.z * 0.5f;

        rigidBody->forceAccum = Vec3(0, 0, 0);
        rigidBody->torqueAccum = Vec3(0, 0, 0);
    }
}

internal void DrawBox(mat4 transform, vec3 halfExtents, vec3 color, float lifetime = 0.0f)
{
    vec3 h = halfExtents;

    // clang-format off
    vec3 vertices[8] = {
        {-h.x, -h.y, -h.z},
        {h.x, -h.y, -h.z},
        {h.x, -h.y, h.z},
        {-h.x, -h.y, h.z},

        {-h.x, h.y, -h.z},
        {h.x, h.y, -h.z},
        {h.x, h.y, h.z},
        {-h.x, h.y, h.z},
    };

    u32 indices[24] = {
        0, 1,
        1, 2,
        2, 3,
        3, 0,

        4, 5,
        5, 6,
        6, 7,
        7, 4,

        0, 4,
        1, 5,
        2, 6,
        3, 7,
    };
    // clang-format on

    vec3 transformedVertices[8];

    for (u32 i = 0; i < 8; ++i)
    {
        transformedVertices[i] = (transform * Vec4(vertices[i], 1.0f)).xyz;
    }

    for (u32 i = 0; i < 24; i+=2)
    {
        u32 start = indices[i];
        u32 end = indices[i+1];
        Debug_DrawLine(
            transformedVertices[start], transformedVertices[end], color);
    }
}

struct RigidBodyContact
{
    vec3 point;
    vec3 normal;
    i32 indices[2];
    float penetration;
    float restitution;
};

struct ContactResolutionData
{
    vec3 point;
    vec3 normal;
    i32 indices[2];
    float penetration;
    float restitution;

    vec3 relativeContactPositions[2];
    mat3 contactToWorld;
    vec3 contactVelocity;
    float desiredDeltaVelocity;
};

inline vec3 CalculateLocalVelocity(
    ContactResolutionData *contact, RigidBody *body, u32 bodyIdx)
{
    vec3 velocity =
        Cross(body->rotation, contact->relativeContactPositions[bodyIdx]);
    velocity += body->velocity;

    mat3 worldToContact = Transpose(contact->contactToWorld);
    vec3 contactVelocity = worldToContact * velocity;

    return contactVelocity;
}

inline float CalculateDesiredDeltaVelocity(ContactResolutionData *data)
{
    float velocityLimit = 0.25f;
    float restitution = data->restitution;

    if (Abs(data->contactVelocity.x) < velocityLimit)
    {
        restitution = 0.0f;
    }

    float desiredDeltaVelocity =
        -data->contactVelocity.x - restitution * data->contactVelocity.x;

    return desiredDeltaVelocity;
}

internal void PrepareContacts(ContactResolutionData *contactData,
    RigidBodyContact *contacts, u32 contactCount, RigidBody *rigidBodies,
    u32 rigidBodyCount)
{
    for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
    {
        RigidBodyContact *rawContact = contacts + contactIdx;
        ContactResolutionData *processedContact = contactData + contactIdx;

        processedContact->point = rawContact->point;
        processedContact->normal = rawContact->normal;
        processedContact->indices[0] = rawContact->indices[0];
        processedContact->indices[1] = rawContact->indices[1];
        processedContact->penetration = rawContact->penetration;
        processedContact->restitution = rawContact->restitution;

        RigidBody *body0 = rigidBodies + processedContact->indices[0];
        RigidBody *body1 = (processedContact->indices[1] >= 0)
                               ? rigidBodies + processedContact->indices[1]
                               : NULL;

        processedContact->relativeContactPositions[0] =
            processedContact->point - body0->position;

        processedContact->relativeContactPositions[1] =
            (body1 != NULL) ? processedContact->point - body1->position
                            : Vec3(0, 0, 0);

        processedContact->contactToWorld =
            CreateChangeOfBasisMatrix(processedContact->normal);

        processedContact->contactVelocity = CalculateLocalVelocity(
                processedContact, body0, 0);

        if (body1 != NULL)
        {
            processedContact->contactVelocity -=
                CalculateLocalVelocity(processedContact, body1, 1);
        }

        processedContact->desiredDeltaVelocity =
            CalculateDesiredDeltaVelocity(processedContact);

#if 0
        mat4 transform;
        transform.col[0] = Vec4(processedContact->contactToWorld.col[0], 0.0f);
        transform.col[1] = Vec4(processedContact->contactToWorld.col[1], 0.0f);
        transform.col[2] = Vec4(processedContact->contactToWorld.col[2], 0.0f);
        transform.col[3] = Vec4(processedContact->point, 1.0f);

        Debug_DrawAxis(transform);
#endif
        //processedContact->contactVelocity;

    }
}

inline void LimitAngularMovement(
    vec3 relativeContactPosition, float *linearMovement, float *angularMovement)
{
    float angularLimitConstant = 0.2f;
    float scaledAngularLimit =
        angularLimitConstant * Length(relativeContactPosition);

    if (Abs(*angularMovement) > scaledAngularLimit)
    {
        float totalMovement = *linearMovement + *angularMovement;

        if (*angularMovement >= 0)
        {
            *angularMovement = scaledAngularLimit;
        }
        else
        {
            *angularMovement = -scaledAngularLimit;
        }

        *linearMovement = totalMovement - *angularMovement;
    }
}

internal void ApplyPositionChange(ContactResolutionData *contact,
    RigidBody *rigidBodies, u32 rigidBodyCount, vec3 linearChange[2],
    vec3 angularChange[2])
{
    RigidBody *bodies[2];
    bodies[0] = rigidBodies + contact->indices[0];
    bodies[1] =
        (contact->indices[1] >= 0) ? rigidBodies + contact->indices[1] : NULL;

    float linearInertia[2] = {};
    float angularInertia[2] = {};

    float totalInertia = 0.0f;

    for (u32 i = 0; i < 2; ++i)
    {
        if (bodies[i] == NULL)
            break;

        vec3 angularInertiaLocal =
            Cross(contact->relativeContactPositions[i], contact->normal);
        vec3 angularInertiaTransformed =
            bodies[i]->inverseInertiaTensorWorld * angularInertiaLocal;
        vec3 angularInertiaWorld = Cross(
            angularInertiaTransformed, contact->relativeContactPositions[i]);

        // Tiny angular inertia appears to be the cause of the the problem
        // Likly introduces floating point issues

        //Debug_Printf("angularInertiaLocal: %g %g %g\n", angularInertiaLocal.x,
            //angularInertiaLocal.y, angularInertiaLocal.z);
        //Debug_Printf("angularInertiaTransformed: %g %g %g\n",
            //angularInertiaTransformed.x, angularInertiaTransformed.y,
            //angularInertiaTransformed.z);
        //Debug_Printf("angularInertiaWorld: %g %g %g\n", angularInertiaWorld.x,
            //angularInertiaWorld.y, angularInertiaWorld.z);

        angularInertia[i] = Dot(angularInertiaWorld, contact->normal);

        //Debug_DrawLine(contact->point, contact->point + contact->normal * 0.25f,
            //Vec3(1, 0, 0));
        //Debug_DrawLine(contact->point,
            //contact->point +
                //Normalize(contact->relativeContactPositions[i]) * 0.25f,
            //Vec3(1, 1, 0));
        //Debug_DrawLine(contact->point,
            //contact->point + Normalize(angularInertiaLocal) * 0.35f,
            //Vec3(0, 1, 0));
        //Debug_DrawLine(contact->point,
            //contact->point + Normalize(angularInertiaTransformed) * 0.25f,
            //Vec3(1, 0, 1));
        //Debug_DrawLine(contact->point,
            //contact->point + Normalize(angularInertiaWorld) * 0.25f,
            //Vec3(0, 1, 1));

        linearInertia[i] = bodies[i]->inverseMass;

        //Debug_Printf("angularInertia: %g\n", angularInertia[i]);

        totalInertia += angularInertia[i] + linearInertia[i];
    }

    float linearMove[2] = {};
    float angularMove[2] = {};

    float sign[2] = {1.0f, -1.0f};

    // Separate loop as we depend on the totalInertia
    for (u32 i = 0; i < 2; ++i)
    {
        if (bodies[i] == NULL)
            break;

        angularMove[i] =
            sign[i] * contact->penetration * (angularInertia[i] / totalInertia);

        linearMove[i] =
            sign[i] * contact->penetration * (linearInertia[i] / totalInertia);

        LimitAngularMovement(contact->relativeContactPositions[i],
            &linearMove[i], &angularMove[i]);

        //Debug_Printf("angularMove: %g\nlinearMove: %g\n",
                //angularMove[i], linearMove[i]);

        // Calculate angularChange required to achieve angularMove
        vec3 impulsiveTorque =
            Cross(contact->relativeContactPositions[i], contact->normal);
        vec3 impulsePerMove =
            bodies[i]->inverseInertiaTensorWorld * impulsiveTorque;

        //Debug_Printf("impulsiveTorque: %g %g %g\n",
                //impulsiveTorque.x, impulsiveTorque.y, impulsiveTorque.z);
        //Debug_Printf("impulsePerMove: %g %g %g\n",
                //impulsePerMove.x, impulsePerMove.y, impulsePerMove.z);

        //Debug_DrawLine(contact->point,
            //contact->point + Normalize(impulsiveTorque) * 0.25f, Vec3(1, 0, 1));
        //Debug_DrawLine(contact->point,
            //contact->point + Normalize(impulsePerMove) * 0.25f, Vec3(0, 1, 1));

        vec3 angularChangePerMove = impulsePerMove * (1.0f / angularInertia[i]);
        angularChange[i] = angularChangePerMove * angularMove[i];

        //Debug_Printf("angularMove: %g\n", angularMove[i]);
        //Debug_Printf("angularInertia: %g\n", angularInertia[i]);
        //Debug_Printf("inverseAngularInertia: %g\n", (1.0f / angularInertia[i]));
        //Debug_Printf("angularChange: %g %g %g\n", angularChange[i].x,
            //angularChange[i].y, angularChange[i].z);

        linearChange[i] = contact->normal * linearMove[i];

        // Apply linear and angular change
        bodies[i]->position += linearChange[i];

#if 1
        vec3 axis = Normalize(angularChange[i]);
        float angle = Length(angularChange[i]);
        quat q = Quat(axis, angle);
        bodies[i]->orientation *= q;
        //bodies[i]->orientation = q * bodies[i]->orientation;
        bodies[i]->orientation = Normalize(bodies[i]->orientation);
#else
        quat q = Quat(
            angularChange[i].x, angularChange[i].y, angularChange[i].z, 0.0f);
        quat orientation = bodies[i]->orientation;
        orientation = q * orientation;
        orientation.x += q.x * 0.5f;
        orientation.y += q.y * 0.5f;
        orientation.z += q.z * 0.5f;
        orientation.w += q.w * 0.5f;

        bodies[i]->orientation = Normalize(orientation);
#endif
    }
}

internal void AdjustPositions(ContactResolutionData *contacts,
    u32 contactCount, RigidBody *rigidBodies, u32 rigidBodyCount)
{
    u32 maxIterations = 32;
    for (u32 iterationCount = 0; iterationCount < maxIterations;
         ++iterationCount)
    {
        // Find contact with highest penetration
        u32 maxIndex = contactCount;
        float maxPenetration =
            0.0f; // TODO: Parameterize this as min penetration
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            float penetration = contacts[contactIdx].penetration;
            if (penetration > maxPenetration)
            {
                maxIndex = contactIdx;
                maxPenetration = penetration;
            }
        }

        if (maxIndex == contactCount)
        {
#if 0
            for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
            {
                vec3 p = contacts[contactIdx].point;
                Debug_PrintInWorld(p, "(%g) : %g", p.y,
                    contacts[contactIdx].penetration);
            }
#endif
            break; // No contacts found to be penetrating
        }

        ContactResolutionData *contactToResolve = contacts + maxIndex;

        vec3 angularChange[2] = {};
        vec3 linearChange[2] = {};

        ApplyPositionChange(contactToResolve, rigidBodies, rigidBodyCount,
            linearChange, angularChange);

        u32 count = 0;
        float sign[2] = {-1.0f, 1.0f};
        // Update penetration value of all contacts
        for (u32 contactIdx = 0; contactIdx < contactCount; contactIdx++)
        {
            ContactResolutionData *contactToUpdate = contacts + contactIdx;

            for (u32 i = 0; i < 2; ++i)
            {
                for (u32 j = 0; j < 2; ++j)
                {
                    if (contactToUpdate->indices[i] != -1 &&
                        contactToResolve->indices[j] != -1)
                    {
                        if (contactToUpdate->indices[i] ==
                            contactToResolve->indices[j])
                        {
                            vec3 angularComponent = Cross(angularChange[j],
                                contactToUpdate->relativeContactPositions[i]);
                            vec3 deltaPosition =
                                linearChange[j] + angularComponent;
                            //Debug_Printf("angularComponent: %g %g %g\n",
                                    //angularComponent.x, angularComponent.y,
                                    //angularComponent.z);
                            //Debug_Printf("deltaPosition: %g %g %g\n",
                                    //deltaPosition.x, deltaPosition.y,
                                    //deltaPosition.z);

                            float initialPenetration =
                                contactToUpdate->penetration;
                            float deltaPenetration =
                                sign[i] *
                                Dot(deltaPosition, contactToUpdate->normal);
                            contactToUpdate->penetration += deltaPenetration;

#if 0
                            float y = iterationCount * 0.1f;
                            Debug_PrintInWorld(
                                contactToUpdate->point + Vec3(0, y, 0),
                                "initial: %g new: %g", initialPenetration,
                                contactToUpdate->penetration);

                            Debug_DrawLine(contactToUpdate->point,
                                    contactToUpdate->point + deltaPosition,
                                    Vec3(1, 0, 1));

                            Debug_DrawPoint(contactToUpdate->point,
                                    Vec3(0.7f, 1.0f, 0.2f), 0.01f);
#endif

#if 0
                            Debug_DrawLine(contactToUpdate->point -
                                               contactToUpdate->normal *
                                                   initialPenetration,
                                contactToUpdate->point -
                                    contactToUpdate->normal *
                                        contactToUpdate->penetration,
                                Vec3(1, 0, 1));
#endif
                            count++;
                        }
                    }
                }
            }
        }

        //Debug_Printf("count: %u\n", count);
    }
}

inline vec3 CalculateFrictionlessImpulse(
    ContactResolutionData *contact, RigidBody *body0, RigidBody *body1)
{
    vec3 torquePerUnitImpulse =
        Cross(contact->relativeContactPositions[0], contact->normal);
    vec3 rotationPerUnitImpulse =
        body0->inverseInertiaTensorWorld * torquePerUnitImpulse;
    vec3 velocityPerUnitImpulse =
        Cross(rotationPerUnitImpulse, contact->relativeContactPositions[0]);

    float angularComponent = Dot(velocityPerUnitImpulse, contact->normal);
    float linearComponent = body0->inverseMass;

    float totalDeltaV = angularComponent + linearComponent;

    if (body1 != NULL)
    {
        // Calculate linear and angular component for body1
        vec3 torquePerUnitImpulse2 =
            Cross(contact->relativeContactPositions[1], contact->normal);
        vec3 rotationPerUnitImpulse2 =
            body1->inverseInertiaTensorWorld * torquePerUnitImpulse2;
        vec3 velocityPerUnitImpulse2 =
            Cross(rotationPerUnitImpulse2, contact->relativeContactPositions[1]);

        float angularComponent2 = Dot(velocityPerUnitImpulse2, contact->normal);
        float linearComponent2 = body1->inverseMass;

        totalDeltaV += angularComponent2 + linearComponent2;
    }

    vec3 impulseContact = Vec3(0, 0, 0);
    impulseContact.x = contact->desiredDeltaVelocity / totalDeltaV;

    return impulseContact;
}

internal void ApplyVelocityChange(ContactResolutionData *contact,
    RigidBody *rigidBodies, u32 rigidBodyCount, vec3 velocityChange[2],
    vec3 rotationChange[2])
{
    RigidBody *bodies[2];
    bodies[0] = rigidBodies + contact->indices[0];
    bodies[1] =
        (contact->indices[1] >= 0) ? rigidBodies + contact->indices[1] : NULL;

    vec3 impulseContact =
        CalculateFrictionlessImpulse(contact, bodies[0], bodies[1]);

    vec3 impulse = contact->contactToWorld * impulseContact;
    velocityChange[0] = impulse * bodies[0]->inverseMass;

    vec3 impulsiveTorque = Cross(impulse, contact->relativeContactPositions[0]);
    rotationChange[0] = bodies[0]->inverseInertiaTensorWorld * impulsiveTorque;

    bodies[0]->velocity += velocityChange[0];
    bodies[0]->rotation += rotationChange[0];

    if (bodies[1] != NULL)
    {
        vec3 impulsiveTorque2 =
            Cross(impulse, contact->relativeContactPositions[1]);
        rotationChange[1] =
            bodies[1]->inverseInertiaTensorWorld * impulsiveTorque2;
        velocityChange[1] = impulse * -bodies[1]->inverseMass;

        bodies[1]->velocity += velocityChange[1];
        bodies[1]->rotation += rotationChange[1];
    }
}

internal void AdjustVelocities(ContactResolutionData *contacts,
    u32 contactCount, RigidBody *rigidBodies, u32 rigidBodyCount, float dt)
{
    u32 maxIterations = 1;
    for (u32 iterationCount = 0; iterationCount < maxIterations;
         ++iterationCount)
    {
        // Find contact with highest penetration
        u32 maxIndex = contactCount;
        float maxVelocity =
            0.0f; // TODO: Parameterize this as min velocity
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            float desiredDeltaVelocity =
                contacts[contactIdx].desiredDeltaVelocity;
            if (desiredDeltaVelocity > maxVelocity)
            {
                maxIndex = contactIdx;
                maxVelocity = desiredDeltaVelocity;
            }
        }

        if (maxIndex == contactCount)
        {
            break;
        }

        ContactResolutionData *contactToResolve = contacts + maxIndex;

        vec3 velocityChange[2];
        vec3 rotationChange[2];
        ApplyVelocityChange(contactToResolve, rigidBodies, rigidBodyCount,
                velocityChange, rotationChange);

        // Recompute relative closing velocities
        float sign[2] = { 1.0f, -1.0f };
        for (u32 contactIdx = 0; contactIdx < contactCount; ++contactIdx)
        {
            ContactResolutionData *contactToUpdate = contacts + contactIdx;

            for (u32 i = 0; i < 2; ++i)
            {
                for (u32 j = 0; j < 2; ++j)
                {
                    if (contactToUpdate->indices[i] != -1 &&
                        contactToResolve->indices[j] != -1)
                    {
                        if (contactToUpdate->indices[i] ==
                            contactToResolve->indices[j])
                        {
                            vec3 deltaVelocity =
                                velocityChange[j] +
                                Cross(rotationChange[j],
                                    contactToUpdate
                                        ->relativeContactPositions[i]);

                            contactToUpdate->contactVelocity +=
                                Transpose(contactToUpdate->contactToWorld) *
                                deltaVelocity * sign[j];

                            contactToUpdate->desiredDeltaVelocity =
                                CalculateDesiredDeltaVelocity(contactToUpdate);
                        }
                    }
                }
            }
        }
    }
}

internal void Resolve(RigidBody *rigidBodies, u32 rigidBodyCount,
    RigidBodyContact *contacts, u32 contactCount, float dt, MemoryArena *arena)
{
    if (contactCount == 0)
        return;

    ContactResolutionData *contactData =
        AllocateArray(arena, ContactResolutionData, contactCount);

    PrepareContacts(
        contactData, contacts, contactCount, rigidBodies, rigidBodyCount);

    AdjustPositions(contactData, contactCount, rigidBodies, rigidBodyCount);

    AdjustVelocities(contactData, contactCount, rigidBodies, rigidBodyCount, dt);

    MemoryArenaFree(arena, contactData);
}
