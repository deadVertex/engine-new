#pragma once

#include "math_lib.h"

struct DebugSystem;
global DebugSystem *g_DebugSystem;

internal void Debug_DrawLine(DebugSystem *debugSystem, vec3 start, vec3 end, vec3 colour, float lifetime);
internal void Debug_DrawPoint(DebugSystem *debugSystem, vec3 position, vec3 colour, float scale, float lifetime);
internal void Debug_DrawAxis(DebugSystem *debugSystem, mat4 transform, float lifetime);
internal void Debug_PrintfLocal(DebugSystem *debugSystem, const char *format, ...);
internal void Debug_PrintInWorld_(DebugSystem *debugSystem, vec3 p, const char *format, ...);

inline void Debug_DrawLine(vec3 start, vec3 end, vec3 colour, float lifetime = 0.0f)
{
    if (g_DebugSystem != NULL)
    {
        Debug_DrawLine(g_DebugSystem, start, end, colour, lifetime);
    }
}

inline void Debug_DrawPoint(vec3 position, vec3 colour, float scale = 0.2f, float lifetime = 0.0f)
{
    if (g_DebugSystem != NULL)
    {
        Debug_DrawPoint(g_DebugSystem, position, colour, scale, lifetime);
    }
}

inline void Debug_DrawAxis(mat4 transform, float lifetime = 0.0f)
{
    if (g_DebugSystem != NULL)
    {
        Debug_DrawAxis(g_DebugSystem, transform, lifetime);
    }
}

inline void Debug_DrawAabb(vec3 min, vec3 max, vec3 colour, float lifetime = 0.0f)
{
    Debug_DrawLine(Vec3(min.x, min.y, min.z), Vec3(max.x, min.y, min.z), colour, lifetime);
    Debug_DrawLine(Vec3(max.x, min.y, min.z), Vec3(max.x, min.y, max.z), colour, lifetime);
    Debug_DrawLine(Vec3(max.x, min.y, max.z), Vec3(min.x, min.y, max.z), colour, lifetime);
    Debug_DrawLine(Vec3(min.x, min.y, max.z), Vec3(min.x, min.y, min.z), colour, lifetime);

    Debug_DrawLine(Vec3(min.x, max.y, min.z), Vec3(max.x, max.y, min.z), colour, lifetime);
    Debug_DrawLine(Vec3(max.x, max.y, min.z), Vec3(max.x, max.y, max.z), colour, lifetime);
    Debug_DrawLine(Vec3(max.x, max.y, max.z), Vec3(min.x, max.y, max.z), colour, lifetime);
    Debug_DrawLine(Vec3(min.x, max.y, max.z), Vec3(min.x, max.y, min.z), colour, lifetime);

    Debug_DrawLine(Vec3(min.x, min.y, min.z), Vec3(min.x, max.y, min.z), colour, lifetime);
    Debug_DrawLine(Vec3(max.x, min.y, min.z), Vec3(max.x, max.y, min.z), colour, lifetime);
    Debug_DrawLine(Vec3(max.x, min.y, max.z), Vec3(max.x, max.y, max.z), colour, lifetime);
    Debug_DrawLine(Vec3(min.x, min.y, max.z), Vec3(min.x, max.y, max.z), colour, lifetime);
}

#ifdef COMPILER_MSVC
#define Debug_Printf(FMT, ...) \
    Debug_PrintfLocal(g_DebugSystem, FMT, __VA_ARGS__)

#define Debug_PrintInWorld(P, FMT, ...) \
    Debug_PrintInWorld_(g_DebugSystem, P, FMT, __VA_ARGS__)
#else
#define Debug_Printf(FMT, ...) \
    Debug_PrintfLocal(g_DebugSystem, FMT, ##__VA_ARGS__)

#define Debug_PrintInWorld(P, FMT, ...) \
    Debug_PrintInWorld_(g_DebugSystem, P, FMT, ##__VA_ARGS__)
#endif
