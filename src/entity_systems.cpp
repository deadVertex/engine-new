internal void DrawPositionSystem(
    ecs_EntitySystem *entitySystem, MemoryArena *tempArena)
{
    vec3 *positions = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 1,
        GetComponent(PositionComponentTypeId, positions));

    for (u32 i = 0; i < result.length; ++i)
    {
        mat4 transform = Translate(positions[i]);
        Debug_DrawAxis(transform);
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawBoxHalfExtentsSystem(ecs_EntitySystem *entitySystem, MemoryArena *arena)
{
    vec3 *halfExtents = NULL;
    vec3 *positions = NULL;
    vec3 *scales = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, arena, 3,
        GetComponent(BoxHalfExtentsComponentTypeId, halfExtents),
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(ScaleComponentTypeId, scales));

    for (u32 i = 0; i < result.length; ++i)
    {
        vec3 min = positions[i] - halfExtents[i] * scales[i];
        vec3 max = positions[i] + halfExtents[i] * scales[i];
        Debug_DrawAabb(min, max, Vec3(0.4, 0.6, 0));
    }

    ecs_ReleaseQueryResult(result);
}

internal void UpdateTransformSystem(
    ecs_EntitySystem *entitySystem, MemoryArena *tempArena)
{
    vec3 *positions = NULL;
    vec3 *scales = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 3,
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(ScaleComponentTypeId, scales),
        HasComponent(TransformComponentTypeId));

    if (result.length > 0)
    {
        mat4 *transforms = AllocateArray(tempArena, mat4, result.length);

        for (u32 i = 0; i < result.length; ++i)
        {
            transforms[i] = Translate(positions[i]) * Scale(scales[i]);
        }

        ecs_SetComponent(entitySystem, TransformComponentTypeId, result.entities,
                result.length, transforms, sizeof(transforms[0]));
    }

    ecs_ReleaseQueryResult(result);
}

#define SHADOW_SCALE (10.0f)

inline mat4 CalculateLightViewMatrix(quat q, vec3 cameraOrigin)
{
    mat4 rotationMatrix = Rotate(-q);
    mat4 translationMatrix = Translate(-cameraOrigin);
    mat4 lightView = rotationMatrix * translationMatrix;

    return lightView;
}

internal void CalculateLightingInfo(ecs_EntitySystem *entitySystem,
    MemoryArena *tempArena, vec3 cameraOrigin, mat4 *lightView,
    mat4 *lightProjection)
{
    u8 directionalLightType = LightType_Directional;
    quat *rotations = NULL;
    vec3 *lightColors = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 3,
        IsEqual(LightTypeComponentTypeId, directionalLightType),
        GetComponent(RotationComponentTypeId, rotations),
        GetComponent(LightColorComponentTypeId, lightColors));

    Assert(result.length <= 1);
    for (u32 i = 0; i < result.length; ++i)
    {
        float scale = SHADOW_SCALE;
        *lightProjection = Orthographic(-scale, scale, scale, -scale, -scale, scale);
        *lightView = CalculateLightViewMatrix(rotations[0], cameraOrigin);
    }
    ecs_ReleaseQueryResult(result);
}

internal void DrawRenderedMeshSystem(ecs_EntitySystem *entitySystem,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands, mat4 view,
    mat4 projection, vec3 cameraOrigin)
{
    mat4 *transforms = NULL;
    u32 *meshes = NULL;
    u32 *shaders = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 3,
        GetComponent(TransformComponentTypeId, transforms),
        GetComponent(RenderedMeshComponentTypeId, meshes),
        GetComponent(ShaderComponentTypeId, shaders));

    Debug_DrawAabb(cameraOrigin + Vec3(-SHADOW_SCALE),
        cameraOrigin + Vec3(SHADOW_SCALE), Vec3(1, 0, 0));

    for (u32 i = 0; i < result.length; ++i)
    {
        RenderCommandDrawMeshUniforms *drawCube = RenderCommandAllocate(
                renderCommands, RenderCommandDrawMeshUniforms, 0);
        drawCube->model = transforms[i];
        drawCube->view = view;
        drawCube->projection = projection;
        drawCube->mesh = meshes[i];
        drawCube->shader = shaders[i];

        if (shaders[i] == VULKAN_SHADER_TEXTURE_ONLY ||
            shaders[i] == VULKAN_SHADER_WORLD_TEX_COORD ||
            shaders[i] == VULKAN_SHADER_TEXTURE_DIFFUSE_LIGHTING)
        {
            u32 material;
            ecs_GetComponent(entitySystem, MaterialComponentTypeId,
                &result.entities[i], 1, &material, sizeof(material));
            PushUniformTexture(drawCube, UNIFORM_KEY_TEX_SAMPLER, material);
        }
        else
        {
            PushUniformV4(
                drawCube, UNIFORM_KEY_COLOR, Vec4(0.8, 0.8, 0.8, 1.0));
        }

        if (shaders[i] == VULKAN_SHADER_DIFFUSE_LIGHTING ||
            shaders[i] == VULKAN_SHADER_TEXTURE_DIFFUSE_LIGHTING ||
            shaders[i] == VULKAN_SHADER_WORLD_TEX_COORD)
        {
            // TODO: Figure out which meshes are lit by which lights
            mat4 lightView;
            mat4 lightProjection;
            CalculateLightingInfo(entitySystem, tempArena, cameraOrigin,
                &lightView, &lightProjection);

            PushUniformMat4(drawCube, UNIFORM_KEY_LIGHT_VIEW_PROJECTION,
                lightProjection * lightView);
            PushUniformSpecialType(
                drawCube, UNIFORM_KEY_SHADOW_MAP, UNIFORM_TYPE_SHADOW_MAP);
        }
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawAmbientLightSystem(ecs_EntitySystem *entitySystem,
        MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    u8 ambientLightType = LightType_Ambient;
    vec3 *lightColors = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 2,
        IsEqual(LightTypeComponentTypeId, ambientLightType),
        GetComponent(LightColorComponentTypeId, lightColors));

    // Only use first result
    if (result.length > 0)
    {
        RenderCommandDrawLight *drawAmbientLight =
            RenderCommandAllocate(renderCommands, RenderCommandDrawLight, 0);
        drawAmbientLight->type = LightType_Ambient;
        drawAmbientLight->color = lightColors[0];
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawDirectionalLightsSystem(ecs_EntitySystem *entitySystem,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    u8 directionalLightType = LightType_Directional;
    quat *rotations = NULL;
    vec3 *lightColors = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 3,
        IsEqual(LightTypeComponentTypeId, directionalLightType),
        GetComponent(RotationComponentTypeId, rotations),
        GetComponent(LightColorComponentTypeId, lightColors));

    for (u32 i = 0; i < result.length; ++i)
    {
        vec3 direction = Vec3(0, 0, -1);
        mat4 rotation = Rotate(rotations[i]); // @Speed: Pretty sure this is unnecessary
        vec4 rotatedVector = rotation * Vec4(direction, 0);
        direction = Normalize(rotatedVector.xyz);

        RenderCommandDrawLight *drawLight =
            RenderCommandAllocate(renderCommands, RenderCommandDrawLight, 0);
        drawLight->type = LightType_Directional;
        drawLight->color = lightColors[i];
        drawLight->direction = direction;
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawPointLightsSystem(ecs_EntitySystem *entitySystem,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    u8 pointLightType = LightType_Point;
    vec3 *positions = NULL;
    vec3 *lightColors = NULL;
    vec3 *attenuation = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 4,
        IsEqual(LightTypeComponentTypeId, pointLightType),
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(LightColorComponentTypeId, lightColors),
        GetComponent(LightAttenuationComponentTypeId, attenuation));

    for (u32 i = 0; i < result.length; ++i)
    {
        RenderCommandDrawLight *drawLight =
            RenderCommandAllocate(renderCommands, RenderCommandDrawLight, 0);
        drawLight->type = LightType_Point;
        drawLight->color = lightColors[i];
        drawLight->position = positions[i];
        drawLight->attenuation = attenuation[i];
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawShadowsForLight(ecs_EntitySystem *entitySystem,
        MemoryArena *tempArena, RenderCommandQueue *renderCommands,
        mat4 projection, mat4 view)
{
    mat4 *transforms = NULL;
    u32 *meshes = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 3,
        GetComponent(TransformComponentTypeId, transforms),
        GetComponent(RenderedMeshComponentTypeId, meshes),
        HasComponent(ShadowCastingMeshComponentTypeId));

    for (u32 i = 0; i < result.length; ++i)
    {
        RenderCommandDrawShadow *drawShadow = RenderCommandAllocate(
                renderCommands, RenderCommandDrawShadow, 0);
        drawShadow->model = transforms[i];
        drawShadow->view = view;
        drawShadow->projection = projection;
        drawShadow->mesh = meshes[i];
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawShadowsSystem(ecs_EntitySystem *entitySystem,
        MemoryArena *tempArena, RenderCommandQueue *renderCommands,
        vec3 cameraOrigin)
{
    u8 directionalLightType = LightType_Directional;
    quat *rotations = NULL;
    vec3 *lightColors = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 3,
        IsEqual(LightTypeComponentTypeId, directionalLightType),
        GetComponent(RotationComponentTypeId, rotations),
        GetComponent(LightColorComponentTypeId, lightColors));

    Assert(result.length <= 1);
    for (u32 i = 0; i < result.length; ++i)
    {
        float scale = SHADOW_SCALE;
        mat4 projection = Orthographic(-scale, scale, scale, -scale, -scale, scale);
        mat4 view = CalculateLightViewMatrix(rotations[0], cameraOrigin);

        DrawShadowsForLight(
            entitySystem, tempArena, renderCommands, projection, view);
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawTracersSystem(ecs_EntitySystem *entitySystem,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands,
    vec3 cameraPosition, mat4 view, mat4 projection)
{
    vec3 *positions = NULL;
    vec3 *velocities = NULL;

    auto result = ecs_ExecuteQuery(entitySystem, tempArena, 3,
            GetComponent(PositionComponentTypeId, positions),
            GetComponent(VelocityComponentTypeId, velocities),
            HasComponent(TracerRendererComponentTypeId));

    for (u32 i = 0; i < result.length; ++i)
    {
        vec3 position = positions[i] + Vec3(0, -0.1f, 0);
        vec3 look = cameraPosition - position;
        if (LengthSq(look) > 2.5f) // Don't draw tracers too close to the camera
        {
            look = Normalize(look);
            vec3 up = Normalize(velocities[i]);
            vec3 right = Normalize(Cross(up, look));
            look = Normalize(Cross(right, up));

            mat4 rotation = Identity();
            rotation.col[0].xyz = right;
            rotation.col[1].xyz = up;
            rotation.col[2].xyz = look;

            mat4 transform = Translate(position) * rotation * Scale(Vec3(0.1f, 8.2f, 0.1f));

            RenderCommandDrawMeshUniforms *drawTracer = RenderCommandAllocate(
                    renderCommands, RenderCommandDrawMeshUniforms, 0);
            drawTracer->model = transform;
            drawTracer->view = view;
            drawTracer->projection = projection;
            drawTracer->mesh = VULKAN_MESH_QUAD;
            drawTracer->shader = VULKAN_SHADER_TEXTURE_ONLY;
            PushUniformTexture(
                drawTracer, UNIFORM_KEY_TEX_SAMPLER, VULKAN_MATERIAL_TRACER);
        }
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawEntitySpawnerSystem(ecs_EntitySystem *entitySystem,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands,
    vec3 cameraPosition, mat4 view, mat4 projection)
{
    vec3 *positions = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 2,
        GetComponent(PositionComponentTypeId, positions),
        HasComponent(EntitySpawnerComponentTypeId));

    for (u32 i = 0; i < result.length; ++i)
    {
        vec3 up = Vec3(0, 1, 0);

        vec3 look = Normalize(cameraPosition - positions[i]);
        vec3 right = Cross(up, look);
        up = Cross(look, right);

        mat4 rotation = Identity();
        rotation.col[0].xyz = right;
        rotation.col[1].xyz = up;
        rotation.col[2].xyz = look;

        mat4 transform = Translate(positions[i]) * rotation;

        RenderCommandDrawMeshUniforms *drawQuad = RenderCommandAllocate(
                renderCommands, RenderCommandDrawMeshUniforms, 0);
        drawQuad->model = transform;
        drawQuad->view = view;
        drawQuad->projection = projection;
        drawQuad->mesh = VULKAN_MESH_QUAD;
        drawQuad->shader = VULKAN_SHADER_COLOR_ONLY;
        PushUniformV4(drawQuad, UNIFORM_KEY_COLOR, Vec4(0.8, 0.8, 0.8, 1.0));
    }

    ecs_ReleaseQueryResult(result);
}

internal void DrawLightVisualisationSystem(ecs_EntitySystem *entitySystem,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands,
    vec3 cameraPosition, mat4 view, mat4 projection)
{
    vec3 *positions = NULL;
    vec3 *lightColors = NULL;

    ecs_QueryResult result = ecs_ExecuteQuery(entitySystem, tempArena, 2,
        GetComponent(PositionComponentTypeId, positions),
        GetComponent(LightColorComponentTypeId, lightColors));

    for (u32 i = 0; i < result.length; ++i)
    {
        vec3 up = Vec3(0, 1, 0);

        vec3 look = Normalize(cameraPosition - positions[i]);
        vec3 right = Cross(up, look);
        up = Cross(look, right);

        mat4 rotation = Identity();
        rotation.col[0].xyz = right;
        rotation.col[1].xyz = up;
        rotation.col[2].xyz = look;

        mat4 transform = Translate(positions[i]) * rotation;

        RenderCommandDrawMeshUniforms *drawQuad = RenderCommandAllocate(
                renderCommands, RenderCommandDrawMeshUniforms, 0);
        drawQuad->model = transform;
        drawQuad->view = view;
        drawQuad->projection = projection;
        drawQuad->mesh = VULKAN_MESH_QUAD;
        drawQuad->shader = VULKAN_SHADER_COLOR_ONLY;
        PushUniformV4(drawQuad, UNIFORM_KEY_COLOR, Vec4(lightColors[i], 1.0));

        bool hasRotation = false;
        ecs_HasComponent(entitySystem, RotationComponentTypeId,
            result.entities + i, 1, &hasRotation);
        if (hasRotation)
        {
            quat q = Quat();
            ecs_GetComponent(entitySystem, RotationComponentTypeId, result.entities + i,
                    1, &q, sizeof(q));

            mat4 rotationMatrix = Rotate(q);
            vec4 v = rotationMatrix * Vec4(0, 0, -1, 0);
            vec3 direction = Normalize(v.xyz);

            Debug_DrawLine(positions[i], positions[i] + direction * 2.5f, Vec3(1, 0, 0));
        }

    }

    ecs_ReleaseQueryResult(result);
}
