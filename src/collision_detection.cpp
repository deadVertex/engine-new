#define CD_ENABLE_DEBUG_DRAWING 0
#if CD_ENABLE_DEBUG_DRAWING

#define CD_DEBUG_DRAW(FLAGS, FUNCTION) \
    if (g_CollisionDebugDrawFlags & (FLAGS)) \
{ \
    FUNCTION; \
}

enum
{
    CD_DEBUG_DRAW_BVH_TRAVERSED_NODES = Bit(0),
    CD_DEBUG_DRAW_BVH_TESTED_TRIANGLES = Bit(1),
    CD_DEBUG_DRAW_RAY_HIT_NORMAL = Bit(2),
    CD_DEBUG_DRAW_BVH_TRIANGLE_RESULT = Bit(3),
    CD_DEBUG_DRAW_RAY = Bit(4),
    CD_DEBUG_DRAW_RAY_VERBOSE = Bit(5),
    CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES = Bit(6),
    CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES_VERBOSE = Bit(7),
};

global u64 g_CollisionDebugDrawFlags = (u64)-1;
#else
#define CD_DEBUG_DRAW(FLAGS, FUNCTION)
#endif

struct AabbShape
{
    vec3 min;
    vec3 max;
};

struct SphereShape
{
    vec3 center;
    float radius;
};

struct TriangleShape
{
    vec3 a;
    vec3 b;
    vec3 c;
};

union ConvexHullEdge
{
    struct
    {
        u32 vertexA;
        u32 vertexB;
    };

    u32 indices[2];
};

struct ConvexHullFaceIndices
{
    u32 *indices;
    u32 indexCount;
};

struct ConvexHullShape
{
    Plane *faces;
    ConvexHullFaceIndices *faceIndices;
    u32 faceCount;

    vec3 *vertices;
    u32 vertexCount;

    ConvexHullEdge *edges;
    u32 edgeCount;

    vec3 center;
};

struct TriangleMeshShape
{
    vec3 *vertices;
    ConvexHullEdge *edges;
    Plane *faces;
    ConvexHullShape *triangles;
    u32 triangleCount;
};

struct BvhNode
{
    AabbShape aabb;
    BvhNode *left;
    BvhNode *right;
    u32 triangleIndexOffset;
    u32 triangleIndexCount;
};

struct Bvh
{
    BvhNode *root;
    BvhNode *nodes;
    u32 nodeCount;
    u32 *triangleIndices;
    u32 triangleIndexCount;
};


struct RaycastResult
{
    vec3 hitPoint;
    vec3 hitNormal;
    bool isValid;
    float t;
};

enum
{
    PhysicsCall_None,
    PhysicsCall_RayAabb,
    PhysicsCall_AabbSweep,
    PhysicsCall_AabbSweepTriangleMesh,
    PhysicsCall_SphereAabbIntersect,
    PhysicsCall_AabbClosestPoint,
    PhysicsCall_RayTriangleMesh,
    PhysicsCall_SphereSweepTriangleMesh,
};

const char *g_PhysicsCallDescriptions[] =
{
    "None",
    "Ray AABB Intersect",
    "AABB vs AABB Sweep Test",
    "AABB vs Triangle Mesh Sweep Test",
    "Sphere AABB Intersect",
    "AABB Closest Point",
    "Ray Triangle Mesh Intersect",
    "Sphere Sweep Triangle Mesh",
};

struct PhysicsFunctionCall
{
    u32 type;
    union
    {
        struct
        {
            AabbShape aabb;
            vec3 start;
            vec3 end;

        } rayAabb;

        struct
        {
            vec3 halfExtents;
            AabbShape aabb;
            vec3 start;
            vec3 end;

        } aabbSweep;

        struct
        {
            vec3 halfExtents;
            vec3 start;
            vec3 end;
            TriangleMeshShape triangleMesh;
            Bvh bvh;

        } aabbSweepTriangleMesh;

        struct
        {
            SphereShape sphere;
            AabbShape aabb;

        } sphereAabbIntersect;

        struct
        {
            AabbShape aabb;
            vec3 p;

        } aabbClosestPoint;

        struct
        {
            vec3 start;
            vec3 end;
            TriangleMeshShape triangleMesh;
            Bvh bvh;

        } rayTriangleMesh;

        struct
        {
            vec3 start;
            vec3 end;
            float radius;
            TriangleMeshShape triangleMesh;
            Bvh bvh;

        } sphereSweepTriangleMesh;
    };
};

struct PhysicsDebugger
{
    PhysicsFunctionCall *callBuffer;
    u32 head;
    u32 tail;
    u32 length;
    u32 capacity;
};

global PhysicsDebugger *g_PhysicsDebugger = NULL;

inline void DrawAabb(AabbShape aabb, vec3 color)
{
    Debug_DrawAabb(aabb.min, aabb.max, color);
}

inline bool AabbContainsPoint(vec3 p, vec3 min, vec3 max)
{
    return (InRange(p.x, min.x, max.x) && InRange(p.y, min.y, max.y) &&
            InRange(p.z, min.z, max.z));
}

inline bool AabbContainsPoint(AabbShape aabb, vec3 p)
{
    return (InRange(p.x, aabb.min.x, aabb.max.x) &&
            InRange(p.y, aabb.min.y, aabb.max.y) &&
            InRange(p.z, aabb.min.z, aabb.max.z));
}

inline AabbShape AabbMerge(AabbShape a, AabbShape b)
{
    AabbShape result = {};
    result.min = Min(a.min, b.min);
    result.max = Max(a.max, b.max);

    return result;
}

inline AabbShape AabbGrow(AabbShape aabb, float radius)
{
    AabbShape result = {};
    result.min = aabb.min - Vec3(radius);
    result.max = aabb.max + Vec3(radius);

    return result;
}

inline vec3 AabbClosestPoint(AabbShape a, vec3 p)
{
    vec3 result = p;
    result = Max(result, a.min);
    result = Min(result, a.max);

    return result;
}

inline float AabbSquareDistanceToPoint(AabbShape a, vec3 p)
{
    float result = 0.0f;
    for (i32 i = 0; i < 3; ++i)
    {
        float v = p.data[i];
        if (v < a.min.data[i])
        {
            result += (a.min.data[i] - v) * (a.min.data[i] - v);
        }
        if (v > a.max.data[i])
        {
            result += (a.max.data[i] - v) * (a.max.data[i] - v);
        }
    }

    return result;
}

inline void DrawSphere(vec3 center, float radius, vec3 color, float lifeTime = 0.0f)
{
    vec3 v[3][2] = {{{1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f}},
        {{1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}},
        {{0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}}};

    for (u32 axis = 0; axis < 3; ++axis)
    {
        u32 segmentCount = 23;
        for (u32 i = 0; i < segmentCount; ++i)
        {
            float t0 = (i / (float)segmentCount) * 2.0f * PI;
            float t1 = ((i + 1) / (float)segmentCount) * 2.0f * PI;

            float x0 = Sin(t0) * radius;
            float y0 = Cos(t0) * radius;
            vec3 p0 = center + v[axis][0] * x0 + v[axis][1] * y0;

            float x1 = Sin(t1) * radius;
            float y1 = Cos(t1) * radius;
            vec3 p1 = center + v[axis][0] * x1 + v[axis][1] * y1;
            Debug_DrawLine(p0, p1, color, lifeTime);
        }
    }
}

inline void DrawCapsule(vec3 a, vec3 b, float radius, vec3 color)
{
    vec3 v = Normalize(b - a);

    vec3 up = v;
    vec3 right = {};
    if (Abs(Dot(up, Vec3(0, 0, 1))) > 0.0f )
    {
        right = Normalize(Cross(Vec3(0, 1, 0), up));
    }
    else
    {
        right = Normalize(Cross(up, Vec3(0, 0, 1)));
    }
    vec3 forward = Normalize(Cross(right, up));



    vec3 axis[3] = {right, up, forward};

    vec3 arcAxis[3][2] = {{right, up}, {right, forward}, {forward, up}};

    for (u32 axisIdx = 0; axisIdx < 3; ++axisIdx)
    {
        // TODO: epsilon
        if (Abs(Dot(v, axis[axisIdx])) == 0.0f)
        {
            u32 segmentCount = 13;
            for (u32 i = 0; i < segmentCount; ++i)
            {
                float t0 = (i / (float)segmentCount) * PI;
                float t1 = ((i + 1) / (float)segmentCount) * PI;

                float x0 = Sin(t0) * radius;
                float y0 = Cos(t0) * radius;
                vec3 p0 =
                    a - arcAxis[axisIdx][1] * x0 - arcAxis[axisIdx][0] * y0;

                float x1 = Sin(t1) * radius;
                float y1 = Cos(t1) * radius;
                vec3 p1 =
                    a - arcAxis[axisIdx][1] * x1 - arcAxis[axisIdx][0] * y1;
                Debug_DrawLine(p0, p1, color);

                vec3 p2 =
                    b + arcAxis[axisIdx][1] * x0 + arcAxis[axisIdx][0] * y0;
                vec3 p3 =
                    b + arcAxis[axisIdx][1] * x1 + arcAxis[axisIdx][0] * y1;
                Debug_DrawLine(p2, p3, color);
            }

            vec3 p0 = a + axis[axisIdx] * radius;
            vec3 p1 = b + axis[axisIdx] * radius;

            vec3 p2 = a - axis[axisIdx] * radius;
            vec3 p3 = b - axis[axisIdx] * radius;

            Debug_DrawLine(p0, p1, color);
            Debug_DrawLine(p2, p3, color);
        }
        else
        {
            u32 segmentCount = 23;
            for (u32 i = 0; i < segmentCount; ++i)
            {
                float t0 = (i / (float)segmentCount) * 2.0f * PI;
                float t1 = ((i + 1) / (float)segmentCount) * 2.0f * PI;

                float x0 = Sin(t0) * radius;
                float y0 = Cos(t0) * radius;
                vec3 p0 =
                    a + arcAxis[axisIdx][1] * x0 + arcAxis[axisIdx][0] * y0;

                float x1 = Sin(t1) * radius;
                float y1 = Cos(t1) * radius;
                vec3 p1 =
                    a + arcAxis[axisIdx][1] * x1 + arcAxis[axisIdx][0] * y1;
                Debug_DrawLine(p0, p1, color);

                vec3 p2 =
                    b + arcAxis[axisIdx][1] * x0 + arcAxis[axisIdx][0] * y0;
                vec3 p3 =
                    b + arcAxis[axisIdx][1] * x1 + arcAxis[axisIdx][0] * y1;
                Debug_DrawLine(p2, p3, color);
            }
        }
    }
}

inline vec3 SphereClosestPoint(vec3 center, float radius, vec3 p)
{
    vec3 result = p;
    vec3 v = p - center;
    float length = Length(v);

    if (length > radius)
    {
        // Normalize and then multiply by radius
        result = center + v * (1.0f / length) * radius;
    }

    return result;
}

inline vec3 CapsuleClosestPoint(vec3 a, vec3 b, float radius, vec3 p)
{
    float radiusSq = radius * radius;
    float distSq = SquareDistancePointLineSegment(a, b, p);

    vec3 result = p;
    if (distSq > radiusSq)
    {
        vec3 c = ClosestPointOnLineSegment(a, b, p);
        vec3 v = Normalize(p - c);
        result = c + v * radius;
    }

    return result;
}

inline b32 SphereAabbIntersect(vec3 center, float radius, AabbShape b)
{
    float sqDist = AabbSquareDistanceToPoint(b, center);
    return sqDist <= radius * radius;
}

inline bool SphereRayIntersect(vec3 center, float radius, vec3 start, vec3 d,
    float *intersectionT, vec3 *intersectionNormal, vec3 *intersectionPoint)
{
    vec3 m = start - center;
    float a = Dot(d, d);
    float b = Dot(m, d);
    float c = Dot(m, m) - radius * radius;

    if (c > 0.0f && b > 0.0f)
        return false;

    float disc = b * b - a * c;

    if (disc < 0.0f)
        return false;

    float t = (-b - SquareRoot(disc)) / a;
    //float t1 = -b + Sqrt(disc);

    if (t < 0.0f)
        t = 0.0f;

    // Don't think this check is correct
    //if (t * t > LengthSq(d))
        //return false;

    *intersectionT = t;
    *intersectionPoint = start + t * d;
    *intersectionNormal = Normalize(*intersectionPoint - center);

    return true;
}

inline bool SphereLineSegmentIntersect(vec3 center, float radius, vec3 start, vec3 end,
    float *intersectionT, vec3 *intersectionNormal, vec3 *intersectionPoint)
{
    vec3 d = end - start;
    return SphereRayIntersect(center, radius, start, d, intersectionT,
        intersectionNormal, intersectionPoint);
}


inline Plane CreatePlane(vec3 a, vec3 b, vec3 c)
{
    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Normalize(Cross(ab, ca));

    Plane result = CreatePlane(normal, a);
    return result;
}

inline vec3 TriangleClosestPoint(vec3 a, vec3 b, vec3 c, vec3 p)
{
    vec3 result = {};

    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Normalize(Cross(ab, ca));

    vec3 bc = c - b;

    //Debug_DrawPoint(t.a, Vec3(1, 0, 0));
    //Debug_DrawPoint(t.b, Vec3(0, 1, 0));
    //Debug_DrawPoint(t.c, Vec3(0, 0, 1));

    Plane plane = CreatePlane(normal, a);

    vec3 projectedPoint = ClosestPointOnPlane(plane, p);

    // Test if projected point is contained with the triangle
    b32 pointIsInTriangle = false;

    vec3 normalAb = Normalize(Cross(normal, ab));
    vec3 centroidAb = (a + b) * 0.5f;
    //Debug_DrawLine(centroidAb, centroidAb + normalAb * 0.5f, Vec3(0, 1, 1));

    vec3 normalAc = Normalize(Cross(normal, ca));
    vec3 centroidAc = (a + c) * 0.5f;
    //Debug_DrawLine(centroidAc, centroidAc + normalAc * 0.5f, Vec3(0, 1, 1));

    vec3 normalBc = Normalize(Cross(normal, bc));
    vec3 centroidBc = (b + c) * 0.5f;
    //Debug_DrawLine(centroidBc, centroidBc + normalBc * 0.5f, Vec3(0, 1, 1));

    // Test Ab plane
    if (Dot(normalAb, projectedPoint - a) <= 0.0f)
    {
        // Test Ac plane
        if (Dot(normalAc, projectedPoint - a) <= 0.0f)
        {
            // Test Bc plane
            if (Dot(normalBc, projectedPoint - b) <= 0.0f)
            {
                result = projectedPoint;
                pointIsInTriangle = true;
            }
        }
    }

    if (!pointIsInTriangle)
    {
        // Closest point lies on one of the triangle edges

        float d[3];
        d[0] = SquareDistancePointLineSegment(a, b, projectedPoint);
        d[1] = SquareDistancePointLineSegment(a, c, projectedPoint);
        d[2] = SquareDistancePointLineSegment(b, c, projectedPoint);

        u32 minIdx = 0;
        if (d[1] < d[minIdx])
        {
            minIdx = 1;
        }

        if (d[2] < d[minIdx])
        {
            minIdx = 2;
        }

        switch (minIdx)
        {
            case 0:
                result = ClosestPointOnLineSegment(a, b, projectedPoint);
                break;
            case 1:
                result = ClosestPointOnLineSegment(a, c, projectedPoint);
                break;
            case 2:
                result = ClosestPointOnLineSegment(b, c, projectedPoint);
                break;
            default:
                InvalidCodePath();
        }

    }

    return result;
}

inline vec3 TriangleClosestPoint(TriangleShape t, vec3 p)
{
    vec3 result = TriangleClosestPoint(t.a, t.b, t.c, p);
    return result;
}

internal bool RayPlaneTest(Plane plane, vec3 start, vec3 end,
        vec3 *intersectionPoint, float *intersectionT = NULL)
{
    vec3 v = end - start;
    float denom = Dot(plane.normal, v);

    // NOTE: May divide by 0 but should still give correct result.
    float t = (plane.distance - Dot(plane.normal, start)) / denom;

    if (t >= 0.0f && t <= 1.0f)
    {
        *intersectionPoint = start + t * v;
        if (intersectionT != NULL)
        {
            *intersectionT = t;
        }
        return true;
    }

    return false;
}

internal void DrawPlane(
    Plane p, vec3 origin, float scale, vec3 color, float lifeTime = 0.0f)
{
    vec3 center = ClosestPointOnPlane(p, origin);
    vec3 forward = p.normal;
    vec3 right;
    vec3 up;

    float epsilon = 0.0000000001f;

    float forwardDotUp = Dot(forward, Vec3(0, 1, 0));
    if (Abs(forwardDotUp) > epsilon)
    {
        up = Normalize(Cross(Vec3(1, 0, 0), forward));
        right = Normalize(Cross(forward, up));
    }
    else
    {
        right = Normalize(Cross(Vec3(0, 1, 0), forward));
        up = Normalize(Cross(forward, right));
    }

    Debug_DrawLine(center, center + forward, Vec3(0, 0, 1), lifeTime);
    Debug_DrawLine(center, center + right, Vec3(1, 0, 0), lifeTime);
    Debug_DrawLine(center, center + up, Vec3(0, 1, 0), lifeTime);

    vec3 vertices[4];
    vertices[0] = center + right * scale + up * scale;
    vertices[1] = center - right * scale + up * scale;
    vertices[2] = center - right * scale - up * scale;
    vertices[3] = center + right * scale - up * scale;

    Debug_DrawLine(vertices[0], vertices[1], color, lifeTime);
    Debug_DrawLine(vertices[1], vertices[2], color, lifeTime);
    Debug_DrawLine(vertices[2], vertices[0], color, lifeTime);

    Debug_DrawLine(vertices[0], vertices[2], color, lifeTime);
    Debug_DrawLine(vertices[2], vertices[3], color, lifeTime);
    Debug_DrawLine(vertices[3], vertices[0], color, lifeTime);
}

internal void DrawTriangle(TriangleShape t, vec3 color, float lifeTime = 0.0f)
{
    vec3 ab = t.b - t.a;
    vec3 ac = t.c - t.a;

    Debug_DrawLine(t.a, t.b, color, lifeTime);
    Debug_DrawLine(t.b, t.c, color, lifeTime);
    Debug_DrawLine(t.c, t.a, color, lifeTime);
}

internal void DrawTriangle(TriangleShape t, vec3 color, vec3 normalColor, float lifeTime = 0.0f)
{
    vec3 ab = t.b - t.a;
    vec3 ca = t.a - t.c;
    vec3 normal = Normalize(Cross(ab, ca));

    vec3 centroid = (t.a + t.b + t.c) * (1.0f / 3.0f);

    Plane p = CreatePlane(normal, t.a);

    Debug_DrawLine(t.a, t.b, color, lifeTime);
    Debug_DrawLine(t.b, t.c, color, lifeTime);
    Debug_DrawLine(t.c, t.a, color, lifeTime);

    Debug_DrawLine(centroid, centroid + normal * 0.5f, normalColor, lifeTime);
}

internal bool RayTriangleTest(TriangleShape t, vec3 rayStart, vec3 rayEnd,
    vec3 *intersectionPoint, vec3 *intersectionNormal, float *intersectionT)
{
    vec3 ab = t.b - t.a;
    vec3 ca = t.a - t.c;
    vec3 normal = Normalize(Cross(ab, ca));

    vec3 bc = t.c - t.b;

    //Debug_DrawPoint(t.a, Vec3(1, 0, 0));
    //Debug_DrawPoint(t.b, Vec3(0, 1, 0));
    //Debug_DrawPoint(t.c, Vec3(0, 0, 1));

    Plane p = CreatePlane(normal, t.a);

    vec3 potentialIntersectionPoint;
    float potentialIntersectionT;
    if (RayPlaneTest(p, rayStart, rayEnd, &potentialIntersectionPoint,
            &potentialIntersectionT))
    {
        vec3 normalAb = Normalize(Cross(normal, ab));
        vec3 centroidAb = (t.a + t.b) * 0.5f;
        //Debug_DrawLine(centroidAb, centroidAb + normalAb * 0.5f, Vec3(0, 1, 1));

        vec3 normalAc = Normalize(Cross(normal, ca));
        vec3 centroidAc = (t.a + t.c) * 0.5f;
        //Debug_DrawLine(centroidAc, centroidAc + normalAc * 0.5f, Vec3(0, 1, 1));

        vec3 normalBc = Normalize(Cross(normal, bc));
        vec3 centroidBc = (t.b + t.c) * 0.5f;
        //Debug_DrawLine(centroidBc, centroidBc + normalBc * 0.5f, Vec3(0, 1, 1));

        // Test Ab plane
        if (Dot(normalAb, potentialIntersectionPoint - t.a) <= 0.0f)
        {
            // Test Ac plane
            if (Dot(normalAc, potentialIntersectionPoint - t.a) <= 0.0f)
            {
                // Test Bc plane
                if (Dot(normalBc, potentialIntersectionPoint - t.b) <= 0.0f)
                {
                    *intersectionPoint = potentialIntersectionPoint;
                    *intersectionNormal = normal;
                    *intersectionT = potentialIntersectionT;
                    return true;
                }
            }
        }
    }

    return false;
}

internal bool RayObbTestSlow(vec3 boxOrigin, vec3 boxHalfExtents, vec3 right,
    vec3 up, vec3 forward, vec3 rayOrigin, vec3 rayDirection, float distance,
    vec3 *intersectionPoint, vec3 *intersectionNormal, float *intersectionT)
{
    float epsilon = 0.0000000000001f;

    Plane planes[6];

    vec3 basisVectors[3] = { right, up, forward };
    vec3 colors[3] = {Vec3(0.8, 0, 0.2), Vec3(0.2, 0.8, 0), Vec3(0, 0.2, 0.8)};

    float scale = 8.0f;
    for (u32 i = 0; i < 3; ++i)
    {
        Plane p0, p1;
        vec3 pointOnPlane0 = boxOrigin + basisVectors[i] * boxHalfExtents.data[i];
        vec3 pointOnPlane1 = boxOrigin - basisVectors[i] * boxHalfExtents.data[i];
        p0.normal = basisVectors[i];
        p0.distance = Dot(p0.normal, pointOnPlane0);
        p1.normal = -basisVectors[i];
        p1.distance = Dot(p1.normal, pointOnPlane1);

        //DrawPlane(p0, boxOrigin, scale, colors[i], 20.0f);
        //DrawPlane(p1, boxOrigin, scale, colors[i], 20.0f);

        planes[i*2] = p0;
        planes[i*2+1] = p1;
    }

    // TODO: R32_MAX;
    float tmin = 0.0f;
    float tmax = distance;

    vec3 hitNormal = Vec3(0);

    for (u32 i = 0; i < 3; ++i)
    {
        float t0, t1;


        Plane p0 = planes[i*2];
        Plane p1 = planes[i*2+1];
        // Plane test
        {
            float denom = Dot(p0.normal, rayDirection);
            if (Abs(denom) > epsilon)
            {
                t0 = (p0.distance - Dot(p0.normal, rayOrigin)) / denom;
            }
            else if ((rayOrigin.data[i] > boxOrigin.data[i] - boxHalfExtents.data[i]) &&
                    (rayOrigin.data[i] < boxOrigin.data[i] + boxHalfExtents.data[i]))
            {
                continue; // Parallel to slab planes but origin is contained within them
            }
            else
            {
                return false;
            }
        }

        {
            float denom = Dot(p1.normal, rayDirection);
            if (Abs(denom) > epsilon)
            {
                t1 = (p1.distance - Dot(p1.normal, rayOrigin)) / denom;
            }
            else if ((rayOrigin.data[i] > boxOrigin.data[i] - boxHalfExtents.data[i]) &&
                    (rayOrigin.data[i] < boxOrigin.data[i] + boxHalfExtents.data[i]))
            {
                continue; // Parallel to slab planes but origin is contained within them
            }
            else
            {
                return false;
            }
        }

        vec3 potentialNormal = p0.normal;
        if (t0 > t1)
        {
            Swap(&t0, &t1);
            potentialNormal = p1.normal;
        }

        if (t0 > tmin)
        {
            tmin = t0;
            hitNormal = potentialNormal;
        }
        if (t1 < tmax)
        {
            tmax = t1;
        }

        if (tmin > tmax)
        {
            return false;
        }

        if (tmax < 0)
        {
            return false; // Box is behind us
        }
    }

    *intersectionPoint = rayOrigin + tmin * rayDirection;
    *intersectionNormal = hitNormal;
    *intersectionT = tmin / distance;

    return true;
}


internal bool RayAabbTest(
    AabbShape aabb, vec3 start, vec3 end, RaycastResult *result)
{
    vec3 boxOrigin = (aabb.min + aabb.max) * 0.5f;
    vec3 boxHalfExtents = (aabb.max - aabb.min) * 0.5f;
    vec3 right = Vec3(1, 0, 0);
    vec3 up = Vec3(0, 1, 0);
    vec3 forward = Vec3(0, 0, 1);
    vec3 direction = end - start;
    float distance = Length(direction);
    direction = Normalize(direction);

    if (RayObbTestSlow(boxOrigin, boxHalfExtents, right, up, forward, start,
            direction, distance, &result->hitPoint, &result->hitNormal,
            &result->t))
    {
        result->isValid = true;
        return true;
    }

    return false;
}

internal bool AabbSweepTest(
    vec3 halfExtents, AabbShape other, vec3 start, vec3 end, RaycastResult *result)
{
    AabbShape sum = other;
    sum.min -= halfExtents;
    sum.max += halfExtents;

    if (RayAabbTest(sum, start, end, result))
    {
        return true;
    }

    return false;
}

internal bool AabbVsAabbIntersect(AabbShape a, AabbShape b)
{
    vec3 centerA = (a.min + a.max) * 0.5f;
    vec3 halfExtentsA = (a.max - a.min) * 0.5f;
    vec3 centerB = (b.min + b.max) * 0.5f;
    vec3 halfExtentsB = (b.max - b.min) * 0.5f;

    vec3 distance = centerB - centerA;

    // Test X axis
    if (halfExtentsA.x + halfExtentsB.x < Abs(distance.x))
    {
        return false;
    }

    // Test Y axis
    if (halfExtentsA.y + halfExtentsB.y < Abs(distance.y))
    {
        return false;
    }

    // Test Z axis
    if (halfExtentsA.z + halfExtentsB.z < Abs(distance.z))
    {
        return false;
    }

    return true;
}

#if 0
internal void DrawAabbSweepVolume(vec3 halfExtents, vec3 start, vec3 end)
{
    vec3 h = halfExtents;

    Debug_DrawPoint(start, Vec3(1, 0, 0));
    Debug_DrawPoint(end, Vec3(0, 0, 1));

    Debug_DrawLine(start, end, Vec3(0, 1, 0));

    vec3 dir = end - start;
    float length = Length(dir);
    dir = Normalize(dir);

    vec3 vertices[] = {
        start + Vec3(-h.x, -h.y, -h.z),
        start + Vec3(h.x, -h.y, -h.z),
        start + Vec3(h.x, -h.y, -h.z),

}
#endif

internal u32 ConvertAabbToTriangles(AabbShape aabb, TriangleShape *triangles, u32 maxTriangles)
{
    // clang-format off
    Vertex vertices[] = {
        // Top
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},

        // Bottom
        {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f}},

        // Back
        {{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {1.0f, 1.0f}},
        {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f}},
        {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {1.0f, 0.0f}},

        // Front
        {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
        {{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
        {{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},

        // Left
        {{-0.5f, 0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{-0.5f, -0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

        // Right
        {{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
        {{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
        {{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
        {{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
    };

    u32 indices[] = {
        2, 1, 0,
        0, 3, 2,

        4, 5, 6,
        6, 7, 4,

        8, 9, 10,
        10, 11, 8,

        14, 13, 12,
        12, 15, 14,

        16, 17, 18,
        18, 19, 16,

        22, 21, 20,
        20, 23, 22
    };

    // clang-format on

    Assert(ArrayCount(indices) % 3 == 0);
    u32 triangleCount = ArrayCount(indices) / 3;
    Assert(triangleCount <= maxTriangles);

    vec3 center = (aabb.min + aabb.max) * 0.5f;
    vec3 halfDim = (aabb.max - aabb.min) * 0.5f;
    vec3 scale = halfDim * 2.0f;

    for (u32 i = 0; i < triangleCount; ++i)
    {
        TriangleShape *t = triangles + i;
        u32 a = indices[i*3];
        t->a = vertices[a].pos * scale + center;

        u32 b = indices[i*3 + 2];
        t->b = vertices[b].pos * scale + center;

        u32 c = indices[i*3 + 1];
        t->c = vertices[c].pos * scale + center;
    }

    return triangleCount;
}

inline vec3 CalculateCentroid(vec3 *points, u32 count)
{
    vec3 result = {};
    for (u32 i = 0; i < count; ++i)
    {
        result += points[i];
    }

    if (count > 0)
    {
        result = result * (1.0f / (float)count);
    }

    return result;
}

internal ConvexHullShape CreateTriangleHull(vec3 a, vec3 b, vec3 c, MemoryArena *arena)
{
    ConvexHullShape result = {};
    result.vertexCount = 3;
    result.edgeCount = 3;
    result.faceCount = 1;
    result.vertices = AllocateArray(arena, vec3, result.vertexCount);
    result.edges = AllocateArray(arena, ConvexHullEdge, result.edgeCount);
    result.faces = AllocateArray(arena, Plane, result.faceCount);

    result.vertices[0] = a;
    result.vertices[1] = b;
    result.vertices[2] = c;

    result.center = CalculateCentroid(result.vertices, result.vertexCount);

    result.edges[0] = {0, 1};
    result.edges[1] = {1, 2};
    result.edges[2] = {2, 0};

    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Normalize(Cross(ab, ca));

    //Debug_DrawLine(result.center, result.center + normal, Vec3(1, 0, 1));

    result.faces[0] = CreatePlane(normal, a);

    return result;
}

internal ConvexHullShape CreateBoxHull(
    vec3 h, mat4 transform, MemoryArena *arena)
{
    ConvexHullShape result = {};
    result.vertexCount = 8;
    result.vertices = AllocateArray(arena, vec3, result.vertexCount);
    result.edgeCount = 12;
    result.edges = AllocateArray(arena, ConvexHullEdge, result.edgeCount);
    result.faceCount = 6;
    result.faces = AllocateArray(arena, Plane, result.faceCount);

    result.vertices[0] = -h;
    result.vertices[1] = Vec3(-h.x, -h.y, h.z);
    result.vertices[2] = Vec3(h.x, -h.y, h.z);
    result.vertices[3] = Vec3(h.x, -h.y, -h.z);

    result.vertices[4] = Vec3(-h.x, h.y, -h.z);
    result.vertices[5] = Vec3(-h.x, h.y, h.z);
    result.vertices[6] = h;
    result.vertices[7] = Vec3(h.x, h.y, -h.z);

    for (u32 i = 0; i < 8; ++i)
    {
        vec4 transformedVertex = transform * Vec4(result.vertices[i], 1.0f);
        result.vertices[i].x = transformedVertex.x;
        result.vertices[i].y = transformedVertex.y;
        result.vertices[i].z = transformedVertex.z;
    }

    result.center = CalculateCentroid(result.vertices, result.vertexCount);

    result.edges[0] = {0, 1};
    result.edges[1] = {1, 2};
    result.edges[2] = {2, 3};
    result.edges[3] = {3, 0};

    result.edges[4] = {4, 5};
    result.edges[5] = {5, 6};
    result.edges[6] = {6, 7};
    result.edges[7] = {7, 4};

    result.edges[8] = {0, 4};
    result.edges[9] = {1, 5};
    result.edges[10] = {2, 6};
    result.edges[11] = {3, 7};

    vec3 normals[6] = {Vec3(0, -1, 0), Vec3(0, 0, -1), Vec3(0, 0, 1),
        Vec3(-1, 0, 0), Vec3(1, 0, 0), Vec3(0, 1, 0)};

    for (u32 i = 0; i < 6; ++i)
    {
        vec4 transformNormal = transform * Vec4(normals[i], 0.0f);
        normals[i].x = transformNormal.x;
        normals[i].y = transformNormal.y;
        normals[i].z = transformNormal.z;
    }

    result.faces[0] = CreatePlane(normals[0], result.vertices[0]);
    result.faces[1] = CreatePlane(normals[1], result.vertices[0]);
    result.faces[2] = CreatePlane(normals[2], result.vertices[6]);
    result.faces[3] = CreatePlane(normals[3], result.vertices[0]);
    result.faces[4] = CreatePlane(normals[4], result.vertices[6]);
    result.faces[5] = CreatePlane(normals[5], result.vertices[6]);

    return result;
}

internal vec3 ConvexHullClosestPoint(ConvexHullShape hull, vec3 p)
{
    vec3 localP = p - hull.center; // Convert to the convex hull's coordinates system
    vec3 closestPoint = p;
    float minDistanceSq = -1.0f;
    for (u32 faceIdx = 0; faceIdx < hull.faceCount; ++faceIdx)
    {
        Plane face = hull.faces[faceIdx];

        vec3 pointOnPlane = face.normal * face.distance;

        // Test if localP is on the outward facing side of the plane
        float projection = Dot(face.normal, localP - pointOnPlane);

        if (projection > 0.0f)
        {
            vec3 c = ClosestPointOnPlane(face, localP);

            // Test if closest point is contained within the face
            b32 isPointInFace = true;
            vec3 worldC = c + hull.center;

            ConvexHullFaceIndices faceIndices = hull.faceIndices[faceIdx];
            for (u32 idx = 0; idx < faceIndices.indexCount; ++idx)
            {
                u32 nextIdx = (idx + 1) % faceIndices.indexCount;

                u32 indexA = faceIndices.indices[idx];
                u32 indexB = faceIndices.indices[nextIdx];

                vec3 a = hull.vertices[indexA]; // In world space
                vec3 b = hull.vertices[indexB]; // In world space

                vec3 ab = b - a;
                vec3 normalAb = Normalize(Cross(face.normal, ab));
                vec3 centroidAb = (a + b) * 0.5f;
                Plane edgePlane = CreatePlane(normalAb, a);
                //DrawPlane(edgePlane, centroidAb, 0.5f, Vec3(0.8, 0.2, 0.8));

                if (Dot(normalAb, worldC - a) > 0.0f)
                {
                    isPointInFace = false;
                }
            }

            if (!isPointInFace)
            {
                for (u32 idx = 0; idx < faceIndices.indexCount; ++idx)
                {
                    u32 nextIdx = (idx + 1) % faceIndices.indexCount;

                    u32 indexA = faceIndices.indices[idx];
                    u32 indexB = faceIndices.indices[nextIdx];

                    vec3 a = hull.vertices[indexA]; // In world space
                    vec3 b = hull.vertices[indexB]; // In world space

                    float distSq = SquareDistancePointLineSegment(a, b, worldC);
                    if (minDistanceSq < 0.0f || distSq < minDistanceSq)
                    {
                        minDistanceSq = distSq;
                        closestPoint = ClosestPointOnLineSegment(a, b, worldC);
                    }
                }
            }
            else
            {
                float distSq = LengthSq(p - worldC);
                if (minDistanceSq < 0.0f || distSq < minDistanceSq)
                {
                    minDistanceSq = distSq;
                    closestPoint = worldC;
                }
            }
        }
    }

    return closestPoint;
}

internal AabbShape ComputeAabbOfConvexHull(ConvexHullShape hull)
{
    Assert(hull.vertexCount > 0);

    AabbShape result = {};
    result.min = hull.vertices[0];
    result.max = hull.vertices[0];

    for (u32 vertexIdx = 0; vertexIdx < hull.vertexCount; ++vertexIdx)
    {
        vec3 vertex = hull.vertices[vertexIdx];
        result.min = Min(result.min, vertex);
        result.max = Max(result.max, vertex);
    }

    return result;
}

internal bool RayConvexHullTest(ConvexHullShape hull, vec3 start, vec3 end,
    vec3 *intersectionPoint, vec3 *intersectionNormal, float *intersectionT)
{
    vec3 delta = end - start;
    vec3 direction = Normalize(delta);
    float distance = Length(delta);
    float epsilon = 0.0000000000001f; // Copied from RayObbTestSlow

    float tmin = 0.0f;
    float tmax = distance;

    vec3 hitNormal = Vec3(0);

    for (u32 i = 0; i < hull.faceCount; ++i)
    {
        // t = (d - Dot(n, s)) / Dot(n, d)
        Plane plane = hull.faces[i];
        plane.distance += Dot(plane.normal, hull.center);
        //DrawPlane(plane, hull.center, 1.5f,
            //Vec3(0.2, 0.2, 0.6), 30.0f);
        float denom = Dot(plane.normal, direction);
        if (Abs(denom) > epsilon)
        {
            float k = Dot(plane.normal, start);
            float t = (plane.distance - k) / denom;

            // Determine which side of the plane we have hit
            if (denom < 0.0f)
            {
                // hit front side so t is for the entry point
                if (t > tmin)
                {
                    tmin = t;
                    hitNormal = plane.normal;
                }
            }
            else
            {
                // hit backside so t is for the exit point
                tmax = Min(tmax, t);
            }

            if (tmin > tmax)
            {
                return false;
            }

            if (tmax < 0.0f)
            {
                return false; // exit point is behind us
            }
        }
        else
        {
            // TODO: Handle case when origin is contained inside the shape
            return false;
        }
    }

    *intersectionPoint = start + tmin * direction;
    *intersectionNormal = hitNormal;
    *intersectionT = tmin / distance;

    return true;
}

inline vec3 Support(vec3 *vertices, u32 count, vec3 v)
{
    float maxProjection = -F32_MAX;
    vec3 result = Vec3(0);

    for (u32 i = 0; i < count; ++i)
    {
        float projection = Dot(vertices[i], v);
        if (projection > maxProjection)
        {
            maxProjection = projection;
            result = vertices[i];
        }
    }

    return result;
}

internal bool FindSeparatingAxisForFaces(ConvexHullShape a, ConvexHullShape b)
{
    for (u32 i = 0; i < a.faceCount; ++i)
    {
        Debug_BeginTimedBlock(FindSeparatingAxisForFace);
        Plane plane = a.faces[i];
        vec3 separatingAxis = plane.normal;

        vec3 origin = a.center;

        vec3 s[4];

        s[0] = Support(a.vertices, a.vertexCount, -separatingAxis);
        s[1] = Support(a.vertices, a.vertexCount, separatingAxis);
        s[2] = Support(b.vertices, b.vertexCount, -separatingAxis);
        s[3] = Support(b.vertices, b.vertexCount, separatingAxis);

        vec3 r[4];
        for (u32 j = 0; j < 4; ++j)
        {
            r[j] = s[j] - origin;
        }

        float t[4];
        for (u32 j = 0; j < 4; ++j)
        {
            t[j] = Dot(r[j], separatingAxis);
        }

        bool overlap = false;
        if (t[1] > t[2])
        {
            // Overlap
            overlap = true;
        }

        vec3 p[4];
        for (u32 j = 0; j < 4; ++j)
        {
            p[j] = origin + t[j] * separatingAxis;
        }
        Debug_EndTimedBlock(FindSeparatingAxisForFace);

        //if (!overlap)
        {
#if 0
            Debug_DrawLine(
                origin, origin + separatingAxis * 10.0f, Vec3(0.2, 0.4, 0.6));
            Debug_DrawLine(
                origin, origin - separatingAxis * 10.0f, Vec3(0.2, 0.4, 0.6));

            for (u32 j = 0; j < 4; ++j)
            {
                vec3 color = j >= 2 ? Vec3(0.6, 1.0, 0.2) : Vec3(1, 0.6, 0.2);
                Debug_DrawPoint(p[j], color);
                Debug_DrawLine(p[j], s[j], color);
            }

            if (overlap)
            {
                Debug_DrawLine(p[1], p[2], Vec3(1));
            }
#endif

            if (!overlap)
                return true;
        }
    }
    return false;
}

//#define SAT_ENABLE_DEBUG

global u32 g_SelectedSatIteration = 0;
global bool g_EnableSatDebug = false;

internal bool FindSeparatingAxisForEdges(
    ConvexHullShape hullA, ConvexHullShape hullB)
{
#ifdef SAT_ENABLE_DEBUG
    u32 maxIndices = hullA.edgeCount * hullB.edgeCount;
    u32 selectedIndex = g_SelectedSatIteration;
    selectedIndex = selectedIndex % maxIndices;

    if (g_EnableSatDebug)
    {
        Debug_Printf("Selected SAT iteration: %u\n", selectedIndex);
    }
#endif

    for (u32 i = 0; i < hullA.edgeCount; ++i)
    {
        ConvexHullEdge edgeA = hullA.edges[i];
        vec3 vertexA0 = hullA.vertices[edgeA.indices[0]];
        vec3 vertexA1 = hullA.vertices[edgeA.indices[1]];
        vec3 directionA = vertexA1 - vertexA0;

        for (u32 j = 0; j < hullB.edgeCount; ++j)
        {
            Debug_BeginTimedBlock(FindSeparatingAxisForEdgePair);

            ConvexHullEdge edgeB = hullB.edges[j];
            vec3 vertexB0 = hullB.vertices[edgeB.indices[0]];
            vec3 vertexB1 = hullB.vertices[edgeB.indices[1]];
            vec3 directionB = vertexB1 - vertexB0;

            vec3 separatingAxis = Cross(directionB, directionA);

#ifdef SAT_ENABLE_DEBUG
            vec3 origin = hullA.center;
#endif

            if (IsZero(separatingAxis))
            {
                vec3 n = Cross(directionA, vertexB0 - vertexA0);
                separatingAxis = Cross(directionA, n);
                if (IsZero(separatingAxis))
                {
                    // Ignore testing axis as edges must be on a line
                    continue;
                }
            }

#ifdef SAT_ENABLE_DEBUG
            // Not necessary but nice for visualization
            separatingAxis = Normalize(separatingAxis);
#endif

            vec3 support[4];
            support[0] =
                Support(hullA.vertices, hullA.vertexCount, -separatingAxis);
            support[1] =
                Support(hullA.vertices, hullA.vertexCount, separatingAxis);
            support[2] =
                Support(hullB.vertices, hullB.vertexCount, -separatingAxis);
            support[3] =
                Support(hullB.vertices, hullB.vertexCount, separatingAxis);

            float t[4];
            for (u32 k = 0; k < 4; ++k)
            {
#ifdef SAT_ENABLE_DEBUG
                t[k] = Dot(support[k] - origin, separatingAxis);
#else
                t[k] = Dot(support[k], separatingAxis);
#endif
            }

            if (t[0] > t[1])
            {
                LOG_DEBUG("Swap");
                Swap(&t[0], &t[1]);
            }
            if (t[2] > t[3])
            {
                LOG_DEBUG("Swap");
                Swap(&t[2], &t[3]);
            }

            // 1d sphere vs sphere style overlap test
            float tc[2];
            tc[0] = (t[0] + t[1]) * 0.5f;
            tc[1] = (t[2] + t[3]) * 0.5f;

            float r[2];
            r[0] = Abs(t[0] - t[1]) * 0.5f;
            r[1] = Abs(t[2] - t[3]) * 0.5f;

            float d = Abs(tc[0] - tc[1]);

            bool overlap = false;
            if (d < r[0] + r[1])
            {
                overlap = true;
            }

            Debug_EndTimedBlock(FindSeparatingAxisForEdgePair);

#ifdef SAT_ENABLE_DEBUG
            u32 currentIndex = i * hullB.edgeCount + j;
            if (currentIndex == selectedIndex && g_EnableSatDebug)
            {
                vec3 centerA = (vertexA0 + vertexA1) * 0.5f;
                vec3 centerB = (vertexB0 + vertexB1) * 0.5f;

                Debug_DrawPoint(origin, Vec3(1));
                Debug_DrawLine(centerB, centerB + separatingAxis * 0.5f,
                        Vec3(1, 0, 1));

                //Debug_PrintInWorld(support[0], "support[0] = {%g, %g, %g}",
                        //support[0].x, support[0].y, support[0].z);
                //Debug_PrintInWorld(support[1], "support[1] = {%g, %g, %g}",
                        //support[1].x, support[1].y, support[1].z);
                //Debug_PrintInWorld(support[2], "support[2] = {%g, %g, %g}",
                        //support[2].x, support[2].y, support[2].z);
                //Debug_PrintInWorld(support[3], "support[3] = {%g, %g, %g}",
                        //support[3].x, support[3].y, support[3].z);

                Debug_DrawLine(origin, origin + separatingAxis * 10.0f,
                    Vec3(0.8, 0.8, 0.8));
                Debug_DrawLine(origin, origin - separatingAxis * 10.0f,
                    Vec3(0.8, 0.8, 0.8));

                Plane separatingPlane = CreatePlane(separatingAxis, origin);
                DrawPlane(separatingPlane, origin, 1.0f, Vec3(1.0, 1.0, 0.2));

                vec3 p[4];
                for (u32 k = 0; k < 4; ++k)
                {
                    p[k] = origin + t[k] * separatingAxis;
                }


                for (u32 k = 0; k < 4; ++k)
                {
                    Debug_PrintInWorld(p[k], "t[%u] = %g", k, t[k]);

                    vec3 color =
                        k >= 2 ? Vec3(0.1, 0.8, 1.0) : Vec3(0.8, 0.1, 0.4);
                    Debug_DrawPoint(p[k], color);
                    Debug_DrawLine(p[k], support[k], color);
                }

                if (overlap)
                {
                    Debug_Printf("Overlap!\n");
                    Debug_DrawLine(p[1], p[2], Vec3(1));
                }
            }
#endif

            if (!overlap)
            {
                return true;
            }
        }
    }

    return false;
}

internal bool ConvexHullVsConvexHullIntersect(
    ConvexHullShape a, ConvexHullShape b)
{
    Debug_BeginTimedBlock(ConvexHullVsConvexHullIntersect);
    if (FindSeparatingAxisForFaces(a, b))
    {
        Debug_EndTimedBlock(ConvexHullVsConvexHullIntersect);
        return false;
    }

    if (FindSeparatingAxisForFaces(b, a))
    {
        Debug_EndTimedBlock(ConvexHullVsConvexHullIntersect);
        return false;
    }

    if (FindSeparatingAxisForEdges(a, b))
    {
        Debug_EndTimedBlock(ConvexHullVsConvexHullIntersect);
        return false;
    }

    Debug_EndTimedBlock(ConvexHullVsConvexHullIntersect);
    return true;
}

internal bool FindSeparatingAxisForFaces(ConvexHullShape a, ConvexHullShape b,
    vec3 v, float *tFirst, float *tLast, vec3 *intersectionNormal, float signMul = 1.0f)
{
    for (u32 i = 0; i < a.faceCount; ++i)
    {
        Debug_BeginTimedBlock(FindSeparatingAxisForFace2);

        Plane plane = a.faces[i];
        vec3 separatingAxis = plane.normal;

        vec3 origin = a.center; // Only needed for visualizations

        vec3 s[4];

        s[0] = Support(a.vertices, a.vertexCount, -separatingAxis);
        s[1] = Support(a.vertices, a.vertexCount, separatingAxis);
        s[2] = Support(b.vertices, b.vertexCount, -separatingAxis);
        s[3] = Support(b.vertices, b.vertexCount, separatingAxis);

        vec3 r[4];
        for (u32 j = 0; j < 4; ++j)
        {
            r[j] = s[j] - origin;
        }

        float t[4];
        for (u32 j = 0; j < 4; ++j)
        {
            t[j] = Dot(r[j], separatingAxis);
        }

        float vt = Dot(v, separatingAxis);
        if (vt < 0.0f)
        {
            if (t[1] < t[2])
            {
                Debug_EndTimedBlock(FindSeparatingAxisForFace2);
                // separating axis found and moving apart
                return true;
            }
            if (t[3] < t[0])
            {
                float t0 = (t[3] - t[0]) / vt;
                if (t0 > *tFirst)
                {
                    *tFirst = t0;
                    *intersectionNormal = separatingAxis * signMul;
                }
            }
            if (t[1] > t[2])
            {
                float t1 = (t[2] - t[1]) / vt;
                *tLast = Min(t1, *tLast);
            }
        }
        else if (vt > 0.0f)
        {
            if (t[0] > t[3])
            {
                Debug_EndTimedBlock(FindSeparatingAxisForFace2);
                // separating axis found and moving apart
                return true;
            }
            if (t[1] < t[2])
            {
                float t0 = (t[2] - t[1]) / vt;
                if (t0 > *tFirst)
                {
                    *tFirst = t0;
                    *intersectionNormal = -separatingAxis * signMul;
                }
            }
            if (t[3] > t[0])
            {
                float t1 = (t[3] - t[0]) / vt;
                *tLast = Min(t1, *tLast);
            }
        }

        if (*tFirst > *tLast)
        {
            Debug_EndTimedBlock(FindSeparatingAxisForFace2);
            // No overlap possible
            return true;
        }

        Debug_EndTimedBlock(FindSeparatingAxisForFace2);
    }
    return false;
}

internal bool FindSeparatingAxisForEdges(ConvexHullShape hullA,
    ConvexHullShape hullB, vec3 v, float *tFirst, float *tLast,
    vec3 *intersectionNormal)
{
    for (u32 i = 0; i < hullA.edgeCount; ++i)
    {
        ConvexHullEdge edgeA = hullA.edges[i];
        vec3 vertexA0 = hullA.vertices[edgeA.indices[0]];
        vec3 vertexA1 = hullA.vertices[edgeA.indices[1]];
        vec3 directionA = vertexA1 - vertexA0;

        for (u32 j = 0; j < hullB.edgeCount; ++j)
        {
            Debug_BeginTimedBlock(FindSeparatingAxisForEdgePair2);

            ConvexHullEdge edgeB = hullB.edges[j];
            vec3 vertexB0 = hullB.vertices[edgeB.indices[0]];
            vec3 vertexB1 = hullB.vertices[edgeB.indices[1]];
            vec3 directionB = vertexB1 - vertexB0;

            vec3 separatingAxis = Cross(directionB, directionA);

            if (IsZero(separatingAxis))
            {
                vec3 n = Cross(directionA, vertexB0 - vertexA0);
                separatingAxis = Cross(directionA, n);
                if (IsZero(separatingAxis))
                {
                    Debug_EndTimedBlock(FindSeparatingAxisForEdgePair2);
                    // Ignore testing axis as edges must be on a line
                    continue;
                }
            }

            vec3 support[4];
            support[0] =
                Support(hullA.vertices, hullA.vertexCount, -separatingAxis);
            support[1] =
                Support(hullA.vertices, hullA.vertexCount, separatingAxis);
            support[2] =
                Support(hullB.vertices, hullB.vertexCount, -separatingAxis);
            support[3] =
                Support(hullB.vertices, hullB.vertexCount, separatingAxis);

            float t[4];
            for (u32 k = 0; k < 4; ++k)
            {
                t[k] = Dot(support[k], separatingAxis);
            }

            float vt = Dot(v, separatingAxis);
            if (vt < 0.0f)
            {
                if (t[1] < t[2])
                {
                    Debug_EndTimedBlock(FindSeparatingAxisForEdgePair2);
                    // separating axis found and moving apart
                    return true;
                }
                if (t[3] < t[0])
                {
                    float t0 = (t[3] - t[0]) / vt;
                    if (t0 > *tFirst)
                    {
                        *tFirst = t0;
                        *intersectionNormal = Normalize(separatingAxis);
                    }
                }
                if (t[1] > t[2])
                {
                    float t1 = (t[2] - t[1]) / vt;
                    *tLast = Min(t1, *tLast);
                }
            }
            else if (vt > 0.0f)
            {
                if (t[0] > t[3])
                {
                    Debug_EndTimedBlock(FindSeparatingAxisForEdgePair2);
                    // separating axis found and moving apart
                    return true;
                }
                if (t[1] < t[2])
                {
                    float t0 = (t[2] - t[1]) / vt;
                    if (t0 > *tFirst)
                    {
                        *tFirst = t0;
                        *intersectionNormal = Normalize(-separatingAxis);
                    }
                }
                if (t[3] > t[0])
                {
                    float t1 = (t[3] - t[0]) / vt;
                    *tLast = Min(t1, *tLast);
                }
            }
            else
            {
                if (t[0] > t[1])
                {
                    LOG_DEBUG("Swap");
                    Swap(&t[0], &t[1]);
                }
                if (t[2] > t[3])
                {
                    LOG_DEBUG("Swap");
                    Swap(&t[2], &t[3]);
                }

                if (t[2] > t[1])
                {
                    Debug_EndTimedBlock(FindSeparatingAxisForEdgePair2);
                    return true; // Separating axis found
                }
            }

            if (*tFirst > *tLast)
            {
                Debug_EndTimedBlock(FindSeparatingAxisForEdgePair2);
                // No overlap possible
                return true;
            }
            Debug_EndTimedBlock(FindSeparatingAxisForEdgePair2);
        }
    }

    return false;
}

// B is static, v is the displacement for A
internal bool ConvexHullVsConvexHullSweepTest(ConvexHullShape a,
    ConvexHullShape b, vec3 v, float *tFirst, float *tLast,
    vec3 *intersectionNormal)
{
    Debug_BeginTimedBlock(ConvexHullVsConvexHullSweepTest);

    *tFirst = 0.0f;
    *tLast = 1.0f;
    if (ConvexHullVsConvexHullIntersect(a, b))
    {
        *tFirst = 0.0f;
        *tLast = 0.0f;
        Debug_EndTimedBlock(ConvexHullVsConvexHullSweepTest);
        return true;
    }

    if (FindSeparatingAxisForFaces(a, b, v, tFirst, tLast, intersectionNormal))
    {
        Debug_EndTimedBlock(ConvexHullVsConvexHullSweepTest);
        return false;
    }

    if (FindSeparatingAxisForFaces(b, a, -v, tFirst, tLast, intersectionNormal, -1.0f))
    {
        Debug_EndTimedBlock(ConvexHullVsConvexHullSweepTest);
        return false;
    }

    if (FindSeparatingAxisForEdges(a, b, v, tFirst, tLast, intersectionNormal))
    {
        Debug_EndTimedBlock(ConvexHullVsConvexHullSweepTest);
        return false;
    }

    Debug_EndTimedBlock(ConvexHullVsConvexHullSweepTest);
    return true;
}

internal TriangleMeshShape CreateTriangleMesh(
    vec3 *vertices, u32 vertexCount, MemoryArena *arena)
{
    Assert(vertexCount % 3 == 0);
    u32 triangleCount = vertexCount / 3;

    TriangleMeshShape result = {};
    result.triangleCount = triangleCount;
    result.vertices = AllocateArray(arena, vec3, vertexCount);
    result.edges = AllocateArray(arena, ConvexHullEdge, triangleCount * 3);
    result.faces = AllocateArray(arena, Plane, triangleCount);
    result.triangles = AllocateArray(arena, ConvexHullShape, triangleCount);

    memcpy(result.vertices, vertices, vertexCount * sizeof(vec3));

    for (u32 i = 0; i < triangleCount; ++i)
    {
        vec3 a = vertices[i*3];
        vec3 b = vertices[i*3+1];
        vec3 c = vertices[i*3+2];

        ConvexHullShape *triangle = result.triangles + i;
        triangle->vertexCount = 3;
        triangle->edgeCount = 3;
        triangle->faceCount = 1;

        triangle->vertices = result.vertices + i * 3;

        triangle->center = CalculateCentroid(triangle->vertices, triangle->vertexCount);

        triangle->edges = result.edges + i * 3;
        triangle->edges[0] = {0, 1};
        triangle->edges[1] = {1, 2};
        triangle->edges[2] = {2, 0};

        vec3 ab = b - a;
        vec3 ca = a - c;
        vec3 normal = Normalize(Cross(ab, ca));

        normal = normal;

        triangle->faces = result.faces + i;
        triangle->faces[0] = CreatePlane(normal, a);
    }

    return result;
}

#if 0
struct Bvh
{
    BvhNode *nodes;
    u32 capacity;
    u32 count;
};
#endif

enum
{
    AXIS_X,
    AXIS_Y,
    AXIS_Z,
};

#if 0
internal Bvh CreateBvh(MemoryArena *arena, u32 capacity)
{
    Bvh tree = {};
    tree.nodes = AllocateArray(arena, BvhNode, capacity);
    tree.capacity = capacity;

    return tree;
}
#endif

struct FindNodesToMergeResult
{
    u32 first;
    u32 second;
};

internal FindNodesToMergeResult FindNodesToMerge(BvhNode **nodes, u32 count)
{
    Debug_BeginTimedBlock(FindNodesToMerge);

    u32 closestPair[2] = {};
    float minLengthSq = FLT_MAX;

    for (u32 a = 0; a < count; ++a)
    {
        AabbShape aabbA = nodes[a]->aabb;
        vec3 centroidA = (aabbA.min + aabbA.max) * 0.5f;

        for (u32 b = a + 1; b < count; ++b)
        {
            AabbShape aabbB = nodes[b]->aabb;
            vec3 centroidB = (aabbB.min + aabbB.max) * 0.5f;

            float lengthSq = LengthSq(centroidB - centroidA);
            if (lengthSq < minLengthSq)
            {
                minLengthSq = lengthSq;
                closestPair[0] = a;
                closestPair[1] = b;
            }
        }
    }

    Assert(minLengthSq < FLT_MAX);

    FindNodesToMergeResult result = {};
    result.first = closestPair[0];
    result.second = closestPair[1];

    Debug_EndTimedBlock(FindNodesToMerge);

    return result;
}

#if 0
internal void BottomUpBuildBvh(Bvh *tree, TriangleMeshShape *mesh,
    MemoryArena *tempArena, MemoryArena *storageArena)
{
    Debug_BeginTimedBlock(BottomUpBuildBvh);
    Assert(mesh->triangleCount > 0);

    LOG_DEBUG("N=%u\n\n\n\n", mesh->triangleCount);

    u32 maxNodes = mesh->triangleCount * 2;
    BvhNode *nodeStorage = AllocateArray(storageArena, BvhNode, maxNodes);
    BvhNode **activeSet = AllocateArray(tempArena, BvhNode*, maxNodes);
    for (u32 triangleIdx = 0; triangleIdx < mesh->triangleCount; triangleIdx++)
    {
        BvhNode *leaf = nodeStorage + triangleIdx;
        leaf->aabb = ComputeAabbOfConvexHull(mesh->triangles[triangleIdx]);
        leaf->left = NULL;
        leaf->right = NULL;
        leaf->triangleIdx = triangleIdx;

        activeSet[triangleIdx] = leaf;
    }
    u32 nodeCount = mesh->triangleCount;


    u32 activeSetLength = mesh->triangleCount;
    while (activeSetLength > 1)
    {
        // Find two nearest nodes
        FindNodesToMergeResult nodesToMerge =
            FindNodesToMerge(activeSet, activeSetLength);

        Assert(nodeCount < maxNodes);

        BvhNode *pair = nodeStorage + nodeCount++;
        pair->triangleIdx = -1;
        pair->left = activeSet[nodesToMerge.first];
        pair->right = activeSet[nodesToMerge.second];

        Assert(pair->left != NULL);
        Assert(pair->right != NULL);
        pair->aabb = AabbMerge(pair->left->aabb, pair->right->aabb);

        u32 min = MinU(nodesToMerge.first, nodesToMerge.second);
        u32 max = MaxU(nodesToMerge.first, nodesToMerge.second);
        activeSet[min] = pair;
        activeSet[max] = activeSet[activeSetLength - 1];
        activeSetLength--;
    }

    tree->root = activeSet[0];
    tree->nodes = nodeStorage;
    tree->nodeCount = nodeCount;

    MemoryArenaFree(tempArena, activeSet);

    Debug_EndTimedBlock(BottomUpBuildBvh);
}
#endif

internal AabbShape AabbMergeIndirect(
    AabbShape *volumes, u32 volumeCount, u32 *indices, u32 indexCount)
{
    Debug_BeginTimedBlock(AabbMergeIndirect);

    Assert(indexCount > 0);
    Assert(volumeCount > 0);

    u32 firstIdx = indices[0];
    AabbShape result = volumes[firstIdx];
    for (u32 i = 1; i < indexCount; ++i)
    {
        u32 idx = indices[i];
        result = AabbMerge(result, volumes[idx]);
    }

    Debug_EndTimedBlock(AabbMergeIndirect);

    return result;
}

// This will reverse the order of indices in the second partition
internal u32 PartitionInTwo(Plane splittingPlane, u32 *indices, u32 indexCount,
    vec3 *centroids, u32 centroidCount, MemoryArena *tempArena)
{
    Debug_BeginTimedBlock(PartitionInTwo);

    u32 *originalIndices = AllocateArray(tempArena, u32, indexCount);
    memcpy(originalIndices, indices, indexCount * sizeof(u32));

    u32 partitionSizes[2] = {};
    for (u32 i = 0; i < indexCount; ++i)
    {
        u32 idx = originalIndices[i];
        vec3 centroid = centroids[idx];
        float d = Dot(splittingPlane.normal, centroid) - splittingPlane.distance;
        if (d < 0)
        {
            // Add to set 2
            Assert(indexCount >= partitionSizes[1]);
            u32 lastIndex = indexCount - 1;
            u32 partitionIndex = lastIndex - partitionSizes[1];
            indices[partitionIndex] = idx;
            partitionSizes[1]++;
        }
        else
        {
            // Add to set 1
            u32 partitionIndex = partitionSizes[0]++;
            indices[partitionIndex] = idx;
        }
    }

    MemoryArenaFree(tempArena, originalIndices);

    Assert(partitionSizes[0] + partitionSizes[1] == indexCount);

    Debug_EndTimedBlock(PartitionInTwo);

    return partitionSizes[0];
}

struct BvhStackEntry
{
    BvhNode *parent;
    u32 *indices;
    u32 indexCount;
    b32 isLeftChild;
};

internal void TopDownBuildBvh(Bvh *tree, TriangleMeshShape *mesh,
    MemoryArena *tempArena, MemoryArena *storageArena)
{
    Debug_BeginTimedBlock(TopDownBuildBvh);

    Assert(mesh->triangleCount > 0);

    u32 maxNodes = mesh->triangleCount * 2;
    BvhNode *nodeStorage = AllocateArray(storageArena, BvhNode, maxNodes);
    u32 nodeCount = 0;

    u32 *triangleIndices = AllocateArray(storageArena, u32, mesh->triangleCount);
    u32 triangleIndexCount = 0;

    u32 boundingVolumeCount = mesh->triangleCount;
    AabbShape *boundingVolumes = AllocateArray(tempArena, AabbShape, boundingVolumeCount);

    for (u32 triangleIdx = 0; triangleIdx < mesh->triangleCount; triangleIdx++)
    {
        boundingVolumes[triangleIdx] =
            ComputeAabbOfConvexHull(mesh->triangles[triangleIdx]);
    }

    u32 centroidCount = mesh->triangleCount;
    vec3 *centroids = AllocateArray(tempArena, vec3, centroidCount);
    for (u32 triangleIdx = 0; triangleIdx < mesh->triangleCount; triangleIdx++)
    {
        centroids[triangleIdx] = mesh->triangles[triangleIdx].center;
    }

    u32 *workingIndices = AllocateArray(tempArena, u32, mesh->triangleCount);
    for (u32 triangleIdx = 0; triangleIdx < mesh->triangleCount; triangleIdx++)
    {
        workingIndices[triangleIdx] = triangleIdx;
    }



    BvhStackEntry stack[0x100];
    u32 stackLength = 1;

    stack[0].parent = NULL;
    stack[0].indices = workingIndices;
    stack[0].indexCount = mesh->triangleCount;
    stack[0].isLeftChild = false;

    while(stackLength > 0)
    {
        BvhStackEntry stackEntry = stack[--stackLength];

        Assert(nodeCount < maxNodes);
        BvhNode *node = nodeStorage + nodeCount++;

        node->aabb = AabbMergeIndirect(boundingVolumes, boundingVolumeCount,
            stackEntry.indices, stackEntry.indexCount);

        if (stackEntry.parent != NULL)
        {
            if (stackEntry.isLeftChild)
            {
                stackEntry.parent->left = node;
            }
            else
            {
                stackEntry.parent->right = node;
            }
        }

        if (stackEntry.indexCount <= 1)
        {
            Assert(triangleIndexCount < mesh->triangleCount);
            node->triangleIndexCount = 1;
            node->triangleIndexOffset = triangleIndexCount;
            triangleIndices[triangleIndexCount++] = stackEntry.indices[0];
        }
        else
        {
            AabbShape aabb = node->aabb;

            vec3 dim = aabb.max - aabb.min;
            vec3 midPoint = (aabb.max + aabb.min) * 0.5f;

            u32 largestAxis = AXIS_X;
            if (dim.y > dim.data[largestAxis])
            {
                largestAxis = AXIS_Y;
            }

            if (dim.z > dim.data[largestAxis])
            {
                largestAxis = AXIS_Z;
            }

            Plane splittingPlanes[3] = {CreatePlane(Vec3(1, 0, 0), midPoint),
                CreatePlane(Vec3(0, 1, 0), midPoint),
                CreatePlane(Vec3(0, 0, 1), midPoint)};

            u32 firstPartitionLength =
                PartitionInTwo(splittingPlanes[largestAxis], stackEntry.indices,
                    stackEntry.indexCount, centroids, centroidCount,
                    tempArena);

            bool splittingPlaneFound = true;
            if (firstPartitionLength == stackEntry.indexCount ||
                firstPartitionLength == 0)
            {
                // Failed to partition triangles using the selected splitting plane.
                splittingPlaneFound = false;
                for (u32 attemptCount = 0; attemptCount < 2; ++attemptCount)
                {
                    u32 nextAxis = (largestAxis + 1) % 3;
                    firstPartitionLength =
                        PartitionInTwo(splittingPlanes[nextAxis],
                            stackEntry.indices, stackEntry.indexCount,
                            centroids, centroidCount, tempArena);

                    if (firstPartitionLength > 0 &&
                        firstPartitionLength < stackEntry.indexCount)
                    {
                        splittingPlaneFound = true;
                        break;
                    }
                }
            }

            if (splittingPlaneFound)
            {
                // Create node for left child
                if (firstPartitionLength > 0)
                {
                    BvhStackEntry leftChild = {};
                    leftChild.parent = node;
                    leftChild.indices = stackEntry.indices;
                    leftChild.indexCount = firstPartitionLength;
                    leftChild.isLeftChild = true;

                    Assert(stackLength < ArrayCount(stack));
                    stack[stackLength++] = leftChild;
                }

                // Create node for right child
                if (firstPartitionLength < stackEntry.indexCount)
                {
                    BvhStackEntry rightChild = {};
                    rightChild.parent = node;
                    rightChild.indices = stackEntry.indices + firstPartitionLength;
                    rightChild.indexCount = stackEntry.indexCount - firstPartitionLength;
                    rightChild.isLeftChild = false;

                    Assert(stackLength < ArrayCount(stack));
                    stack[stackLength++] = rightChild;
                }
            }
            else
            {
                // Could not find a splitting plane so the triangle bounding
                // volume centroids must be the same. Need to store multiple
                // triangle indices per node.
                Assert(triangleIndexCount + stackEntry.indexCount <= mesh->triangleCount);
                node->triangleIndexCount = stackEntry.indexCount;
                node->triangleIndexOffset = triangleIndexCount;
                memcpy(triangleIndices + node->triangleIndexOffset,
                    stackEntry.indices, sizeof(u32) * stackEntry.indexCount);
                triangleIndexCount += stackEntry.indexCount;
            }
        }
    }

    MemoryArenaFree(tempArena, workingIndices);
    MemoryArenaFree(tempArena, centroids);
    MemoryArenaFree(tempArena, boundingVolumes);

    tree->root = nodeStorage;
    tree->nodes = nodeStorage;
    tree->nodeCount = nodeCount;
    tree->triangleIndices = triangleIndices;
    tree->triangleIndexCount = triangleIndexCount;

    Debug_EndTimedBlock(TopDownBuildBvh);
}

enum
{
    DRAW_EDGES = Bit(0),
    DRAW_NORMALS = Bit(1),
    DRAW_VERTICES = Bit(2),
};

internal void DrawConvexHullShape(ConvexHullShape hull, vec3 color, u32 flags = DRAW_EDGES)
{
    //Debug_DrawPoint(hull.center, color);
    for (u32 i = 0; i < hull.edgeCount; ++i)
    {
        ConvexHullEdge edge = hull.edges[i];
        Assert(edge.vertexA < hull.vertexCount);
        Assert(edge.vertexB < hull.vertexCount);
        vec3 start = hull.vertices[edge.vertexA];
        vec3 end = hull.vertices[edge.vertexB];

        Debug_DrawLine(start, end, color);
    }

    // TODO: Draw center flag
    //Debug_DrawPoint(hull.center, Vec3(1, 0, 1));

    if (flags & DRAW_NORMALS)
    {
        for (u32 i = 0; i < hull.faceCount; ++i)
        {
            vec3 normal = hull.faces[i].normal;
            float distance = hull.faces[i].distance;

            vec3 start = hull.center + normal * distance;
            vec3 end = hull.center + normal * (distance + 0.5f);
            Debug_DrawLine(start, end, Vec3(0.5, 0.8, 0.3));
        }
    }

    if (flags & DRAW_VERTICES)
    {
        for (u32 i = 0; i < hull.vertexCount; ++i)
        {
            Debug_DrawPoint(hull.vertices[i], Vec3(0.6, 0.7, 0.7), 0.2f);
        }
    }
}


internal void DrawBvh(Bvh *tree, TriangleMeshShape *mesh)
{
    vec3 colors[] = {
        Vec3(1, 0, 0),
        Vec3(0, 1, 0),
        Vec3(0, 0, 1),
        Vec3(1, 1, 0),
        Vec3(1, 0, 1),
        Vec3(0, 1, 1),
        Vec3(1, 1, 1)
    };

    u32 colorIdx = 0;
    BvhNode *node = tree->root;
    u32 depth = 0;
    while (node != NULL)
    {
        Debug_DrawAabb(node->aabb.min, node->aabb.max, colors[colorIdx]);
        colorIdx = (colorIdx + 1) % ArrayCount(colors);
        if (node->left == NULL)
        {
            Assert(node->right == NULL);
            for (u32 idx = 0; idx < node->triangleIndexCount; ++idx)
            {
                u32 triangleIndex =
                    tree->triangleIndices[node->triangleIndexOffset + idx];
                Assert(triangleIndex < mesh->triangleCount);
                ConvexHullShape triangle = mesh->triangles[triangleIndex];

                DrawConvexHullShape(triangle, Vec3(1));
            }
        }
        node = node->left;
        depth++;
    }

    Debug_Printf("Depth: %u\n", depth);
}

internal void DrawAllTrianglesInBvhNode(Bvh *tree, TriangleMeshShape *mesh, u32 nodeIdx)
{
    Assert(nodeIdx < tree->nodeCount);
    BvhNode *root = tree->nodes + nodeIdx;
    Debug_DrawAabb(root->aabb.min, root->aabb.max, Vec3(0.7, 0.4, 0.1));

    BvhNode *stack[0x1000];
    u32 stackSize = 1;
    stack[0] = root;

    u32 drawCount = 0;
    while (stackSize > 0)
    {
        BvhNode *node = stack[--stackSize];

        if (node->left == NULL && node->right == NULL)
        {
            // leaf node
            for (u32 idx = 0; idx < node->triangleIndexCount; ++idx)
            {
                u32 triangleIndex =
                    tree->triangleIndices[node->triangleIndexOffset + idx];
                Assert(triangleIndex < mesh->triangleCount);
                ConvexHullShape triangle = mesh->triangles[triangleIndex];

                DrawConvexHullShape(triangle, Vec3(1));
                drawCount++;
            }
        }
        else
        {
            if (node->left != NULL)
            {
                Assert(stackSize < ArrayCount(stack));
                stack[stackSize++] = node->left;
            }
            if (node->right != NULL)
            {
                Assert(stackSize < ArrayCount(stack));
                stack[stackSize++] = node->right;
            }
        }
    }

    Debug_Printf("Draw count: %u/%u\n", drawCount, mesh->triangleCount);
}

internal bool RayIntersectTriangleMesh(TriangleMeshShape mesh, Bvh tree,
    MemoryArena *arena, vec3 start, vec3 end, RaycastResult *raycastResult)
{
    vec3 v = end - start;

    Assert(tree.root);

    BvhNode *stack[0x1000];
    u32 stackSize = 1;
    stack[0] = tree.root;

    u32 drawCount = 0;

    float tmin = 1.0f;
    vec3 intersectionNormal = {};
    u32 minIdx = 0;
    while (stackSize > 0)
    {
        BvhNode *node = stack[--stackSize];

        // TODO: Use faster implementation that only tests for intersection and
        // doesn't calculate hit normal, hit point, etc.
        RaycastResult ignored = {};
        if (RayAabbTest(node->aabb, start, end, &ignored))
        {

            CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TRAVERSED_NODES,
                DrawAabb(node->aabb, Vec3(0.4)));

            if (node->left == NULL && node->right == NULL)
            {
                CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES,
                    Debug_DrawAabb(
                        node->aabb.min, node->aabb.max, Vec3(0.4, 0.8, 0.2)));

                CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES |
                              CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES_VERBOSE,
                    Debug_PrintInWorld((node->aabb.min + node->aabb.max) * 0.5f,
                        "Num Triangles in Leaf: %u", node->triangleIndexCount));

                // leaf node
                for (u32 idx = 0; idx < node->triangleIndexCount; ++idx)
                {
                    u32 triangleIndex =
                        tree.triangleIndices[node->triangleIndexOffset + idx];
                    Assert(triangleIndex < mesh.triangleCount);
                    ConvexHullShape triangle = mesh.triangles[triangleIndex];

                    CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_TRIANGLES,
                        DrawConvexHullShape(triangle, Vec3(0.7),
                            DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES));

                    TriangleShape t = {};
                    t.a = triangle.vertices[0];
                    t.b = triangle.vertices[1];
                    t.c = triangle.vertices[2];

                    float tFirst;
                    vec3 potentialPoint = {};
                    vec3 potentialNormal = {};
                    if (RayTriangleTest(t, start, end, &potentialPoint,
                            &potentialNormal, &tFirst))
                    {
                        if (tFirst < tmin)
                        {
                            tmin = tFirst;
                            intersectionNormal = potentialNormal;
                            minIdx = triangleIndex;
                        }

                        CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_TRIANGLES,
                            DrawConvexHullShape(triangle, Vec3(0.8, 0.6, 0.4),
                                DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES));
                    }

                    drawCount++;
                }
            }
            else
            {
                if (node->left != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->left;
                }
                if (node->right != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->right;
                }
            }
        }
    }

    bool result = false;
    if (tmin < 1.0f)
    {
        raycastResult->t = tmin;
        raycastResult->hitNormal = intersectionNormal;
        raycastResult->hitPoint = start + v * tmin;
        raycastResult->isValid = true;

        end = raycastResult->hitPoint;

        CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TRIANGLE_RESULT,
            DrawConvexHullShape(mesh.triangles[minIdx], Vec3(0.8, 0.2, 0.4),
                DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES));

        result = true;
    }

    CD_DEBUG_DRAW(
        CD_DEBUG_DRAW_RAY_VERBOSE,
        Debug_PrintInWorld(
            start, "Start: (%g %g %g)", start.x, start.y, start.z);
        Debug_PrintInWorld(end, "End: (%g %g %g)", end.x, end.y, end.z));

    CD_DEBUG_DRAW(
        CD_DEBUG_DRAW_RAY, Debug_DrawLine(start, end, Vec3(0.4f, 0.8f, 0.2f)));

    if (result)
    {
        CD_DEBUG_DRAW(CD_DEBUG_DRAW_RAY_HIT_NORMAL,
            Debug_DrawLine(end, end + raycastResult->hitNormal * 0.5f,
                Vec3(0.8f, 0.2f, 0.0f)));
    }

    //Debug_Printf("Draw count: %u/%u\n", drawCount, mesh.triangleCount);

    return result;
}

global u32 g_PrevTriangleIndex = 0;


// ==================================================================
//                  WHY DOES THIS ALWAYS RETURN FALSE!!!!!!!
// ==================================================================
internal bool AabbSweepTriangleMesh(TriangleMeshShape mesh, Bvh tree,
    MemoryArena *arena, vec3 aabbHalfExtents, vec3 start, vec3 end,
    RaycastResult *raycastResult)
{
    Debug_BeginTimedBlock(AabbSweepTriangleMeshBvh);

    vec3 v = end - start;

    ConvexHullShape box =
        CreateBoxHull(aabbHalfExtents, Translate(start), arena);
    DrawConvexHullShape(box, Vec3(0.2f, 0.4f, 0.8f));

    // Find root node to start traversal at that contains the full sweep
    AabbShape startAabb = {};
    startAabb.min = start - aabbHalfExtents;
    startAabb.max = start + aabbHalfExtents;

    AabbShape endAabb = {};
    endAabb.min = end - aabbHalfExtents;
    endAabb.max = end + aabbHalfExtents;

    AabbShape fullSweepAabb = AabbMerge(startAabb, endAabb);

    Debug_DrawAabb(fullSweepAabb.min, fullSweepAabb.max, Vec3(0.6));

    Assert(tree.root);

    BvhNode *stack[0x1000];
    u32 stackSize = 1;
    stack[0] = tree.root;

    u32 drawCount = 0;

    float tmin = 1.0f;
    vec3 intersectionNormal = {};
    u32 minIdx = 0;
    while (stackSize > 0)
    {
        BvhNode *node = stack[--stackSize];

        if (AabbVsAabbIntersect(fullSweepAabb, node->aabb))
        {
            if (node->left == NULL && node->right == NULL)
            {
                Debug_DrawAabb(node->aabb.min, node->aabb.max, Vec3(0.4));
                // leaf node
                for (u32 idx = 0; idx < node->triangleIndexCount; ++idx)
                {
                    u32 triangleIndex =
                        tree.triangleIndices[node->triangleIndexOffset + idx];
                    Assert(triangleIndex < mesh.triangleCount);
                    ConvexHullShape triangle = mesh.triangles[triangleIndex];

                    g_EnableSatDebug = (triangleIndex == g_PrevTriangleIndex);
                    float tFirst;
                    float tLast;
                    vec3 potentialNormal = {};
                    if (ConvexHullVsConvexHullSweepTest(
                                box, triangle, v, &tFirst, &tLast, &potentialNormal))
                    {
                        if (tFirst < tmin)
                        {
                            tmin = tFirst;
                            intersectionNormal = tmin > 0.0f
                                ? potentialNormal
                                : triangle.faces[0].normal;
                            minIdx = triangleIndex;
                        }
                        DrawConvexHullShape(triangle, Vec3(0.2, 0.8, 0.4),
                                DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES);
                    }
                    g_EnableSatDebug = false;

                    drawCount++;
                }
            }
            else
            {
                if (node->left != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->left;
                }
                if (node->right != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->right;
                }
            }
        }
    }

    if (tmin < 1.0f)
    {
        raycastResult->t = tmin;
        raycastResult->hitNormal = intersectionNormal;
        raycastResult->hitPoint = start + v * tmin;
        raycastResult->isValid = true;

        g_PrevTriangleIndex = minIdx;

        end = raycastResult->hitPoint;
        Debug_DrawLine(end, end + raycastResult->hitNormal * 0.5f,
                Vec3(0.8f, 0.2f, 0.0f));

        DrawConvexHullShape(mesh.triangles[minIdx], Vec3(0.8, 0.2, 0.4),
            DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES);
    }

    Debug_PrintInWorld(start, "Start: (%g %g %g)", start.x, start.y, start.z);
    Debug_PrintInWorld(end, "End: (%g %g %g)", end.x, end.y, end.z);
    Debug_DrawLine(start, end, Vec3(0.4f, 0.8f, 0.2f));
    ConvexHullShape boxEnd = CreateBoxHull(
            Vec3(0.5f), Translate(end), arena);
    DrawConvexHullShape(boxEnd, Vec3(0.2f, 0.4f, 0.8f));

    Debug_EndTimedBlock(AabbSweepTriangleMeshBvh);

    Debug_Printf("Draw count: %u/%u\n", drawCount, mesh.triangleCount);

    return false;
}

internal bool AabbSweepTriangleMesh(TriangleMeshShape mesh, MemoryArena *arena,
    vec3 aabbHalfExtents, vec3 start, vec3 end, RaycastResult *raycastResult)
{
    Debug_BeginTimedBlock(AabbSweepTerrain);

    vec3 v = end - start;

    ConvexHullShape box =
        CreateBoxHull(aabbHalfExtents, Translate(start), arena);
    DrawConvexHullShape(box, Vec3(0.2f, 0.4f, 0.8f));

    Debug_BeginTimedBlock(AabbSweepTerrainTestTriangles);
    float tmin = 1.0f;
    vec3 intersectionNormal = {};
    u32 minIdx = 0;
    for (u32 i = 0; i < mesh.triangleCount; ++i)
    {
        ConvexHullShape triangle = mesh.triangles[i];

        g_EnableSatDebug = (i == g_PrevTriangleIndex);
        float tFirst;
        float tLast;
        vec3 potentialNormal = {};
        if (ConvexHullVsConvexHullSweepTest(
                    box, triangle, v, &tFirst, &tLast, &potentialNormal))
        {
            if (tFirst < tmin)
            {
                tmin = tFirst;
                intersectionNormal = tmin > 0.0f
                    ? potentialNormal
                    : triangle.faces[0].normal;
                minIdx = i;
            }
            DrawConvexHullShape(triangle, Vec3(0.2, 0.8, 0.4),
                    DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES);
        }
        g_EnableSatDebug = false;
    }
    Debug_EndTimedBlock(AabbSweepTerrainTestTriangles);

    if (tmin < 1.0f)
    {
        raycastResult->t = tmin;
        raycastResult->hitNormal = intersectionNormal;
        raycastResult->hitPoint = start + v * tmin;
        raycastResult->isValid = true;

        g_PrevTriangleIndex = minIdx;

        end = raycastResult->hitPoint;
        Debug_DrawLine(end, end + raycastResult->hitNormal * 0.5f,
                Vec3(0.8f, 0.2f, 0.0f));

        DrawConvexHullShape(mesh.triangles[minIdx], Vec3(0.8, 0.2, 0.4),
            DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES);
    }

    Debug_DrawLine(start, end, Vec3(0.4f, 0.8f, 0.2f));
    ConvexHullShape boxEnd = CreateBoxHull(
            Vec3(0.5f), Translate(end), arena);
    DrawConvexHullShape(boxEnd, Vec3(0.2f, 0.4f, 0.8f));

    Debug_EndTimedBlock(AabbSweepTerrain);

    return (tmin < 1.0f);
}

internal void DrawTriangleMeshShape(TriangleMeshShape mesh, vec3 color)
{
    for (u32 i = 0; i < mesh.triangleCount; ++i)
    {
        DrawConvexHullShape(mesh.triangles[i], color);
    }
}

internal void PhysicsDebugger_Init(
    PhysicsDebugger *debugger, MemoryArena *arena, u32 capacity)
{
    debugger->callBuffer = AllocateArray(arena, PhysicsFunctionCall, capacity);
    debugger->capacity = capacity;
}

internal void PhysicsDebugger_AddCall(
    PhysicsDebugger *debugger, PhysicsFunctionCall call)
{
    Assert(debugger);
    Assert(debugger->tail < debugger->capacity);
    debugger->callBuffer[debugger->tail] = call;
    u32 nextTail = (debugger->tail + 1) % debugger->capacity;
    debugger->tail = nextTail;
    if (nextTail == debugger->head)
    {
        debugger->head = (debugger->head + 1) % debugger->capacity;
    }
    debugger->length = MinU(debugger->length + 1, debugger->capacity);
}

internal bool SphereSweepTriangleMesh(TriangleMeshShape mesh, Bvh tree,
    vec3 start, vec3 end, float radius, RaycastResult *raycastResult);

internal void PhysicsDebugger_SelectCall(
    PhysicsDebugger *debugger, u32 index, MemoryArena *arena)
{
    if (index < debugger->length)
    {
        PhysicsFunctionCall *call = debugger->callBuffer + index;

        switch (call->type)
        {
        case PhysicsCall_RayAabb:
        {
            RaycastResult raycastResult = {};
            AabbShape aabb = call->rayAabb.aabb;
            vec3 start = call->rayAabb.start;
            vec3 end = call->rayAabb.end;
            Debug_DrawAabb(aabb.min, aabb.max, Vec3(0, 0.4, 0.6));

            // TODO: May want a parameter to enable debug drawing inside the
            // function
            if (RayAabbTest(aabb, start, end, &raycastResult))
            {
                end = raycastResult.hitPoint;
                Debug_DrawPoint(raycastResult.hitPoint, Vec3(1, 0, 0));
            }
            Debug_DrawLine(start, end, Vec3(1, 1, 0));
        }
        break;
        case PhysicsCall_AabbSweep:
        {
            RaycastResult raycastResult = {};
            vec3 halfExtents = call->aabbSweep.halfExtents;
            AabbShape aabb = call->aabbSweep.aabb;
            vec3 start = call->aabbSweep.start;
            vec3 end = call->aabbSweep.end;
            Debug_DrawAabb(aabb.min, aabb.max, Vec3(0, 0.4, 0.6));

            Debug_DrawAabb(
                start - halfExtents, start + halfExtents, Vec3(1, 0.6, 0));
            if (AabbSweepTest(halfExtents, aabb, start, end, &raycastResult))
            {
                end = raycastResult.hitPoint;
                Debug_DrawPoint(raycastResult.hitPoint, Vec3(1, 0, 0));
            }
            Debug_DrawLine(start, end, Vec3(1, 1, 0));
            Debug_DrawAabb(
                end - halfExtents, end + halfExtents, Vec3(1, 0.6, 0));
        }
        break;
        case PhysicsCall_AabbSweepTriangleMesh:
        {
            RaycastResult raycastResult = {};
            vec3 halfExtents = call->aabbSweepTriangleMesh.halfExtents;
            vec3 start = call->aabbSweepTriangleMesh.start;
            vec3 end = call->aabbSweepTriangleMesh.end;
            TriangleMeshShape mesh = call->aabbSweepTriangleMesh.triangleMesh;
            Bvh bvh = call->aabbSweepTriangleMesh.bvh;
            DrawTriangleMeshShape(mesh, Vec3(0.2f, 0.4f, 0.8f));

            Debug_DrawAabb(
                start - halfExtents, start + halfExtents, Vec3(1, 0.6, 0));

            if (AabbSweepTriangleMesh(
                    mesh, bvh, arena, halfExtents, start, end, &raycastResult))
            {
                end = raycastResult.hitPoint;
                Debug_DrawLine(end, end + raycastResult.hitNormal * 0.2f, Vec3(1, 0, 0));
            }
            Debug_DrawLine(start, end, Vec3(1, 1, 0));
            Debug_DrawAabb(
                end - halfExtents, end + halfExtents, Vec3(1, 0.6, 0));
        }
        break;
        case PhysicsCall_SphereAabbIntersect:
        {
            SphereShape sphere = call->sphereAabbIntersect.sphere;
            AabbShape aabb = call->sphereAabbIntersect.aabb;

            vec3 color = Vec3(1, 1, 0);
            if (SphereAabbIntersect(sphere.center, sphere.radius, aabb))
            {
                color = Vec3(1, 0, 0);
            }

            DrawSphere(sphere.center, sphere.radius, color);
            DrawAabb(aabb, color);
        }
        break;
        case PhysicsCall_AabbClosestPoint:
        {
            AabbShape aabb = call->aabbClosestPoint.aabb;
            vec3 p = call->aabbClosestPoint.p;

            vec3 closestPoint = AabbClosestPoint(aabb, p);

            DrawAabb(aabb, Vec3(1, 1, 0));
            Debug_DrawPoint(closestPoint, Vec3(1, 0, 0));
        }
        break;
        case PhysicsCall_RayTriangleMesh:
        {
            vec3 start = call->rayTriangleMesh.start;
            vec3 end = call->rayTriangleMesh.end;
            TriangleMeshShape mesh = call->rayTriangleMesh.triangleMesh;
            Bvh bvh = call->rayTriangleMesh.bvh;
            DrawTriangleMeshShape(mesh, Vec3(0.2f, 0.4f, 0.8f));

            RaycastResult raycastResult = {};
            if (RayIntersectTriangleMesh(mesh, bvh, arena, start, end, &raycastResult))
            {
                end = raycastResult.hitPoint;
                Debug_DrawLine(
                    end, end + raycastResult.hitNormal * 0.2f, Vec3(1, 0, 0));
            }
        }
        break;
        case PhysicsCall_SphereSweepTriangleMesh:
        {
            vec3 start = call->sphereSweepTriangleMesh.start;
            vec3 end = call->sphereSweepTriangleMesh.end;
            float radius = call->sphereSweepTriangleMesh.radius;
            TriangleMeshShape mesh = call->sphereSweepTriangleMesh.triangleMesh;
            Bvh bvh = call->sphereSweepTriangleMesh.bvh;
            DrawTriangleMeshShape(mesh, Vec3(0.2f, 0.4f, 0.8f));

            DrawSphere(start, radius, Vec3(0.5f));

            RaycastResult raycastResult = {};
            if (SphereSweepTriangleMesh(mesh, bvh, start, end, radius, &raycastResult))
            {
                end = start + (end - start) * raycastResult.t;
                Debug_DrawLine(raycastResult.hitPoint,
                    raycastResult.hitPoint + raycastResult.hitNormal * 0.2f,
                    Vec3(1, 0, 0));

                DrawSphere(end, radius, Vec3(0.5f));
            }
        }
        break;
        default:
            InvalidCodePath();
            break;
        }
    }
}

internal void TriangleSimplex(vec3 *simplex, u32 *vertexCount, vec3 *searchDirection)
{
    vec3 a = simplex[2];
    vec3 b = simplex[1];
    vec3 c = simplex[0];
    vec3 ao = -a;
    vec3 ab = b - a;
    vec3 ac = c - a;
    vec3 abc = Cross(ab, ac);

    if (Dot(Cross(abc, ac), ao) > 0.0f)
    {
        if(Dot(ac, ao) > 0.0f)
        {
            simplex[0] = c;
            simplex[1] = a;
            *vertexCount = 2;
            *searchDirection = Cross(Cross(ac, ao), ac);
        }
        else
        {
            if (Dot(ab, ao) > 0.0f)
            {
                simplex[0] = b;
                simplex[1] = a;
                *vertexCount = 2;
                *searchDirection = Cross(Cross(ab, ao), ab);
            }
            else
            {
                simplex[0] = a;
                *vertexCount = 1;
                *searchDirection = ao;
            }
        }
    }
    else
    {
        if (Dot(Cross(ab, abc), ao) > 0.0f)
        {
            if (Dot(ab, ao) > 0.0f)
            {
                simplex[0] = b;
                simplex[1] = a;
                *vertexCount = 2;
                *searchDirection = Cross(Cross(ab, ao), ab);
            }
            else
            {
                simplex[0] = a;
                *vertexCount = 1;
                *searchDirection = ao;
            }
        }
        else
        {
            if (Dot(abc, ao) > 0.0f)
            {
                *searchDirection = abc;
            }
            else
            {
                simplex[0] = a;
                simplex[1] = c;
                simplex[2] = b;
                *searchDirection = -abc;
            }
        }
    }
}

internal bool DoSimplex(vec3 *simplex, u32 *vertexCount, vec3 *searchDirection)
{
    switch (*vertexCount)
    {
        case 2:
            {
                vec3 a = simplex[1];
                vec3 b = simplex[0];
                vec3 ab = b - a;

                if (Dot(ab, -a) > 0.0f)
                {
                    *searchDirection = Cross(Cross(ab, -a), ab);
                }
                else
                {
                    simplex[0] = a;
                    *vertexCount = 1;
                    *searchDirection = -a;
                }
            }
            break;
        case 3:
            {
                TriangleSimplex(simplex, vertexCount, searchDirection);
            }
            break;
        case 4:
            {
                vec3 a = simplex[3];
                vec3 b = simplex[2];
                vec3 c = simplex[1];
                vec3 d = simplex[0];

                vec3 ab = b - a;
                vec3 ac = c - a;
                vec3 ad = d - a;
                vec3 ao = -a;

                if (Dot(Cross(ab,ac), ao) > 0.0f)
                {
                    simplex[0] = c;
                    simplex[1] = b;
                    simplex[2] = a;
                    *vertexCount = 3;
                    TriangleSimplex(simplex, vertexCount, searchDirection);
                    return false;
                }
                
                if (Dot(Cross(ac, ad), ao) > 0.0f)
                {
                    simplex[0] = d;
                    simplex[1] = c;
                    simplex[2] = a;
                    *vertexCount = 3;
                    TriangleSimplex(simplex, vertexCount, searchDirection);
                    return false;
                }

                if (Dot(Cross(ad, ab), ao) > 0.0f)
                {
                    simplex[0] = b;
                    simplex[1] = d;
                    simplex[2] = a;
                    *vertexCount = 3;
                    TriangleSimplex(simplex, vertexCount, searchDirection);
                    return false;
                }

                return true;
            }
            break;
        default:
            break;
    };

    return false;
}

inline vec3 SphereSupport(vec3 center, float radius, vec3 direction)
{
    vec3 d = Normalize(direction);
    vec3 result = center + d * radius;

    return result;
}

#define GJK_SUPPORT_FUNCTION(NAME) vec3 NAME(void *shapeData, vec3 direction)
typedef GJK_SUPPORT_FUNCTION(GjkSupportFunction);

GJK_SUPPORT_FUNCTION(SphereSupport)
{
    SphereShape *sphere = (SphereShape *)shapeData;
    return SphereSupport(sphere->center, sphere->radius, direction);
}

struct GjkSupportResult
{
    vec3 minkowskiSupport;
    vec3 supportA;
    vec3 supportB;
};

internal GjkSupportResult GjkSupport(void *shapeDataA,
    GjkSupportFunction *supportFunctionA, void *shapeDataB,
    GjkSupportFunction *supportFunctionB, vec3 direction)
{
    GjkSupportResult result = {};
    result.supportA = supportFunctionA(shapeDataA, direction);
    result.supportB = supportFunctionB(shapeDataB, direction);
    result.minkowskiSupport = result.supportA - result.supportB;

    return result;
}

internal vec3 GjkSupport(SphereShape sphereA, SphereShape sphereB,
    vec3 direction, vec3 *supportA, vec3 *supportB)
{
    *supportA = SphereSupport(sphereA.center, sphereA.radius, direction);
    *supportB = SphereSupport(sphereB.center, sphereB.radius, -direction);

    vec3 result = *supportA - *supportB;
    return result;
}

internal bool GjkIntersect(SphereShape sphereA, SphereShape sphereB)
{
    vec3 supportA;
    vec3 supportB;
    vec3 s = GjkSupport(sphereA, sphereB, Vec3(0,0,1), &supportA, &supportB);
    vec3 simplex[4];
    simplex[0] = s;
    u32 vertexCount = 1;
    vec3 d = -s;

    Debug_DrawPoint(Vec3(0, 0, 0), Vec3(1, 1, 0));

    bool result = false;
    for (u32 iterationCount = 0; iterationCount < 32; ++iterationCount)
    {
        vec3 a = GjkSupport(sphereA, sphereB, d, &supportA, &supportB);
        if (iterationCount == 0)
        {
            Debug_DrawLine(Vec3(0, 0, 0), d, Vec3(0.9f, 0.5f, 0.5f));
            for (u32 i = 0; i < vertexCount; ++i)
            {
                Debug_DrawPoint(simplex[i], Vec3(1, 0, 1));
            }
            Debug_DrawPoint(a, Vec3(1, 0, 1));

            vec3 center = sphereA.center - sphereB.center;
            float radius = sphereA.radius + sphereB.radius;
            DrawSphere(center, radius, Vec3(0.4f, 0.4f, 0.4f));
        }

        if (Dot(a, d) < 0.0f)
        {
            break;
        }

        Assert(vertexCount < ArrayCount(simplex));
        simplex[vertexCount++] = a;

        if (DoSimplex(simplex, &vertexCount, &d))
        {
            result = true;
            break;
        }
    }
    //LOG_DEBUG("Failed to find intersection: iteration count exceeded");
    if (!result)
    {
        LOG_DEBUG("Failed to find intersection");
    }
    if (vertexCount == 4)
    {
        Debug_DrawLine(simplex[3], simplex[2], Vec3(0,1,0));
        Debug_DrawLine(simplex[2], simplex[1], Vec3(0,1,0));
        Debug_DrawLine(simplex[1], simplex[3], Vec3(0,1,0));

        Debug_DrawLine(simplex[3], simplex[0], Vec3(0,1,0));
        Debug_DrawLine(simplex[2], simplex[0], Vec3(0,1,0));
        Debug_DrawLine(simplex[1], simplex[0], Vec3(0,1,0));
    }
    Debug_Printf("Simplex Point Count: %u\n", vertexCount);
    return result;
}

internal vec3 SimplexClosestPointToOrigin(vec3 a, vec3 b, vec3 c)
{
    vec3 origin = Vec3(0, 0, 0);

    vec3 p[3];
    p[0] = ClosestPointOnLineSegment(a, b, origin);
    p[1] = ClosestPointOnLineSegment(a, c, origin);
    p[2] = ClosestPointOnLineSegment(b, c, origin);

    vec3 closest = p[0];
    if (LengthSq(p[1]) < LengthSq(closest))
    {
        closest = p[1];
    }

    if (LengthSq(p[2]) < LengthSq(closest))
    {
        closest = p[2];
    }

    return closest;
}

// Assuming GjkIntersection has already been called and has determined that the
// two shapes are not intersecting.
internal bool GjkDistance(SphereShape sphereA, SphereShape sphereB)
{
    vec3 d = sphereB.center - sphereA.center;

    vec3 simplex[3];
    u32 vertexCount = 3;

    vec3 supportA;
    vec3 supportB;
    simplex[0] = GjkSupport(sphereA, sphereB, d, &supportA, &supportB);
    simplex[1] = GjkSupport(sphereA, sphereB, -d, &supportA, &supportB);
    {
        vec3 a = simplex[1];
        vec3 b = simplex[0];
        vec3 ab = b - a;

        if (Dot(ab, -a) > 0.0f)
        {
            d = Cross(Cross(ab, -a), ab);
        }
        else
        {
            InvalidCodePath();
        }
    }
    simplex[2] = GjkSupport(sphereA, sphereB, d, &supportA, &supportB);
    d = SimplexClosestPointToOrigin(simplex[0], simplex[1], simplex[2]);

    float tolerance = 0.0001f;

    for (u32 iterationCount = 0; iterationCount < 8; ++iterationCount)
    {
        d = -d;
        if (IsZero(d))
        {
            return false;
        }

        vec3 c = GjkSupport(sphereA, sphereB, d, &supportA, &supportB);

        Debug_DrawLine(simplex[0], simplex[1], Vec3(1, 1, 0));
        Debug_DrawLine(simplex[1], simplex[2], Vec3(1, 1, 0));
        Debug_DrawLine(simplex[2], simplex[0], Vec3(1, 1, 0));

        Debug_PrintInWorld(simplex[0], "[0]");
        Debug_PrintInWorld(simplex[1], "[1]");
        Debug_PrintInWorld(simplex[2], "[2]");

        Debug_DrawPoint(Vec3(0, 0, 0), Vec3(1, 1, 1));
        Debug_DrawPoint(c, Vec3(1, 0.6, 0.1));
        Debug_PrintInWorld(c, "C");

        DrawSphere(sphereA.center - sphereB.center,
            sphereA.radius + sphereB.radius, Vec3(0.4, 0.4, 0.4));

        Debug_DrawPoint(supportA, Vec3(1, 0, 0));
        Debug_DrawPoint(supportB, Vec3(1, 0, 0));

        float dc = Dot(c, d);
        float d0 = Dot(simplex[0], d);

        if (dc - d0 < tolerance)
        {
            float distance = Length(d);
            return true;
        }

        // TODO: More than a single iteration
        vec3 p =
            SimplexClosestPointToOrigin(simplex[0], simplex[1], simplex[2]);
    }

    return false;
}

struct ClosestPointResult
{
    vec3 c0;
    vec3 c1;
    float distanceSq;
};

internal ClosestPointResult ClosestPointOfTwoLineSegments(
    vec3 p0, vec3 q0, vec3 p1, vec3 q1)
{
    ClosestPointResult result = {};

    vec3 d0 = q0 - p0;
    vec3 d1 = q1 - p1;
    vec3 r = p0 - p1;

    float a = Dot(d0, d0);
    float e = Dot(d1, d1);
    float f = Dot(d1, r);

    float s = 0.0f;
    float t = 0.0f;

    if (a <= EPSILON && e <= EPSILON)
    {
        // Both segments degenerate into points
        result.c0 = p0;
        result.c1 = p1;
    }
    else
    {
        if (a <= EPSILON)
        {
            // First segment degenerates into a point
            s = 0.0f;
            t = Clamp01(f / e);
        }
        else
        {
            float c = Dot(d0, r);
            if (e <= EPSILON)
            {
                // Second segment degenerates into a point
                t = 0.0f;
                s = Clamp01(-c / a);
            }
            else
            {
                float b = Dot(d0, d1);
                float denom = a * e - b * b;

                if (denom != 0.0f)
                {
                    s = Clamp01((b * f - c * e) / denom);
                }
                else
                {
                    s = 0.0f;
                }

                // Closest point on second line segment
                t = (b * s + f) / e;

                if (t < 0.0f)
                {
                    t = 0.0f;
                    s = Clamp01(-c / a);
                }
                else if (t > 1.0f)
                {
                    t = 1.0f;
                    s = Clamp01((b - c) / a);
                }
            }
        }

        result.c0 = p0 + d0 * s;
        result.c1 = p1 + d1 * t;
    }

    result.distanceSq = Dot(result.c0 - result.c1, result.c0 - result.c1);

    return result;
}

internal ClosestPointResult CapsuleVsCapulseClosestPoint(
    vec3 p0, vec3 q0, float r0, vec3 p1, vec3 q1, float r1)
{
    ClosestPointResult result = {};

    ClosestPointResult lineSegmentResult =
        ClosestPointOfTwoLineSegments(p0, q0, p1, q1);

    result.c0 =
        SphereClosestPoint(lineSegmentResult.c0, r0, lineSegmentResult.c1);
    result.c1 =
        SphereClosestPoint(lineSegmentResult.c1, r1, lineSegmentResult.c0);

    result.distanceSq = Dot(result.c1 - result.c0, result.c1 - result.c0);

    return result;
}

// NOTE: P is a point on the plane formed by the triangle vertices
inline bool IsPointInTriangle(vec3 a, vec3 b, vec3 c, vec3 p)
{
    bool result = false;

    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 bc = c - b;
    vec3 normal = Normalize(Cross(ab, ca));

    vec3 normalAb = Normalize(Cross(normal, ab));
    vec3 normalCa = Normalize(Cross(normal, ca));
    vec3 normalBc = Normalize(Cross(normal, bc));

    // Test Ab plane
    if (Dot(normalAb, p - a) <= 0.0f)
    {
        // Test Ac plane
        if (Dot(normalCa, p - a) <= 0.0f)
        {
            // Test Bc plane
            if (Dot(normalBc, p - b) <= 0.0f)
            {
                result = true;
            }
        }
    }

    return result;
}

internal ClosestPointResult LineSegmentVsTriangleClosestPoint(
    vec3 p, vec3 q, vec3 a, vec3 b, vec3 c)
{
    ClosestPointResult edgeResults[3];
    edgeResults[0] = ClosestPointOfTwoLineSegments(p, q, b, a);
    edgeResults[1] = ClosestPointOfTwoLineSegments(p, q, a, c);
    edgeResults[2] = ClosestPointOfTwoLineSegments(p, q, c, b);

    u32 minEdgeIdx = 0;
    for (u32 i = 0; i < 3; ++i)
    {
        if (edgeResults[i].distanceSq < edgeResults[minEdgeIdx].distanceSq)
        {
            minEdgeIdx = i;
        }
    }

    float minDistSq = edgeResults[minEdgeIdx].distanceSq;

    // Create triangle plane
    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Normalize(Cross(ab, ca));
    Plane plane = CreatePlane(normal, a);

    vec3 s[2] = {p, q};

    ClosestPointResult faceResult[2];
    i32 minFaceIdx = -1;
    for (u32 i = 0; i < 2; ++i)
    {
        vec3 f = ClosestPointOnPlane(plane, s[i]);
        if (IsPointInTriangle(a, b, c, f))
        {
            float d = Dot(f - s[i], f - s[i]);
            if (d < minDistSq)
            {
                faceResult[i].c0 = s[i];
                faceResult[i].c1 = f;
                faceResult[i].distanceSq = d;

                minDistSq = d;
                minFaceIdx = i;
            }
        }
    }

    ClosestPointResult result = {};
    vec3 center = {};
    if (minFaceIdx >= 0)
    {
        // Closest pair of points lie on the triangle's face and the line segment end points
        result = faceResult[minFaceIdx];
    }
    else
    {
        // Closest pair of points lie on one of triangle's edges
        result = edgeResults[minEdgeIdx];
    }

    //Debug_PrintInWorld(center, "DistanceSq: %g", result.distanceSq);

    return result;
}

// NOTE: Returned distanceSq is negative if the shapes are intersecting
internal ClosestPointResult CapsuleVsTriangleClosestPoint(
        vec3 p, vec3 q, float r, vec3 a, vec3 b, vec3 c)
{
    ClosestPointResult edgeResults[3];
    edgeResults[0] = ClosestPointOfTwoLineSegments(p, q, b, a);
    edgeResults[1] = ClosestPointOfTwoLineSegments(p, q, a, c);
    edgeResults[2] = ClosestPointOfTwoLineSegments(p, q, c, b);

    u32 minEdgeIdx = 0;
    for (u32 i = 0; i < 3; ++i)
    {
        if (edgeResults[i].distanceSq < edgeResults[minEdgeIdx].distanceSq)
        {
            minEdgeIdx = i;
        }
    }

    float minDistSq = edgeResults[minEdgeIdx].distanceSq;

    // Create triangle plane
    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Normalize(Cross(ab, ca));
    Plane plane = CreatePlane(normal, a);

    vec3 s[2] = {p, q};

    ClosestPointResult faceResult[2];
    i32 minFaceIdx = -1;
    for (u32 i = 0; i < 2; ++i)
    {
        vec3 f = ClosestPointOnPlane(plane, s[i]);
        if (IsPointInTriangle(a, b, c, f))
        {
            float d = Dot(f - s[i], f - s[i]);
            if (d < minDistSq)
            {
                faceResult[i].c0 = s[i];
                faceResult[i].c1 = f;
                faceResult[i].distanceSq = d;

                minDistSq = d;
                minFaceIdx = i;
            }
        }
    }

    ClosestPointResult result = {};
    vec3 center = {};
    if (minFaceIdx >= 0)
    {
        // Closest pair of points lie on the triangle's face and the line segment end points
        result.c0 = SphereClosestPoint(
            faceResult[minFaceIdx].c0, r, faceResult[minFaceIdx].c1);
        result.c1 = faceResult[minFaceIdx].c1;
        result.distanceSq = faceResult[minFaceIdx].distanceSq - r * r;

        //DrawSphere(faceResult[minFaceIdx].c0, r, Vec3(0.3, 0.7, 0.8));
        //center = (faceResult[minFaceIdx].c0 + faceResult[minFaceIdx].c1) * 0.5f;

        //Debug_DrawPoint(faceResult[minFaceIdx].c0, Vec3(1, 0, 0));
        //Debug_DrawPoint(faceResult[minFaceIdx].c1, Vec3(1, 0, 0));
    }
    else
    {
        // Closest pair of points lie on one of triangle's edges
        result.c0 = SphereClosestPoint(
            edgeResults[minEdgeIdx].c0, r, edgeResults[minEdgeIdx].c1);
        result.c1 = edgeResults[minEdgeIdx].c1;
        result.distanceSq = edgeResults[minEdgeIdx].distanceSq - r * r;

        //DrawSphere(edgeResults[minEdgeIdx].c0, r, Vec3(0.3, 0.7, 0.8));
        //center = (edgeResults[minEdgeIdx].c0 + edgeResults[minEdgeIdx].c1) * 0.5f;

        //Debug_DrawPoint(edgeResults[minEdgeIdx].c0, Vec3(1, 0, 0));
        //Debug_DrawPoint(edgeResults[minEdgeIdx].c1, Vec3(1, 0, 0));
    }

    //Debug_PrintInWorld(center, "DistanceSq: %g", result.distanceSq);

    return result;
}

internal bool SphereSweepTriangle(vec3 start, vec3 end, float radius, vec3 a,
    vec3 b, vec3 c, RaycastResult *raycastResult)
{
    bool result = false;
    // Create triangle plane
    vec3 ab = b - a;
    vec3 ca = a - c;
    vec3 normal = Normalize(Cross(ab, ca));
    Plane plane = CreatePlane(normal, a);

    vec3 d = start - normal * radius;

    // Copied from RayPlaneTest
    vec3 v = end - start;
    float denom = Dot(plane.normal, v);
    if (denom >= 0.0f)
    {
        return false;
    }

    // NOTE: May divide by 0 but should still give correct result.
    float t = (plane.distance - Dot(plane.normal, d)) / denom;

    if (t >= 0.0f && t <= 1.0f)
    {
        vec3 p = start + t * v;
        Debug_DrawPoint(p, Vec3(0, 0, 1));
        //DrawSphere(p, radius, Vec3(0.3, 0.1, 0.1));
        if (IsPointInTriangle(a, b, c, p))
        {
            raycastResult->hitPoint = p;
            raycastResult->t = t;
            raycastResult->hitNormal = normal;
            raycastResult->isValid = true;
            result = true;
        }
        else
        {
            vec3 q = TriangleClosestPoint(a, b, c, p);
            //Debug_DrawPoint(q, Vec3(1, 0, 0));
            //Debug_DrawLine(q, q - v, Vec3(0, 1, 1));
            //DrawSphere(start, radius, Vec3(0.6, 0.8, 0.3));
            float sphereT;
            vec3 sphereNormal;
            vec3 sphereHitPoint;
            if (SphereRayIntersect(start, radius, q, -v, &sphereT,
                    &sphereNormal, &sphereHitPoint))
            {
                raycastResult->hitPoint = q;
                raycastResult->t = sphereT;
                raycastResult->hitNormal = normal;
                raycastResult->isValid = true;
                result = true;
            }
        }
    }

    return result;
}

internal bool SphereSweepTriangleMesh(TriangleMeshShape mesh, Bvh tree,
    vec3 start, vec3 end, float radius, RaycastResult *raycastResult)
{
    vec3 v = end - start;

    Assert(tree.root);

    BvhNode *stack[0x1000];
    u32 stackSize = 1;
    stack[0] = tree.root;

    u32 drawCount = 0;

    float tmin = 1.0f;
    vec3 intersectionNormal = {};
    u32 minIdx = 0;
    while (stackSize > 0)
    {
        BvhNode *node = stack[--stackSize];

        // TODO: Use faster implementation that only tests for intersection and
        // doesn't calculate hit normal, hit point, etc.
        RaycastResult ignored = {};
        AabbShape nodeAabb = AabbGrow(node->aabb, radius);
        if (RayAabbTest(nodeAabb, start, end, &ignored))
        {
            CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TRAVERSED_NODES,
                DrawAabb(node->aabb, Vec3(0.4)));

            if (node->left == NULL && node->right == NULL)
            {
                CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES,
                    Debug_DrawAabb(
                        node->aabb.min, node->aabb.max, Vec3(0.4, 0.8, 0.2)));

                CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES |
                              CD_DEBUG_DRAW_BVH_TESTED_LEAF_NODES_VERBOSE,
                    Debug_PrintInWorld((node->aabb.min + node->aabb.max) * 0.5f,
                        "Num Triangles in Leaf: %u", node->triangleIndexCount));

                // leaf node
                for (u32 idx = 0; idx < node->triangleIndexCount; ++idx)
                {
                    u32 triangleIndex =
                        tree.triangleIndices[node->triangleIndexOffset + idx];
                    Assert(triangleIndex < mesh.triangleCount);
                    ConvexHullShape triangle = mesh.triangles[triangleIndex];

                    TriangleShape t = {};
                    t.a = triangle.vertices[0];
                    t.b = triangle.vertices[1];
                    t.c = triangle.vertices[2];

                    CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_TRIANGLES,
                            DrawTriangle(t, Vec3(0.7), Vec3(1, 0, 1)));

                    RaycastResult potentialResult = {};
                    if (SphereSweepTriangle(start, end,
                            radius, t.a, t.b, t.c, &potentialResult))
                    {
                        if (potentialResult.t < tmin)
                        {
                            tmin = potentialResult.t;
                            intersectionNormal = potentialResult.hitNormal;
                            minIdx = triangleIndex;
                        }

                        CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TESTED_TRIANGLES,
                            DrawTriangle(t, Vec3(0.8, 0.6, 0.4), Vec3(1, 0, 1)));
                    }

                    drawCount++;
                }
            }
            else
            {
                if (node->left != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->left;
                }
                if (node->right != NULL)
                {
                    Assert(stackSize < ArrayCount(stack));
                    stack[stackSize++] = node->right;
                }
            }
        }
    }

    bool result = false;
    if (tmin < 1.0f)
    {
        raycastResult->t = tmin;
        raycastResult->hitNormal = intersectionNormal;
        raycastResult->hitPoint = start + v * tmin;
        raycastResult->isValid = true;

        end = raycastResult->hitPoint;

        CD_DEBUG_DRAW(CD_DEBUG_DRAW_BVH_TRIANGLE_RESULT,
            DrawConvexHullShape(mesh.triangles[minIdx], Vec3(0.8, 0.2, 0.4),
                DRAW_EDGES | DRAW_NORMALS | DRAW_VERTICES));

        result = true;
    }

    CD_DEBUG_DRAW(
        CD_DEBUG_DRAW_RAY_VERBOSE,
        Debug_PrintInWorld(
            start, "Start: (%g %g %g)", start.x, start.y, start.z);
        Debug_PrintInWorld(end, "End: (%g %g %g)", end.x, end.y, end.z));

    CD_DEBUG_DRAW(
        CD_DEBUG_DRAW_RAY, Debug_DrawLine(start, end, Vec3(0.4f, 0.8f, 0.2f)));

    if (result)
    {
        CD_DEBUG_DRAW(CD_DEBUG_DRAW_RAY_HIT_NORMAL,
            Debug_DrawLine(end, end + raycastResult->hitNormal * 0.5f,
                Vec3(0.8f, 0.2f, 0.0f)));
    }

    Debug_Printf("Draw count: %u/%u\n", drawCount, mesh.triangleCount);

    return result;
}
