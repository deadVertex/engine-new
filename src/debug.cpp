#include "debug.h"

enum
{
    DebugElement_Line,
    DebugElement_Point,
    DebugElement_Axis,
    DebugElement_Text,
};

struct DebugElement
{
    u32 type;
    float timeRemaining;
    union
    {
        struct
        {
            vec3 start;
            vec3 end;
            vec3 color;
        } line;

        struct
        {
            vec3 position;
            vec3 color;
            float scale;
        } point;

        struct
        {
            mat4 transform;
        } axis;

        struct
        {
            vec3 position;
            const char *text;
            u32 length;
        } text; // Doesn't use time remaining as text buffer is cleared every frame
    };
};

struct DebugSystem
{
    DebugElement *elements;
    u32 count;
    u32 capacity;
    char *textBuffer;
    u32 textBufferLength;
    u32 textBufferCapacity;

    char *worldTextBuffer;
    u32 worldTextBufferLength;
    u32 worldTextBufferCapacity;
};

internal void Debug_Init(DebugSystem *debugSystem, u32 elementCapacity,
    u32 textCapacity, MemoryArena *arena)
{
    debugSystem->elements = AllocateArray(arena, DebugElement, elementCapacity);
    debugSystem->capacity = elementCapacity;

    debugSystem->textBuffer = AllocateArray(arena, char, textCapacity);
    debugSystem->textBufferCapacity = textCapacity;

    debugSystem->worldTextBuffer = AllocateArray(arena, char, textCapacity);
    debugSystem->worldTextBufferCapacity = textCapacity;
}

internal void Debug_DrawLine(DebugSystem *debugSystem, vec3 start, vec3 end, vec3 color, float lifetime)
{
    if (debugSystem->count < debugSystem->capacity)
    {
        DebugElement *element = debugSystem->elements + debugSystem->count++;
        element->type = DebugElement_Line;
        element->timeRemaining = lifetime;
        element->line.start = start;
        element->line.end = end;
        element->line.color = color;
    }
}

internal void Debug_DrawPoint(DebugSystem *debugSystem, vec3 position, vec3 color, float scale, float lifetime)
{
    if (debugSystem->count < debugSystem->capacity)
    {
        DebugElement *element = debugSystem->elements + debugSystem->count++;
        element->type = DebugElement_Point;
        element->timeRemaining = lifetime;
        element->point.position = position;
        element->point.scale = scale;
        element->point.color = color;
    }
}

internal void Debug_DrawAxis(DebugSystem *debugSystem, mat4 transform, float lifetime)
{
    if (debugSystem->count < debugSystem->capacity)
    {
        DebugElement *element = debugSystem->elements + debugSystem->count++;
        element->type = DebugElement_Axis;
        element->timeRemaining = lifetime;
        element->axis.transform = transform;
    }
}

internal void Debug_Cleanup(DebugSystem *debugSystem, float dt)
{
    u32 i = 0;
    while (i < debugSystem->count)
    {
        DebugElement *element = debugSystem->elements + i;
        if (element->timeRemaining < 0.0f)
        {
            // Free object
            DebugElement *lastElement = debugSystem->elements + (--debugSystem->count);
            *element = *lastElement;
            // NOTE: Run loop again with same value because new value is in its place.
        }
        else
        {
            element->timeRemaining -= dt;
            i++;
        }
    }

    debugSystem->textBufferLength = 0;
    debugSystem->worldTextBufferLength = 0;
}

inline u32 Debug_AddLineVertices(
    VertexPC *vertices, u32 maxVertices, vec3 start, vec3 end, vec3 color)
{
    if (maxVertices >= 2)
    {
        VertexPC p0, p1;
        p0.position = start;
        p0.color = color;

        p1.position = end;
        p1.color = color;

        vertices[0] = p0;
        vertices[1] = p1;
        return 2;
    }

    return 0;
}

internal u32 Debug_GetData(
    DebugSystem *debugSystem, VertexPC *vertices, u32 maxVertices)
{
    u32 vertexCount = 0;
    for (u32 i = 0; i < debugSystem->count; ++i)
    {
        DebugElement *element = debugSystem->elements + i;

        switch (element->type)
        {
            case DebugElement_Line:
                {
                    u32 remainingVertices = maxVertices - vertexCount;
                    VertexPC *verticesCursor = vertices + vertexCount;

                    vertexCount += Debug_AddLineVertices(
                            verticesCursor, remainingVertices,
                            element->line.start, element->line.end,
                            element->line.color);
                }
                break;
            case DebugElement_Point:
                {
                    vec3 position = element->point.position;
                    float scale = element->point.scale;
                    vec3 color = element->point.color;
                    vec3 basisVectors[3] = {Vec3(1, 0, 0), Vec3(0, 1, 0),
                                            Vec3(0, 0, 1)};

                    for (u32 j = 0; j < 3; ++j)
                    {
                        u32 remainingVertices = maxVertices - vertexCount;
                        VertexPC *verticesCursor = vertices + vertexCount;

                        vec3 start = position - basisVectors[j] * scale;
                        vec3 end = position + basisVectors[j] * scale;
                        vertexCount += Debug_AddLineVertices(
                                verticesCursor, remainingVertices, start, end,
                                color);
                    }
                }
                break;
            case DebugElement_Axis:
                {
                    vec3 colors[3] = { {1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

                    vec4 v[3];
                    v[0] = element->axis.transform * Vec4(1, 0, 0, 1);
                    v[1] = element->axis.transform * Vec4(0, 1, 0, 1);
                    v[2] = element->axis.transform * Vec4(0, 0, 1, 1);

                    vec4 origin = element->axis.transform * Vec4(0, 0, 0, 1);

                    for (u32 j = 0; j < 3; ++j)
                    {
                        u32 remainingVertices = maxVertices - vertexCount;
                        VertexPC *verticesCursor = vertices + vertexCount;
                        vertexCount += Debug_AddLineVertices(
                                verticesCursor, remainingVertices,
                                origin.xyz, v[j].xyz, colors[j]);
                    }
                }
                break;
            default:
                // Skip DebugElement_Text as it doesn't generate vertices directly
                break;
        }
    }

    return vertexCount;
}

internal void Debug_PrintfLocal(DebugSystem *debugSystem, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    i32 remaining = debugSystem->textBufferCapacity - debugSystem->textBufferLength;
    i32 result = vsnprintf(debugSystem->textBuffer + debugSystem->textBufferLength,
        remaining, format, args);

    if (result > 0 && result < remaining)
        debugSystem->textBufferLength += result;

    va_end(args);
}

internal void Debug_PrintInWorld_(DebugSystem *debugSystem, vec3 p, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    i32 remaining = debugSystem->worldTextBufferCapacity - debugSystem->worldTextBufferLength;
    char *start = debugSystem->worldTextBuffer + debugSystem->worldTextBufferLength;
    i32 length = vsnprintf(start, remaining, format, args);

    if (length > 0 && length < remaining)
        debugSystem->worldTextBufferLength += length;

    if (debugSystem->count < debugSystem->capacity)
    {
        DebugElement *element = debugSystem->elements + debugSystem->count++;
        element->type = DebugElement_Text;
        element->text.position = p;
        element->text.text = start;
        element->text.length = length;

        // Text buffer is cleared each frame so we can't have a
        // lifetime of more than a single frame
        element->timeRemaining = 0.0f;
    }

    va_end(args);
}

internal u32 Debug_GetTextElements(
    DebugSystem *debugSystem, DebugElement **textElements, u32 capacity)
{
    u32 count = 0;
    for (u32 i = 0; i < debugSystem->count; ++i)
    {
        DebugElement *element = debugSystem->elements + i;

        if (element->type == DebugElement_Text)
        {
            if (count < capacity)
            {
                textElements[count++] = element;
            }
        }
    }

    return count;
}
