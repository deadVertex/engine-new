enum
{
    CollisionShapeType_None,
    CollisionShapeType_TriangleMesh,
    CollisionShapeType_HeightMap,
};

struct BroadphaseEntry
{
    AabbShape aabb;

    // TODO: Should these be combined into a handle object?
    u32 shapeType;
    u32 shapeIndex;
};

struct CollisionWorld
{
    BroadphaseEntry entries[256];
    u32 entryCount;

    TriangleMeshShape triangleMeshes[8];
    Bvh triangleMeshBvhs[8];
    u32 triangleMeshCount;

    HeightMapCollisionMesh heightMaps[8];
    u32 heightMapCount;
};

internal RaycastResult RayIntersectWorld(CollisionWorld *world, MemoryArena *arena,
    vec3 start, vec3 end)
{
    RaycastResult result = {};
    result.t = 1.0f;
    for (u32 broadIndex = 0; broadIndex < world->entryCount; ++broadIndex)
    {
        BroadphaseEntry entry = world->entries[broadIndex];

        RaycastResult ignored;
        if (!RayAabbTest(entry.aabb, start, end, &ignored))
        {
            // Broadphase test failed, skip testing this shape
            continue;
        }

        RaycastResult localResult = {};
        switch (entry.shapeType)
        {
        case CollisionShapeType_TriangleMesh:
        {
            Assert(entry.shapeIndex < world->triangleMeshCount);
            TriangleMeshShape mesh = world->triangleMeshes[entry.shapeIndex];
            Bvh bvh = world->triangleMeshBvhs[entry.shapeIndex];

            RayIntersectTriangleMesh(mesh, bvh, arena, start, end, &localResult);
        }
        break;
        case CollisionShapeType_HeightMap:
        {
            Assert(entry.shapeIndex < world->heightMapCount);
            HeightMapCollisionMesh mesh = world->heightMaps[entry.shapeIndex];

            RayIntersectHeightMapMesh(mesh, start, end, &localResult);
        }
        break;
        default:
            InvalidCodePath();
            break;
        }

        if (localResult.isValid)
        {
            if (localResult.t < result.t)
            {
                result = localResult;
            }
        }
    }

    return result;
}

internal RaycastResult SphereSweepWorld(CollisionWorld *world, MemoryArena *arena,
    vec3 start, vec3 end, float radius)
{
    RaycastResult result = {};
    result.t = 1.0f;
    for (u32 broadIndex = 0; broadIndex < world->entryCount; ++broadIndex)
    {
        BroadphaseEntry entry = world->entries[broadIndex];

        RaycastResult ignored;
        AabbShape aabb = AabbGrow(entry.aabb, radius);
        if (!RayAabbTest(aabb, start, end, &ignored))
        {
            // Broadphase test failed, skip testing this shape
            continue;
        }

        RaycastResult localResult = {};
        switch (entry.shapeType)
        {
        case CollisionShapeType_TriangleMesh:
        {
            Assert(entry.shapeIndex < world->triangleMeshCount);
            TriangleMeshShape mesh = world->triangleMeshes[entry.shapeIndex];
            Bvh bvh = world->triangleMeshBvhs[entry.shapeIndex];

            {
                PhysicsFunctionCall call = {};
                call.type = PhysicsCall_SphereSweepTriangleMesh;
                call.sphereSweepTriangleMesh.start = start;
                call.sphereSweepTriangleMesh.end = end;
                call.sphereSweepTriangleMesh.radius = radius;
                call.sphereSweepTriangleMesh.triangleMesh = mesh;
                call.sphereSweepTriangleMesh.bvh = bvh;
                PhysicsDebugger_AddCall(g_PhysicsDebugger, call);
            }
            SphereSweepTriangleMesh(mesh, bvh, start, end, radius, &localResult);
        }
        break;
        case CollisionShapeType_HeightMap:
        {
            Assert(entry.shapeIndex < world->heightMapCount);
            HeightMapCollisionMesh mesh = world->heightMaps[entry.shapeIndex];

            RayIntersectHeightMapMesh(mesh, start, end, &localResult);
        }
        break;
        default:
            InvalidCodePath();
            break;
        }

        if (localResult.isValid)
        {
            if (localResult.t < result.t)
            {
                result = localResult;
            }
        }
    }

    return result;
}

internal void DrawCollisionWorld(CollisionWorld *world)
{
    for (u32 broadIndex = 0; broadIndex < world->entryCount; ++broadIndex)
    {
        BroadphaseEntry entry = world->entries[broadIndex];

        DrawAabb(entry.aabb, Vec3(0.4, 0.6, 1.0));

        // TODO: Draw collision shapes for each broadphase entry
    }
}

internal void AddTriangleMeshToCollisionWorld(
    CollisionWorld *world, TriangleMeshShape mesh, Bvh bvh)
{
    Assert(world->triangleMeshCount < ArrayCount(world->triangleMeshes));
    Assert(world->entryCount < ArrayCount(world->entries));

    u32 shapeIndex = world->triangleMeshCount++;
    world->triangleMeshes[shapeIndex] = mesh;
    world->triangleMeshBvhs[shapeIndex] = bvh;

    Assert(bvh.root != NULL);

    BroadphaseEntry entry = {};
    entry.aabb = bvh.root->aabb;
    entry.shapeType = CollisionShapeType_TriangleMesh;
    entry.shapeIndex = shapeIndex;

    world->entries[world->entryCount++] = entry;
}

internal void AddHeightMapMeshToCollisionWorld(
    CollisionWorld *world, HeightMapCollisionMesh mesh)
{
    Assert(world->heightMapCount < ArrayCount(world->heightMaps));
    Assert(world->entryCount < ArrayCount(world->entries));

    u32 shapeIndex = world->heightMapCount++;
    world->heightMaps[shapeIndex] = mesh;

    Assert(mesh.root != NULL);

    BroadphaseEntry entry = {};
    entry.aabb = mesh.root->aabb;
    entry.shapeType = CollisionShapeType_HeightMap;
    entry.shapeIndex = shapeIndex;

    world->entries[world->entryCount++] = entry;
}

internal void ClearCollisionWorld(CollisionWorld *world)
{
    world->entryCount = 0;
    world->triangleMeshCount = 0;
    world->heightMapCount = 0;
}
