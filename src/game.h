#pragma once

#include "platform_events.h"

#ifndef _MSC_VER
// TODO: Ifdef
#include <x86intrin.h>
#endif

#define ENGINE_INTERNAL

#ifdef ENGINE_INTERNAL
struct DebugCounter
{
    u64 start;
    u64 elapsed;
    u32 hitCount;
};

enum
{
    DebugCounters_GameUpdate,
    DebugCounters_UpdatePlayer,
    DebugCounters_MovePlayer,
    DebugCounters_AabbSweepWorldTriangleMesh,
    DebugCounters_AabbSweepTerrain,
    DebugCounters_AabbSweepTerrainFillVertices,
    DebugCounters_AabbSweepTerrainTestTriangles,
    DebugCounters_ConvexHullVsConvexHullSweepTest,
    DebugCounters_ConvexHullVsConvexHullIntersect,
    DebugCounters_FindSeparatingAxisForFace,
    DebugCounters_FindSeparatingAxisForFace2,
    DebugCounters_FindSeparatingAxisForEdgePair,
    DebugCounters_FindSeparatingAxisForEdgePair2,
    DebugCounters_BottomUpBuildBvh,
    DebugCounters_FindNodesToMerge,
    DebugCounters_TopDownBuildBvh,
    DebugCounters_AabbMergeIndirect,
    DebugCounters_PartitionInTwo,
    DebugCounters_AabbSweepTriangleMeshBvh,
    DebugCounters_ProcessRenderCommands,
    DebugCounters_SwapBuffers,
    DebugCounters_MaxCounters,
};

const char *gDebugCounterNames[DebugCounters_MaxCounters] = {
    "GameUpdate",
    "UpdatePlayer",
    "MovePlayer",
    "AabbSweepWorldAndTerrain",
    "AabbSweepTerrain",
    "AabbSweepTerrainFillVertices",
    "AabbSweepTerrainTestTriangles",
    "ConvexHullVsConvexHullSweepTest",
    "ConvexHullVsConvexHullIntersect",
    "FindSeparatingAxisForFace",
    "FindSeparatingAxisForFace2",
    "FindSeparatingAxisForEdgePair",
    "FindSeparatingAxisForEdgePair2",
    "BottomUpBuildBvh",
    "FindNodesToMerge",
    "TopDownBuildBvh",
    "AabbMergeIndirect",
    "PartitionInTwo",
    "AabbSweepTriangleMeshBvh",
    "ProcessRenderCommands",
    "SwapBuffers",
};

#endif

struct ReadFileResult
{
    void *memory;
    u32 size;
};

#define DEBUG_READ_ENTIRE_FILE(NAME) ReadFileResult NAME(const char *path)
typedef DEBUG_READ_ENTIRE_FILE(DebugReadEntireFileFunction);

#define DEBUG_WRITE_FILE(NAME) bool NAME(const char *path, void *data, u32 length)
typedef DEBUG_WRITE_FILE(DebugWriteFileFunction);

#define DEBUG_FREE_FILE(NAME) void NAME(ReadFileResult fileResult)
typedef DEBUG_FREE_FILE(DebugFreeFileFunction);

#define DEBUG_SHOW_CURSOR(NAME) void NAME(b32 showCursor)
typedef DEBUG_SHOW_CURSOR(DebugShowCursorFunction);

struct GameMemory
{
    void *persistentStorageBase;
    size_t persistentStorageSize;
    void *transientStorageBase;
    size_t transientStorageSize;

    DebugReadEntireFileFunction *readEntireFile;
    DebugWriteFileFunction *writeFile;
    DebugFreeFileFunction *freeFile;
    DebugShowCursorFunction *showCursor;

    bool usingVulkan;

#ifdef ENGINE_INTERNAL
    DebugCounter debugCounters[DebugCounters_MaxCounters];
#endif
};

#ifdef ENGINE_INTERNAL
extern struct GameMemory *gDebugGameMemory;
#define Debug_BeginTimedBlock(ID)                                              \
    gDebugGameMemory->debugCounters[DebugCounters_##ID].start = __rdtsc()

#define Debug_EndTimedBlock(ID)                                                \
    gDebugGameMemory->debugCounters[DebugCounters_##ID].elapsed +=             \
        __rdtsc() - gDebugGameMemory->debugCounters[DebugCounters_##ID].start; \
    gDebugGameMemory->debugCounters[DebugCounters_##ID].hitCount++
#endif

struct RenderCommandQueue;

#define GAME_UPDATE(NAME)                                                      \
    void NAME(GameMemory *memory, float dt, PlatformEvent *events,             \
        u32 eventCount, RenderCommandQueue *renderCommands, u32 frameBufferWidth,  \
        u32 frameBufferHeight)
typedef GAME_UPDATE(GameUpdateFunction);

extern "C" GAME_UPDATE(GameUpdate);

enum
{
    DataType_None,
    DataType_Vec3,
    DataType_Vec2,
    DataType_U32,
    DataType_F32,
    DataType_U8,
    DataType_Quat,
    MAX_DATA_TYPES,
};

enum
{
    InvalidComponentTypeId,
    PositionComponentTypeId,
    RotationComponentTypeId,
    ScaleComponentTypeId,
    VelocityComponentTypeId,
    ViewAnglesComponentTypeId,
    RenderedMeshComponentTypeId,
    TransformComponentTypeId,
    ShaderComponentTypeId,
    MaterialComponentTypeId,
    BulletOwnerComponentTypeId,
    LastShotTimeComponentTypeId,
    LifeTimeComponentTypeId,
    HealthComponentTypeId,
    BoxHalfExtentsComponentTypeId, // Local space, multiplied by scale
    CollisionMaskComponentTypeId,
    MoveTowardsTargetComponentTypeId, // TODO: Rework this
    TeamComponentTypeId,
    EntitySpawnerComponentTypeId,
    PreviousCommandComponentTypeId,
    LightTypeComponentTypeId,
    LightColorComponentTypeId,
    LightAttenuationComponentTypeId,
    IsJumpingComponentTypeId,
    DamageComponentTypeId,
    WeaponControllerComponentTypeId, // Merge together and try it out
    TracerRendererComponentTypeId,
    ShadowCastingMeshComponentTypeId,
    MAX_COMPONENT_TYPES,
};
