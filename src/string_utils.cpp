struct StringBuilder
{
    char *buffer;
    u32 length;
    u32 capacity;
};

struct String
{
    const char *data;
    u32 length;
};

inline StringBuilder CreateStringBuilder(char *buffer, u32 length)
{
    StringBuilder result = {};
    result.buffer = buffer;
    result.capacity = length;

    return result;
}

inline void Append(StringBuilder *builder, const char *string)
{
    // TODO: Handle overflow without asserting
    u32 length = SafeTruncateU64ToU32(strlen(string)) + 1;
    u32 start = builder->length > 0 ? builder->length - 1 : 0;
    Assert(start + length < builder->capacity);
    memcpy(builder->buffer + start, string, length);
    builder->length = start + length; // Use start to account for replacing NUL character
}

inline void Print(StringBuilder *builder, const char *fmt, ...)
{
    char staging[256]; // TODO: Don't use a fixed size buffer for this

    va_list args;
    va_start(args, fmt);
    vsnprintf(staging, sizeof(staging), fmt, args); // TODO: Check result
    va_end(args);

    Append(builder, staging);
}



inline String CreateString(const char *str, u32 length)
{
    String result = {};
    result.data = str;
    result.length = length;

    return result;
}

inline String CreateString(const char *str)
{
    return CreateString(str, (u32)strlen(str));
}


internal u32 SplitIntoLines(String str, String *lines, u32 maxLines)
{
    u32 count = 0;
    const char *lineStart = str.data;
    for (u32 i = 0; i < str.length; ++i)
    {
        if (str.data[i] == '\n')
        {
            Assert(count < maxLines);
            u32 lineLength = SafeTruncateU64ToU32((str.data + i) - lineStart);
            lines[count++] = CreateString(lineStart, lineLength);
            lineStart = str.data + i + 1;
        }
    }

    return count;
}

internal u32 Split(String str, String *tokens, u32 maxTokens, char delimiter)
{
    u32 count = 0;
    const char *start = str.data;
    for (u32 i = 0; i < str.length; ++i)
    {
        if (str.data[i] == delimiter)
        {
            u32 tokenLength = SafeTruncateU64ToU32((str.data + i) - start);
            if (tokenLength > 0)
            {
                Assert(count < maxTokens);
                tokens[count++] = CreateString(start, tokenLength);
            }
            start = str.data + i + 1;
        }
    }

    u32 tokenLength = SafeTruncateU64ToU32((str.data + str.length) - start);
    if (tokenLength > 0)
    {
        Assert(count < maxTokens);
        tokens[count++] = CreateString(start, tokenLength);
    }

    return count;
}

internal bool Split(String str, char delimiter, String *a, String *b)
{
    for (u32 i = 0; i < str.length; ++i)
    {
        if (str.data[i] == delimiter)
        {
            *a = CreateString(str.data, i);
            *b = CreateString(str.data + i + 1, str.length - i - 1);
            return true;
        }
    }

    return false;
}

inline String ConsumePreceedingWhitespace(String str)
{
    String result = {};
    for (u32 i = 0; i < str.length; ++i)
    {
        if (str.data[i] > ' ')
        {
            result = CreateString(str.data + i, str.length - i);
            break;
        }
    }

    return result;
}

struct SplitStringResult
{
    String str[2];
    u32 count;
};

inline SplitStringResult SplitString(String str, char delimiter)
{
    SplitStringResult result = {};
    for (u32 i = 0; i < str.length; ++i)
    {
        if (str.data[i] == delimiter)
        {
            result.str[0] = CreateString(str.data, i);
            result.count = 1;
            if (i + 1 < str.length)
            {
                result.str[1] = CreateString(str.data + i + 1, str.length - (i+1));
                result.count = 2;
            }
            break;
        }
    }

    return result;
}

inline bool StringCompare(String a, String b)
{
    if (a.length == b.length)
    {
        for (u32 i = 0; i < a.length; ++i)
        {
            if (a.data[i] != b.data[i])
                return false;
        }

        return true;
    }

    return false;
}

inline bool StringCompare(String a, const char *rawStringB)
{
    String b = CreateString(rawStringB);
    return StringCompare(a, b);
}

inline u32 StringToU32(String str)
{
    u32 result = 0;
    for (u32 i = 0; i < str.length; ++i)
    {
        u32 value = str.data[i] - '0';
        result = result * 10 + value;
    }

    return result;
}

inline String ConsumeNonNumericCharacters(String str)
{
    String result = str;
    for (u32 i = 0; i < str.length; ++i)
    {
        if ((str.data[i] >= '0' && str.data[i] <= '9') ||
            (str.data[i] == '-' || str.data[i] == '+'))
        {
            result.data = str.data + i;
            result.length = str.length - i;
            break;
        }
    }

    return result;
}
