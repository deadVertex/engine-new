//Currenly only OpenGL is supported on Linux


//#define USE_VULKAN
#define USE_OPENGL

#ifdef USE_VULKAN
#undef USE_OPENGL
#endif

#ifdef USE_VULKAN
#define GLFW_EXPOSE_NATIVE_WIN32
#define VK_USE_PLATFORM_WIN32_KHR
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>
#else
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#endif

#include <dlfcn.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

#include "utils.h"
#include "key_codes.h"
#include "platform_events.h"
#include "logging.h"
#include "math_lib.h"

#include "game.h"
#include "render_commands.h"

#define GAME_LIB "./game.so"

internal DEBUG_READ_ENTIRE_FILE(DebugReadEntireFile);
internal DEBUG_FREE_FILE(DebugFreeFile);

internal void Linux_FreeMemory(void *p);
internal void *Linux_AllocateMemory(size_t numBytes, size_t baseAddress = 0);

#ifdef USE_VULKAN
#include "vulkan_renderer.cpp"
#endif

#ifdef USE_OPENGL
#include "opengl_renderer.cpp"
#endif

u32 log_activeChannels = U32_MAX;

global bool g_IsRunning = true;
internal void WindowCloseCallback(GLFWwindow *window) { g_IsRunning = false; }

internal void MouseCursorPositionCallback(GLFWwindow *window, double x,
                                          double y)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event;
    event.type = PlatformEvent_MouseMotion;
    event.mouseMotion.x = x;
    event.mouseMotion.y = y;

    PushPlatformEvent(eventQueue, event);
}

internal u8 ConvertMouseButton(int button)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        return K_MOUSE_BUTTON_LEFT;
    case GLFW_MOUSE_BUTTON_MIDDLE:
        return K_MOUSE_BUTTON_MIDDLE;
    case GLFW_MOUSE_BUTTON_RIGHT:
        return K_MOUSE_BUTTON_RIGHT;
    }
    return K_UNKNOWN;
}

internal void MouseButtonCallback(GLFWwindow *window, int button, int action,
                                  int mods)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    // Hopefully GLFW_REPEAT is not valid for mouse buttons
    event.type = (action == GLFW_PRESS) ? (u8)PlatformEvent_KeyPress
                                        : (u8)PlatformEvent_KeyRelease;
    event.key = ConvertMouseButton(button);

    PushPlatformEvent(eventQueue, event);
}

void MouseScrollCallback(GLFWwindow *window, double xoffset, double yoffset)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    event.type = PlatformEvent_MouseScroll;
    event.mouseScroll = (float)yoffset; // Win32: Observed to be +/- 1.0

    PushPlatformEvent(eventQueue, event);
}

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return K_##NAME;
internal u8 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return (u8)key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return K_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return K_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return K_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return K_NUM0;
    case GLFW_KEY_KP_1:
        return K_NUM1;
    case GLFW_KEY_KP_2:
        return K_NUM2;
    case GLFW_KEY_KP_3:
        return K_NUM3;
    case GLFW_KEY_KP_4:
        return K_NUM4;
    case GLFW_KEY_KP_5:
        return K_NUM5;
    case GLFW_KEY_KP_6:
        return K_NUM6;
    case GLFW_KEY_KP_7:
        return K_NUM7;
    case GLFW_KEY_KP_8:
        return K_NUM8;
    case GLFW_KEY_KP_9:
        return K_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return K_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return K_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return K_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return K_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return K_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return K_NUM_ENTER;
    }
    return K_UNKNOWN;
}

internal void KeyCallback(GLFWwindow *window, int glfwKey, int scancode,
                          int action, int mods)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    u8 key = ConvertKey(glfwKey);
    if (key != K_UNKNOWN)
    {
        PlatformEvent event = {};
        event.key = key;

        if (action == GLFW_RELEASE)
        {
            event.type = PlatformEvent_KeyRelease;
        }
        else if (action == GLFW_PRESS)
        {
            event.type = PlatformEvent_KeyPress;
        }
        else
        {
            // Ignore key repeats as we use character events for text input
            return;
        }

        PushPlatformEvent(eventQueue, event);
    }
}

internal void CharacterCallback(GLFWwindow *window, u32 codepoint)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    event.type = PlatformEvent_CharacterPressed;
    event.character = codepoint;

    PushPlatformEvent(eventQueue, event);
}

internal void WindowFocusCallback(GLFWwindow *window, int focused)
{
    PlatformEventQueue *eventQueue =
            (PlatformEventQueue *)glfwGetWindowUserPointer(window);

    PlatformEvent event = {};
    event.type = PlatformEvent_WindowFocusChanged;
    event.windowHasFocus = (focused == GLFW_TRUE);

    PushPlatformEvent(eventQueue, event);
}

internal void ErrorCallback(int error, const char *description)
{
    LOG_ERROR("GLFW ERROR: %s", description);
}

// This creates a single frame buffer resize event when the user resizes the
// window. When the window is being resized the application is paused until the
// user stops resizing. This creates a storm of resize events which invokes the
// however we are only interested in the last one.
global u32 g_FrameBufferNewWidth;
global u32 g_FrameBufferNewHeight;
global b32 g_FrameBufferWasResized = false;
internal void FrameBufferResizeCallback(GLFWwindow *window, int width,
                                        int height)
{
    g_FrameBufferNewWidth = width;
    g_FrameBufferNewHeight = height;
    g_FrameBufferWasResized = true;
}

internal void *Linux_AllocateMemory(size_t numBytes, size_t baseAddress)
{
    void *result = mmap((void *)baseAddress, numBytes, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    Assert(result != MAP_FAILED);

    return result;
}

internal void Linux_FreeMemory(void *p)
{
    munmap(p, 0);
}

internal void DebugFreeFile(ReadFileResult fileResult)
{
    Linux_FreeMemory(fileResult.memory);
}

// NOTE: bytesToRead must equal the size of the file, if great we will enter an
// infinite loop.
internal bool ReadFile(int file, void *buf, int bytesToRead)
{
    while (bytesToRead)
    {
        int bytesRead = read(file, buf, bytesToRead);
        if (bytesRead == -1)
        {
            return false;
        }
        bytesToRead -= bytesRead;
        buf = (u8 *)buf + bytesRead;
    }
    return true;
}


internal DEBUG_READ_ENTIRE_FILE(DebugReadEntireFile)
{
    ReadFileResult result = {};
    int file = open(path, O_RDONLY);
    if (file != -1)
    {
        struct stat fileStatus;
        if (fstat(file, &fileStatus) != -1)
        {
            result.size = SafeTruncateU64ToU32(fileStatus.st_size);
            // NOTE: Size + 1 to allow for null to be written if needed.
            result.memory = Linux_AllocateMemory(result.size + 1);
            if (result.memory)
            {
                if (!ReadFile(file, result.memory, result.size))
                {
                    LOG_ERROR("Failed to read file %s", path);
                    Linux_FreeMemory(result.memory);
                    result.memory = nullptr;
                    result.size = 0;
                }
            }
            else
            {
                LOG_ERROR("Failed to allocate %d bytes for file %s",
                          result.size, path);
                result.size = 0;
            }
        }
        else
        {
            LOG_ERROR("Failed to read file size for file %s", path);
        }
        close(file);
    }
    else
    {
        LOG_ERROR("Failed to open file %s", path);
    }
    return result;
}

internal bool WriteFile( int file, void *buf, int bytesToWrite )
{
  while ( bytesToWrite )
  {
    int bytesWritten = write( file, buf, bytesToWrite );
    if ( bytesToWrite == -1 )
    {
      return false;
    }
    bytesToWrite -= bytesWritten;
    buf = (uint8_t*)buf + bytesWritten;
  }
  return true;
}

internal DEBUG_WRITE_FILE(DebugWriteFile)
{
  bool result = false;
  int file = open( path, O_RDWR | O_CREAT | O_TRUNC,
                   S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
  if ( file != -1 )
  {
    result = WriteFile( file, data, length );
    close( file );
  }
  return result;
}


struct GameCode
{
    void *handle;
    GameUpdateFunction *update;

    bool isValid;
    time_t lastWriteTime;
};

internal time_t GetFileLastWriteTime(const char *path)
{
    struct stat attr;
    stat(path, &attr);
    return attr.st_mtime;
}

internal GameCode LoadGameCode(const char *libraryName)
{
    GameCode result = {};
    result.lastWriteTime = GetFileLastWriteTime(libraryName);
    result.handle = dlopen(libraryName, RTLD_NOW);
    if (result.handle)
    {
        auto error = dlerror();
        if (error)
        {
            fprintf(stderr, "Open: %s\n", error);
        }
        result.update =
                (GameUpdateFunction *)dlsym(result.handle, "GameUpdate");
        error = dlerror();
        if (error)
        {
            fprintf(stderr, "GameUpdate: %s\n", error);
        }

        result.isValid = result.update != NULL;
    }
    else
    {
        fprintf(stderr, "%s\n", dlerror());
    }

    return result;
}

internal void UnloadGameCode(GameCode *gameCode)
{
    if (gameCode->handle)
    {
        dlclose(gameCode->handle);
        gameCode->handle = 0;
        auto error = dlerror();
        if (error)
        {
            fprintf(stderr, "Error while closing game code library: %s\n",
                    error);
        }
    }
    gameCode->isValid = false;
    gameCode->update = NULL;
}

// TODO: Probably want to remove this
global GLFWwindow *g_Window;
DEBUG_SHOW_CURSOR(DebugShowCursor)
{
    if (showCursor)
    {
        glfwSetInputMode(g_Window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
    else
    {
        glfwSetInputMode(g_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
}


int main(int argc, char **argv)
{
    bool isSleepGranular = true;

    GameCode gameCode = LoadGameCode(GAME_LIB);
    if (!gameCode.isValid)
    {
        LOG_ERROR("Failed to load game code.");
        return -1;
    }


    if (glfwInit() != GLFW_TRUE)
    {
        return -1;
    }
    glfwSetErrorCallback(ErrorCallback);

    glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
#ifdef USE_VULKAN
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
#endif

#ifdef USE_OPENGL
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);
#endif

    int windowWidth = 1024;
    int windowHeight = 768;
    GLFWwindow *window = glfwCreateWindow(windowWidth, windowHeight,
                                          "Vulkan Engine", NULL, NULL);
    g_Window = window;

    if (window == NULL)
    {
        glfwTerminate();
        return -1;
    }
    glfwSetWindowCloseCallback(window, WindowCloseCallback);
    glfwSetCursorPosCallback(window, MouseCursorPositionCallback);
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetScrollCallback(window, MouseScrollCallback);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetCharCallback(window, CharacterCallback);
    glfwSetWindowFocusCallback(window, WindowFocusCallback);
    glfwSetFramebufferSizeCallback(window, FrameBufferResizeCallback);

    u32 frameBufferWidth = windowWidth;
    u32 frameBufferHeight = windowHeight;
#ifdef USE_VULKAN
    VulkanInit(window, frameBufferWidth, frameBufferHeight);
#endif

#ifdef USE_OPENGL
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    OpenGL_Init(window, frameBufferWidth, frameBufferHeight);
#endif

    GameMemory gameMemory = {};
    gameMemory.persistentStorageSize = MEGABYTES(64);
    gameMemory.persistentStorageBase =
            Linux_AllocateMemory(gameMemory.persistentStorageSize, 0);
    gameMemory.transientStorageSize = MEGABYTES(100);
    gameMemory.transientStorageBase =
            Linux_AllocateMemory(gameMemory.transientStorageSize, 0);
    gameMemory.readEntireFile = &DebugReadEntireFile;
    gameMemory.writeFile = &DebugWriteFile;
    gameMemory.freeFile = &DebugFreeFile;
    gameMemory.showCursor = &DebugShowCursor;
#ifdef USE_VULKAN
    gameMemory.usingVulkan = true;
#endif





    PlatformEventQueue eventQueue = {};

    glfwSetWindowUserPointer(window, &eventQueue);

    i32 frameRateCap = 145;
    double minFrameTime = 1.0 / (double)frameRateCap;
    double maxFrameTime = 0.25;
    double currentTime = glfwGetTime();
    double usleepMaxError = 0.0;
    double accumulator = 0.0;
    while (g_IsRunning)
    {
        double loopFrameTime = glfwGetTime() - currentTime;

        double newTime = glfwGetTime();
        double dt = newTime - currentTime;
        if (dt > maxFrameTime)
            dt = maxFrameTime;

        currentTime = newTime;

        u32 fps = (u32)(1.0 / dt);
        //LOG_DEBUG("FPS: %u (%g ms)", fps, dt * 1000.0);

        time_t newWriteTime = GetFileLastWriteTime(GAME_LIB);
        if (newWriteTime != gameCode.lastWriteTime)
        {
            UnloadGameCode(&gameCode);
            auto newGameCode = LoadGameCode(GAME_LIB);
            if (newGameCode.isValid)
            {
                gameCode = newGameCode;
                PlatformEvent event;
                event.type = PlatformEvent_GameCodeReload;

                PushPlatformEvent(&eventQueue, event);
                LOG_DEBUG("Game code reloaded!");
            }
        }

        glfwPollEvents();

        if (g_FrameBufferWasResized)
        {
#ifdef USE_VULKAN
            g_VkIsSwapChainValid = false;
#endif
            frameBufferWidth = g_FrameBufferNewWidth;
            frameBufferHeight = g_FrameBufferNewHeight;


            PlatformEvent event = {};
            event.type = PlatformEvent_FrameBufferResized;
            event.frameBuffer.width = g_FrameBufferNewWidth;
            event.frameBuffer.height = g_FrameBufferNewHeight;

            PushPlatformEvent(&eventQueue, event);
            g_FrameBufferWasResized = false;
        }

        u8 renderCommandsBuffer[KILOBYTES(512)] = {};
        RenderCommandQueue renderCommands = {};
        renderCommands.start = renderCommandsBuffer;
        renderCommands.capacity = sizeof(renderCommandsBuffer);

        gameCode.update(&gameMemory, (float)dt, eventQueue.events,
            eventQueue.count, &renderCommands, frameBufferWidth, frameBufferHeight);
        eventQueue.count = 0;

#ifdef USE_VULKAN
        if (!g_VkIsSwapChainValid)
        {
            if (frameBufferWidth > 0 && frameBufferHeight > 0)
            {
                VulkanRecreateSwapChain(frameBufferWidth, frameBufferHeight);
            }
        }

        if (g_VkIsSwapChainValid)
        {
            // TODO: Pass framebuffer dimensions
            VulkanDrawFrame(1.0f / 60.0f, &renderCommands);
            //glfwSwapBuffers(window);
        }
#endif

#ifdef USE_OPENGL
        OpenGL_DrawFrame(&renderCommands, frameBufferWidth, frameBufferHeight);
        glfwSwapBuffers(window);
#endif

        double totalFrameTime = glfwGetTime() - currentTime;
        if (totalFrameTime < minFrameTime)
        {
            double remainder = minFrameTime - totalFrameTime;
            /* printf("totalFrameTime: %g ms\n", totalFrameTime * 1000.0); */
            u32 microsecondsRemaining = (u32)(remainder * 1000.0 * 1000.0);
            if (microsecondsRemaining > 2000)
            {
                microsecondsRemaining -= 1000; // account of overhead of usleep
                /* printf("microsecondsRemaining: %u\n", microsecondsRemaining); */
                double usleepStart = glfwGetTime();
                usleep(microsecondsRemaining);
                double usleepTime = glfwGetTime() - usleepStart;
                double usleepError = usleepTime - ((double)microsecondsRemaining / (1000.0 * 1000.0));
                if (usleepError > usleepMaxError)
                    usleepMaxError = usleepError;
                /* printf("usleepTime: %g ms error %g ms, max error %g ms\n", usleepTime * 1000.0, usleepError * 1000.0, usleepMaxError * 1000.0); */
            }
            totalFrameTime = glfwGetTime() - currentTime;
            remainder = minFrameTime - totalFrameTime;
            if (remainder > 0.0)
            {
                double targetTime = glfwGetTime() + remainder;
                while (glfwGetTime() <= targetTime);
            }

            double waitedFrameTime = glfwGetTime() - currentTime;
            /* printf("target: %g ms waitedFrameTime: %g ms error: %g ms\n", minFrameTime * 1000.0, waitedFrameTime * 1000.0, (waitedFrameTime - minFrameTime) * 1000.0); */
        }
    }
#ifdef USE_VULKAN
    VulkanCleanUp();
#endif

#ifdef USE_OPENGL
    OpenGL_CleanUp();
#endif

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
