#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

enum
{
    ASSET_TYPE_TEXTURE,
    ASSET_TYPE_CUBE_MAP,
    ASSET_TYPE_FONT,
    ASSET_TYPE_MESH,
};

struct AssetDescription
{
    u32 type;
    const char *path;
    union
    {
        struct
        {
            u32 textureIndex;
            u32 materialIndex;
            b32 generateMipMaps;
        } texture;

        struct
        {
        } font;

        struct
        {
            u32 meshIndex;
        } mesh;
    };
};

enum
{
    ASSET_TEXTURE_DEV_GRID512,
    ASSET_TEXTURE_DEV_GRID512_DARK,
    ASSET_CUBE_MAP_SKY_CLEAR,
    ASSET_TEXTURE_TRACER,
    ASSET_TEXTURE_DIRT01,
    ASSET_MESH_TEST,
    ASSET_MESH_BADGER,
    ASSET_ICON_WOOD,
    ASSET_ICON_STONE,
    MAX_ASSETS,
};

struct AssetCatalog
{
    AssetDescription descriptions[MAX_ASSETS];
};

internal bool CreateTexture(u32 textureIndex, u32 materialIndex, void *data,
    u32 length, MemoryArena *tempArena, RenderCommandQueue *renderCommands,
    b32 generateMipMaps)
{
    i32 width, height, channels;
    u8 *pixels = stbi_load_from_memory(
        (const u8 *)data, length, &width, &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        // FIXME: Not all textures are 32BPP 
        u32 bufferSize = width * height * 4;
        void *tempBuffer = MemoryArenaAllocate(tempArena, bufferSize);
        memcpy(tempBuffer, pixels, bufferSize);

        stbi_image_free(pixels);

        RenderCommandAllocateTexture *allocateTexture =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateTexture, 0);
        allocateTexture->pixels = tempBuffer;
        allocateTexture->width = width;
        allocateTexture->height = height;
        allocateTexture->bytesPerPixel = 4; // FIXME
        allocateTexture->index = textureIndex;
        allocateTexture->materialIndex = materialIndex;
        allocateTexture->useSRGB = true;
        allocateTexture->repeatMapping = true;
        allocateTexture->bytesPerChannel = 1;
        allocateTexture->generateMipMaps = generateMipMaps;

        // NOTE: Memory is not freed, must use temp allocator

        return true;
    }

    return false;
}

internal void CopySubImage( u8 *subImageData, const u8 *imageData,
                            u32 subImageWidth, u32 subImageHeight,
                            u32 numChannels, u32 xStart,
                            u32 yStart, u32 imagePitch )
{
  u32 subImagePitch = subImageWidth * numChannels;
  for ( u32 y = yStart; y < yStart + subImageHeight; ++y )
  {
    u32 start = y * imagePitch + xStart * numChannels;
    memcpy( subImageData + ( y - yStart ) * subImagePitch, imageData + start,
            subImagePitch );
  }
}

internal bool CreateCubeMap(u32 textureIndex, u32 materialIndex, void *data,
    u32 length, MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    i32 width, height, channels;
    u8 *pixels = stbi_load_from_memory(
        (const u8 *)data, length, &width, &height, &channels, STBI_rgb_alpha);
    if (pixels != NULL)
    {
        u32 bytesPerPixel = 4;
        u32 faceWidth = width / 4;
        Assert(height / faceWidth == 3);
        u32 pitch = width * bytesPerPixel;
        u32 facePitch = faceWidth * bytesPerPixel;

        u32 faceSize = faceWidth * faceWidth * bytesPerPixel;
        u8 *faces[MAX_CUBE_MAP_FACES];

        for (u32 i = 0; i < MAX_CUBE_MAP_FACES; ++i)
        {
            faces[i] = (u8 *)MemoryArenaAllocate(tempArena, faceSize);
        }

        // +X
        CopySubImage(faces[CUBE_MAP_FACE_X_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth * 2, faceWidth, pitch);

        // -X
        CopySubImage(faces[CUBE_MAP_FACE_X_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, 0, faceWidth, pitch);

        // +Y
        CopySubImage(faces[CUBE_MAP_FACE_Y_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, 0, pitch);

        // -Y
        CopySubImage(faces[CUBE_MAP_FACE_Y_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, faceWidth * 2, pitch);

        // +Z
        CopySubImage(faces[CUBE_MAP_FACE_Z_POS], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth, faceWidth, pitch);

        // -Z
        CopySubImage(faces[CUBE_MAP_FACE_Z_NEG], pixels, faceWidth, faceWidth,
            bytesPerPixel, faceWidth * 3, faceWidth, pitch);


        // Fill out render command
        RenderCommandAllocateCubeMap *allocateCubeMap =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateCubeMap, 0);
        for (u32 i = 0; i < MAX_CUBE_MAP_FACES; ++i)
        {
            allocateCubeMap->pixels[i] = faces[i];
        }
        allocateCubeMap->width = faceWidth;
        allocateCubeMap->bytesPerPixel = bytesPerPixel;
        allocateCubeMap->textureIndex = textureIndex;
        allocateCubeMap->materialIndex = materialIndex;

        // NOTE: Memory is not freed, must use temp allocator

        return true;
    }

    return false;
}

internal bool CreateMesh(u32 meshIndex, void *data, u32 length,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    MeshData meshData = {};
    if (!LoadObjMeshData((const char *)data, length, tempArena, &meshData))
    {
        return false;
    }

    RenderCommandAllocateMesh *allocateMesh =
        RenderCommandAllocate(renderCommands, RenderCommandAllocateMesh, 0);
    allocateMesh->vertices = meshData.vertices;
    allocateMesh->indices = meshData.indices;
    allocateMesh->vertexCount = meshData.vertexCount;
    allocateMesh->indexCount = meshData.indexCount;
    allocateMesh->meshIndex = meshIndex;

    return true;
}

internal void CreateAssetCatalog(AssetCatalog *catalog)
{
    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_TEXTURE_DEV_GRID512];
        desc->type = ASSET_TYPE_TEXTURE;
        desc->path = "../content/dev_grid512.png";
        desc->texture.textureIndex = VULKAN_TEXTURE_DEV_GRID512;
        desc->texture.materialIndex = VULKAN_MATERIAL_DEV_GRID512;
        desc->texture.generateMipMaps = true;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_TEXTURE_DEV_GRID512_DARK];
        desc->type = ASSET_TYPE_TEXTURE;
        desc->path = "../content/dev_grid512_dark.png";
        desc->texture.textureIndex = VULKAN_TEXTURE_DEV_GRID512_DARK;
        desc->texture.materialIndex = VULKAN_MATERIAL_DEV_GRID512_DARK;
        desc->texture.generateMipMaps = true;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_CUBE_MAP_SKY_CLEAR];
        desc->type = ASSET_TYPE_CUBE_MAP;
        desc->path = "../content/skybox_clear.png";
        desc->texture.textureIndex = VULKAN_TEXTURE_CUBE_MAP;
        desc->texture.materialIndex = VULKAN_MATERIAL_CUBE_MAP;
        desc->texture.generateMipMaps = false;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_TEXTURE_TRACER];
        desc->type = ASSET_TYPE_TEXTURE;
        desc->path = "../content/tracer.png";
        desc->texture.textureIndex = VULKAN_TEXTURE_TRACER;
        desc->texture.materialIndex = VULKAN_MATERIAL_TRACER;
        desc->texture.generateMipMaps = true;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_TEXTURE_DIRT01];
        desc->type = ASSET_TYPE_TEXTURE;
        desc->path = "../content/dirt01.jpg";
        desc->texture.textureIndex = VULKAN_TEXTURE_DIRT01;
        desc->texture.materialIndex = VULKAN_MATERIAL_DIRT01;
        desc->texture.generateMipMaps = true;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_MESH_TEST];
        desc->type = ASSET_TYPE_MESH;
        desc->path = "../content/test.obj";
        desc->mesh.meshIndex = VULKAN_MESH_TEST;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_MESH_BADGER];
        desc->type = ASSET_TYPE_MESH;
        desc->path = "../content/badger.obj";
        desc->mesh.meshIndex = VULKAN_MESH_BADGER;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_ICON_WOOD];
        desc->type = ASSET_TYPE_TEXTURE;
        desc->path = "../content/icons/wood.png";
        desc->texture.textureIndex = VULKAN_TEXTURE_ICON_WOOD;
        desc->texture.materialIndex = VULKAN_MATERIAL_ICON_WOOD;
        desc->texture.generateMipMaps = false;
    }

    {
        AssetDescription *desc =
            &catalog->descriptions[ASSET_ICON_STONE];
        desc->type = ASSET_TYPE_TEXTURE;
        desc->path = "../content/icons/stone.png";
        desc->texture.textureIndex = VULKAN_TEXTURE_ICON_STONE;
        desc->texture.materialIndex = VULKAN_MATERIAL_ICON_STONE;
        desc->texture.generateMipMaps = false;
    }
}


internal void LoadAssetsFromCatalog(AssetCatalog *catalog, GameMemory *memory,
    MemoryArena *tempArena, RenderCommandQueue *renderCommands)
{
    for (u32 i = 0; i < MAX_ASSETS; ++i)
    {
        AssetDescription *desc = catalog->descriptions + i;

        ReadFileResult file = memory->readEntireFile(desc->path);
        Assert(file.memory);

        switch (desc->type)
        {
            case ASSET_TYPE_TEXTURE:
                if (!CreateTexture(desc->texture.textureIndex,
                        desc->texture.materialIndex, file.memory, file.size,
                        tempArena, renderCommands, desc->texture.generateMipMaps))
                {
                    InvalidCodePath();
                }
                break;
            case ASSET_TYPE_CUBE_MAP:
                if (!CreateCubeMap(desc->texture.textureIndex,
                        desc->texture.materialIndex, file.memory, file.size,
                        tempArena, renderCommands))
                {
                    InvalidCodePath();
                }
                break;
            case ASSET_TYPE_MESH:
                if (!CreateMesh(desc->mesh.meshIndex, file.memory, file.size,
                        tempArena, renderCommands))
                {
                    InvalidCodePath();
                }
                break;
            default:
                InvalidCodePath();
                break;
        }

        memory->freeFile(file);
    }
}
