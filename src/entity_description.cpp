#define MAX_COMPONENT_DATA 64
struct EntityComponentDescription
{
    u32 typeId;
    u8 data[MAX_COMPONENT_DATA];
    u32 dataLength;
};

#define MAX_COMPONENT_DESCRIPTIONS 16
struct EntityDescription
{
    EntityComponentDescription components[MAX_COMPONENT_DESCRIPTIONS];
    u32 count;
};

inline void EntityDescription_AddComponent(
    EntityDescription *desc, u32 typeId, void *data, u32 dataLength)
{
    Assert(desc->count < ArrayCount(desc->components));
    EntityComponentDescription *component = desc->components + desc->count++;
    component->typeId = typeId;
    component->dataLength = dataLength;
    if (dataLength > 0)
    {
        Assert(dataLength < sizeof(component->data));
        memcpy(component->data, data, dataLength);
    }
}

inline void EntityDescription_AddComponentDefault(
    EntityDescription *desc, u32 typeId)
{
    EntityDescription_AddComponent(desc, typeId, NULL, 0);
}

inline void EntityDescription_AddComponentVec3(
    EntityDescription *desc, u32 typeId, vec3 v)
{
    EntityDescription_AddComponent(desc, typeId, &v, sizeof(v));
}

inline void EntityDescription_AddComponentVec2(
    EntityDescription *desc, u32 typeId, vec2 v)
{
    EntityDescription_AddComponent(desc, typeId, &v, sizeof(v));
}

inline void EntityDescription_AddComponentU32(
    EntityDescription *desc, u32 typeId, u32 u)
{
    EntityDescription_AddComponent(desc, typeId, &u, sizeof(u));
}

inline void EntityDescription_AddComponentF32(
    EntityDescription *desc, u32 typeId, f32 f)
{
    EntityDescription_AddComponent(desc, typeId, &f, sizeof(f));
}

inline void EntityDescription_AddComponentU8(
    EntityDescription *desc, u32 typeId, u8 u)
{
    EntityDescription_AddComponent(desc, typeId, &u, sizeof(u));
}

inline void EntityDescription_AddComponentQuat(
    EntityDescription *desc, u32 typeId, quat q)
{
    EntityDescription_AddComponent(desc, typeId, &q, sizeof(q));
}

internal EntityId CreateEntityFromDescription(
    ecs_EntitySystem *entitySystem, EntityDescription *desc)
{
    EntityId entity;
    ecs_CreateEntities(entitySystem, &entity, 1);

    for (u32 i = 0; i < desc->count; ++i)
    {
        EntityComponentDescription *component = desc->components + i;

        void *data = (component->dataLength > 0) ? component->data : NULL;
        ecs_AddComponent(entitySystem, component->typeId, &entity, 1,
            data, component->dataLength);
    }

    return entity;
}



global const char *g_ComponentNames[MAX_COMPONENT_TYPES];
global u32 g_ComponentDataTypes[MAX_COMPONENT_TYPES];

inline u32 GetComponentDataType(u32 componentTypeId)
{
    Assert(componentTypeId != InvalidComponentTypeId);
    Assert(componentTypeId < ArrayCount(g_ComponentDataTypes));
    return g_ComponentDataTypes[componentTypeId];
}

inline const char *GetComponentName(u32 componentTypeId)
{
    Assert(componentTypeId != InvalidComponentTypeId);
    Assert(componentTypeId < ArrayCount(g_ComponentDataTypes));
    return g_ComponentNames[componentTypeId];
}

inline u32 GetComponentTypeIdFromName(String str)
{
    // FIXME: Should be able to start from 0 without crashing
    for (u32 i = PositionComponentTypeId; i < ArrayCount(g_ComponentNames); ++i)
    {
        if (StringCompare(str, g_ComponentNames[i]))
        {
            return i;
        }
    }

    return InvalidComponentTypeId;
}

internal void CreateDescriptionOfEntity(ecs_EntitySystem *entitySystem,
    EntityId entity, EntityDescription *desc)
{
    for (u32 componentTypeId = PositionComponentTypeId;
         componentTypeId < MAX_COMPONENT_TYPES; ++componentTypeId)
    {
        bool hasComponent = false;
        ecs_HasComponent(
            entitySystem, componentTypeId, &entity, 1, &hasComponent);

        if (hasComponent)
        {
            u32 dataType = GetComponentDataType(componentTypeId);
            u32 dataLength = GetDataTypeLength(dataType);

            u8 data[MAX_COMPONENT_DATA];
            if (dataLength > 0)
            {
                ecs_GetComponent(entitySystem, componentTypeId, &entity, 1,
                    data, dataLength);
                EntityDescription_AddComponent(
                    desc, componentTypeId, data, dataLength);
            }
            else
            {
                EntityDescription_AddComponentDefault(desc, componentTypeId);
            }
        }
    }
}

internal u32 ReadComponentData(String str, u32 typeId, u8 *data, u32 capacity)
{
    u32 dataType = GetComponentDataType(typeId);
    return ReadDataType(str, dataType, data, capacity);
}

internal void ReadEntityDescriptions(ecs_EntitySystem *entitySystem, String str)
{
    String lines[0x1000];
    u32 lineCount = SplitIntoLines(str, lines, ArrayCount(lines));

    u32 entityCount = 0;
    EntityDescription desc = {};
    for (u32 i = 0; i < lineCount; ++i)
    {
        lines[i] = ConsumePreceedingWhitespace(lines[i]);
        SplitStringResult result = SplitString(lines[i], ' ');
        if (result.count > 1)
        {
            String key = result.str[0];
            String value = result.str[1];

            if (StringCompare(key, CreateString("Count")))
            {
                entityCount = StringToU32(value);
            }
            else if (StringCompare(key, CreateString("Entity")))
            {
                if (desc.count > 0)
                {
                    CreateEntityFromDescription(entitySystem, &desc);
                }
                desc.count = 0;
            }
            else
            {
                // Component name
                u32 typeId = GetComponentTypeIdFromName(key);
                if (typeId == InvalidComponentTypeId)
                {
                    LOG_ERROR("Unknown component type name \"%.*s\" on line %u", key.length, key.data, i);
                    break;
                }

                u8 componentData[MAX_COMPONENT_DATA];
                u32 componentDataLength = ReadComponentData(value,
                        typeId, componentData, sizeof(componentData));

                EntityDescription_AddComponent(
                    &desc, typeId, componentData, componentDataLength);
            }
        }
        else
        {
            LOG_ERROR("Expected key value pair on line %u \"%.*s\"", i, lines[i].length, lines[i].data);
            break;
        }

    }


    if (desc.count > 0)
    {
        CreateEntityFromDescription(entitySystem, &desc);
    }

    LOG_DEBUG("Entity Count: %u", entityCount);
}

internal void DumpEntityDescription(
    StringBuilder *builder, EntityDescription *desc)
{
    for (u32 j = 0; j < desc->count; ++j)
    {
        EntityComponentDescription *component = desc->components + j;
        const char *type = GetComponentName(component->typeId);
        Assert(type);
        u32 dataType = GetComponentDataType(component->typeId);

        char buffer[64];
        PrintDataTypeToBuffer(
            dataType, component->data, buffer, ArrayCount(buffer));

        Print(builder, "\t%s %s\n", type, buffer);
    }
}

internal void DumpEntitySystem(
    StringBuilder *builder, ecs_EntitySystem *entitySystem)
{
    Print(builder, "Count %u\n", entitySystem->entityCount);
    for (u32 i = 0; i < entitySystem->entityCount; ++i)
    {
        EntityId entity = entitySystem->entities[i];

        EntityDescription desc = {};
        CreateDescriptionOfEntity(entitySystem, entity, &desc);

        Print(builder, "Entity %u\n", entity);

        DumpEntityDescription(builder, &desc);
    }
}
