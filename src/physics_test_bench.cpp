enum
{
    PhysicsTestBenchScene_None,
    PhysicsTestBenchScene_ClosestPoint,
    PhysicsTestBenchScene_CapsuleTriangleClosestPoint,
    PhysicsTestBenchScene_Intersection,
    PhysicsTestBenchScene_SphereSweepTest,
    PhysicsTestBenchScene_Raycasting,
    PhysicsTestBenchScene_ParticleSimulation,
    PhysicsTestBenchScene_RigidBodySimulation,
    MAX_PHYSICS_TEST_BENCH_SCENES,
};

const char *g_PhysicsTestBenchSceneDescriptions[MAX_PHYSICS_TEST_BENCH_SCENES] =
{
        "None",
        "Closest Point",
        "Capsule Triangle Closest Point",
        "Intersection",
        "Sphere Sweep Test",
        "Ray Casting",
        "Particle Simulation",
        "Rigid Body Simulation",
};

enum
{
    PhysicsTestBenchMode_Selecting,
    PhysicsTestBenchMode_Translating,
};

enum
{
    SceneShape_None,
    SceneShape_Point,
    SceneShape_Sphere,
    SceneShape_Box,
    SceneShape_Triangle,
    SceneShape_ConvexHull,
    SceneShape_Capsule,
};

internal void PhysicsTestBench_LoadScene(GameState *gameState, u32 sceneIdx)
{
    Assert(sceneIdx < MAX_PHYSICS_TEST_BENCH_SCENES);
    gameState->selectedPhysicsTestBenchScene = sceneIdx;

    switch (gameState->selectedPhysicsTestBenchScene)
    {
    case PhysicsTestBenchScene_ClosestPoint:
    {
        ClosestPointSceneState *sceneState = &gameState->closestPointState;
        ClosestPointSceneShape *sphere = &sceneState->shapes[0];
        ClosestPointSceneShape *box = &sceneState->shapes[1];
        ClosestPointSceneShape *point = &sceneState->shapes[2];
        ClosestPointSceneShape *triangle = &sceneState->shapes[3];
        ClosestPointSceneShape *convexHull = &sceneState->shapes[4];
        ClosestPointSceneShape *capsule = &sceneState->shapes[5];
        sceneState->shapeCount = 6;

        sphere->type = SceneShape_Sphere;
        sphere->position = Vec3(0.5f, 0.5f, -1.7f);
        sphere->radius = 0.75f;

        box->type = SceneShape_Box;
        box->position = Vec3(0.5f, -0.125, -0.5f);
        box->boxHalfExtents = Vec3(2.0f, 0.75f, 1.0f) * 0.5f;

        point->type = SceneShape_Point;
        point->position = Vec3(-0.5f, 0.5f, -1.0f);

        triangle->type = SceneShape_Triangle;
        triangle->position = Vec3(-4, 0.5f, -2);
        triangle->vertexA = Vec3(-3.0f, 0.2, 0.6);
        triangle->vertexB = Vec3(-1.2f, 0.4, -0.2);
        triangle->vertexC = Vec3(-1.9f, 0.3, 0.6);

        convexHull->type = SceneShape_ConvexHull;
        convexHull->position = Vec3(1.0f, 0.5f, 3.0f);

        capsule->type = SceneShape_Capsule;
        capsule->position = Vec3(0, 0, -5);
        capsule->offsetP2 = Vec3(0, 1, 0);
        capsule->capsuleRadius = 0.25f;
    }
    break;
    case PhysicsTestBenchScene_Raycasting:
    break;
    case PhysicsTestBenchScene_CapsuleTriangleClosestPoint:
    {
        ClosestPointSceneState *sceneState = &gameState->capsuleTriangleState;
        ClosestPointSceneShape *capsule = &sceneState->shapes[0];
        //ClosestPointSceneShape *capsule2 = &sceneState->shapes[1];
        ClosestPointSceneShape *triangle = &sceneState->shapes[1];
        sceneState->shapeCount = 2;

        capsule->type = SceneShape_Capsule;
        capsule->position = Vec3(0.5f, 0.5f, -1.7f);
        capsule->offsetP2 = Vec3(0.6, 0.4, 0);
        capsule->capsuleRadius = 0.5f;

        //capsule2->type = SceneShape_Capsule;
        //capsule2->position = Vec3(0.5f, 0.5f, -0.7f);
        //capsule2->offsetP2 = Vec3(0, 1, 0);
        //capsule2->capsuleRadius = 0.7f;

        triangle->type = SceneShape_Triangle;
        triangle->position = Vec3(-4, 0.5f, -2);
        triangle->vertexA = Vec3(-3.0f, 0.2, 0.6);
        triangle->vertexB = Vec3(-1.2f, 0.4, -0.2);
        triangle->vertexC = Vec3(-1.9f, 0.3, 0.6);
    }
    break;
    case PhysicsTestBenchScene_Intersection:
    {
        ClosestPointSceneState *sceneState = &gameState->intersectionState;
        ClosestPointSceneShape *sphereA = &sceneState->shapes[0];
        ClosestPointSceneShape *sphereB = &sceneState->shapes[1];
        sceneState->shapeCount = 2;

        sphereA->type = SceneShape_Sphere;
        sphereA->position = Vec3(0.5f, 0.5f, -1.7f);
        sphereA->radius = 0.75f;

        sphereB->type = SceneShape_Sphere;
        sphereB->position = Vec3(1.5f, 0.5f, -0.6f);
        sphereB->radius = 0.5f;
    }
    break;
    case PhysicsTestBenchScene_SphereSweepTest:
    break;
    case PhysicsTestBenchScene_ParticleSimulation:
    {
        PhysicsEngine *physicsEngine = &gameState->physicsEngine;
        for (u32 i = 0; i < 10; i++)
        {
            physicsEngine->positions[i] =
                Vec3((float)i, 10.0f + i * 0.2f, 0.0f);
            physicsEngine->velocities[i] = Vec3(0);
            physicsEngine->accelerations[i] = Vec3(0);
            physicsEngine->gravity[i] = Vec3(0, -10, 0);
            physicsEngine->invMasses[i] = 1.0f / 3.5f;
            physicsEngine->dampening[i] = 0.9999f;
        }
        physicsEngine->particleCount = 10;
    }
    break;
    case PhysicsTestBenchScene_RigidBodySimulation:
    {
        PhysicsEngine *physicsEngine = &gameState->physicsEngine;
        RigidBody *rigidBody = &physicsEngine->rigidBodies[0];
        physicsEngine->rigidBodyCount = 1;

        ZeroPointerToStruct(rigidBody);

        rigidBody->position = Vec3(0, 10, 0);
        rigidBody->orientation = Quat(Vec3(0, 0, 1), PI * 0.35f);
        rigidBody->inverseMass = 1.0f / 30.0f;
        rigidBody->linearDamping = 0.98f;
        rigidBody->angularDamping = 0.98f;
        rigidBody->inverseInertiaTensor =
            Inverse(BoxInertiaTensor(Vec3(1, 0.5, 0.5), 30.0f));
        rigidBody->acceleration = Vec3(0, -10, 0);

        UpdateDerivedData(rigidBody, 1);
    }
    break;
    default:
        InvalidCodePath();
        break;
    }
}

internal void UpdatePhysicsTestBenchEditorControls(
    ClosestPointSceneState *sceneState, InputSystem *inputSystem, Camera camera,
    float frameBufferWidth, float frameBufferHeight)
{
    float x = (float)inputSystem->mouseX;
    float y = (float)inputSystem->mouseY;

    switch (sceneState->mode)
    {
    case PhysicsTestBenchMode_Selecting:
    {
        if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
        {
            LineSegment lineSegment =
                GenerateLineSegment(x, y, frameBufferWidth, frameBufferHeight,
                    g_Fov, g_NearClip, g_FarClip, camera);

            // Debug_DrawLine(lineSegment.start, lineSegment.end,
            // Vec3(1, 0, 0), 20.0f);

            i32 selection = -1;
            float tmin = 1.0f;

            for (u32 shapeIdx = 0; shapeIdx < sceneState->shapeCount;
                 ++shapeIdx)
            {
                ClosestPointSceneShape *shape = sceneState->shapes + shapeIdx;

                vec3 halfExtents = {};
                switch (shape->type)
                {
                case SceneShape_Point:
                    halfExtents = Vec3(0.5f);
                    break;
                case SceneShape_Sphere:
                    halfExtents = Vec3(shape->radius);
                    break;
                case SceneShape_Box:
                    halfExtents = shape->boxHalfExtents;
                    break;
                case SceneShape_Triangle:
                    halfExtents = Max(Abs(shape->vertexA),
                        Max(Abs(shape->vertexB), Abs(shape->vertexC)));
                    break;
                case SceneShape_ConvexHull:
                    halfExtents = Vec3(1.0f);
                    break;
                case SceneShape_Capsule:
                    halfExtents = Vec3(1.0f);
                    break;
                default:
                    InvalidCodePath();
                    break;
                }

                AabbShape box = {};
                box.min = shape->position - halfExtents;
                box.max = shape->position + halfExtents;

                RaycastResult raycastResult;
                if (RayAabbTest(box, lineSegment.start, lineSegment.end,
                        &raycastResult))
                {
                    if (raycastResult.t < tmin || selection != -1)
                    {
                        tmin = raycastResult.t;
                        selection = shapeIdx;
                    }
                }
            }

            sceneState->selectedShape = selection;
        }

        if (sceneState->selectedShape != -1)
        {
            Assert(sceneState->selectedShape < (i32)sceneState->shapeCount);

            ClosestPointSceneShape *shape =
                sceneState->shapes + sceneState->selectedShape;

            vec3 position = shape->position;

            if (WasKeyPressed(inputSystem, K_T))
            {
                sceneState->mode = PhysicsTestBenchMode_Translating;
                sceneState->shapeBegin = position;

                if (IsKeyDown(inputSystem, K_LEFT_SHIFT))
                {
                    sceneState->isVerticalTranslation = true;
                    // TODO: Should really have our own camera
                    vec3 d = camera.position - position;
                    d.y = 0.0f;
                    sceneState->translationPlane =
                        CreatePlane(Normalize(d), position);
                }
                else
                {
                    sceneState->isVerticalTranslation = false;
                    sceneState->translationPlane =
                        CreatePlane(Vec3(0, 1, 0), position);
                }

                LineSegment lineSegment = GenerateLineSegment(x, y,
                    frameBufferWidth, frameBufferHeight, g_Fov, g_NearClip,
                    g_FarClip, camera);

                vec3 intersectionPoint;
                if (RayPlaneTest(sceneState->translationPlane,
                        lineSegment.start, lineSegment.end, &intersectionPoint))
                {
                    sceneState->translationBegin = intersectionPoint;
                }
            }
        }
    }
    break;
    case PhysicsTestBenchMode_Translating:
    {
        Assert(sceneState->selectedShape != -1);
        Assert(sceneState->selectedShape < (i32)sceneState->shapeCount);

        ClosestPointSceneShape *shape =
            sceneState->shapes + sceneState->selectedShape;

        LineSegment lineSegment = GenerateLineSegment(x, y, frameBufferWidth,
            frameBufferHeight, g_Fov, g_NearClip, g_FarClip, camera);

        vec3 intersectionPoint;
        if (RayPlaneTest(sceneState->translationPlane, lineSegment.start,
                lineSegment.end, &intersectionPoint))
        {
            sceneState->translationEnd = intersectionPoint;
        }

        vec3 delta = sceneState->translationEnd - sceneState->translationBegin;
        if (sceneState->isVerticalTranslation)
        {
            delta.x = 0.0f;
            delta.z = 0.0f;
        }
        vec3 newPosition = sceneState->shapeBegin + delta;

        shape->position = newPosition;

        if (WasKeyPressed(inputSystem, K_ESCAPE))
        {
            sceneState->mode = PhysicsTestBenchMode_Selecting;
            shape->position = sceneState->shapeBegin;
        }

        if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
        {
            sceneState->mode = PhysicsTestBenchMode_Selecting;
        }
    }
    break;
    default:
        InvalidCodePath();
        break;
    }
}

internal void DrawPhysicsTestBenchScene(ClosestPointSceneState *sceneState)
{
    for (u32 shapeIdx = 0; shapeIdx < sceneState->shapeCount; ++shapeIdx)
    {
        ClosestPointSceneShape *shape = sceneState->shapes + shapeIdx;

        b32 isShapeSelected = (i32)shapeIdx == sceneState->selectedShape;

        vec3 unselectedColor = Vec3(0.2f, 0.5f, 1.0f);
        vec3 selectedColor = Vec3(0.9f, 0.3f, 0.3f);

        switch (shape->type)
        {
        case SceneShape_Point:
            Debug_DrawPoint(shape->position,
                isShapeSelected ? selectedColor : unselectedColor);
            break;
        case SceneShape_Sphere:
        {
            DrawSphere(shape->position, shape->radius,
                isShapeSelected ? selectedColor : unselectedColor);
        }
        break;
        case SceneShape_Box:
        {
            AabbShape aabb = {};
            aabb.min = shape->position - shape->boxHalfExtents;
            aabb.max = shape->position + shape->boxHalfExtents;
            DrawAabb(aabb, isShapeSelected ? selectedColor : unselectedColor);
        }
        break;
        case SceneShape_Triangle:
        {
            TriangleShape triangle = {};
            triangle.a = shape->position + shape->vertexA;
            triangle.b = shape->position + shape->vertexB;
            triangle.c = shape->position + shape->vertexC;
            DrawTriangle(
                triangle, isShapeSelected ? selectedColor : unselectedColor);
        }
        break;
        case SceneShape_ConvexHull:
        {
            vec3 vertices[4] = {Vec3(-1.0f, -0.5f, 1.0f) + shape->position,
                Vec3(1.0f, -0.5f, 1.0f) + shape->position,
                Vec3(0.0f, -0.5f, -1.0f) + shape->position,
                Vec3(0.0f, 0.5f, 0.0f) + shape->position};

            vec3 center = CalculateCentroid(vertices, ArrayCount(vertices));

            ConvexHullEdge edges[6] = {
                {0, 1}, {1, 2}, {2, 0}, {0, 3}, {1, 3}, {2, 3}};

            vec3 v[4];
            for (u32 i = 0; i < 4; ++i)
            {
                v[i] = vertices[i] - center;
            }

            Plane faces[4] = {CreatePlane(v[0], v[1], v[2]),
                CreatePlane(v[3], v[1], v[0]), CreatePlane(v[3], v[2], v[1]),
                CreatePlane(v[3], v[0], v[2])};

            u32 faceIndices[12] = {
                0,
                1,
                2,
                3,
                1,
                0,
                3,
                2,
                1,
                3,
                0,
                2,
            };

            ConvexHullFaceIndices hullFaceIndices[4] = {
                {&faceIndices[0], 3},
                {&faceIndices[3], 3},
                {&faceIndices[6], 3},
                {&faceIndices[9], 3},
            };

            ConvexHullShape convexHull = {};
            convexHull.vertexCount = 4;
            convexHull.vertices = vertices;
            convexHull.center = center;
            convexHull.edgeCount = 6;
            convexHull.edges = edges;
            convexHull.faceCount = 4;
            convexHull.faces = faces;
            convexHull.faceIndices = hullFaceIndices;

            DrawConvexHullShape(
                convexHull, isShapeSelected ? selectedColor : unselectedColor);
        }
        break;
        case SceneShape_Capsule:
        {
            DrawCapsule(shape->position, shape->position + shape->offsetP2,
                shape->capsuleRadius,
                isShapeSelected ? selectedColor : unselectedColor);
        }
        break;
        default:
            InvalidCodePath();
            break;
        }

        if (isShapeSelected)
        {
            Debug_DrawAxis(Translate(shape->position));
        }
    }
}

internal void UpdateClosestPointScene(ClosestPointSceneState *sceneState)
{
    vec3 closestPointColor = Vec3(0.8f, 0.6f, 0.3f);

    ClosestPointSceneShape *pointShape = NULL;
    for (u32 shapeIdx = 0; shapeIdx < sceneState->shapeCount; ++shapeIdx)
    {
        if (sceneState->shapes[shapeIdx].type == SceneShape_Point)
        {
            pointShape = &sceneState->shapes[shapeIdx];
            break;
        }
    }

    for (u32 shapeIdx = 0; shapeIdx < sceneState->shapeCount; ++shapeIdx)
    {
        ClosestPointSceneShape *shape = sceneState->shapes + shapeIdx;

        switch (shape->type)
        {
        case SceneShape_Point:
            break;
        case SceneShape_Sphere:
        {
            vec3 p = SphereClosestPoint(
                shape->position, shape->radius, pointShape->position);
            Debug_DrawPoint(p, closestPointColor);
        }
        break;
        case SceneShape_Box:
        {
            AabbShape aabb = {};
            aabb.min = shape->position - shape->boxHalfExtents;
            aabb.max = shape->position + shape->boxHalfExtents;

            vec3 p = AabbClosestPoint(aabb, pointShape->position);
            Debug_DrawPoint(p, closestPointColor);
        }
        break;
        case SceneShape_Triangle:
        {
            TriangleShape triangle = {};
            triangle.a = shape->position + shape->vertexA;
            triangle.b = shape->position + shape->vertexB;
            triangle.c = shape->position + shape->vertexC;

            vec3 p = TriangleClosestPoint(triangle, pointShape->position);
            Debug_DrawPoint(p, closestPointColor);
        }
        break;
        case SceneShape_ConvexHull:
        {
            vec3 vertices[4] = {Vec3(-1.0f, -0.5f, 1.0f) + shape->position,
                Vec3(1.0f, -0.5f, 1.0f) + shape->position,
                Vec3(0.0f, -0.5f, -1.0f) + shape->position,
                Vec3(0.0f, 0.5f, 0.0f) + shape->position};

            vec3 center = CalculateCentroid(vertices, ArrayCount(vertices));

            ConvexHullEdge edges[6] = {
                {0, 1}, {1, 2}, {2, 0}, {0, 3}, {1, 3}, {2, 3}};

            vec3 v[4];
            for (u32 i = 0; i < 4; ++i)
            {
                v[i] = vertices[i] - center;
            }

            Plane faces[4] = {CreatePlane(v[0], v[1], v[2]),
                CreatePlane(v[3], v[1], v[0]), CreatePlane(v[3], v[2], v[1]),
                CreatePlane(v[3], v[0], v[2])};

            u32 faceIndices[12] = {
                0,
                1,
                2,
                3,
                1,
                0,
                3,
                2,
                1,
                3,
                0,
                2,
            };

            ConvexHullFaceIndices hullFaceIndices[4] = {
                {&faceIndices[0], 3},
                {&faceIndices[3], 3},
                {&faceIndices[6], 3},
                {&faceIndices[9], 3},
            };

            ConvexHullShape convexHull = {};
            convexHull.vertexCount = 4;
            convexHull.vertices = vertices;
            convexHull.center = center;
            convexHull.edgeCount = 6;
            convexHull.edges = edges;
            convexHull.faceCount = 4;
            convexHull.faces = faces;
            convexHull.faceIndices = hullFaceIndices;

            vec3 p = ConvexHullClosestPoint(convexHull, pointShape->position);
            Debug_DrawPoint(p, closestPointColor);
        }
        break;
        case SceneShape_Capsule:
        {
            vec3 p = CapsuleClosestPoint(shape->position,
                shape->position + shape->offsetP2, shape->capsuleRadius,
                pointShape->position);
            Debug_DrawPoint(p, closestPointColor);
        }
        break;
        default:
            InvalidCodePath();
            break;
        }
    }
}

internal void UpdateIntersectionScene(ClosestPointSceneState *sceneState)
{
    Assert(sceneState->shapeCount == 2);
    ClosestPointSceneShape *shapeA = sceneState->shapes + 0;
    ClosestPointSceneShape *shapeB = sceneState->shapes + 1;

    Assert(shapeA->type == SceneShape_Sphere);
    Assert(shapeB->type == SceneShape_Sphere);

    SphereShape sphereA;
    sphereA.radius = shapeA->radius;
    sphereA.center = shapeA->position;

    SphereShape sphereB;
    sphereB.radius = shapeB->radius;
    sphereB.center = shapeB->position;

    //if (GjkIntersect(sphereA, sphereB))
    //{
        //Debug_DrawLine(sphereA.center, sphereB.center, Vec3(1, 0, 0));
    //}

    GjkDistance(sphereA, sphereB);
}

internal void UpdateCapsuleTriangleClosestPointScene(ClosestPointSceneState *sceneState)
{
    Assert(sceneState->shapeCount == 2);
    ClosestPointSceneShape *capsule = sceneState->shapes + 0;
    //ClosestPointSceneShape *capsule2 = sceneState->shapes + 1;
    ClosestPointSceneShape *triangle = sceneState->shapes + 1;

    Assert(capsule->type == SceneShape_Capsule);
    //Assert(capsule2->type == SceneShape_Capsule);
    Assert(triangle->type == SceneShape_Triangle);

    vec3 p0 = capsule->position;
    vec3 q0 = capsule->position + capsule->offsetP2;
    //vec3 p1 = capsule2->position;
    //vec3 q1 = capsule2->position + capsule2->offsetP2;

    //ClosestPointResult result = CapsuleVsCapulseClosestPoint(
        //p0, q0, capsule->capsuleRadius, p1, q1, capsule2->capsuleRadius);

    vec3 a = triangle->vertexA + triangle->position;
    vec3 b = triangle->vertexB + triangle->position;
    vec3 c = triangle->vertexC + triangle->position;

    ClosestPointResult result =
        CapsuleVsTriangleClosestPoint(p0, q0, capsule->capsuleRadius, a, b, c);

    Debug_DrawPoint(result.c0, Vec3(1, 0, 0));
    Debug_DrawPoint(result.c1, Vec3(1, 0, 0));

}

internal void UpdateSphereSweepTestScene(RaycastSceneState *sceneState,
    InputSystem *inputSystem, Camera camera, float frameBufferWidth,
    float frameBufferHeight)
{
    float x = (float)inputSystem->mouseX;
    float y = (float)inputSystem->mouseY;

    TriangleShape triangle = {};
    triangle.a = Vec3(2.0f, -3.0f, 1.0f);
    triangle.b = Vec3(3.0f, -2.2f, 1.5f);
    triangle.c = Vec3(4.0f, -2.9f, 1.9f);

    DrawTriangle(triangle, Vec3(0.7f, 0.7f, 0.9f), Vec3(1, 0, 1));

    if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
    {
        LineSegment lineSegment = GenerateLineSegment(x, y, frameBufferWidth,
            frameBufferHeight, g_Fov, g_NearClip, g_FarClip, camera);
        sceneState->start = lineSegment.start;
        sceneState->end = lineSegment.end;
    }

    float radius = 0.5f;
    vec3 start = sceneState->start;
    vec3 end = sceneState->end;

    RaycastResult raycastResult = {};
    if (SphereSweepTriangle(start, end, radius, triangle.a,
                triangle.b, triangle.c, &raycastResult))
    {
        vec3 d = end - start;
        end = start + d * raycastResult.t;
        Debug_DrawPoint(raycastResult.hitPoint, Vec3(1, 0, 0), 0.2f);
        Debug_DrawLine(raycastResult.hitPoint, end, Vec3(1, 0, 1));
        float dist = Length(end - raycastResult.hitPoint);
        Debug_PrintInWorld((end + raycastResult.hitPoint) * 0.5f, "%g", dist);
    }
    Debug_DrawLine(start, end, Vec3(0.8));
    DrawSphere(start, radius, Vec3(0.4));
    DrawSphere(end, radius, Vec3(0.4));
}

internal void UpdateRaycastingScene(InputSystem *inputSystem, Camera camera,
    float frameBufferWidth, float frameBufferHeight)
{
    float x = (float)inputSystem->mouseX;
    float y = (float)inputSystem->mouseY;

    AabbShape box = {};
    box.min = Vec3(-5.0f, -0.5f, 1.0f);
    box.max = Vec3(-3.0f, 0.5f, 1.5f);

    DrawAabb(box, Vec3(0.7f, 0.7f, 0.9f));

    SphereShape sphere = {};
    sphere.center = Vec3(3.0f, 0.5f, -1.0f);
    sphere.radius = 0.6f;

    DrawSphere(sphere.center, sphere.radius, Vec3(0.7f, 0.7f, 0.9f));

    TriangleShape triangle = {};
    triangle.a = Vec3(2.0f, -3.0f, 1.0f);
    triangle.b = Vec3(3.0f, -2.2f, 1.5f);
    triangle.c = Vec3(4.0f, -2.9f, 1.9f);

    DrawTriangle(triangle, Vec3(0.7f, 0.7f, 0.9f));

    // TODO: Need a better way of creating convex hulls

    vec3 hullOrigin = Vec3(-3, -1, -1);
    vec3 vertices[4] = {Vec3(-1.0f, -0.5f, 1.0f) + hullOrigin,
        Vec3(1.0f, -0.5f, 1.0f) + hullOrigin,
        Vec3(0.0f, -0.5f, -1.0f) + hullOrigin,
        Vec3(0.0f, 0.5f, 0.0f) + hullOrigin};

    vec3 center = CalculateCentroid(vertices, ArrayCount(vertices));

    ConvexHullEdge edges[6] = {{0, 1}, {1, 2}, {2, 0}, {0, 3}, {1, 3}, {2, 3}};

    vec3 v[4];
    for (u32 i = 0; i < 4; ++i)
    {
        v[i] = vertices[i] - center;
    }

    Plane faces[4] = {CreatePlane(v[0], v[1], v[2]),
        CreatePlane(v[3], v[1], v[0]), CreatePlane(v[3], v[2], v[1]),
        CreatePlane(v[3], v[0], v[2])};

    u32 faceIndices[12] = {
        0,
        1,
        2,
        3,
        1,
        0,
        3,
        2,
        1,
        3,
        0,
        2,
    };

    ConvexHullFaceIndices hullFaceIndices[4] = {
        {&faceIndices[0], 3},
        {&faceIndices[3], 3},
        {&faceIndices[6], 3},
        {&faceIndices[9], 3},
    };

    ConvexHullShape convexHull = {};
    convexHull.vertexCount = 4;
    convexHull.vertices = vertices;
    convexHull.center = center;
    convexHull.edgeCount = 6;
    convexHull.edges = edges;
    convexHull.faceCount = 4;
    convexHull.faces = faces;
    convexHull.faceIndices = hullFaceIndices;

    DrawConvexHullShape(convexHull, Vec3(0.7f, 0.7f, 0.9f));

    if (WasKeyPressed(inputSystem, K_MOUSE_BUTTON_LEFT))
    {
        LineSegment lineSegment = GenerateLineSegment(x, y, frameBufferWidth,
            frameBufferHeight, g_Fov, g_NearClip, g_FarClip, camera);

        RaycastResult raycastResult = {};
        if (RayAabbTest(
                box, lineSegment.start, lineSegment.end, &raycastResult))
        {
            Debug_DrawLine(lineSegment.start, raycastResult.hitPoint,
                Vec3(0.7f, 0.1f, 0.15f), 30.0f);
            Debug_DrawLine(raycastResult.hitPoint,
                raycastResult.hitPoint + raycastResult.hitNormal * 0.25f,
                Vec3(1, 0, 1), 30.0f);
        }

        float intersectionT = 0.0f;
        vec3 intersectionNormal = {};
        vec3 intersectionPoint = {};
        if (SphereLineSegmentIntersect(sphere.center, sphere.radius, lineSegment.start,
                lineSegment.end, &intersectionT, &intersectionNormal,
                &intersectionPoint))
        {
            Debug_DrawLine(lineSegment.start, intersectionPoint,
                Vec3(0.7f, 0.1f, 0.15f), 30.0f);
            Debug_DrawLine(intersectionPoint,
                intersectionPoint + intersectionNormal * 0.25f, Vec3(1, 0, 1),
                30.0f);
        }

        // Capsule

        // Triangle
        if (RayTriangleTest(triangle, lineSegment.start, lineSegment.end,
                &intersectionPoint, &intersectionNormal, &intersectionT))
        {
            Debug_DrawLine(lineSegment.start, intersectionPoint,
                Vec3(0.7f, 0.1f, 0.15f), 30.0f);
            Debug_DrawLine(intersectionPoint,
                intersectionPoint + intersectionNormal * 0.25f, Vec3(1, 0, 1),
                30.0f);
        }

        // Convex Hull
        if (RayConvexHullTest(convexHull, lineSegment.start, lineSegment.end,
                &intersectionPoint, &intersectionNormal, &intersectionT))
        {
            Debug_DrawLine(lineSegment.start, intersectionPoint,
                Vec3(0.7f, 0.1f, 0.15f), 30.0f);
            Debug_DrawLine(intersectionPoint,
                intersectionPoint + intersectionNormal * 0.25f, Vec3(1, 0, 1),
                30.0f);
        }
    }
}

internal void UpdatePhysicsTestBench(GameState *gameState, GameMemory *memory,
    float frameBufferWidth, float frameBufferHeight, float dt)
{
    InputSystem *inputSystem = &gameState->inputSystem;
    float x = (float)gameState->inputSystem.mouseX;
    float y = (float)gameState->inputSystem.mouseY;

    memory->showCursor(true);
    UpdateCamera(&gameState->camera, inputSystem, dt, frameBufferWidth,
        frameBufferHeight);

    if (gameState->selectedPhysicsTestBenchScene == PhysicsTestBenchScene_ClosestPoint)
    {
        ClosestPointSceneState *sceneState = &gameState->closestPointState;
        UpdatePhysicsTestBenchEditorControls(sceneState, inputSystem, gameState->camera,
            frameBufferWidth, frameBufferHeight);
        DrawPhysicsTestBenchScene(sceneState);
        UpdateClosestPointScene(sceneState);
    }
    else if (gameState->selectedPhysicsTestBenchScene == PhysicsTestBenchScene_Intersection)
    {
        ClosestPointSceneState *sceneState = &gameState->intersectionState;
        UpdatePhysicsTestBenchEditorControls(sceneState, inputSystem, gameState->camera,
            frameBufferWidth, frameBufferHeight);
        DrawPhysicsTestBenchScene(sceneState);
        UpdateIntersectionScene(sceneState);
    }
    else if (gameState->selectedPhysicsTestBenchScene ==
             PhysicsTestBenchScene_CapsuleTriangleClosestPoint)
    {
        ClosestPointSceneState *sceneState = &gameState->capsuleTriangleState;
        UpdatePhysicsTestBenchEditorControls(sceneState, inputSystem, gameState->camera,
            frameBufferWidth, frameBufferHeight);
        DrawPhysicsTestBenchScene(sceneState);
        UpdateCapsuleTriangleClosestPointScene(sceneState);
    }
    else if (gameState->selectedPhysicsTestBenchScene ==
             PhysicsTestBenchScene_SphereSweepTest)
    {
        UpdateSphereSweepTestScene(&gameState->sphereTriangleSweepState,
            inputSystem, gameState->camera, frameBufferWidth,
            frameBufferHeight);
    }
    else if (gameState->selectedPhysicsTestBenchScene ==
             PhysicsTestBenchScene_Raycasting)
    {
        UpdateRaycastingScene(inputSystem, gameState->camera, frameBufferWidth,
            frameBufferHeight);
    }
    else if (gameState->selectedPhysicsTestBenchScene ==
             PhysicsTestBenchScene_ParticleSimulation)
    {
        PhysicsEngine *physicsEngine = &gameState->physicsEngine;

#if 0
        // Attraction to point force generator
        vec3 attractionPoint = Vec3(0, 0, 0);
        Debug_DrawPoint(attractionPoint, Vec3(0, 0, 1));
        for (u32 i = 0; i < physicsEngine->particleCount; ++i)
        {
            vec3 v = attractionPoint - physicsEngine->positions[i];
            float distance = Length(v);
            vec3 direction = Normalize(v);
            float invSq = 1.0f / ( distance * distance);
            float mass = 1.0f / physicsEngine->invMasses[i];
            float strength = 1.0f;
            vec3 force = direction * strength * mass * invSq;
            physicsEngine->forces[i] += force;
        }
#endif

#if 0
        // Spring force generator
        float restingLength = 0.0f;
        float springConstant = 3.3f;
        for (u32 i = 0; i < physicsEngine->particleCount; ++i)
        {
            vec3 anchor = Vec3(i, 10, 0);
            vec3 d = physicsEngine->positions[i] - anchor;
            float length = Length(d);
            float magnitude = (restingLength - length) * springConstant;

            vec3 force = Normalize(d) * magnitude;
            physicsEngine->forces[i] += force;
        }
#endif

#if 0
        // Bungee force generator
        float restingLength = 3.0f;
        float springConstant = 7.3f;
        for (u32 i = 0; i < physicsEngine->particleCount; ++i)
        {
            vec3 anchor = Vec3(i, 10, 0);
            vec3 d = physicsEngine->positions[i] - anchor;
            float length = Length(d);
            if (length > restingLength)
            {
                float magnitude = (restingLength - length) * springConstant;

                vec3 force = Normalize(d) * magnitude;
                physicsEngine->forces[i] += force;
            }
        }
#endif

#if 0
        // Buoyancy force generator
        float maxDepth = 1.0f;
        float volume = 0.1f;
        float waterHeight = 1.0f;
        float liquidDensity = 1000.0f;
        for (u32 i = 0; i < physicsEngine->particleCount; ++i)
        {
            float depth = physicsEngine->positions[i].y;

            if (depth >= waterHeight + maxDepth)
                continue;

            vec3 force = Vec3(0, 0, 0);
            if (depth <= waterHeight - maxDepth)
            {
                force.y = liquidDensity * volume;
            }
            else
            {
                float d = (depth - maxDepth - waterHeight) / ( 2 * maxDepth);
                d = Abs(d);
                LOG_DEBUG("d=%g", d);
                force.y = liquidDensity * volume * d;
            }

            LOG_DEBUG("%g: %g", depth, force.y);

            physicsEngine->forces[i] += force;
        }

        Debug_DrawAabb(Vec3(-10, waterHeight - 10, -10),
                Vec3(10, waterHeight, 10), Vec3(0.2, 0.6, 1.0));
#endif

        Integrate(physicsEngine, dt);

        Contact contacts[32];
        u32 contactCount = 0;
        // Generate contacts
        Plane groundPlane = CreatePlane(Vec3(0, 1, 0), Vec3(0));
        float radius = 0.5f;
        for (u32 i = 0; i < physicsEngine->particleCount; ++i)
        {
            if (Dot(physicsEngine->positions[i], groundPlane.normal) > 0.0f)
            {
                float dist = Distance(groundPlane, physicsEngine->positions[i]);
                float penetration = radius - dist;
                if (penetration >= 0.0f)
                {
                    Contact contact = {};
                    contact.normal = Vec3(0, 1, 0);
                    contact.indices[0] = i;
                    contact.indices[1] = -1;
                    contact.penetration = penetration;
                    contact.restitution = 0.8f;

                    Assert(contactCount < ArrayCount(contacts));
                    contacts[contactCount++] = contact;
                }
            }
        }

        ResolveContacts(physicsEngine, contacts, contactCount, dt);

        DrawPlane(groundPlane, Vec3(0), 10.0f, Vec3(1, 0, 0));


        for (u32 i = 0; i < physicsEngine->particleCount; ++i)
        {
            DrawSphere(
                physicsEngine->positions[i], 0.5f, Vec3(0.7f, 1.0f, 0.2f));
        }
    }
    else if (gameState->selectedPhysicsTestBenchScene ==
             PhysicsTestBenchScene_RigidBodySimulation)
    {
        PhysicsEngine *physicsEngine = &gameState->physicsEngine;

#if 0
        // Spring force generator
        float restingLength = 2.0f;
        float springConstant = 8.3f;
        vec3 anchor = Vec3(-4, 4, 0);
        {
            RigidBody *rigidBody = &physicsEngine->rigidBodies[0];
            vec3 localPoint = Vec3(0, 0.25f, 0);
            vec3 attachmentPoint =
                (rigidBody->transformMatrix * Vec4(localPoint, 1.0f)).xyz;

            vec3 d = attachmentPoint - anchor;
            float length = Length(d);
            float magnitude = (restingLength - length) * springConstant;

            vec3 force = Normalize(d) * magnitude;
            AddForceAtWorldPoint(rigidBody, 1, force, attachmentPoint);

            Debug_DrawPoint(anchor, Vec3(1, 0, 0));
            Debug_DrawLine(anchor, attachmentPoint, Vec3(1, 0, 0));
        }
#endif

        float rotMul = 1.0f;
        if (IsKeyDown(inputSystem, K_SPACE))
            rotMul = 0.0f;

        // Init
        {
            RigidBody *rigidBody = &physicsEngine->rigidBodies[0];
            physicsEngine->rigidBodyCount = 1;

            ZeroPointerToStruct(rigidBody);

            rigidBody->position = Vec3(0, 2, 0);
            rigidBody->orientation = Quat(Vec3(0, 0, 1), PI * rotMul);
            rigidBody->inverseMass = 1.0f / 30.0f;
            rigidBody->linearDamping = 0.98f;
            rigidBody->angularDamping = 0.98f;
            rigidBody->inverseInertiaTensor =
                Inverse(BoxInertiaTensor(Vec3(0.5, 0.5, 0.5), 30.0f));
            rigidBody->acceleration = Vec3(0, -10, 0);

            UpdateDerivedData(rigidBody, 1);
        }

        i32 increment = 0;
        if (WasKeyPressed(inputSystem, K_LEFT_BRACKET) ||
                WasKeyRepeated(inputSystem, K_LEFT_BRACKET))
        {

            increment = -1;
        }
        if (WasKeyPressed(inputSystem, K_RIGHT_BRACKET) ||
                WasKeyRepeated(inputSystem, K_RIGHT_BRACKET))
        {
            increment = 1;
        }

        if (IsKeyDown(inputSystem, K_LEFT_SHIFT) ||
            IsKeyDown(inputSystem, K_RIGHT_SHIFT))
        {
            increment *= 10;
        }

        i32 value = (i32)gameState->maxIterations + increment;
        gameState->maxIterations = (u32)Max(value, 0);

        Debug_Printf("Max Iterations: %u\n", gameState->maxIterations);

        Plane groundPlane = CreatePlane(Vec3(0, 1, 0), Vec3(0, 0, 0));

        RigidBodyContact contacts[32] = {};
        u32 contactCount = 0;

        float timestep = 1.0f / 60.0f;
        for (u32 iterationCount = 0; iterationCount < gameState->maxIterations;
             ++iterationCount)
        {
            Integrate(physicsEngine->rigidBodies, physicsEngine->rigidBodyCount,
                timestep);

            UpdateDerivedData(
                physicsEngine->rigidBodies, physicsEngine->rigidBodyCount);

            contactCount = 0;
            // Calculate contacts
            {
                RigidBody *rigidBody = physicsEngine->rigidBodies;
                vec3 h = Vec3(0.5, 0.5, 0.5) * 0.5f;
                vec3 vertices[8] = {
                    {-h.x, -h.y, -h.z},
                    {h.x, -h.y, -h.z},
                    {h.x, -h.y, h.z},
                    {-h.x, -h.y, h.z},

                    {-h.x, h.y, -h.z},
                    {h.x, h.y, -h.z},
                    {h.x, h.y, h.z},
                    {-h.x, h.y, h.z},
                };

                vec3 verticesWorld[8];
                for (u32 i = 0; i < 8; ++i)
                {
                    verticesWorld[i] =
                        (rigidBody->transformMatrix * Vec4(vertices[i], 1.0))
                            .xyz;
                }

                for (u32 i = 0; i < 8; ++i)
                {
                    float penetration =
                        Dot(groundPlane.normal, verticesWorld[i]) -
                        groundPlane.distance;

                    if (penetration < 0.0f)
                    {
                        // Generate contact
                        Assert(contactCount < ArrayCount(contacts));
                        RigidBodyContact *contact = contacts + contactCount++;

                        contact->point = verticesWorld[i];
                        contact->normal = groundPlane.normal;
                        contact->indices[0] = 0;
                        contact->indices[1] = -1;
                        contact->penetration = -penetration;
                        contact->restitution = 0.4f;
                    }
                }
            }

            Resolve(physicsEngine->rigidBodies, physicsEngine->rigidBodyCount,
                contacts, contactCount, timestep, &gameState->transArena);
        }

        DrawPlane(groundPlane, Vec3(0, 0, 0), 5.0f, Vec3(0, 1, 0));

        for (u32 i = 0; i < contactCount; ++i)
        {
            RigidBodyContact *contact = contacts + i;
#if 0
            Debug_DrawPoint(contact->point, Vec3(1, 0, 0), 0.15f);
            Debug_DrawLine(contact->point,
                contact->point + contact->normal * contact->penetration,
                Vec3(1, 0, 1));
            Debug_PrintInWorld(
                contact->point, "Penetration: %g", contact->penetration);
#endif
        }

        for (u32 i = 0; i < physicsEngine->rigidBodyCount; ++i)
        {
            RigidBody *rigidBody = physicsEngine->rigidBodies + i;
            mat4 transform = Translate(rigidBody->position) *
                Rotate(rigidBody->orientation);

            Debug_DrawAxis(transform);
            DrawBox(transform, Vec3(0.5, 0.5, 0.5) * 0.5f,
                    Vec3(0.7f, 1.0f, 0.2f));

            Debug_Printf("Position: %g %g %g\n", rigidBody->position.x,
                rigidBody->position.y, rigidBody->position.z);

            Debug_Printf("Velocity: %g %g %g\n", rigidBody->velocity.x,
                rigidBody->velocity.y, rigidBody->velocity.z);

            Debug_Printf("Acceleration: %g %g %g\n", rigidBody->acceleration.x,
                rigidBody->acceleration.y, rigidBody->acceleration.z);

            Debug_Printf("Orientation: %g %g %g %g\n", rigidBody->orientation.x,
                rigidBody->orientation.y, rigidBody->orientation.z,
                rigidBody->orientation.w);

            Debug_Printf("Rotation: %g %g %g\n", rigidBody->rotation.x,
                rigidBody->rotation.y, rigidBody->rotation.z);

            Debug_PrintInWorld(rigidBody->position, "Velocity: %g %g %g",
                    rigidBody->velocity.x, rigidBody->velocity.y,
                    rigidBody->velocity.z);
        }
    }
}
