#pragma once

// FIXME: Only need this for the lineBuffer and vertex type
#include "vulkan_renderer.h"

enum
{
    UNIFORM_KEY_NONE,
    UNIFORM_KEY_TEX_SAMPLER,
    UNIFORM_KEY_COLOR,
    UNIFORM_KEY_LIGHT_VIEW_PROJECTION,
    UNIFORM_KEY_SHADOW_MAP,
    UNIFORM_KEY_TERRAIN_HEIGHT_MAP,
    UNIFORM_KEY_TERRAIN_NORMAL_MAP,
    UNIFORM_KEY_TERRAIN_TEX_COORD_OFFSET,
    UNIFORM_KEY_TERRAIN_TEX_COORD_SCALE,
    UNIFORM_KEY_TEX_COORD_OFFSET,
    UNIFORM_KEY_TEX_COORD_SCALE,
    MAX_UNIFORM_KEYS,
};

global const char* g_UniformKeyNames[MAX_UNIFORM_KEYS] = {
    "UNIFORM_KEY_NONE",
    "UNIFORM_KEY_TEX_SAMPLER",
    "UNIFORM_KEY_COLOR",
    "UNIFORM_KEY_LIGHT_VIEW_PROJECTION",
    "UNIFORM_KEY_SHADOW_MAP",
    "UNIFORM_KEY_TERRAIN_HEIGHT_MAP",
    "UNIFORM_KEY_TERRAIN_NORMAL_MAP",
    "UNIFORM_KEY_TERRAIN_TEX_COORD_OFFSET",
    "UNIFORM_KEY_TERRAIN_TEX_COORD_SCALE",
    "UNIFORM_KEY_TEX_COORD_OFFSET",
    "UNIFORM_KEY_TEX_COORD_SCALE",
};

struct ShaderUniformValue
{
    u32 key;
    u32 type;
    union
    {
        vec2 v2;
        vec3 v3;
        vec4 v4;
        mat4 m4;
        u32 texture;
    };
};

enum
{
    UNIFORM_TYPE_2F,
    UNIFORM_TYPE_3F,
    UNIFORM_TYPE_4F,
    UNIFORM_TYPE_MAT4,
    UNIFORM_TYPE_TEXTURE2D,
    UNIFORM_TYPE_TEXTURE_CUBE_MAP,
    UNIFORM_TYPE_SHADOW_MAP,
    MAX_UNIFORM_TYPES,
};

global const char* g_UniformTypeNames[MAX_UNIFORM_TYPES] = {
    "UNIFORM_TYPE_2F",
    "UNIFORM_TYPE_3F",
    "UNIFORM_TYPE_4F",
    "UNIFORM_TYPE_MAT4",
    "UNIFORM_TYPE_TEXTURE2D",
    "UNIFORM_TYPE_TEXTURE_CUBE_MAP",
    "UNIFORM_TYPE_SHADOW_MAP",
};


struct RenderCommandHeader
{
    u32 type;
    u32 size;

    u32 sortKey;
};


struct RenderCommandQueue
{
    u8 *start;
    u32 capacity;
    u32 length;
};

enum
{
    RenderCommandDrawMeshUniformsTypeId,
    RenderCommandAllocateTextureTypeId,
    RenderCommandAllocateCubeMapTypeId,
    RenderCommandUpdateVertexBufferTypeId,
    RenderCommandDrawVertexBufferTypeId,
    RenderCommandAllocateMeshTypeId,
    RenderCommandDrawLightTypeId,
    RenderCommandDrawShadowTypeId,
    MAX_RENDER_COMMAND_TYPES,
};

global const char* g_RenderCommandTypeNames[MAX_RENDER_COMMAND_TYPES] = {
    "RenderCommandDrawMeshUniformsTypeId",
    "RenderCommandAllocateTextureTypeId",
    "RenderCommandAllocateCubeMapTypeId",
    "RenderCommandUpdateVertexBufferTypeId",
    "RenderCommandDrawVertexBufferTypeId",
    "RenderCommandAllocateMeshTypeId",
    "RenderCommandDrawLightTypeId",
    "RenderCommandDrawShadowTypeId",
};

// TODO: Support parameter for vertex layout
// TODO: Support parameter for index layout or converting from u32 indices to u16
struct RenderCommandAllocateMesh
{
    Vertex *vertices; // FIXME: Only Vulkan 'Vertex' format supported
    u32 vertexCount;
    u32 *indices;
    u32 indexCount;
    u32 meshIndex;
};

struct RenderCommandDrawMeshUniforms
{
    mat4 model;
    mat4 view;
    mat4 projection;
    u32 shader;
    u32 mesh;

    // TODO: This will probably blow out our render command queue
    ShaderUniformValue values[10];
    u32 count;
};

inline void PushUniformTexture(
    RenderCommandDrawMeshUniforms *cmd, u32 key, u32 texture)
{
    Assert(cmd->count < ArrayCount(cmd->values));
    u32 idx = cmd->count++;
    cmd->values[idx].key = key;
    cmd->values[idx].type = UNIFORM_TYPE_TEXTURE2D;
    cmd->values[idx].texture = texture;
}

inline void PushUniformV2(RenderCommandDrawMeshUniforms *cmd, u32 key, vec2 v)
{
    Assert(cmd->count < ArrayCount(cmd->values));
    u32 idx = cmd->count++;
    cmd->values[idx].key = key;
    cmd->values[idx].type = UNIFORM_TYPE_2F;
    cmd->values[idx].v2 = v;
}

inline void PushUniformV4(RenderCommandDrawMeshUniforms *cmd, u32 key, vec4 v)
{
    Assert(cmd->count < ArrayCount(cmd->values));
    u32 idx = cmd->count++;
    cmd->values[idx].key = key;
    cmd->values[idx].type = UNIFORM_TYPE_4F;
    cmd->values[idx].v4 = v;
}

inline void PushUniformMat4(RenderCommandDrawMeshUniforms *cmd, u32 key, mat4 m)
{
    Assert(cmd->count < ArrayCount(cmd->values));
    u32 idx = cmd->count++;
    cmd->values[idx].key = key;
    cmd->values[idx].type = UNIFORM_TYPE_MAT4;
    cmd->values[idx].m4 = m;
}

inline void PushUniformSpecialType(RenderCommandDrawMeshUniforms *cmd, u32 key, u32 type)
{
    Assert(cmd->count < ArrayCount(cmd->values));
    u32 idx = cmd->count++;
    cmd->values[idx].key = key;
    cmd->values[idx].type = type;
}

struct RenderCommandDrawShadow
{
    mat4 model;
    mat4 view;
    mat4 projection;
    u32 mesh;
};

struct RenderCommandDrawVertexBuffer
{
    mat4 model;
    mat4 view;
    mat4 projection;
    vec4 color;
    u32 vbo;
    u32 shader;
    u32 material;
    u32 vertexCount;
    u32 firstVertex;
};

enum LightType
{
    LightType_Ambient,
    LightType_Directional,
    LightType_Point,
    LightType_Spot,
};

struct RenderCommandDrawLight
{
    LightType type;
    vec3 color;
    union
    {
        vec3 direction;
        vec3 position;
    };
    vec3 attenuation; // Only used for point lights
};

struct RenderCommandAllocateTexture
{
    void *pixels;
    u32 width;
    u32 height;
    u32 bytesPerPixel;
    u32 index;
    u32 materialIndex;
    b32 useSRGB;
    b32 repeatMapping;
    u32 bytesPerChannel;
    b32 generateMipMaps;
};

struct RenderCommandUpdateVertexBuffer
{
    void *vertices;
    u32 lengthInBytes;
    u32 bufferIndex;
};

enum
{
    CUBE_MAP_FACE_X_POS,
    CUBE_MAP_FACE_X_NEG,
    CUBE_MAP_FACE_Y_POS,
    CUBE_MAP_FACE_Y_NEG,
    CUBE_MAP_FACE_Z_POS,
    CUBE_MAP_FACE_Z_NEG,
    MAX_CUBE_MAP_FACES,
};

struct RenderCommandAllocateCubeMap
{
    void *pixels[MAX_CUBE_MAP_FACES];
    u32 width; // Cubemap images must be square
    u32 bytesPerPixel;
    u32 textureIndex;
    u32 materialIndex;
};


#define RenderCommandAllocate(QUEUE, TYPE, RENDER_PASS) \
    (TYPE *)RenderCommandAllocate_(QUEUE, sizeof(TYPE), TYPE##TypeId, RENDER_PASS)

// FIXME: Currently rely on renderCommands buffer being cleared to zero
inline void *RenderCommandAllocate_(RenderCommandQueue *queue, u32 size, u32 type, u32 sortKey)
{
    void *result = NULL;
    if (queue->length + sizeof(RenderCommandHeader) + size <= queue->capacity)
    {
        RenderCommandHeader *header = (RenderCommandHeader*)(queue->start + queue->length);
        queue->length += sizeof(RenderCommandHeader) + size;
        header->type = type;
        header->sortKey = sortKey;
        header->size = size;
        result = (void*)(header + 1);
    }

    return result;
}



//inline void AddLineToBuffer(LineBuffer *buffer, vec3 p0, vec3 p1, vec3 color)
//{
    //if (buffer->vertexCount + 2 <= buffer->maxVertices)
    //{
        //void *dst = (u8 *)buffer->vertices +
                    //sizeof(VertexPC) * buffer->vertexCount;

        //VertexPC src[2];
        //src[0].position = p0;
        //src[0].color = color;
        //src[1].position = p1;
        //src[1].color = color;

        //memcpy(dst, src, sizeof(VertexPC) * 2);
        //buffer->vertexCount += 2;
    //}
//}
