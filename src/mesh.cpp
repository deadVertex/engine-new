struct MeshData
{
    Vertex *vertices;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
};

struct TriangleFace
{
    u32 i, j, k;
};

inline Vertex CreateUnitVertex(float x, float y, float z)
{
    Vertex result;
    result.pos = Normalize(Vec3(x, y, z));
    result.color = result.pos; // FIXME: This is actually the normal!!
    result.texCoord = Vec2(result.pos.x, result.pos.y);
    return result;
}

struct GetIcosahedronMeshDataResult
{
    u32 vertexCount;
    u32 indexCount;
};

GetIcosahedronMeshDataResult
GetIcosahedronMeshData(u32 tesselationLevel, MemoryArena *arena,
                       Vertex *vertices, u32 maxVertices,
                       u32 *indices, u32 maxIndices)
{
    Assert(maxVertices > 12);

    float t = ( 1.0f + SquareRoot( 5.0f ) ) * 0.5f;

    vertices[0] = CreateUnitVertex(-1, t, 0);
    vertices[1] = CreateUnitVertex(1, t, 0);
    vertices[2] = CreateUnitVertex(-1, -t, 0);
    vertices[3] = CreateUnitVertex(1, -t, 0);

    vertices[4] = CreateUnitVertex(0, -1, t);
    vertices[5] = CreateUnitVertex(0, 1, t);
    vertices[6] = CreateUnitVertex(0, -1, -t);
    vertices[7] = CreateUnitVertex(0, 1, -t);

    vertices[8] = CreateUnitVertex(t, 0, -1);
    vertices[9] = CreateUnitVertex(t, 0, 1);
    vertices[10] = CreateUnitVertex(-t, 0, -1);
    vertices[11] = CreateUnitVertex(-t, 0, 1);

    u32 vertexCount = 12;

    u32 initialFaceCount = 20;
    TriangleFace *initialFaces =
            AllocateArray(arena, TriangleFace, initialFaceCount);

    initialFaces[0] = TriangleFace{0, 11, 5};
    initialFaces[1] = TriangleFace{0, 5, 1};
    initialFaces[2] = TriangleFace{0, 1, 7};
    initialFaces[3] = TriangleFace{0, 7, 10};
    initialFaces[4] = TriangleFace{0, 10, 11};

    initialFaces[5] = TriangleFace{1, 5, 9};
    initialFaces[6] = TriangleFace{5, 11, 4};
    initialFaces[7] = TriangleFace{11, 10, 2};
    initialFaces[8] = TriangleFace{10, 7, 6};
    initialFaces[9] = TriangleFace{7, 1, 8};

    initialFaces[10] = TriangleFace{3, 9, 4};
    initialFaces[11] = TriangleFace{3, 4, 2};
    initialFaces[12] = TriangleFace{3, 2, 6};
    initialFaces[13] = TriangleFace{3, 6, 8};
    initialFaces[14] = TriangleFace{3, 8, 9};

    initialFaces[15] = TriangleFace{4, 9, 5};
    initialFaces[16] = TriangleFace{2, 4, 11};
    initialFaces[17] = TriangleFace{6, 2, 10};
    initialFaces[18] = TriangleFace{8, 6, 7};
    initialFaces[19] = TriangleFace{9, 8, 1};

    u32 currentFaceCount = initialFaceCount;
    TriangleFace *currentFaces = initialFaces;
    for (u32 i = 0; i < tesselationLevel; ++i)
    {
        TriangleFace *newFaces =
                AllocateArray(arena, TriangleFace, currentFaceCount * 4);
        u32 newFaceCount = 0;

        for (size_t j = 0; j < currentFaceCount; ++j)
        {
            TriangleFace currentFace = currentFaces[j];

            // 3 edges in total comprised of 2 indices each
            u32 edges[6] = {currentFace.i, currentFace.j, currentFace.j,
                            currentFace.k, currentFace.k, currentFace.i};

            u32 indicesAdded[3];

            for (u32 k = 0; k < 3; ++k)
            {
                // Retrieve vertices for each edge
                u32 idx0 = edges[k*2];
                u32 idx1 = edges[k*2 + 1];
                Vertex v0 = vertices[idx0];
                Vertex v1 = vertices[idx1];

                // Add mid point vertex
                vec3 u = v0.pos + v1.pos;

                Assert(vertexCount < maxVertices);
                vertices[vertexCount] = CreateUnitVertex(u.x, u.y, u.z);

                indicesAdded[k] = vertexCount++;
            }

            newFaces[newFaceCount++] = TriangleFace{
                    currentFace.i, indicesAdded[0], indicesAdded[2]};
            newFaces[newFaceCount++] = TriangleFace{
                    currentFace.j, indicesAdded[1], indicesAdded[0]};
            newFaces[newFaceCount++] = TriangleFace{
                    currentFace.k, indicesAdded[2], indicesAdded[1]};
            newFaces[newFaceCount++] = TriangleFace{
                    indicesAdded[0], indicesAdded[1], indicesAdded[2]};
        }

        currentFaces = newFaces;
        currentFaceCount = newFaceCount;
    }

    Assert(currentFaceCount * 3 < maxIndices);
    memcpy(indices, currentFaces,
           currentFaceCount * sizeof(TriangleFace));

    // Also frees the allocated faces for all tesselation levels
    MemoryArenaFree(arena, initialFaces);

    GetIcosahedronMeshDataResult result = {};
    result.vertexCount = vertexCount;
    result.indexCount = currentFaceCount * 3;

    return result;
}

internal MeshData GeneratePatchMesh(MemoryArena *arena, u32 gridDim)
{
    MeshData result = {};

    u32 vertDim = gridDim + 1;
    u32 totalVertices = vertDim * vertDim;
    Vertex *vertices = AllocateArray(arena, Vertex, totalVertices);

    u32 totalIndices = gridDim * gridDim * 2 * 3;
    u32 *indices = AllocateArray(arena, u32, totalIndices);

    for (u32 y = 0; y < vertDim; ++y)
    {
        for (u32 x = 0; x < vertDim; ++x)
        {
            float fx = x / (float)gridDim;
            float fy = y / (float)gridDim;

            Vertex *vertex = &vertices[y * vertDim + x];
            vertex->pos = Vec3(fx, 0.0f, -fy);
            vertex->normal = Vec3(0.0f, 0.0f, 1.0f);
            vertex->texCoord = Vec2(fx, fy);
        }
    }

    u32 index = 0;
    for (u32 y = 0; y < gridDim; y++)
    {
        for (u32 x = 0; x < gridDim; x++)
        {
            indices[index++] = (x + vertDim * y);
            indices[index++] = ((x + 1) + vertDim * y);
            indices[index++] = (x + vertDim * (y + 1));

            indices[index++] = ((x + 1) + vertDim * y);
            indices[index++] = ((x + 1) + vertDim * (y + 1));
            indices[index++] = (x + vertDim * (y + 1));
        }
    }

    result.vertices = vertices;
    result.indices = indices;
    result.vertexCount = totalVertices;
    result.indexCount = totalIndices;

    return result;
}

internal void CreateHardcodedMeshes(RenderCommandQueue *renderCommands, MemoryArena *tempArena)
{
    { // Quad
        Vertex vertices[] = {
            {{-0.5f, -0.5f, 0.0f}, {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, -0.5f, 0.0f}, {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, 0.5f, 0.0f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
            {{-0.5f, 0.5f, 0.0f}, {1.0f, 1.0f, 1.0f}, {0.0f, 0.0f}}};

        u32 indices[] = {0, 1, 2, 2, 3, 0};

        RenderCommandAllocateMesh *allocateMesh =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateMesh, 0);
        allocateMesh->vertices =
            AllocateArray(tempArena, Vertex, ArrayCount(vertices));
        memcpy(allocateMesh->vertices, vertices, sizeof(vertices));
        allocateMesh->indices =
            AllocateArray(tempArena, u32, ArrayCount(indices));
        memcpy(allocateMesh->indices, indices, sizeof(indices));
        allocateMesh->vertexCount = ArrayCount(vertices);
        allocateMesh->indexCount = ArrayCount(indices);
        allocateMesh->meshIndex = VULKAN_MESH_QUAD;
    }

    { // Cube

        // clang-format off
        Vertex vertices[] = {
            // Top
            {{-0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, 0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, 0.5f, 0.5f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f}},

            // Bottom
            {{-0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, -0.5f, -0.5f}, {0.0f, -1.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, -0.5f, 0.5f}, {0.0f, -1.0f, 0.0f}, {0.0f, 0.0f}},

            // Back
            {{-0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {1.0f, 1.0f}},
            {{0.5f, 0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 1.0f}},
            {{0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {0.0f, 0.0f}},
            {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f, -1.0f}, {1.0f, 0.0f}},

            // Front
            {{-0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 1.0f}},
            {{0.5f, 0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
            {{0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 0.0f}},
            {{-0.5f, -0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f}},

            // Left
            {{-0.5f, 0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{-0.5f, -0.5f, -0.5f}, {-1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{-0.5f, -0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{-0.5f, 0.5f, 0.5f}, {-1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},

            // Right
            {{0.5f, 0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
            {{0.5f, -0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
            {{0.5f, -0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
            {{0.5f, 0.5f, 0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
        };

        u32 indices[] = {
            2, 1, 0,
            0, 3, 2,

            4, 5, 6,
            6, 7, 4,

            8, 9, 10,
            10, 11, 8,

            14, 13, 12,
            12, 15, 14,

            16, 17, 18,
            18, 19, 16,

            22, 21, 20,
            20, 23, 22
        };

        // clang-format on

        RenderCommandAllocateMesh *allocateMesh =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateMesh, 0);
        allocateMesh->vertices =
            AllocateArray(tempArena, Vertex, ArrayCount(vertices));
        memcpy(allocateMesh->vertices, vertices, sizeof(vertices));
        allocateMesh->indices =
            AllocateArray(tempArena, u32, ArrayCount(indices));
        memcpy(allocateMesh->indices, indices, sizeof(indices));
        allocateMesh->vertexCount = ArrayCount(vertices);
        allocateMesh->indexCount = ArrayCount(indices);
        allocateMesh->meshIndex = VULKAN_MESH_CUBE;
    }

    {
        u32 maxVertices = 64;
        u32 maxIndices = 256;
        Vertex *vertices = AllocateArray(tempArena, Vertex, maxVertices);
        u32 *indices = AllocateArray(tempArena, u32, maxIndices);
        GetIcosahedronMeshDataResult result = GetIcosahedronMeshData(
            0, tempArena, vertices, maxVertices, indices, maxIndices);

        RenderCommandAllocateMesh *allocateMesh =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateMesh, 0);
        allocateMesh->vertices = vertices;
        allocateMesh->indices = indices;
        allocateMesh->vertexCount = result.vertexCount;
        allocateMesh->indexCount = result.indexCount;
        allocateMesh->meshIndex = VULKAN_MESH_ICOSAHEDRON;
    }


    {
        u32 maxVertices = 128;
        u32 maxIndices = 512;
        Vertex *vertices = AllocateArray(tempArena, Vertex, maxVertices);
        u32 *indices = AllocateArray(tempArena, u32, maxIndices);
        GetIcosahedronMeshDataResult result = GetIcosahedronMeshData(
            1, tempArena, vertices, maxVertices, indices, maxIndices);

        RenderCommandAllocateMesh *allocateMesh =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateMesh, 0);
        allocateMesh->vertices = vertices;
        allocateMesh->indices = indices;
        allocateMesh->vertexCount = result.vertexCount;
        allocateMesh->indexCount = result.indexCount;
        allocateMesh->meshIndex = VULKAN_MESH_ICOSAHEDRON_T1;
    }

    {
        u32 maxVertices = 512;
        u32 maxIndices = 1024;
        Vertex *vertices = AllocateArray(tempArena, Vertex, maxVertices);
        u32 *indices = AllocateArray(tempArena, u32, maxIndices);
        GetIcosahedronMeshDataResult result = GetIcosahedronMeshData(
            2, tempArena, vertices, maxVertices, indices, maxIndices);

        RenderCommandAllocateMesh *allocateMesh =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateMesh, 0);
        allocateMesh->vertices = vertices;
        allocateMesh->indices = indices;
        allocateMesh->vertexCount = result.vertexCount;
        allocateMesh->indexCount = result.indexCount;
        allocateMesh->meshIndex = VULKAN_MESH_ICOSAHEDRON_T2;
    }

    {
        MeshData meshData = GeneratePatchMesh(tempArena, TERRAIN_GRID_DIM);
        RenderCommandAllocateMesh *allocateMesh =
            RenderCommandAllocate(renderCommands, RenderCommandAllocateMesh, 0);
        allocateMesh->vertices = meshData.vertices;
        allocateMesh->indices = meshData.indices;
        allocateMesh->vertexCount = meshData.vertexCount;
        allocateMesh->indexCount = meshData.indexCount;
        allocateMesh->meshIndex = VULKAN_MESH_TERRAIN_PATCH;
    }
}

struct ObjVertex
{
    u32 position;
    u32 texCoord;
    u32 normal;
};

struct ObjFace
{
    u32 indices[3];
};

// TODO: Support missing texCoord or normal data
internal bool LoadObjMeshData(
    const char *meshDataRaw, u32 length, MemoryArena *arena, MeshData *output)
{
    String meshData = CreateString(meshDataRaw, length);

    vec3 vertexPositions[0x2000];
    u32 vertexPositionCount = 0;

    vec3 vertexNormals[0x2000];
    u32 vertexNormalCount = 0;

    vec2 vertexTexCoords[0x2000];
    u32 vertexTexCoordsCount = 0;

    ObjVertex objVertices[0x8000];
    u32 objVertexCount = 0;

    ObjFace objFaces[0x4000];
    u32 objFaceCount = 0;

    String line = {};
    while (Split(meshData, '\n', &line, &meshData))
    {
        line = ConsumePreceedingWhitespace(line);
        if (line.length < 1)
        {
            continue; // Empty line
        }

        if (line.data[0] == '#')
        {
            continue; // Skip comment lines
        }

        if (line.data[0] == 'o')
        {
            continue; // Don't do anything for objects yet
        }

        if (line.data[0] == 'v')
        {
            SplitStringResult splitResult = SplitString(line, ' ');

            if (splitResult.count != 2)
            {
                return false; // Malformed data
            }

            String type = splitResult.str[0];
            String data = splitResult.str[1];

            if (type.data[1] == ' ') // Vertex position
            {
                // TODO: Handle this being malformed
                vec3 v = ReadVec3(data);

                if (vertexPositionCount < ArrayCount(vertexPositions))
                {
                    vertexPositions[vertexPositionCount++] = v;
                }
            }
            else if (line.data[1] == 'n') // Vertex normal
            {
                vec3 v = ReadVec3(data);

                if (vertexNormalCount < ArrayCount(vertexNormals))
                {
                    vertexNormals[vertexNormalCount++] = v;
                }
            }
            else if (line.data[1] == 't') // Vertex Texture Coordinate
            {
                vec2 v = ReadVec2(data);

                if (vertexTexCoordsCount < ArrayCount(vertexTexCoords))
                {
                    vertexTexCoords[vertexTexCoordsCount++] = v;
                }
            }
        }
        else if (line.data[0] == 'f')
        {
            SplitStringResult splitResult = SplitString(line, ' ');

            if (splitResult.count != 2)
            {
                return false; // Malformed data
            }

            String data = splitResult.str[1];

            String faceData[3]; // Only support triangles for now
            u32 faceCount = Split(data, faceData, ArrayCount(faceData), ' ');
            Assert(faceCount == 3);

            ObjFace face = {};

            for (u32 j = 0; j < faceCount; ++j)
            {
                String indices[3];
                u32 indexCount =
                    Split(faceData[j], indices, ArrayCount(indices), '/');

                ObjVertex vertex = {};
                if (indexCount == 3)
                {
                    vertex.position = StringToU32(indices[0]);
                    vertex.texCoord = StringToU32(indices[1]);
                    vertex.normal   = StringToU32(indices[2]);
                }
                else if (indexCount == 2)
                {
                    vertex.position = StringToU32(indices[0]);
                    vertex.normal   = StringToU32(indices[1]);
                    vertex.texCoord = 0;
                }
                else
                {
                    InvalidCodePath();
                }

                if (objVertexCount < ArrayCount(objVertices))
                {
                    face.indices[j] = objVertexCount;
                    objVertices[objVertexCount++] = vertex;
                }
            }

            if (objFaceCount < ArrayCount(objFaces))
            {
                objFaces[objFaceCount++] = face;
            }
        }
    }

    bool removeDuplicateVertices = false;
    if (removeDuplicateVertices)
    {
        u32 indexRedirectionTable[0x1000];
        Assert(ArrayCount(indexRedirectionTable) == ArrayCount(objVertices));
        for (u32 j = 0; j < objVertexCount; ++j)
        {
            indexRedirectionTable[j] = j;
        }

        for (i32 j = 0; j < (i32)objVertexCount - 1; ++j)
        {
            ObjVertex a = objVertices[j];

            // Don't need to check previous vertices as they have already been
            // checked
            for (u32 k = j + 1; k < objVertexCount; ++k)
            {
                ObjVertex b = objVertices[k];
                if (a.position == b.position && a.normal == b.normal &&
                    a.texCoord == b.texCoord)
                {
                    // Duplicate vertex
                    indexRedirectionTable[k] = j;
                }
            }
        }
    }


    Vertex *vertices = AllocateArray(arena, Vertex, objVertexCount);

    u32 indexCount = objFaceCount * 3;
    Assert(indexCount == objVertexCount);
    u32 *indices = AllocateArray(arena, u32, indexCount);

    for (u32 vertexIdx = 0; vertexIdx < objVertexCount; ++vertexIdx)
    {
        ObjVertex src = objVertices[vertexIdx];
        Vertex *dst = vertices + vertexIdx;

        Assert(src.position != 0);
        Assert(src.normal != 0);

        // ObjVertex have all indices offset by 1
        dst->pos = vertexPositions[src.position - 1];
        dst->normal = vertexNormals[src.normal - 1];
        if (src.texCoord != 0)
        {
            dst->texCoord = vertexTexCoords[src.texCoord - 1];
        }
        else
        {
            dst->texCoord = Vec2(0, 0);
        }
    }

    for (u32 faceIdx = 0; faceIdx < objFaceCount; ++faceIdx)
    {
        ObjFace src = objFaces[faceIdx];

        // ObjFace have all indices offset by 1
        indices[faceIdx * 3]     = src.indices[0];
        indices[faceIdx * 3 + 1] = src.indices[1];
        indices[faceIdx * 3 + 2] = src.indices[2];
    }

    output->vertices = vertices;
    output->vertexCount = objVertexCount;

    output->indices = indices;
    output->indexCount = indexCount;

    LOG_DEBUG("Vertex positions: %u", vertexPositionCount);
    LOG_DEBUG("Vertex normals: %u", vertexNormalCount);
    LOG_DEBUG("Vertex texture coords: %u", vertexTexCoordsCount);
    LOG_DEBUG("Vertex Count: %u, Index Count: %u, Triangle Count: %u",
        output->vertexCount, output->indexCount, output->indexCount / 3);
    return true;
}

