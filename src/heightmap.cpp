struct HeightMap
{
  float *values;
  u32 width;
  u32 height;
};

inline HeightMap AllocateHeightMap(MemoryArena *arena, u32 width, u32 height)
{
    HeightMap result = {};
    result.width = width;
    result.height = height;
    result.values = AllocateArray(arena, float, width * height);

    return result;
}

inline float SampleHeightMap(HeightMap heightMap, i32 x, i32 y)
{
    return heightMap.values[y * heightMap.width + x];
}

inline float HeightMapSampleNearest(HeightMap heightMap, float u, float v)
{
    Assert(u >= 0.0f && u <= 1.0f);
    Assert(v >= 0.0f && v <= 1.0f);
    float sampleU = u * (heightMap.width - 1);
    float sampleV = v * (heightMap.height - 1);
    i32 x = (i32)Ceil(sampleU);
    i32 y = (i32)Ceil(sampleV);

    return SampleHeightMap(heightMap, x, y);
}

inline float HeightMapSampleBilinear(HeightMap heightMap, float u, float v)
{
    Assert(u >= 0.0f && u <= 1.0f);
    Assert(v >= 0.0f && v <= 1.0f);
    float sampleU = u * heightMap.width - 0.5f;
    float sampleV = v * heightMap.height - 0.5f;
    i32 x = (i32)Floor(sampleU);
    i32 y = (i32)Floor(sampleV);
    float s = sampleU - x;
    float t = sampleV - y;
    float negS = 1.0f - s;
    float negT = 1.0f - t;

    // Wrapping mode clamp to edge
    i32 x0 = Max(x, 0);
    i32 x1 = Min(x + 1, heightMap.width - 1);
    i32 y0 = Max(y, 0);
    i32 y1 = Min(y + 1, heightMap.height - 1);

    float samples[4];
    samples[0] = SampleHeightMap(heightMap, x0, y0);
    samples[1] = SampleHeightMap(heightMap, x1, y0);
    samples[2] = SampleHeightMap(heightMap, x0, y1);
    samples[3] = SampleHeightMap(heightMap, x1, y1);

    float p = Lerp(samples[3], samples[2], s);
    float q = Lerp(samples[1], samples[0], s);
    float result = Lerp(p, q, t);
    Assert(result >= 0.0f && result <= 1.0f);

    return result;
}
