#pragma once

#include <cstdint>
#include <cstdio>
#include <cstring>
#include <cfloat>

#ifdef _MSC_VER
#define COMPILER_MSVC
#else
#define COMPILER_GCC
#endif

typedef uint8_t u8;
typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef uint16_t u16;
typedef int16_t i16;
typedef float f32;
typedef uint32_t b32;

#define U16_MAX (0xFFFF)
#define U32_MAX (0xFFFFFFFF)
#define U64_MAX (0xFFFFFFFFFFFFFFFF)
#define F32_MAX (FLT_MAX)

#define KILOBYTES(N) (N*1024)
#define MEGABYTES(N) (N*1024*1024)

#define internal static
#define global static

#define ArrayCount(array) (sizeof(array) / sizeof(array[0]))
#define FillArray(ARRAY, LENGTH, VALUE)                                        \
    for (u32 _idx = 0; _idx < LENGTH; ++_idx)                                  \
    {                                                                          \
        ARRAY[_idx] = VALUE;                                                      \
    }

#define DEBUG_BREAK() (*(int *)0 = 0)

#define Assert(condition)                                                      \
    if (!(condition))                                                          \
    {                                                                          \
        printf("ASSERT_FAILED: %s(%d): %s\n\t%s\n", __FILE__, __LINE__,        \
               __FUNCTION__, #condition);                                      \
        DEBUG_BREAK();                                                          \
    }

#define InvalidCodePath() Assert(!"Invalid code path")

#define Unused(x) (void)x
#define Bit( N ) ( 1 << ( N ) )

inline u32 SafeTruncateU64ToU32(u64 value)
{
    Assert(value <= 0xFFFFFFFF);
    u32 result = (u32)value;
    return result;
}

inline u8 SafeTruncateU32ToU8(u32 value)
{
    Assert(value <= 0xFF);
    u8 result = (u8)value;
    return result;
}

inline u16 SafeTruncateU32ToU16(u32 value)
{
    Assert(value <= 0xFFFF);
    u16 result = (u16)value;
    return result;
}

inline u32 SafeCastI32ToU32(i32 value)
{
    Assert(value >= 0);
    u32 result = (u32)value;
    return result;
}

// TODO: Replace with intrinsic
inline b32 Memcmp(void *a, void *b, u32 length)
{
    u8 *p1 = (u8*)a;
    u8 *p2 = (u8*)b;
    for (u32 i = 0; i < length; ++i)
    {
        if (*p1++ != *p2++)
            return false;
    }

    return true;
}


// TODO: Use intrinsic
inline void ClearToZero(void *memory, size_t length) { memset(memory, 0, length); }

#define ZeroStruct( STRUCT ) ClearToZero( &STRUCT, sizeof( STRUCT ) )
#define ZeroPointerToStruct(POINTER) ClearToZero(POINTER, sizeof(*POINTER))

struct MemoryArena
{
    size_t size, used;
    u8 *base;
};

inline void MemoryArenaInitialize(MemoryArena *arena, size_t size, u8 *base)
{
    arena->size = size;
    arena->base = base;
    arena->used = 0;
}

#define AllocateStruct(ARENA, TYPE)                                            \
    (TYPE *)MemoryArenaAllocate(ARENA, sizeof(TYPE))

#define AllocateArray(ARENA, TYPE, SIZE)                                       \
    (TYPE *)MemoryArenaAllocate(ARENA, sizeof(TYPE) * (SIZE))

inline void *MemoryArenaAllocate(MemoryArena *arena, size_t size)
{
    Assert(size != 0);
    Assert(arena->used + size < arena->size);
    void *result = arena->base + arena->used;
    arena->used += size;
    ClearToZero(result, size);
    return result;
}

// NOTE: This also frees all memory that was allocated after the given pointer.
inline void MemoryArenaFree(MemoryArena *arena, void *memory)
{
    Assert(memory >= arena->base);
    Assert(memory < arena->base + arena->used);
    size_t size = (arena->base + arena->used) - (u8 *)memory;
    arena->used -= size;
}

inline void AllocateAndInitialzeArena(
    MemoryArena *child, MemoryArena *parent, size_t size)
{
    MemoryArenaInitialize(child, size, (u8 *)MemoryArenaAllocate(parent, size));
}
