#include "vulkan_renderer.h"

#define MAX_FRAMES_IN_FLIGHT 2

enum
{
    Vulkan_GraphicsQueueFamily = Bit(0),
    Vulkan_PresentationQueueFamily = Bit(1),
};

struct VulkanQueueFamilyIndices
{
    u32 graphicsFamily;
    u32 presentationFamily;

    u32 isPresent;
};

struct UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
};

struct DirectionalLight
{
    vec4 direction;
    vec4 color;
};

struct PointLight
{
    vec4 position;
    vec4 color;
    vec4 attenuation;
};

// FIXME: Must match constants defined in standard_shader.uber
#define MAX_DIRECTIONAL_LIGHTS 4
#define MAX_POINT_LIGHTS 32

struct LightUbo
{
    DirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
    PointLight pointLights[MAX_POINT_LIGHTS];
    vec4 ambientLight;
    u32 directionalLightCount;
    u32 pointLightCount;
};

global VkInstance g_VkInstance;
global VkDebugUtilsMessengerEXT g_VkCallback;
global VkPhysicalDevice g_VkPhysicalDevice = VK_NULL_HANDLE;
global VkDevice g_VkDevice;
global VkQueue g_VkGraphicsQueue;
global VkQueue g_VkPresentQueue;
global VkSurfaceKHR g_VkSurface;
global VkSwapchainKHR g_VkSwapChain;
global VkImage g_VkSwapChainImages[8];
global VkImageView g_VkSwapChainImageViews[8]; // MUST match length of g_VkSwapChainImages
global VkFramebuffer g_VkSwapChainFrameBuffers[8];
global u32 g_VkSwapChainImagesCount;
global VkFormat g_VkSwapChainImageFormat;
global VkFormat g_VkFrameBufferDepthFormat = VK_FORMAT_D24_UNORM_S8_UINT;
global VkExtent2D g_VkSwapChainExtent;
global VkRenderPass g_VkRenderPass;
global VkCommandPool g_VkCommandPool;
global VulkanQueueFamilyIndices g_VkQueueFamilyIndices;
global VkSemaphore g_VkImageAvailableSemaphores[MAX_FRAMES_IN_FLIGHT];
global VkSemaphore g_VkRenderFinishedSemaphores[MAX_FRAMES_IN_FLIGHT];
global VkFence g_VkInFlightFences[MAX_FRAMES_IN_FLIGHT];
global u32 g_VkCurrentFrame = 0;
global b32 g_VkIsSwapChainInitialized = false;
global b32 g_VkIsSwapChainValid = false;
global u32 g_VkFrameBufferWidth = 0;
global u32 g_VkFrameBufferHeight = 0;

// TODO: Remove this as we are not using it anymore
global VkBuffer g_VkUniformBuffers[8]; // MUST match length of g_VkSwapChainImages
global VkDeviceMemory g_VkUniformBuffersMemory[8];
global VkDescriptorPool g_VkDescriptorPool;

global VkSampler g_VkTextureSampler;

global VkImage g_VkDepthImage;
global VkDeviceMemory g_VkDepthImageMemory;
global VkImageView g_VkDepthImageView;

global u32 g_VkMinUboAlignment;
global u32 g_VkMaxInstanceCount = 256;
global u32 g_VkLightUboBaseOffset;
global VkBuffer g_VkDynamicUniformBuffers[8]; // MUST match length of g_VkSwapChainImages
global u32 g_VkDynamicUboAlignment;
global VkDeviceMemory g_VkDynamicUniformBuffersMemory[8];
global VkDescriptorSetLayout g_VkDynamicDescriptorSetLayout;
global VkDescriptorSet g_VkDynamicDescriptorSets[8];
global VkDescriptorSet g_VkLightUboDescriptorSets[8];
global VkDescriptorSetLayout g_VkStaticDescriptorSetLayout;
global VkDescriptorSetLayout g_VkLightUboDescriptorSetLayout;

// Dynamic command buffers
global VkCommandPool g_VkDynamicCommandBufferPools[8];
// In the future this could be a multi-dim array to support multiple command
// buffers per swap chain image
global VkCommandBuffer g_VkDynamicCommandBuffers[8]; // Currently single command buffer per swap chain image
global u32 g_VkDynamicCommandBufferCounts[8];

struct VulkanShader
{
    VkPipelineLayout layout;
    VkPipeline pipeline;
};

struct VulkanVertexBuffer
{
    VkBuffer buffer;
    VkDeviceMemory memory;
    u32 capacity;
};

// @speed Reduce the number of VkBuffers!
// TODO:: index type
struct VulkanMesh
{
    VkBuffer buffer;
    VkDeviceMemory bufferMemory;
    u32 indexBufferOffset;
    u32 indexCount;
};

struct VulkanTexture
{
    VkImage image;
    VkDeviceMemory imageMemory;
    VkImageView imageView;
};

// TODO: Reference shader and descriptorSet layout
struct VulkanMaterial
{
    VkDescriptorSet descriptorSet;
};


global VulkanShader g_Shaders[VULKAN_MAX_SHADERS];
global VulkanMesh g_VkMeshes[VULKAN_MAX_MESHES];
global VulkanTexture g_VkTextures[VULKAN_MAX_TEXTURES];
global VulkanMaterial g_VkMaterials[VULKAN_MAX_MATERIALS];

// TODO: Merge into same dynamic vertex buffer
global VulkanVertexBuffer g_VkLineVertexBuffers[8]; // MUST match swap chain length
global VulkanVertexBuffer g_VkTextVertexBuffers[8];


internal u32 VulkanGetRequiredExtensions(const char **requiredExtensions, u32 length)
{
    u32 count = 0;
    u32 glfwExtensionCount = 0;
    auto glfwExtensions =
            glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    Assert(glfwExtensionCount < length);

    for (u32 i = 0; i < glfwExtensionCount; ++i)
    {
        requiredExtensions[i] = glfwExtensions[i];
    }
    count += glfwExtensionCount;

    // TODO: Check if we want to enable this
    Assert(count < length);
    requiredExtensions[count++] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;

    return count;
}

internal b32 VulkanCheckIfExtensionsArePresent(const char **requestExtensions,
                                               u32 requestExtensionsCount)
{
    u32 availableExtensionsCount = 0;
    vkEnumerateInstanceExtensionProperties(NULL, &availableExtensionsCount,
                                           NULL);

    VkExtensionProperties availableExtensions[1024];
    Assert(availableExtensionsCount < ArrayCount(availableExtensions));

    vkEnumerateInstanceExtensionProperties(NULL, &availableExtensionsCount,
                                           availableExtensions);

    for (u32 i = 0; i < requestExtensionsCount; ++i)
    {
        bool found = false;

        for (u32 j = 0; j < availableExtensionsCount; ++j)
        {
            if (!strcmp(requestExtensions[i],
                        availableExtensions[j].extensionName) != 0)
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            return false;
        }
    }

    return true;
}

internal b32 VulkanCheckIfValidationLayersArePresent(
        const char **requestedLayers, u32 requestLayersCount)
{
    u32 availableLayersCount;
    vkEnumerateInstanceLayerProperties(&availableLayersCount, nullptr);

    VkLayerProperties availableLayers[1024];
    vkEnumerateInstanceLayerProperties(&availableLayersCount, availableLayers);

    for (u32 i = 0; i < requestLayersCount; ++i)
    {
        bool found = false;

        for (u32 j = 0; j < availableLayersCount; ++j)
        {
            if (!strcmp(requestedLayers[i],
                        availableLayers[j].layerName) != 0)
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            return false;
        }
    }

    return true;
}

internal VKAPI_ATTR VkBool32 VKAPI_CALL
VulkanDebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                    VkDebugUtilsMessageTypeFlagsEXT messageType,
                    const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
                    void *pUserData)
{
    OutputDebugString(pCallbackData->pMessage);
    OutputDebugString("\n");

    return VK_FALSE;
}

VkResult VulkanCreateDebugUtilsMessengerEXT(
        VkInstance instance,
        const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
        const VkAllocationCallbacks *pAllocator,
        VkDebugUtilsMessengerEXT *pCallback)
{
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
            instance, "vkCreateDebugUtilsMessengerEXT");
    if (func != NULL)
    {
        return func(instance, pCreateInfo, pAllocator, pCallback);
    }
    else
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void VulkanDestroyDebugUtilsMessengerEXT(
        VkInstance instance, VkDebugUtilsMessengerEXT callback,
        const VkAllocationCallbacks *pAllocator)
{
    auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
            instance, "vkDestroyDebugUtilsMessengerEXT");
    if (func != NULL)
    {
        func(instance, callback, pAllocator);
    }
}

internal VulkanQueueFamilyIndices VulkanPickQueueFamilies(VkPhysicalDevice device)
{
    VulkanQueueFamilyIndices result = {};

    u32 queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, NULL);

    VkQueueFamilyProperties queueFamilies[1024];
    Assert(queueFamilyCount < ArrayCount(queueFamilies));
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies);

    for (u32 i = 0; i < queueFamilyCount; ++i)
    {
        if (queueFamilies[i].queueCount > 0)
        {
            if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
            {
                result.graphicsFamily = i;
                result.isPresent |= Vulkan_GraphicsQueueFamily;
            }

            VkBool32 presentSupport = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(device, i, g_VkSurface, &presentSupport);
            if (presentSupport)
            {
                result.presentationFamily = i;
                result.isPresent |= Vulkan_PresentationQueueFamily;
            }
        }


        if (result.isPresent & Vulkan_GraphicsQueueFamily &&
            result.isPresent & Vulkan_GraphicsQueueFamily)
        {
            return result;
        }
    }

    Assert(!"Failed to find a suitable queue family");
    return result;
}

internal b32 VulkanCheckIfDeviceExtensionsAreSupported(
        VkPhysicalDevice device, const char **requiredExtensions,
        u32 requiredExtensionsCount)
{
    u32 availableExtensionsCount;
    vkEnumerateDeviceExtensionProperties(device, NULL,
                                         &availableExtensionsCount, NULL);

    VkExtensionProperties availableExtensions[1024];
    Assert(availableExtensionsCount < ArrayCount(availableExtensions));
    vkEnumerateDeviceExtensionProperties(
            device, NULL, &availableExtensionsCount, availableExtensions);

    for (u32 i = 0; i < requiredExtensionsCount; ++i)
    {
        bool found = false;
        for (u32 j = 0; j < availableExtensionsCount; ++j)
        {
            if (!strcmp(requiredExtensions[i],
                        availableExtensions[j].extensionName))
            {
                found = true;
                break;
            }
        }

        if (!found)
        {
            return false;
        }
    }

    return true;
}

inline b32 VulkanIsDeviceSuitable(VkPhysicalDevice device)
{
    // TODO: Can check 
    //    vkGetPhysicalDeviceProperties(device, &deviceProperties);
    //    vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

    return true;
}

internal void VulkanPickPhysicalDevice()
{
    u32 deviceCount = 0;
    vkEnumeratePhysicalDevices(g_VkInstance, &deviceCount, NULL);
    if (deviceCount == 0)
    {
        Assert(!"Failed to find any GPUs in the system with Vulkan support!");
    }

    VkPhysicalDevice devices[1024];
    Assert(deviceCount < ArrayCount(devices));
    vkEnumeratePhysicalDevices(g_VkInstance, &deviceCount, devices);

    for (u32 i = 0; i < deviceCount; ++i)
    {
        if (VulkanIsDeviceSuitable(devices[i]))
        {
            g_VkPhysicalDevice = devices[i];
            break;
        }
    }

    Assert((g_VkPhysicalDevice != VK_NULL_HANDLE) &&
           "Failed to find a suitable GPU in the system");
}

internal void VulkanSetupDebugCallback()
{
    VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity =
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                             VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                             VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    createInfo.pfnUserCallback = VulkanDebugCallback;
    createInfo.pUserData = nullptr; // Optional

    if (VulkanCreateDebugUtilsMessengerEXT(g_VkInstance, &createInfo, NULL,
                                           &g_VkCallback) != VK_SUCCESS)
    {
        Assert(!"Failed to set up debug callback!");
    }
}

internal void VulkanCreateLogicalDevice(const char **deviceExtensions,
                                        u32 deviceExtensionsCount,
                                        const char **validationLayers,
                                        u32 validationLayersCount)
{
    VulkanQueueFamilyIndices familyIndices = VulkanPickQueueFamilies(g_VkPhysicalDevice);

    Assert(familyIndices.graphicsFamily == familyIndices.presentationFamily);

    g_VkQueueFamilyIndices = familyIndices;

    VkDeviceQueueCreateInfo queueCreateInfo = {};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = familyIndices.graphicsFamily;
    queueCreateInfo.queueCount = 1;

    float queuePriority = 1.0f;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy = VK_TRUE;
    deviceFeatures.fillModeNonSolid = VK_TRUE;

    VkDeviceCreateInfo deviceCreateInfo = {};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
    deviceCreateInfo.queueCreateInfoCount = 1;
    deviceCreateInfo.pEnabledFeatures = &deviceFeatures;
    deviceCreateInfo.enabledExtensionCount = deviceExtensionsCount;
    deviceCreateInfo.ppEnabledExtensionNames = deviceExtensions;
    deviceCreateInfo.enabledLayerCount = validationLayersCount;
    deviceCreateInfo.ppEnabledLayerNames = validationLayers;

    if (vkCreateDevice(g_VkPhysicalDevice, &deviceCreateInfo, NULL,
                       &g_VkDevice) != VK_SUCCESS)
    {
        Assert(!"Failed to create logical device!");
    }

    vkGetDeviceQueue(g_VkDevice, familyIndices.graphicsFamily, 0, &g_VkGraphicsQueue);
    vkGetDeviceQueue(g_VkDevice, familyIndices.presentationFamily, 0, &g_VkPresentQueue);
}

internal void VulkanCreateSwapChain(u32 frameBufferWidth, u32 frameBufferHeight)
{
    Assert(frameBufferWidth > 0);
    Assert(frameBufferHeight > 0);

    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(g_VkPhysicalDevice, g_VkSurface, &surfaceCapabilities);

    Assert(surfaceCapabilities.currentExtent.width != U32_MAX);
    VkExtent2D swapExtent = {};
    swapExtent.width = frameBufferWidth;
    swapExtent.height = frameBufferHeight;

    u32 swapChainLength = surfaceCapabilities.minImageCount + 1;
    if ((surfaceCapabilities.maxImageCount > 0))
    {
        swapChainLength = Min(swapChainLength, surfaceCapabilities.maxImageCount);
    }

    u32 surfaceFormatsCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(g_VkPhysicalDevice, g_VkSurface, &surfaceFormatsCount, NULL);
    Assert(surfaceFormatsCount > 0 && "Physical device does not support any surface formats");

    VkSurfaceFormatKHR surfaceFormats[64];
    Assert(surfaceFormatsCount < ArrayCount(surfaceFormats));
    vkGetPhysicalDeviceSurfaceFormatsKHR(g_VkPhysicalDevice, g_VkSurface, &surfaceFormatsCount, surfaceFormats);

    bool desiredSurfaceFormatFound = false;
    VkSurfaceFormatKHR desiredSurfaceFormat = {
            VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};

    if (surfaceFormatsCount == 1 && surfaceFormats[0].format == VK_FORMAT_UNDEFINED)
    {
        desiredSurfaceFormatFound = true;
    }

    for (u32 i = 0; i < surfaceFormatsCount; ++i)
    {
        if ((surfaceFormats[i].format == desiredSurfaceFormat.format) &&
            (surfaceFormats[i].colorSpace == desiredSurfaceFormat.colorSpace))
        {
            desiredSurfaceFormatFound = true;
            break;
        }
    }
    Assert(desiredSurfaceFormatFound);

    u32 presentModesCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(g_VkPhysicalDevice, g_VkSurface, &presentModesCount, NULL);
    Assert(presentModesCount > 0 && "Physical device does not support any present modes");

    VkPresentModeKHR presentModes[64];
    Assert(presentModesCount < ArrayCount(presentModes));
    vkGetPhysicalDeviceSurfacePresentModesKHR(g_VkPhysicalDevice, g_VkSurface, &presentModesCount, presentModes);

    b32 desiredPresentModeFound = false;
    VkPresentModeKHR desiredPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    for (u32 i = 0; i < presentModesCount; ++i)
    {
        if (presentModes[i] == desiredPresentMode)
        {
            desiredPresentModeFound = true;
            break;
        }
    }
    Assert(desiredPresentModeFound);

    VkSwapchainCreateInfoKHR swapChainCreateInfo = {};
    swapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapChainCreateInfo.surface = g_VkSurface;
    swapChainCreateInfo.minImageCount = swapChainLength;
    swapChainCreateInfo.imageFormat = desiredSurfaceFormat.format;
    swapChainCreateInfo.imageColorSpace = desiredSurfaceFormat.colorSpace;
    swapChainCreateInfo.imageExtent = swapExtent;
    swapChainCreateInfo.imageArrayLayers = 1;
    swapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; // ASSERT graphicsFamily == presentFamily
    swapChainCreateInfo.preTransform = surfaceCapabilities.currentTransform;
    swapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapChainCreateInfo.presentMode = desiredPresentMode;
    swapChainCreateInfo.clipped = VK_TRUE;
    swapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE; // Used if window is resized
    if (vkCreateSwapchainKHR(g_VkDevice, &swapChainCreateInfo, NULL,
                             &g_VkSwapChain) != VK_SUCCESS)
    {
        Assert(!"Failed to create swap chain!");
    }

    g_VkSwapChainImageFormat = desiredSurfaceFormat.format;
    g_VkSwapChainExtent = swapExtent;

    u32 imageCount;
    vkGetSwapchainImagesKHR(g_VkDevice, g_VkSwapChain, &imageCount, NULL);
    Assert(imageCount < ArrayCount(g_VkSwapChainImages));

    g_VkSwapChainImagesCount = imageCount;
    vkGetSwapchainImagesKHR(g_VkDevice, g_VkSwapChain, &imageCount, g_VkSwapChainImages);
}

internal VkImageView VulkanCreateImageView(
    VkImage image, VkFormat format, VkImageAspectFlags aspectFlags)
{
    VkImageViewCreateInfo viewInfo = {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    VkImageView imageView;
    if (vkCreateImageView(g_VkDevice, &viewInfo, NULL, &imageView) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to create image view!");
    }

    return imageView;
}

internal void VulkanCreateImageViews()
{
    Assert(g_VkSwapChainImagesCount < ArrayCount(g_VkSwapChainImageViews));

    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        g_VkSwapChainImageViews[i] =
            VulkanCreateImageView(g_VkSwapChainImages[i],
                g_VkSwapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT);
    }
}

internal VkShaderModule VulkanCreateShaderModule(const void *byteCode, u32 length)
{
    // Must be 4-byte aligned as we are going to cast it to a uint32_t
    Assert(((size_t)byteCode & 0x3) == 0);

    VkShaderModuleCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = length;
    createInfo.pCode = (const u32*)byteCode;

    VkShaderModule shaderModule;
    if (vkCreateShaderModule(g_VkDevice, &createInfo, NULL, &shaderModule) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to create shader module!");
    }

    return shaderModule;
}

inline VkPipelineShaderStageCreateInfo VulkanBuildShaderStageCreateInfo(
    VkShaderStageFlagBits stage, VkShaderModule module)
{
    VkPipelineShaderStageCreateInfo result = {};
    result.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    result.stage = stage;
    result.module = module;
    result.pName = "main";

    return result;
}

internal u32 VulkanBuildVertexInputDescription(
    VkVertexInputBindingDescription *bindingDescription,
    VkVertexInputAttributeDescription *attributeDescriptions,
    u32 attributeDescriptionsCount)
{
    bindingDescription->binding = 0;
    bindingDescription->stride = sizeof(Vertex);
    bindingDescription->inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    Assert(attributeDescriptionsCount >= 3)
    attributeDescriptions[0].binding = 0;
    attributeDescriptions[0].location = 0;
    attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[0].offset = offsetof(Vertex, pos);

    attributeDescriptions[1].binding = 0;
    attributeDescriptions[1].location = 1;
    attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[1].offset = offsetof(Vertex, color);

    attributeDescriptions[2].binding = 0;
    attributeDescriptions[2].location = 2;
    attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
    attributeDescriptions[2].offset = offsetof(Vertex, texCoord);

    return 3;
}

internal u32 VulkanBuildTextVertexInputDescription(
    VkVertexInputBindingDescription *bindingDescription,
    VkVertexInputAttributeDescription *attributeDescriptions,
    u32 attributeDescriptionsCount)
{
    bindingDescription->binding = 0;
    bindingDescription->stride = sizeof(TextVertex);
    bindingDescription->inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    Assert(attributeDescriptionsCount >= 2);
    attributeDescriptions[0].binding = 0;
    attributeDescriptions[0].location = 0;
    attributeDescriptions[0].format = VK_FORMAT_R32G32_SFLOAT;
    attributeDescriptions[0].offset = offsetof(TextVertex, position);

    attributeDescriptions[1].binding = 0;
    attributeDescriptions[1].location = 1;
    attributeDescriptions[1].format = VK_FORMAT_R32G32_SFLOAT;
    attributeDescriptions[1].offset = offsetof(TextVertex, texCoord);

    return 2;
}

internal u32 VulkanBuildPositionColorVertexInputDescription(
    VkVertexInputBindingDescription *bindingDescription,
    VkVertexInputAttributeDescription *attributeDescriptions,
    u32 attributeDescriptionsCount)
{
    bindingDescription->binding = 0;
    bindingDescription->stride = sizeof(VertexPC);
    bindingDescription->inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    Assert(attributeDescriptionsCount >= 2);
    attributeDescriptions[0].binding = 0;
    attributeDescriptions[0].location = 0;
    attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[0].offset = offsetof(VertexPC, position);

    attributeDescriptions[1].binding = 0;
    attributeDescriptions[1].location = 1;
    attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[1].offset = offsetof(VertexPC, color);

    return 2;
}


inline VkViewport VulkanCreateDefaultViewport()
{
    VkViewport viewport = {};
    viewport.x = 0.0f;
    viewport.y = 0.0f;
    viewport.width = (float)g_VkSwapChainExtent.width;
    viewport.height = (float)g_VkSwapChainExtent.height;
    viewport.minDepth = 0.0f;
    viewport.maxDepth = 1.0f;

    return viewport;
}

inline VkRect2D VulkanCreateDefaultScissor()
{
    VkRect2D scissor = {};
    scissor.offset = {0, 0};
    scissor.extent = g_VkSwapChainExtent;

    return scissor;
}

inline VkPipelineViewportStateCreateInfo VulkanBuildPipelineViewportState(
        VkViewport *viewport, VkRect2D *scissor)
{
    VkPipelineViewportStateCreateInfo viewportState = {};
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = scissor;

    return viewportState;
}

inline VkPipelineDepthStencilStateCreateInfo VulkanBuildPipelineDepthStencilState()
{
    VkPipelineDepthStencilStateCreateInfo depthStencil = {};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.depthTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds = 0.0f; // Optional
    depthStencil.maxDepthBounds = 1.0f; // Optional
    depthStencil.stencilTestEnable = VK_FALSE;
    depthStencil.front = {}; // Optional
    depthStencil.back = {}; // Optional

    return depthStencil;
}

inline VkPipelineRasterizationStateCreateInfo VulkanBuildPipelineRasterizationState()
{
    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    return rasterizer;
}

inline VkPipelineMultisampleStateCreateInfo VulkanBuildPipelineMultisampleState()
{
    VkPipelineMultisampleStateCreateInfo multisampling = {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = NULL; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    return multisampling;
}

inline VkPipelineColorBlendAttachmentState VulkanBuildPipelineColorBlendAttachment()
{
    VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    return colorBlendAttachment;
}

inline VkPipelineColorBlendStateCreateInfo VulkanBuildPipelineBlendState(
    VkPipelineColorBlendAttachmentState *colorBlendAttachment)
{
    VkPipelineColorBlendStateCreateInfo colorBlending = {};
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f; // Optional
    colorBlending.blendConstants[1] = 0.0f; // Optional
    colorBlending.blendConstants[2] = 0.0f; // Optional
    colorBlending.blendConstants[3] = 0.0f; // Optional

    return colorBlending;
}

struct VulkanPipelineCreateInfo
{
    VkPipelineVertexInputStateCreateInfo vertexInputInfo;
    VkPipelineInputAssemblyStateCreateInfo inputAssembly;
    VkViewport viewport;
    VkRect2D scissor;

    VkPipelineViewportStateCreateInfo viewportState;
    VkPipelineDepthStencilStateCreateInfo depthStencil;
    VkPipelineRasterizationStateCreateInfo rasterizer;
    VkPipelineMultisampleStateCreateInfo multisampling;
    VkPipelineColorBlendAttachmentState colorBlendAttachment;
    VkPipelineColorBlendStateCreateInfo colorBlending;
    VkPipelineLayoutCreateInfo pipelineLayoutInfo;
    VkGraphicsPipelineCreateInfo pipelineInfo;
};

internal void VulkanBuildDefaultShader(VulkanPipelineCreateInfo *output,
    VkVertexInputBindingDescription *bindingDescription, 
    VkVertexInputAttributeDescription *attributeDescriptions, u32 attributeDescriptionsCount,
    VkDescriptorSetLayout *descriptorSetLayouts, u32 descriptorSetLayoutCount,
    VkPipelineShaderStageCreateInfo *shaderStages, u32 shaderStageCount)
{
    output->vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    output->vertexInputInfo.vertexBindingDescriptionCount = 1;
    output->vertexInputInfo.pVertexBindingDescriptions = bindingDescription;
    output->vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptionsCount;
    output->vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions;

    output->inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    output->inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    output->inputAssembly.primitiveRestartEnable = VK_FALSE;

    output->viewport = VulkanCreateDefaultViewport();
    output->scissor = VulkanCreateDefaultScissor();

    output->viewportState =
        VulkanBuildPipelineViewportState(&output->viewport, &output->scissor);

    output->depthStencil = VulkanBuildPipelineDepthStencilState();

    output->rasterizer = VulkanBuildPipelineRasterizationState();

    output->multisampling = VulkanBuildPipelineMultisampleState();

    output->colorBlendAttachment = VulkanBuildPipelineColorBlendAttachment();

    output->colorBlending =
        VulkanBuildPipelineBlendState(&output->colorBlendAttachment);

    output->pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    output->pipelineLayoutInfo.setLayoutCount = descriptorSetLayoutCount;
    output->pipelineLayoutInfo.pSetLayouts = descriptorSetLayouts;
    output->pipelineLayoutInfo.pushConstantRangeCount = 0; // Optional
    output->pipelineLayoutInfo.pPushConstantRanges = NULL; // Optional

    output->pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    output->pipelineInfo.stageCount = shaderStageCount;
    output->pipelineInfo.pStages = shaderStages;
    output->pipelineInfo.pVertexInputState = &output->vertexInputInfo;
    output->pipelineInfo.pInputAssemblyState = &output->inputAssembly;
    output->pipelineInfo.pViewportState = &output->viewportState;
    output->pipelineInfo.pRasterizationState = &output->rasterizer;
    output->pipelineInfo.pMultisampleState = &output->multisampling;
    output->pipelineInfo.pDepthStencilState = &output->depthStencil;
    output->pipelineInfo.pColorBlendState = &output->colorBlending;
    output->pipelineInfo.pDynamicState = NULL; // Optional
    output->pipelineInfo.layout = VK_NULL_HANDLE;
    output->pipelineInfo.renderPass = g_VkRenderPass;
    output->pipelineInfo.subpass = 0;
    output->pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
    output->pipelineInfo.basePipelineIndex = -1; // Optional
}

internal void VulkanCreateShader(
    VulkanShader *shader, VulkanPipelineCreateInfo *createInfo)
{
    if (vkCreatePipelineLayout(g_VkDevice, &createInfo->pipelineLayoutInfo,
            NULL, &shader->layout) != VK_SUCCESS)
    {
        Assert(!"Failed to create pipeline layout!");
    }
    createInfo->pipelineInfo.layout = shader->layout;

    if (vkCreateGraphicsPipelines(g_VkDevice, VK_NULL_HANDLE, 1,
            &createInfo->pipelineInfo, NULL, &shader->pipeline) != VK_SUCCESS)
    {
        Assert(!"Failed to create graphics pipeline!");
    }
}

enum
{
    VULKAN_VERTEX_LAYOUT_STANDARD,
    VULKAN_VERTEX_LAYOUT_TEXT,
    VULKAN_VERTEX_LAYOUT_POSITION_COLOR,
};

enum
{
    VULKAN_DISABLE_DEPTH_TEST = Bit(0),
    VULKAN_ENABLE_ALPHA_BLENDING = Bit(1),
    VULKAN_FRONT_FACE_CULLING = Bit(2),
    VULKAN_COLOR_PUSH_CONSTANT = Bit(3),
    VULKAN_USE_TEXTURE_DESCRIPTOR_SET = Bit(4), // TODO: Better name
    VULKAN_USE_WIREFRAME = Bit(5),
    VULKAN_USE_LINE_LIST = Bit(6),
    VULKAN_USE_LIGHT_UBO_DESCRIPTOR_SET = Bit(7), // TODO: Better name
};

struct ShaderDescription
{
    const char *vertexPath;
    const char *fragmentPath;
    u32 vertexLayout;
    u32 flags;
    u32 index;
};

internal void VulkanCreateShaderFromDescription(ShaderDescription *desc)
{
    ReadFileResult vertShaderFile = Win32_ReadEntireFile(desc->vertexPath);
    ReadFileResult fragShaderFile = Win32_ReadEntireFile(desc->fragmentPath);

    VkShaderModule vertShaderModule = VulkanCreateShaderModule(
            vertShaderFile.memory, vertShaderFile.size);
    VkShaderModule fragShaderModule = VulkanCreateShaderModule(
            fragShaderFile.memory, fragShaderFile.size);

    Win32_FreeMemory(vertShaderFile.memory);
    Win32_FreeMemory(fragShaderFile.memory);

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        VulkanBuildShaderStageCreateInfo(
            VK_SHADER_STAGE_VERTEX_BIT, vertShaderModule),
        VulkanBuildShaderStageCreateInfo(
            VK_SHADER_STAGE_FRAGMENT_BIT, fragShaderModule)};

    VkVertexInputBindingDescription bindingDescription = {};
    VkVertexInputAttributeDescription attributeDescriptions[3];
    u32 attributeCount = 0;
    if (desc->vertexLayout == VULKAN_VERTEX_LAYOUT_STANDARD)
    {
        attributeCount = VulkanBuildVertexInputDescription(&bindingDescription,
            attributeDescriptions, ArrayCount(attributeDescriptions));
    }
    else if (desc->vertexLayout == VULKAN_VERTEX_LAYOUT_TEXT)
    {
        attributeCount = VulkanBuildTextVertexInputDescription(
            &bindingDescription, attributeDescriptions, 2);
    }
    else if (desc->vertexLayout == VULKAN_VERTEX_LAYOUT_POSITION_COLOR)
    {
        attributeCount =
            VulkanBuildPositionColorVertexInputDescription(&bindingDescription,
                attributeDescriptions, ArrayCount(attributeDescriptions));
    }
    else
    {
        InvalidCodePath();
    }

    u32 descriptorSetLayoutCount = 0;
    VkDescriptorSetLayout descriptorSetLayouts[2];
    if (desc->flags & VULKAN_USE_LIGHT_UBO_DESCRIPTOR_SET)
    {
        descriptorSetLayouts[descriptorSetLayoutCount++] = g_VkLightUboDescriptorSetLayout;
    }
    else
    {
        descriptorSetLayouts[descriptorSetLayoutCount++] = g_VkDynamicDescriptorSetLayout;
    }

    if (desc->flags & VULKAN_USE_TEXTURE_DESCRIPTOR_SET)
    {
        descriptorSetLayouts[descriptorSetLayoutCount++] = g_VkStaticDescriptorSetLayout;
    }

    VulkanPipelineCreateInfo createInfo = {};
    VulkanBuildDefaultShader(&createInfo, &bindingDescription,
        attributeDescriptions, attributeCount,
        descriptorSetLayouts, descriptorSetLayoutCount, shaderStages,
        ArrayCount(shaderStages));

    // TODO: Support more configuration for push constants
    if (desc->flags & VULKAN_COLOR_PUSH_CONSTANT)
    {
        VkPushConstantRange pushConstantRange = {};
        pushConstantRange.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        pushConstantRange.size = sizeof(vec4);
        pushConstantRange.offset = 0;

        createInfo.pipelineLayoutInfo.pushConstantRangeCount = 1;
        createInfo.pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;
    }

    if (desc->flags & VULKAN_DISABLE_DEPTH_TEST)
    {
        createInfo.depthStencil.depthTestEnable = VK_TRUE;
        createInfo.depthStencil.depthWriteEnable = VK_FALSE;
        createInfo.depthStencil.depthCompareOp = VK_COMPARE_OP_ALWAYS;
        createInfo.depthStencil.depthBoundsTestEnable = VK_FALSE;
    }

    if (desc->flags & VULKAN_ENABLE_ALPHA_BLENDING)
    {
        createInfo.colorBlendAttachment.blendEnable = VK_TRUE;
        createInfo.colorBlendAttachment.srcColorBlendFactor =
            VK_BLEND_FACTOR_SRC_ALPHA;
        createInfo.colorBlendAttachment.dstColorBlendFactor =
            VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        createInfo.colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
        createInfo.colorBlendAttachment.srcAlphaBlendFactor =
            VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        createInfo.colorBlendAttachment.dstAlphaBlendFactor =
            VK_BLEND_FACTOR_ZERO;
        createInfo.colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
    }

    if (desc->flags & VULKAN_FRONT_FACE_CULLING)
    {
        createInfo.rasterizer.cullMode = VK_CULL_MODE_FRONT_BIT;
    }

    if (desc->flags & VULKAN_USE_WIREFRAME)
    {
        createInfo.rasterizer.polygonMode = VK_POLYGON_MODE_LINE;
    }

    if (desc->flags & VULKAN_USE_LINE_LIST)
    {
        createInfo.inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    }

    VulkanShader shader = {};
    VulkanCreateShader(&shader, &createInfo);

    vkDestroyShaderModule(g_VkDevice, fragShaderModule, NULL);
    vkDestroyShaderModule(g_VkDevice, vertShaderModule, NULL);

    Assert(desc->index < ArrayCount(g_Shaders));
    g_Shaders[desc->index] = shader;
}

internal void VulkanCreateShaders()
{
    {   // TextureOnly
        ShaderDescription desc = {};
        desc.vertexPath = "vert.spv";
        desc.fragmentPath = "frag.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_USE_TEXTURE_DESCRIPTOR_SET | VULKAN_ENABLE_ALPHA_BLENDING;
        desc.index = VULKAN_SHADER_TEXTURE_ONLY;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // ColorOnly
        ShaderDescription desc = {};
        desc.vertexPath = "vert.spv";
        desc.fragmentPath = "frag2.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_COLOR_PUSH_CONSTANT;
        desc.index = VULKAN_SHADER_COLOR_ONLY;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // ColorWireframe
        ShaderDescription desc = {};
        desc.vertexPath = "vert.spv";
        desc.fragmentPath = "frag2.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_COLOR_PUSH_CONSTANT | VULKAN_USE_WIREFRAME;
        desc.index = VULKAN_SHADER_COLOR_WIREFRAME;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // ColorOnlyNoDepth
        ShaderDescription desc = {};
        desc.vertexPath = "vert.spv";
        desc.fragmentPath = "frag2.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_DISABLE_DEPTH_TEST | VULKAN_COLOR_PUSH_CONSTANT;
        desc.index = VULKAN_SHADER_COLOR_ONLY_NO_DEPTH;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // VertexColor
        ShaderDescription desc = {};
        desc.vertexPath = "vertex_color_vert.spv";
        desc.fragmentPath = "vertex_color_frag.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_POSITION_COLOR;
        desc.flags = VULKAN_DISABLE_DEPTH_TEST | VULKAN_USE_LINE_LIST;
        desc.index = VULKAN_SHADER_VERTEX_COLOR;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // WorldTextureCoords
        ShaderDescription desc = {};
        desc.vertexPath = "world_tex_coord_vert.spv";
        desc.fragmentPath = "texture_diffuse_lighting_frag.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_USE_TEXTURE_DESCRIPTOR_SET | VULKAN_USE_LIGHT_UBO_DESCRIPTOR_SET;
        desc.index = VULKAN_SHADER_WORLD_TEX_COORD;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // TextureDiffuseLighting
        ShaderDescription desc = {};
        desc.vertexPath = "diffuse_lighting_vert.spv";
        desc.fragmentPath = "texture_diffuse_lighting_frag.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_USE_TEXTURE_DESCRIPTOR_SET | VULKAN_USE_LIGHT_UBO_DESCRIPTOR_SET;
        desc.index = VULKAN_SHADER_TEXTURE_DIFFUSE_LIGHTING;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // CubeMap
        ShaderDescription desc = {};
        desc.vertexPath = "cube_vert.spv";
        desc.fragmentPath = "cube_frag.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_DISABLE_DEPTH_TEST | VULKAN_FRONT_FACE_CULLING |
                     VULKAN_USE_TEXTURE_DESCRIPTOR_SET;
        desc.index = VULKAN_SHADER_CUBE_MAP;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // Text
        ShaderDescription desc = {};
        desc.vertexPath = "text_vert.spv";
        desc.fragmentPath = "text_frag.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_TEXT;
        desc.flags = VULKAN_DISABLE_DEPTH_TEST | VULKAN_ENABLE_ALPHA_BLENDING |
                     VULKAN_COLOR_PUSH_CONSTANT | VULKAN_USE_TEXTURE_DESCRIPTOR_SET;
        desc.index = VULKAN_SHADER_TEXT;

        VulkanCreateShaderFromDescription(&desc);
    }

    {   // DiffuseLighting
        ShaderDescription desc = {};
        desc.vertexPath = "diffuse_lighting_vert.spv";
        desc.fragmentPath = "diffuse_lighting_frag.spv";
        desc.vertexLayout = VULKAN_VERTEX_LAYOUT_STANDARD;
        desc.flags = VULKAN_COLOR_PUSH_CONSTANT | VULKAN_USE_LIGHT_UBO_DESCRIPTOR_SET;
        desc.index = VULKAN_SHADER_DIFFUSE_LIGHTING;

        VulkanCreateShaderFromDescription(&desc);
    }
}

internal void VulkanCreateRenderPass()
{
    VkAttachmentDescription colorAttachment = {};
    colorAttachment.format = g_VkSwapChainImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentDescription depthAttachment = {};
    depthAttachment.format = g_VkFrameBufferDepthFormat;
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference colorAttachmentRef = {};
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthAttachmentRef = {};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    VkSubpassDependency dependency = {};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    VkAttachmentDescription attachments[] = {colorAttachment, depthAttachment};
    VkRenderPassCreateInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = ArrayCount(attachments);
    renderPassInfo.pAttachments = attachments;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;

    if (vkCreateRenderPass(g_VkDevice, &renderPassInfo, NULL,
                           &g_VkRenderPass) != VK_SUCCESS)
    {
        Assert(!"Failed to create render pass!");
    }
}

internal void VulkanCreateFrameBuffers()
{
    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        VkImageView attachments[] = {
            g_VkSwapChainImageViews[i], g_VkDepthImageView};

        VkFramebufferCreateInfo framebufferInfo = {};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = g_VkRenderPass;
        framebufferInfo.attachmentCount = ArrayCount(attachments);
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = g_VkSwapChainExtent.width;
        framebufferInfo.height = g_VkSwapChainExtent.height;
        framebufferInfo.layers = 1;

        if (vkCreateFramebuffer(g_VkDevice, &framebufferInfo, NULL,
                                &g_VkSwapChainFrameBuffers[i]) != VK_SUCCESS)
        {
            Assert(!"Failed to create framebuffer!");
        }
    }
}

internal void VulkanCreateCommandPool()
{
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = g_VkQueueFamilyIndices.graphicsFamily;
    poolInfo.flags = 0; // Optional

    if (vkCreateCommandPool(g_VkDevice, &poolInfo, NULL, &g_VkCommandPool) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to create command pool!");
    }
}

internal void VulkanCreateDynamicCommandPools()
{
    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = g_VkQueueFamilyIndices.graphicsFamily;
    poolInfo.flags = 0;

    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        if (vkCreateCommandPool(g_VkDevice, &poolInfo, NULL,
                &g_VkDynamicCommandBufferPools[i]) != VK_SUCCESS)
        {
            Assert(!"Failed to create dyanmic command pool!");
        }
    }
}

internal void VulkanCreateSyncObjects()
{
    VkSemaphoreCreateInfo semaphoreInfo = {};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo = {};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (u32 i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        if (vkCreateSemaphore(g_VkDevice, &semaphoreInfo, NULL,
                              &g_VkImageAvailableSemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(g_VkDevice, &semaphoreInfo, NULL,
                              &g_VkRenderFinishedSemaphores[i]) != VK_SUCCESS ||
            vkCreateFence(g_VkDevice, &fenceInfo, NULL,
                          &g_VkInFlightFences[i]) != VK_SUCCESS)
        {

            Assert(!"failed to create sync objects for frame!");
        }
    }
}

internal u32 VulkanFindMemoryType(u32 typeFilter, VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(g_VkPhysicalDevice, &memProperties);
    for (u32 i = 0; i < memProperties.memoryTypeCount; i++)
    {
        if (typeFilter & (1 << i) &&
            (memProperties.memoryTypes[i].propertyFlags & properties) ==
                    properties)
        {
            return i;
        }
    }

    Assert(!"Failed to find suitable memory type!");
    return 0;
}

internal void VulkanCreateBuffer(VkDeviceSize size, VkBufferUsageFlags usage,
                                 VkMemoryPropertyFlags properties,
                                 VkBuffer *buffer, VkDeviceMemory *bufferMemory)
{
    VkBufferCreateInfo bufferInfo = {};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateBuffer(g_VkDevice, &bufferInfo, NULL, buffer) != VK_SUCCESS)
    {
        Assert(!"Failed to create buffer!");
    }

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(g_VkDevice, *buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex =
            VulkanFindMemoryType(memRequirements.memoryTypeBits, properties);
    if (vkAllocateMemory(g_VkDevice, &allocInfo, NULL, bufferMemory) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to allocate vertex buffer memory!");
    }

    vkBindBufferMemory(g_VkDevice, *buffer, *bufferMemory, 0);
}

internal VkCommandBuffer VulkanBeginSingleTimeCommands()
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = g_VkCommandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(g_VkDevice, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

internal void VulkanEndSingleTimeCommands(VkCommandBuffer commandBuffer)
{
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    vkQueueSubmit(g_VkGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(g_VkGraphicsQueue);

    vkFreeCommandBuffers(g_VkDevice, g_VkCommandPool, 1, &commandBuffer);
}


// TODO: Reuse command buffer for multiple copy buffer calls
internal void VulkanCopyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
{
    VkCommandBuffer commandBuffer = VulkanBeginSingleTimeCommands();

    VkBufferCopy copyRegion = {};
    copyRegion.srcOffset = 0; // Optional
    copyRegion.dstOffset = 0; // Optional
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    VulkanEndSingleTimeCommands(commandBuffer);
}

internal void VulkanUpdateVertexBuffer(
    VulkanVertexBuffer vertexBuffer, void *vertices, u32 size)
{
    Assert(size <= vertexBuffer.capacity);

    void *data;
    vkMapMemory(
        g_VkDevice, vertexBuffer.memory, 0, size, 0, &data);

    memcpy(data, vertices, size);

    vkUnmapMemory(g_VkDevice, vertexBuffer.memory);
}

internal VulkanVertexBuffer VulkanCreateVertexBuffer(u32 capacity)
{
    VulkanVertexBuffer result = {};
    result.capacity = capacity;

    VulkanCreateBuffer(capacity, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
            VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &result.buffer, &result.memory);

    return result;
}

internal void VulkanDestroyVertexBuffer(VulkanVertexBuffer buffer)
{
    vkDestroyBuffer(g_VkDevice, buffer.buffer, NULL);
    vkFreeMemory(g_VkDevice, buffer.memory, NULL);
}

internal VulkanMesh VulkanCreateMesh(
    Vertex *vertices, u32 vertexCount, u32 *indices, u32 indexCount)
{
    VulkanMesh result = {};
    result.indexCount = indexCount;

    u32 vertexBufferSize = sizeof(Vertex) * vertexCount;
    u32 indexBufferSize = sizeof(u32) * indexCount;
    u32 bufferSize = vertexBufferSize + indexBufferSize;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    VulkanCreateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                       VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                               VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                       &stagingBuffer, &stagingBufferMemory);

    void* data;
    vkMapMemory(g_VkDevice, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, vertices, vertexBufferSize);
    memcpy((u8*)data + vertexBufferSize, indices, indexBufferSize);
    vkUnmapMemory(g_VkDevice, stagingBufferMemory);

    result.indexBufferOffset = vertexBufferSize;

    VulkanCreateBuffer(bufferSize,
                       VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                               VK_BUFFER_USAGE_VERTEX_BUFFER_BIT |
                               VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                       VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &result.buffer,
                       &result.bufferMemory);

    VulkanCopyBuffer(stagingBuffer, result.buffer, bufferSize);

    vkDestroyBuffer(g_VkDevice, stagingBuffer, NULL);
    vkFreeMemory(g_VkDevice, stagingBufferMemory, NULL);

    return result;
}

internal void VulkanCreateUniformBuffers()
{
    VkDeviceSize bufferSize = sizeof(UniformBufferObject);

    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        VulkanCreateBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                   VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                           &g_VkUniformBuffers[i], &g_VkUniformBuffersMemory[i]);
    }

    u32 alignment = sizeof(UniformBufferObject);
    if (g_VkMinUboAlignment > 0)
    {
        alignment =
            (alignment + g_VkMinUboAlignment - 1) & ~(g_VkMinUboAlignment - 1);
    }
    g_VkDynamicUboAlignment = alignment;

    g_VkLightUboBaseOffset = g_VkMaxInstanceCount * alignment;

    VkDeviceSize dynamicBufferSize = g_VkLightUboBaseOffset + sizeof(LightUbo);
    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        VulkanCreateBuffer(dynamicBufferSize,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            &g_VkDynamicUniformBuffers[i], &g_VkDynamicUniformBuffersMemory[i]);
    }
}

internal void VulkanCreateDescriptorPool()
{
    VkDescriptorPoolSize poolSizes[3] = {};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER; // Used by LightUBO
    poolSizes[0].descriptorCount = g_VkSwapChainImagesCount;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = VULKAN_MAX_MATERIALS;
    poolSizes[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    poolSizes[2].descriptorCount = g_VkSwapChainImagesCount * 2; // UBO + LightUBO

    VkDescriptorPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = ArrayCount(poolSizes);
    poolInfo.pPoolSizes = poolSizes;
    poolInfo.maxSets = g_VkSwapChainImagesCount * 2 + VULKAN_MAX_MATERIALS;

    if (vkCreateDescriptorPool(
            g_VkDevice, &poolInfo, NULL, &g_VkDescriptorPool) != VK_SUCCESS)
    {
        Assert(!"Failed to create descriptor pool!");
    }
}

internal void VulkanCreateDynamicDescriptorSetLayout()
{
    VkDescriptorSetLayoutBinding dynamicUboLayoutBinding = {};
    dynamicUboLayoutBinding.binding = 0;
    dynamicUboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    dynamicUboLayoutBinding.descriptorCount = 1;
    dynamicUboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    dynamicUboLayoutBinding.pImmutableSamplers = NULL; // Optional

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = 1;
    layoutInfo.pBindings = &dynamicUboLayoutBinding;

    if (vkCreateDescriptorSetLayout(g_VkDevice, &layoutInfo, NULL,
                                    &g_VkDynamicDescriptorSetLayout) != VK_SUCCESS)
    {
        Assert(!"Failed to create descriptor set layout!");
    }
}

internal void VulkanCreateLightUboDescriptorSetLayout()
{
    VkDescriptorSetLayoutBinding dynamicUboLayoutBinding = {};
    dynamicUboLayoutBinding.binding = 0;
    dynamicUboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    dynamicUboLayoutBinding.descriptorCount = 1;
    dynamicUboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    dynamicUboLayoutBinding.pImmutableSamplers = NULL; // Optional

    VkDescriptorSetLayoutBinding lightUboLayoutBinding = {};
    lightUboLayoutBinding.binding = 1;
    lightUboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    lightUboLayoutBinding.descriptorCount = 1;
    lightUboLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    lightUboLayoutBinding.pImmutableSamplers = NULL; // Optional

    VkDescriptorSetLayoutBinding bindings[] = {
        dynamicUboLayoutBinding, lightUboLayoutBinding};

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = ArrayCount(bindings);
    layoutInfo.pBindings = bindings;

    if (vkCreateDescriptorSetLayout(g_VkDevice, &layoutInfo, NULL,
                                    &g_VkLightUboDescriptorSetLayout) != VK_SUCCESS)
    {
        Assert(!"Failed to create descriptor set layout!");
    }
}

internal void VulkanCreateStaticDescriptorSetLayout()
{
    VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
    samplerLayoutBinding.binding = 0;
    samplerLayoutBinding.descriptorCount = 1;
    samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.pImmutableSamplers = NULL;
    samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorSetLayoutBinding bindings[] = {samplerLayoutBinding};

    VkDescriptorSetLayoutCreateInfo layoutInfo = {};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = ArrayCount(bindings);
    layoutInfo.pBindings = bindings;

    if (vkCreateDescriptorSetLayout(g_VkDevice, &layoutInfo, NULL,
                                    &g_VkStaticDescriptorSetLayout) != VK_SUCCESS)
    {
        Assert(!"Failed to create descriptor set layout!");
    }
}

// TODO: Use separate pool for material descriptor sets and per object descriptor sets
internal VulkanMaterial VulkanCreateTextureOnlyMaterial(
    VulkanTexture texture, VkSampler sampler = g_VkTextureSampler)
{
    VulkanMaterial result = {};

    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = g_VkDescriptorPool;
    allocInfo.descriptorSetCount = 1;
    allocInfo.pSetLayouts = &g_VkStaticDescriptorSetLayout;

    if (vkAllocateDescriptorSets(
            g_VkDevice, &allocInfo, &result.descriptorSet) != VK_SUCCESS)
    {
        Assert(!"Failed to allocate descriptor sets!");
    }

    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = texture.imageView;
    imageInfo.sampler = sampler;

    VkWriteDescriptorSet descriptorWrite = {};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = result.descriptorSet;
    descriptorWrite.dstBinding = 0;
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrite.descriptorCount = 1;
    descriptorWrite.pImageInfo = &imageInfo;

    vkUpdateDescriptorSets(g_VkDevice, 1, &descriptorWrite, 0, NULL);

    return result;
}

internal void VulkanCreateDynamicDescriptorSets()
{
    VkDescriptorSetLayout layouts[8];
    // REFACTOR: Array fill macro
    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        layouts[i] = g_VkDynamicDescriptorSetLayout;
    }

    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = g_VkDescriptorPool;
    allocInfo.descriptorSetCount = g_VkSwapChainImagesCount;
    allocInfo.pSetLayouts = layouts;

    if (vkAllocateDescriptorSets(g_VkDevice, &allocInfo, g_VkDynamicDescriptorSets) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to allocate descriptor sets!");
    }

    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        // UBO
        VkDescriptorBufferInfo bufferInfo = {};
        bufferInfo.buffer = g_VkDynamicUniformBuffers[i];
        bufferInfo.offset = 0;
        bufferInfo.range = sizeof(UniformBufferObject);

        // @speed: Looks like we could maybe do this in one call to vkUpdateDescriptorSets
        // see https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkWriteDescriptorSet.html
        VkWriteDescriptorSet descriptorWrite = {};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = g_VkDynamicDescriptorSets[i];
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.pBufferInfo = &bufferInfo;

        vkUpdateDescriptorSets(g_VkDevice, 1, &descriptorWrite, 0, NULL);
    }
}

internal void VulkanCreateLightUboDescriptorSets()
{
    VkDescriptorSetLayout layouts[8];
    // REFACTOR: Array fill macro
    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        layouts[i] = g_VkLightUboDescriptorSetLayout;
    }

    VkDescriptorSetAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = g_VkDescriptorPool;
    allocInfo.descriptorSetCount = g_VkSwapChainImagesCount;
    allocInfo.pSetLayouts = layouts;

    if (vkAllocateDescriptorSets(g_VkDevice, &allocInfo, g_VkLightUboDescriptorSets) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to allocate descriptor sets!");
    }

    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        VkDescriptorBufferInfo dynamicUboBufferInfo = {};
        dynamicUboBufferInfo.buffer = g_VkDynamicUniformBuffers[i];
        dynamicUboBufferInfo.offset = 0;
        dynamicUboBufferInfo.range = sizeof(UniformBufferObject);

        VkDescriptorBufferInfo lightUboBufferInfo = {};
        lightUboBufferInfo.buffer = g_VkDynamicUniformBuffers[i];
        lightUboBufferInfo.offset = g_VkLightUboBaseOffset;
        lightUboBufferInfo.range = sizeof(LightUbo);

        VkWriteDescriptorSet dynamicUboDescriptorWrite = {};
        dynamicUboDescriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        dynamicUboDescriptorWrite.dstSet = g_VkLightUboDescriptorSets[i];
        dynamicUboDescriptorWrite.dstBinding = 0;
        dynamicUboDescriptorWrite.dstArrayElement = 0;
        dynamicUboDescriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        dynamicUboDescriptorWrite.descriptorCount = 1;
        dynamicUboDescriptorWrite.pBufferInfo = &dynamicUboBufferInfo;

        VkWriteDescriptorSet lightUboDescriptorWrite = {};
        lightUboDescriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        lightUboDescriptorWrite.dstSet = g_VkLightUboDescriptorSets[i];
        lightUboDescriptorWrite.dstBinding = 1;
        lightUboDescriptorWrite.dstArrayElement = 0;
        lightUboDescriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        lightUboDescriptorWrite.descriptorCount = 1;
        lightUboDescriptorWrite.pBufferInfo = &lightUboBufferInfo;

        VkWriteDescriptorSet descriptorWrites[] = {
            dynamicUboDescriptorWrite, lightUboDescriptorWrite};

        vkUpdateDescriptorSets(g_VkDevice, ArrayCount(descriptorWrites),
            descriptorWrites, 0, NULL);
    }
}

internal void VulkanCreateImage(u32 width, u32 height, VkFormat format,
    VkImageTiling tiling, VkImageUsageFlags usage,
    VkMemoryPropertyFlags properties, VkImage *image,
    VkDeviceMemory *imageMemory)
{
    VkImageCreateInfo imageInfo = {};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.format = format;
    imageInfo.tiling = tiling;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = usage;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.flags = 0; // Optional

    // TODO: We should check that VK_FORMAT_R8G8B8A8_UNORM is supported by the
    // graphics hardware during initialization
    if (vkCreateImage(g_VkDevice, &imageInfo, NULL, image) != VK_SUCCESS)
    {
        Assert(!"Failed to create image!");
    }

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(g_VkDevice, *image, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex =
        VulkanFindMemoryType(memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(g_VkDevice, &allocInfo, NULL, imageMemory) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to allocate image memory!");
    }

    vkBindImageMemory(g_VkDevice, *image, *imageMemory, 0);
}

internal void VulkanCopyBufferToImage(VkBuffer buffer, VkImage image, u32 width, u32 height)
{
    VkCommandBuffer commandBuffer = VulkanBeginSingleTimeCommands();

    VkBufferImageCopy region = {};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = {0, 0, 0};
    region.imageExtent = {width, height, 1};

    vkCmdCopyBufferToImage(commandBuffer, buffer, image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    VulkanEndSingleTimeCommands(commandBuffer);
}

// TODO: Don't allocate a new command buffer
internal void VulkanTransitionImageLayout(VkImage image, VkFormat format,
    VkImageLayout oldLayout, VkImageLayout newLayout, u32 layerCount = 1)
{
    VkCommandBuffer commandBuffer = VulkanBeginSingleTimeCommands();

    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = layerCount;

    VkPipelineStageFlags sourceStage = 0;
    VkPipelineStageFlags destinationStage = 0;

    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

        Assert(format == g_VkFrameBufferDepthFormat);
        barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
    }
    else
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
        newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
             newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED &&
             newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
                                VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    }
    else
    {
        Assert(!"Unsupported layout transition!");
    }

    vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0,
        NULL, 0, NULL, 1, &barrier);

    VulkanEndSingleTimeCommands(commandBuffer);
}

internal VulkanTexture VulkanCreateTexture(void *pixels, u32 width, u32 height, u32 bytesPerPixel)
{
    VulkanTexture result = {};

    VkDeviceSize imageSize = width * height * bytesPerPixel;
    VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;
    if (bytesPerPixel == 1)
    {
        format = VK_FORMAT_R8_UNORM;
    }

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    VulkanCreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
            VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &stagingBuffer, &stagingBufferMemory);

    void *data;
    vkMapMemory(g_VkDevice, stagingBufferMemory, 0, imageSize, 0, &data);
    memcpy(data, pixels, imageSize);
    vkUnmapMemory(g_VkDevice, stagingBufferMemory);

    VulkanCreateImage(SafeCastI32ToU32(width), SafeCastI32ToU32(height),
        format, VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &result.image,
        &result.imageMemory);

    VulkanTransitionImageLayout(result.image, format,
        VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    VulkanCopyBufferToImage(stagingBuffer, result.image, width, height);

    VulkanTransitionImageLayout(result.image, format,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    vkDestroyBuffer(g_VkDevice, stagingBuffer, NULL);
    vkFreeMemory(g_VkDevice, stagingBufferMemory, NULL);

    result.imageView = VulkanCreateImageView(
        result.image, format, VK_IMAGE_ASPECT_COLOR_BIT);

    return result;
}

internal void VulkanCreateCubeMap(RenderCommandAllocateCubeMap *cmd)
{
    Assert(cmd->bytesPerPixel == 4);
    VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;
    VkDeviceSize faceSize = cmd->width * cmd->width * cmd->bytesPerPixel;
    VkDeviceSize totalSize = faceSize * 6;

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;

    VulkanCreateBuffer(totalSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
            VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &stagingBuffer, &stagingBufferMemory);

    void *imageData;
    vkMapMemory(g_VkDevice, stagingBufferMemory, 0, totalSize, 0, &imageData);

    u8 *cursor = (u8 *)imageData;

    // +Z
    memcpy(cursor, cmd->pixels[CUBE_MAP_FACE_Z_POS], faceSize);
    cursor += faceSize;

    // -Z
    memcpy(cursor, cmd->pixels[CUBE_MAP_FACE_Z_NEG], faceSize);
    cursor += faceSize;

    // +Y
    memcpy(cursor, cmd->pixels[CUBE_MAP_FACE_Y_POS], faceSize);
    cursor += faceSize;

    // -Y
    memcpy(cursor, cmd->pixels[CUBE_MAP_FACE_Y_NEG], faceSize);
    cursor += faceSize;

    // +X
    memcpy(cursor, cmd->pixels[CUBE_MAP_FACE_X_POS], faceSize);
    cursor += faceSize;

    // -X
    memcpy(cursor, cmd->pixels[CUBE_MAP_FACE_X_NEG], faceSize);
    cursor += faceSize;

    vkUnmapMemory(g_VkDevice, stagingBufferMemory);

    VkImageCreateInfo imageInfo = {};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = cmd->width;
    imageInfo.extent.height = cmd->width;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 6; // 6 faces for the cubemap
    imageInfo.format = format;
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

    VulkanTexture texture = {};
    if (vkCreateImage(g_VkDevice, &imageInfo, NULL, &texture.image) != VK_SUCCESS)
    {
        Assert(!"Failed to create image!");
    }

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(g_VkDevice, texture.image, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = VulkanFindMemoryType(
        memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    if (vkAllocateMemory(g_VkDevice, &allocInfo, NULL,
            &texture.imageMemory) != VK_SUCCESS)
    {
        Assert(!"Failed to allocate image memory!");
    }

    vkBindImageMemory(g_VkDevice, texture.image, texture.imageMemory, 0);

    VulkanTransitionImageLayout(texture.image, format,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 6);

    // CopyBufferToImage
    {
        VkCommandBuffer commandBuffer = VulkanBeginSingleTimeCommands();

        VkBufferImageCopy bufferCopyRegions = {};
        bufferCopyRegions.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferCopyRegions.imageSubresource.mipLevel = 0;
        bufferCopyRegions.imageSubresource.baseArrayLayer = 0;
        bufferCopyRegions.imageSubresource.layerCount = 6;
        bufferCopyRegions.imageExtent.width = cmd->width;
        bufferCopyRegions.imageExtent.height = cmd->width;
        bufferCopyRegions.imageExtent.depth = 1;
        bufferCopyRegions.bufferOffset = 0;

        vkCmdCopyBufferToImage(commandBuffer, stagingBuffer, texture.image,
                VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &bufferCopyRegions);

        VulkanEndSingleTimeCommands(commandBuffer);
    }

    VulkanTransitionImageLayout(texture.image, format,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, 6);

    vkDestroyBuffer(g_VkDevice, stagingBuffer, NULL);
    vkFreeMemory(g_VkDevice, stagingBufferMemory, NULL);

    VkImageViewCreateInfo viewInfo = {};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = texture.image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 6;

    if (vkCreateImageView(g_VkDevice, &viewInfo, NULL, &texture.imageView) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to create image view!");
    }

    Assert(cmd->textureIndex < VULKAN_MAX_TEXTURES);
    g_VkTextures[cmd->textureIndex] = texture;

    Assert(cmd->materialIndex < VULKAN_MAX_MATERIALS);
    g_VkMaterials[cmd->materialIndex] =
        VulkanCreateTextureOnlyMaterial(g_VkTextures[cmd->textureIndex]);
}

internal void VulkanCreateTextureSampler()
{
    VkSamplerCreateInfo samplerInfo = {};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;

    if (vkCreateSampler(g_VkDevice, &samplerInfo, NULL, &g_VkTextureSampler) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to create texture sampler!");
    }
}

internal void VulkanCheckIfRequiredDepthFormatIsSupported()
{
    VkFormatProperties props;
    vkGetPhysicalDeviceFormatProperties(
        g_VkPhysicalDevice, g_VkFrameBufferDepthFormat, &props);
    if ((props.optimalTilingFeatures &
            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) == 0)
    {
        Assert(!"Image tiling optimal not supported for "
                "VK_FORMAT_D24_UNORM_S8_UINT");
    }
}

internal void VulkanCreateDepthResources()
{
    VulkanCheckIfRequiredDepthFormatIsSupported();

    VulkanCreateImage(g_VkSwapChainExtent.width, g_VkSwapChainExtent.height,
        g_VkFrameBufferDepthFormat, VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &g_VkDepthImage,
        &g_VkDepthImageMemory);

    g_VkDepthImageView = VulkanCreateImageView(
        g_VkDepthImage, g_VkFrameBufferDepthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

    VulkanTransitionImageLayout(g_VkDepthImage, g_VkFrameBufferDepthFormat,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
}

internal void VulkanInit(GLFWwindow *window, u32 frameBufferWidth, u32 frameBufferHeight)
{
    const char *extensions[1024];
    u32 extensionCount =
            VulkanGetRequiredExtensions(extensions, ArrayCount(extensions));
    if (!VulkanCheckIfExtensionsArePresent(extensions, extensionCount))
    {
        Assert(!"Not all of the required extensions are present");
        return;
    }

    const char *validationLayers[] = {"VK_LAYER_LUNARG_standard_validation"};
    if (!VulkanCheckIfValidationLayersArePresent(validationLayers,
                                                 ArrayCount(validationLayers)))
    {
        Assert(!"Not all of the requested validation layers are present");
        return;
    }

    // Create instance
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Vulkan Engine";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "New Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = extensions;
    createInfo.enabledLayerCount = ArrayCount(validationLayers);
    createInfo.ppEnabledLayerNames = validationLayers;

    if (vkCreateInstance(&createInfo, NULL, &g_VkInstance) != VK_SUCCESS)
    {
        Assert(!"Failed to create instance!");
    }

    VulkanSetupDebugCallback(); 

    VkWin32SurfaceCreateInfoKHR surfaceCreateInfo = {};
    surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.hwnd = glfwGetWin32Window(window);
    surfaceCreateInfo.hinstance = GetModuleHandle(NULL);

    auto CreateWin32SurfaceKHR =
            (PFN_vkCreateWin32SurfaceKHR)vkGetInstanceProcAddr(
                    g_VkInstance, "vkCreateWin32SurfaceKHR");

    if (!CreateWin32SurfaceKHR ||
        CreateWin32SurfaceKHR(g_VkInstance, &surfaceCreateInfo, NULL,
                              &g_VkSurface) != VK_SUCCESS)
    {
        Assert(!"Failed to create window surface!");
    }

    if (glfwCreateWindowSurface(g_VkInstance, window, NULL, &g_VkSurface) !=
        VK_SUCCESS)
    {
        Assert(!"GLFW failed to create window surface!");
    }


    VulkanPickPhysicalDevice();
    const char *deviceExtensions[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
    if (!VulkanCheckIfDeviceExtensionsAreSupported(
                g_VkPhysicalDevice, deviceExtensions,
                ArrayCount(deviceExtensions)))
    {
        Assert(!"Physical device does not support all required extensions");
    }

    VkPhysicalDeviceProperties deviceProperties = {};
    vkGetPhysicalDeviceProperties(g_VkPhysicalDevice, &deviceProperties);
    g_VkMinUboAlignment = SafeTruncateU64ToU32(
        deviceProperties.limits.minUniformBufferOffsetAlignment);

    VulkanCreateLogicalDevice(deviceExtensions, ArrayCount(deviceExtensions),
                              validationLayers, ArrayCount(validationLayers));

    VulkanCreateSwapChain(frameBufferWidth, frameBufferHeight);

    VulkanCreateImageViews();

    VulkanCreateRenderPass();

    VulkanCreateDynamicDescriptorSetLayout();
    VulkanCreateStaticDescriptorSetLayout();
    VulkanCreateLightUboDescriptorSetLayout();

    VulkanCreateShaders();

    VulkanCreateCommandPool();
    VulkanCreateDynamicCommandPools();

    VulkanCreateDepthResources();
    VulkanCreateFrameBuffers();

    VulkanCreateTextureSampler();

    VulkanCreateUniformBuffers();

    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        g_VkLineVertexBuffers[i] =
            VulkanCreateVertexBuffer(VULKAN_MAX_LINES * 2 * sizeof(VertexPC));
        g_VkTextVertexBuffers[i] = VulkanCreateVertexBuffer(
            VULKAN_MAX_TEXT_VERTICES * sizeof(TextVertex));
    }

    VulkanCreateDescriptorPool();
    VulkanCreateDynamicDescriptorSets();
    VulkanCreateLightUboDescriptorSets();

    VulkanCreateSyncObjects();


    g_VkIsSwapChainInitialized = true;
    g_VkIsSwapChainValid = true;
}

inline void VulkanDestroyShader(VulkanShader shader)
{
    vkDestroyPipeline(g_VkDevice, shader.pipeline, NULL);
    vkDestroyPipelineLayout(g_VkDevice, shader.layout, NULL);
}

internal void VulkanCleanUpSwapChain()
{
    if (g_VkIsSwapChainInitialized)
    {
        vkDestroyImageView(g_VkDevice, g_VkDepthImageView, NULL);
        vkDestroyImage(g_VkDevice, g_VkDepthImage, NULL);
        vkFreeMemory(g_VkDevice, g_VkDepthImageMemory, NULL);

        for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
        {
            vkDestroyFramebuffer(g_VkDevice, g_VkSwapChainFrameBuffers[i],
                                 NULL);
        }

        for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
        {
            if (g_VkDynamicCommandBufferCounts[i] > 0)
            {
                vkFreeCommandBuffers(g_VkDevice,
                    g_VkDynamicCommandBufferPools[i],
                    g_VkDynamicCommandBufferCounts[i],
                    &g_VkDynamicCommandBuffers[i]);

                g_VkDynamicCommandBufferCounts[i] = 0;
            }
        }

        for (u32 i = 0; i < VULKAN_MAX_SHADERS; ++i)
        {
            VulkanDestroyShader(g_Shaders[i]);
        }

        vkDestroyRenderPass(g_VkDevice, g_VkRenderPass, NULL);

        for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
        {
            vkDestroyImageView(g_VkDevice, g_VkSwapChainImageViews[i], NULL);
        }

        vkDestroySwapchainKHR(g_VkDevice, g_VkSwapChain, NULL);

        g_VkIsSwapChainInitialized = false;
    }
}

internal void VulkanRecreateSwapChain(u32 frameBufferWidth, u32 frameBufferHeight)
{
    OutputDebugString("Recreating swap chain\n");
    vkDeviceWaitIdle(g_VkDevice);

    VulkanCleanUpSwapChain();

    VulkanCreateSwapChain(frameBufferWidth, frameBufferHeight);
    VulkanCreateImageViews();
    VulkanCreateRenderPass();
    VulkanCreateShaders();
    VulkanCreateDepthResources();
    VulkanCreateFrameBuffers();

    g_VkIsSwapChainInitialized = true;
    g_VkIsSwapChainValid = true;
}

internal void VulkanProcessRenderCommands(u32 imageIndex, RenderCommandQueue *renderCommands)
{
    // Process asset allocations and buffer updates
    {
        u32 cursor = 0;
        while (cursor < renderCommands->length)
        {
            RenderCommandHeader *header =
                (RenderCommandHeader *)(renderCommands->start + cursor);
            void *data = (void *)(header + 1);
            Assert(cursor + header->size <= renderCommands->length);

            if (header->type == RenderCommandAllocateTextureTypeId)
            {
                RenderCommandAllocateTexture *cmd = (RenderCommandAllocateTexture *)data;
                // FIXME: Need a better system for assets indices/ids
                Assert(cmd->index < VULKAN_MAX_TEXTURES);
                Assert(cmd->materialIndex < VULKAN_MAX_MATERIALS);

                // @speed: Render asset creation functions currently wait for
                // the graphics queue to be idle, this would cause frame hitchs
                g_VkTextures[cmd->index] = VulkanCreateTexture(
                    cmd->pixels, cmd->width, cmd->height, cmd->bytesPerPixel);

                // FIXME: Poorly named function, we only rely on the same
                // descriptor set layout
                g_VkMaterials[cmd->materialIndex] =
                    VulkanCreateTextureOnlyMaterial(g_VkTextures[cmd->index]);
            }
            else if (header->type == RenderCommandUpdateVertexBufferTypeId)
            {
                RenderCommandUpdateVertexBuffer *cmd =
                    (RenderCommandUpdateVertexBuffer *)data;

                if (cmd->lengthInBytes > 0)
                {
                    // TODO: Merge into same buffer with 2 partitions
                    VulkanVertexBuffer vbo = {};
                    if (cmd->bufferIndex == VULKAN_VERTEX_BUFFER_TEXT)
                    {
                        vbo = g_VkTextVertexBuffers[imageIndex];
                    }
                    else if (cmd->bufferIndex == VULKAN_VERTEX_BUFFER_LINES)
                    {
                        vbo = g_VkLineVertexBuffers[imageIndex];
                    }
                    else
                    {
                        InvalidCodePath();
                    }
                    VulkanUpdateVertexBuffer(
                        vbo, cmd->vertices, cmd->lengthInBytes);
                }
            }
            else if (header->type == RenderCommandAllocateCubeMapTypeId)
            {
                RenderCommandAllocateCubeMap *cmd =
                    (RenderCommandAllocateCubeMap *)data;

                VulkanCreateCubeMap(cmd);
            }
            else if (header->type == RenderCommandAllocateMeshTypeId)
            {
                RenderCommandAllocateMesh *cmd = (RenderCommandAllocateMesh *)data;

                Assert(cmd->meshIndex < VULKAN_MAX_MESHES);
                g_VkMeshes[cmd->meshIndex] = VulkanCreateMesh(cmd->vertices,
                    cmd->vertexCount, cmd->indices, cmd->indexCount);
            }

            cursor += sizeof(RenderCommandHeader) + header->size;
        }
    }

    // Setup command buffer
    if (g_VkDynamicCommandBufferCounts[imageIndex] > 0)
    {
        vkFreeCommandBuffers(g_VkDevice,
            g_VkDynamicCommandBufferPools[imageIndex],
            g_VkDynamicCommandBufferCounts[imageIndex],
            &g_VkDynamicCommandBuffers[imageIndex]);

        g_VkDynamicCommandBufferCounts[imageIndex] = 0;
    }
    
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = g_VkDynamicCommandBufferPools[imageIndex];
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    if (vkAllocateCommandBuffers(g_VkDevice, &allocInfo, &commandBuffer) !=
        VK_SUCCESS)
    {
        Assert(!"Failed to allocate dynamic command buffer!");
    }
    g_VkDynamicCommandBuffers[imageIndex] = commandBuffer;
    g_VkDynamicCommandBufferCounts[imageIndex] = 1;

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS)
    {
        Assert(!"Failed to begin recording command buffer!");
    }

    VkRenderPassBeginInfo renderPassInfo = {};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = g_VkRenderPass;
    renderPassInfo.framebuffer = g_VkSwapChainFrameBuffers[imageIndex];
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = g_VkSwapChainExtent;

    VkClearValue clearValues[2];
    clearValues[0] = {0.0f, 0.0f, 0.0f, 1.0f};
    clearValues[1] = {1.0f, 0.0f};

    renderPassInfo.clearValueCount = ArrayCount(clearValues);
    renderPassInfo.pClearValues = clearValues;

    vkCmdBeginRenderPass(
        commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    // Dynamic UBOs
    void *uboData;
    vkMapMemory(g_VkDevice, g_VkDynamicUniformBuffersMemory[imageIndex], 0, VK_WHOLE_SIZE, 0, &uboData);

    LightUbo lightUbo = {};


    mat4 correctionMatrix = {};
    correctionMatrix.col[0] = Vec4(1, 0, 0, 0);
    correctionMatrix.col[1] = Vec4(0, -1, 0, 0);
    correctionMatrix.col[2] = Vec4(0, 0, 0.5f, 0);
    correctionMatrix.col[3] = Vec4(0, 0, 0.5f, 1);



    u32 maxEntries = g_VkMaxInstanceCount;
    u32 entryCount = 0;

    u32 cursor = 0;
    while (cursor < renderCommands->length)
    {
        RenderCommandHeader *header =
            (RenderCommandHeader *)(renderCommands->start + cursor);
        void *data = (void *)(header + 1);
        Assert(cursor + header->size <= renderCommands->length);

        // FIXME: Refactor this into a switch statement
        Assert(entryCount < maxEntries);
        if (header->type == RenderCommandDrawVertexBufferTypeId)
        {
            // TODO: Bounds checking for writing to the ubo buffer

            // Draw line buffer
            RenderCommandDrawVertexBuffer *cmd =
                (RenderCommandDrawVertexBuffer *)data;
            if (cmd->vertexCount > 0)
            {
                VulkanVertexBuffer vbo = {};
                if (cmd->vbo == VULKAN_VERTEX_BUFFER_TEXT)
                {
                    vbo = g_VkTextVertexBuffers[imageIndex];
                }
                else if (cmd->vbo == VULKAN_VERTEX_BUFFER_LINES)
                {
                    vbo = g_VkLineVertexBuffers[imageIndex];
                }
                else
                {
                    InvalidCodePath();
                }

                VkBuffer vertexBuffers[] = {vbo.buffer};
                VkDeviceSize offsets[] = {0};
                vkCmdBindVertexBuffers(
                    commandBuffer, 0, 1, vertexBuffers, offsets);

                Assert(cmd->shader < VULKAN_MAX_SHADERS);
                VulkanShader shader = g_Shaders[cmd->shader];

                u32 desciptorSetCount = 1;
                VkDescriptorSet descriptorSets[2];
                descriptorSets[0] = g_VkDynamicDescriptorSets[imageIndex];

                if (cmd->shader == VULKAN_SHADER_TEXT)
                {
                    Assert(cmd->material < VULKAN_MAX_MATERIALS);
                    VulkanMaterial material = g_VkMaterials[cmd->material];

                    descriptorSets[desciptorSetCount++] =
                        material.descriptorSet;

                    vkCmdPushConstants(commandBuffer, shader.layout,
                            VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(vec4), &cmd->color);
                }

                vkCmdBindPipeline(commandBuffer,
                    VK_PIPELINE_BIND_POINT_GRAPHICS, shader.pipeline);

                u32 offset = entryCount * g_VkDynamicUboAlignment;
                vkCmdBindDescriptorSets(commandBuffer,
                    VK_PIPELINE_BIND_POINT_GRAPHICS, shader.layout, 0,
                    desciptorSetCount, descriptorSets, 1, &offset);

                vkCmdDraw(
                    commandBuffer, cmd->vertexCount, 1, cmd->firstVertex, 0);


                UniformBufferObject ubo;
                ubo.model = cmd->model;
                ubo.view = cmd->view;
                ubo.proj = correctionMatrix * cmd->projection;

                memcpy((u8 *)uboData + offset, &ubo, sizeof(ubo));

                entryCount++;
            }
        }
        else if (header->type == RenderCommandDrawMeshTypeId)
        {
            RenderCommandDrawMesh *cmd = (RenderCommandDrawMesh *)data;

            Assert(cmd->mesh < VULKAN_MAX_MESHES);
            VulkanMesh mesh = g_VkMeshes[cmd->mesh];

            VkBuffer vertexBuffers[] = {mesh.buffer};
            VkDeviceSize offsets[] = {0};
            vkCmdBindVertexBuffers(commandBuffer, 0, 1, vertexBuffers, offsets);
            vkCmdBindIndexBuffer(commandBuffer, mesh.buffer,
                mesh.indexBufferOffset, VK_INDEX_TYPE_UINT32);

            Assert(cmd->shader < VULKAN_MAX_SHADERS);
            VulkanShader shader = g_Shaders[cmd->shader];

            // @speed: don't rebind pipeline for each draw call!
            vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS,
                shader.pipeline);

            if (cmd->shader == VULKAN_SHADER_COLOR_ONLY ||
                cmd->shader == VULKAN_SHADER_COLOR_WIREFRAME ||
                cmd->shader == VULKAN_SHADER_COLOR_ONLY_NO_DEPTH ||
                cmd->shader == VULKAN_SHADER_DIFFUSE_LIGHTING)
            {
                vkCmdPushConstants(commandBuffer, shader.layout,
                    VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(vec4), &cmd->color);
            }

            u32 desciptorSetCount = 0;
            VkDescriptorSet descriptorSets[2];

            if (cmd->shader == VULKAN_SHADER_WORLD_TEX_COORD ||
                cmd->shader == VULKAN_SHADER_TEXTURE_DIFFUSE_LIGHTING ||
                cmd->shader == VULKAN_SHADER_DIFFUSE_LIGHTING)
            {
                descriptorSets[desciptorSetCount++] = g_VkLightUboDescriptorSets[imageIndex];
            }
            else
            {
                descriptorSets[desciptorSetCount++] = g_VkDynamicDescriptorSets[imageIndex];
            }

            // TODO: Should store shader descriptions and use flag to test
            // TODO: Currently only textureOnly shader supports materials
            if (cmd->shader == VULKAN_SHADER_TEXTURE_ONLY ||
                cmd->shader == VULKAN_SHADER_WORLD_TEX_COORD ||
                cmd->shader == VULKAN_SHADER_CUBE_MAP ||
                cmd->shader == VULKAN_SHADER_TEXT ||
                cmd->shader == VULKAN_SHADER_TEXTURE_DIFFUSE_LIGHTING )
            {
                Assert(cmd->material < VULKAN_MAX_MATERIALS);
                VulkanMaterial material = g_VkMaterials[cmd->material];

                descriptorSets[desciptorSetCount++] = material.descriptorSet;
            }

            u32 offset = entryCount * g_VkDynamicUboAlignment;
            vkCmdBindDescriptorSets(commandBuffer,
                VK_PIPELINE_BIND_POINT_GRAPHICS, shader.layout, 0,
                desciptorSetCount, descriptorSets, 1, &offset);

            vkCmdDrawIndexed(commandBuffer, mesh.indexCount, 1, 0, 0, 0);

            // Write to UBO
            UniformBufferObject ubo;
            ubo.model = cmd->model;
            ubo.view = cmd->view;
            ubo.proj = correctionMatrix * cmd->projection;

            memcpy((u8 *)uboData + offset, &ubo, sizeof(ubo));

            entryCount++;
        }
        else if (header->type == RenderCommandDrawLightTypeId)
        {
            RenderCommandDrawLight *cmd = (RenderCommandDrawLight *)data;
            switch (cmd->type)
            {
                case LightType_Ambient:
                    lightUbo.ambientLight = Vec4(cmd->color, 0);
                    break;
                case LightType_Directional:
                    {
                        Assert(lightUbo.directionalLightCount < MAX_DIRECTIONAL_LIGHTS);
                        DirectionalLight *directionalLight =
                            lightUbo.directionalLights +
                            lightUbo.directionalLightCount++;
                        directionalLight->direction = Vec4(cmd->direction, 0);
                        directionalLight->color = Vec4(cmd->color, 0);
                    }
                    break;
                case LightType_Point:
                    {
                        Assert(lightUbo.pointLightCount < MAX_POINT_LIGHTS);
                        PointLight *pointLight =
                            lightUbo.pointLights + lightUbo.pointLightCount++;
                        pointLight->position = Vec4(cmd->position, 0);
                        pointLight->color = Vec4(cmd->color, 0);
                        pointLight->attenuation = Vec4(cmd->attenuation, 0);
                    }
                    break;
                case LightType_Spot:
                    InvalidCodePath();
                    break;
                default:
                    InvalidCodePath();
                    break;
            }
        }
        else
        {
            // FIXME: Probably worth having a separate queue for asset commands
            // Skip asset render commands
        }

        cursor += sizeof(RenderCommandHeader) + header->size;
    }
    memcpy((u8 *)uboData + g_VkLightUboBaseOffset, &lightUbo, sizeof(lightUbo));

    vkUnmapMemory(g_VkDevice, g_VkDynamicUniformBuffersMemory[imageIndex]);


    vkCmdEndRenderPass(commandBuffer);

    if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
    {
        Assert(!"Failed to record command buffer!");
    }
}

internal void VulkanDrawFrame(float dt, RenderCommandQueue *renderCommands)
{
    // Prepare frame
    vkWaitForFences(g_VkDevice, 1, &g_VkInFlightFences[g_VkCurrentFrame], VK_TRUE, U64_MAX);

    u32 imageIndex;
    VkResult result = vkAcquireNextImageKHR(g_VkDevice, g_VkSwapChain, U64_MAX,
        g_VkImageAvailableSemaphores[g_VkCurrentFrame], VK_NULL_HANDLE,
        &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR)
    {
        g_VkIsSwapChainValid = false;
        return;
    }
    else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
    {
        Assert(!"Failed to acquire swap chain image!");
    }

    VulkanProcessRenderCommands(imageIndex, renderCommands);

    // Submit draw commands
    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore waitSemaphores[] = {g_VkImageAvailableSemaphores[g_VkCurrentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &g_VkDynamicCommandBuffers[imageIndex];

    VkSemaphore signalSemaphores[] = {g_VkRenderFinishedSemaphores[g_VkCurrentFrame]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    vkResetFences(g_VkDevice, 1, &g_VkInFlightFences[g_VkCurrentFrame]);

    if (vkQueueSubmit(g_VkGraphicsQueue, 1, &submitInfo,
                      g_VkInFlightFences[g_VkCurrentFrame]) != VK_SUCCESS)
    {
        Assert(!"Failed to submit draw command buffer!");
    }


    // Present frame
    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;

    VkSwapchainKHR swapChains[] = {g_VkSwapChain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = NULL; // Optional

    result = vkQueuePresentKHR(g_VkPresentQueue, &presentInfo);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
    {
        g_VkIsSwapChainValid = true;
    }
    else if (result != VK_SUCCESS)
    {
        Assert(!"Failed to acquire swap chain image!");
    }

    vkQueueWaitIdle(g_VkPresentQueue);

    g_VkCurrentFrame = (g_VkCurrentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}


internal void VulkanDestroyMesh(VulkanMesh mesh)
{
    vkDestroyBuffer(g_VkDevice, mesh.buffer, NULL);
    vkFreeMemory(g_VkDevice, mesh.bufferMemory, NULL);
}

internal void VulkanDestroyTexture(VulkanTexture texture)
{
    vkDestroyImageView(g_VkDevice, texture.imageView, NULL);
    vkDestroyImage(g_VkDevice, texture.image, NULL);
    vkFreeMemory(g_VkDevice, texture.imageMemory, NULL);
}

internal void VulkanCleanUp()
{
    vkDeviceWaitIdle(g_VkDevice);

    VulkanCleanUpSwapChain();

    for (u32 i = 0; i < VULKAN_MAX_MESHES; ++i)
    {
        VulkanDestroyMesh(g_VkMeshes[i]);
    }

    vkDestroyDescriptorSetLayout(g_VkDevice, g_VkDynamicDescriptorSetLayout, NULL);
    vkDestroyDescriptorSetLayout(g_VkDevice, g_VkStaticDescriptorSetLayout, NULL);
    vkDestroyDescriptorSetLayout(g_VkDevice, g_VkLightUboDescriptorSetLayout, NULL);
    vkDestroyDescriptorPool(g_VkDevice, g_VkDescriptorPool, NULL);

    vkDestroySampler(g_VkDevice, g_VkTextureSampler, NULL);

    for (u32 i = 0; i < VULKAN_MAX_TEXTURES; ++i)
    {
        VulkanDestroyTexture(g_VkTextures[i]);
    }

    for (u32 i = 0; i < g_VkSwapChainImagesCount; ++i)
    {
        VulkanDestroyVertexBuffer(g_VkLineVertexBuffers[i]);
        VulkanDestroyVertexBuffer(g_VkTextVertexBuffers[i]);

        vkDestroyBuffer(g_VkDevice, g_VkUniformBuffers[i], NULL);
        vkFreeMemory(g_VkDevice, g_VkUniformBuffersMemory[i], NULL);
        vkDestroyBuffer(g_VkDevice, g_VkDynamicUniformBuffers[i], NULL);
        vkFreeMemory(g_VkDevice, g_VkDynamicUniformBuffersMemory[i], NULL);

        vkDestroyCommandPool(g_VkDevice, g_VkDynamicCommandBufferPools[i], NULL);
    }

    for (u32 i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
    {
        vkDestroySemaphore(g_VkDevice, g_VkRenderFinishedSemaphores[i], NULL);
        vkDestroySemaphore(g_VkDevice, g_VkImageAvailableSemaphores[i], NULL);
        vkDestroyFence(g_VkDevice, g_VkInFlightFences[i], NULL);
    }
    vkDestroyCommandPool(g_VkDevice, g_VkCommandPool, NULL);
    vkDestroyDevice(g_VkDevice, NULL);
    vkDestroySurfaceKHR(g_VkInstance, g_VkSurface, NULL);
    VulkanDestroyDebugUtilsMessengerEXT(g_VkInstance, g_VkCallback, NULL);
    vkDestroyInstance(g_VkInstance, NULL);
}
