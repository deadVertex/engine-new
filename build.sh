#! /bin/sh
COMPILER=g++-8

CFLAGS="-O0 -g -fno-exceptions -Wall -Wextra -Wno-unused-variable \
-Wno-unused-function -Wno-comment -Wno-unused-parameter -Wno-implicit-fallthrough \
-Wno-missing-field-initializers -Wno-type-limits -Wno-unused-but-set-variable \
-std=c++11 -Isrc -Ithirdparty"

mkdir -p build

# Build platform executable
$COMPILER src/linux_main.cpp -o build/linux_main $CFLAGS -lGLEW -lglfw -lGL -ldl -g -O0

# Build game runtime
$COMPILER src/game.cpp -o build/game_temp.so $CFLAGS -shared -rdynamic -fPIC -g -O0

mv build/game_temp.so build/game.so
