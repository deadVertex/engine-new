@echo off

REM Commented out Vulkan shaders as we focusing on OpenGL renderer

set IncludePaths=-I..\src -I..\thirdparty -I..\windows-dependencies\include -I"F:\Program Files\VulkanSDK\1.1.82.1\Include"
set CompilerFlags=-MT -F16777216 -nologo -Gm- -GR- -EHa -Od -W4 -WX -wd4305 -wd4127 -wd4201 -wd4189 -wd4100 -wd4996 -wd4505 -FC -Z7 %IncludePaths%
set PlatformLibraries=..\windows-dependencies\lib\glew32.lib ..\windows-dependencies\lib\glfw3dll.lib opengl32.lib vulkan-1.lib winmm.lib
set LinkerFlags=-opt:ref -incremental:no -libpath:"F:\Program Files\VulkanSDK\1.1.82.1\Lib\"

IF NOT EXIST build mkdir build
pushd build

REM Remove old shaders
del *.spv

REM Generate standalone shaders
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\shader.vert -o vert.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\shader.frag -o frag.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\shader2.frag -o frag2.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\vertex_color.vert -o vertex_color_vert.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\vertex_color.frag -o vertex_color_frag.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\cube.vert -o cube_vert.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\cube.frag -o cube_frag.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\text.vert -o text_vert.spv
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\text.frag -o text_frag.spv

REM Generate shaders from standard uber shader
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\standard_shader.uber -DVERTEX_SHADER -o diffuse_lighting_vert.spv -S vert
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\standard_shader.uber -DFRAGMENT_SHADER -o diffuse_lighting_frag.spv -S frag
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\standard_shader.uber -DVERTEX_SHADER -DWORLD_TEXTURE_COORDINATES -o world_tex_coord_vert.spv -S vert
REM "F:\Program Files\VulkanSDK\1.1.82.1\Bin\glslangValidator.exe" -V ..\shaders\standard_shader.uber -DFRAGMENT_SHADER -DDIFFUSE_TEXTURE -o texture_diffuse_lighting_frag.spv -S frag

REM Build game DLL
del game_lib_*.pdb > NUL 2> NUL
echo WATING FOR PDB > lock.tmp
cl %CompilerFlags% ..\src\game.cpp -Fe:game_lib -LD -link -PDB:game_lib_%random%.pdb %LinkerFlags% -EXPORT:GameUpdate
del lock.tmp

REM Build game exe
cl %CompilerFlags% ..\src\windows_main.cpp -link %LinkerFlags% %PlatformLibraries%
popd
