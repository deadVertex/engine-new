#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform UniformBufferObject
{
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec2 fragTexCoord;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 1.0);

    vec3 p = vec3(ubo.model * vec4(inPosition, 1.0));

    vec3 v;
    v.x = round(inNormal.x);
    v.y = round(inNormal.y);
    v.z = round(inNormal.z);

    vec2 t = vec2(-p.x, -p.y);
    t = (v.x > 0.0) ? vec2(-p.z, -p.y) : t;
    t = (v.x < 0.0) ? vec2(p.z, -p.y) : t;
    t = (v.y > 0.0) ? vec2(p.x, p.z) : t;
    t = (v.y < 0.0) ? vec2(-p.x, p.z) : t;
    t = (v.z > 0.0) ? vec2(p.x, -p.y) : t;

    fragTexCoord = t;
    fragNormal = inNormal; // TODO: Multiply by inverse of model matrix
}
