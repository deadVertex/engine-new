#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform PushConsts
{
    vec4 color;
} pushConsts;

layout(set = 1, binding = 0) uniform sampler2D texSampler;

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

void main() {
    float a = texture(texSampler, fragTexCoord).r;
    vec4 color = pushConsts.color;
    outColor = vec4(color.rgb, color.a * a);
}
